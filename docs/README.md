<!--声明：本文使用[Creative Commons License version 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)许可协议，转载、引用或修改等操作请遵循此许可协议。-->

## 支持的API

本源码仓覆盖的高阶API列表如下，下表列出简单功能说明，具体使用说明请参考[高阶API](https://hiascend.com/document/redirect/CannCommunityAscendCHighLevelApi)。同时我们还配套了其具体实现的算法框图，可配合源码阅读。

<table>
    <td> 类别 </td>
    <td> API </td>
    <td> 描述 </td>
    <tr>
        <th rowspan="2"> 数学库 </th>
        <td> Fmod </td>
        <td> 按元素计算两个浮点数相除后的余数。 </td>
    </tr>
    <tr>
        <td> Trunc </td>
        <td> 按元素做浮点数截断操作，即向零取整操作。 </td>
    </tr>
    <tr>
        <th rowspan="3"> 量化反量化 </th>
        <td> AscendAntiQuant </td>
        <td> 按元素做伪量化计算，比如将int8_t数据类型伪量化为half数据类型。 </td>
    </tr>
    <tr>
        <td> AscendDequant </td>
        <td> 按元素做反量化计算，比如将int32_t数据类型反量化为half/float等数据类型。 </td>
    </tr>
    <tr>
        <td> AscendQuant </td>
        <td> 按元素做量化计算，比如将half/float数据类型量化为int8_t数据类型。 </td>
    </tr>
    <tr>
        <th rowspan="10"> 数据归一化 </th>
        <td> BatchNorm </td>
        <td> 对于每个batch中的样本，对其输入的每个特征在batch的维度上进行归一化。 </td>
    </tr>
    <tr>
        <td> DeepNorm </td>
        <td> 在深层神经网络训练过程中，可以替代LayerNorm的一种归一化方法。 </td>
    </tr>
    <tr>
        <td> LayerNorm </td>
        <td> 将输入数据收敛到[0, 1]之间，可以规范网络层输入输出数据分布的一种归一化方法。 </td>
    </tr>
    <tr>
        <td> LayerNormGrad </td>
        <td> 用于计算LayerNorm的反向传播梯度。 </td>
    </tr>
    <tr>
        <td> LayerNormGradBeta </td>
        <td> 用于获取反向beta/gmma的数值，和LayerNormGrad共同输出pdx, gmma和beta。 </td>
    </tr>
    <tr>
        <td> RmsNorm </td>
        <td> 实现对shape大小为[B，S，H]的输入数据的RmsNorm归一化。 </td>
    </tr>
    <tr>
        <td> GroupNorm </td>
        <td> 对输入数据在 channel 维度进行分组并对每个组做归一化的方法。 </td>
    </tr>
    <tr>
        <td> Normalize </td>
        <td> 已知均值和方差，计算shape为[A, R]的输入数据的标准差倒数rstd和归一化结果y的方法。 </td>
    </tr>
    <tr>
        <td> WelfordUpdate </td>
        <td> Welford算法的前处理，一种在线计算均值和方差的方法。 </td>
    </tr>
    <tr>
        <td> WelfordFinalize </td>
        <td> Welford算法的后处理，一种在线计算均值和方差的方法。 </td>
    </tr>
    <tr>
        <th rowspan="16"> 激活函数 </th>
        <td> AdjustSoftMaxRes </td>
        <td> 用于对SoftMax相关计算结果做后处理，调整SoftMax的计算结果为指定的值。 </td>
    </tr>
    <tr>
        <td> FasterGelu </td>
        <td> FastGelu化简版本的一种激活函数。 </td>
    </tr>
    <tr>
        <td> GeGLU </td>
        <td> 采用GELU作为激活函数的GLU变体。 </td>
    </tr>
    <tr>
        <td> Gelu </td>
        <td> GELU是一个重要的激活函数，其灵感来源于Relu和Dropout，在激活中引入了随机正则的思想。 </td>
    </tr>
    <tr>
        <td> LogSoftMax </td>
        <td> 对输入tensor做LogSoftmax计算。 </td>
    </tr>
    <tr>
        <td> ReGlu </td>
        <td> 一种GLU变体，使用Relu作为激活函数。 </td>
    </tr>
    <tr>
        <td> Sigmoid </td>
        <td> 按元素做逻辑回归Sigmoid。 </td>
    </tr>
    <tr>
        <td> Silu </td>
        <td> 按元素做Silu运算。 </td>
    </tr>
    <tr>
        <td> SimpleSoftMax </td>
        <td> 使用计算好的sum和max数据对输入tensor做Softmax计算。 </td>
    </tr>
    <tr>
        <td> SoftMax </td>
        <td> 对输入tensor按行做Softmax计算。 </td>
    </tr>
    <tr>
        <td> SoftmaxFlash </td>
        <td> Softmax增强版本，除了可以对输入tensor做SoftmaxFlash计算，还可以根据上一次Softmax计算的sum和max来更新本次的Softmax计算结果。 </td>
    </tr>
    <tr>
        <td> SoftmaxFlashV2 </td>
        <td> SoftmaxFlash增强版本，对应FlashAttention-2算法。 </td>
    </tr>
    <tr>
        <td> SoftmaxGrad </td>
        <td> 对输入tensor做grad反向计算的一种方法。 </td>
    </tr>
    <tr>
        <td> SoftmaxGradFront </td>
        <td> 对输入tensor做grad反向计算的一种方法，其中dstTensor的last长度固定为32Byte。 </td>
    </tr>
    <tr>
        <td> SwiGLU </td>
        <td> 采用Swish作为激活函数的GLU变体。 </td>
    </tr>
    <tr>
        <td> Swish </td>
        <td> 神经网络中的Swish激活函数。 </td>
    </tr>
    <tr>
        <th rowspan="2"> 排序 </th>
        <td> TopK </td>
        <td> 获取最后一个维度的前k个最大值或最小值及其对应的索引。 </td>
    </tr>
    <tr>
        <td> Sort </td>
        <td> 对输入tensor做Sort计算，按照数值大小进行降序排序。 </td>
    </tr>
    <tr>
        <th rowspan="1"> Matmul </th>
        <td> Matmul </td>
        <td> Matmul矩阵乘法的运算。 </td>
    </tr>
    <tr>
        <th rowspan="1"> 工具类 </th>
        <td> InitGlobalMemory </td>
        <td> 将Global Memory上的数据初始化为指定值。 </td>
    </tr>
</table>


