<!--声明：本文使用[Creative Commons License version 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)许可协议，转载、引用或修改等操作请遵循此许可协议。-->
## 概述

本样例介绍了调用Matmul高阶API实现MatmulAsync单算子，即实现异步场景下的Matmul矩阵乘法，并按照不同的算子调用方式分别给出了对应的端到端实现。

异步模式指的是程序执行时，不需要等待某个操作完成就可以执行下一步操作。异步方式可以减少同步等待，提高并行度，开发者对计算性能要求较高时，可以选用该方式。

本样例包含Iterate&GetTensorC的异步实现、IterateAll的异步实现两种场景。

- 直调：使用核函数直调MatmulAsync自定义算子。

  核函数的基础调用方式，开发者完成算子核函数的开发和Tiling实现后，即可通过AscendCL运行时接口，完成算子的调用。

- 框架调用：使用框架调用MatmulAsync自定义算子。

  按照工程创建->算子实现->编译部署>算子调用的流程完成算子开发。整个过程都依赖于算子工程：基于工程代码框架完成算子核函数的开发和Tiling实现，通过工程编译脚本完成算子的编译部署，继而实现单算子调用或第三方框架中的算子调用。

本样例中包含如下调用方式：

| 调用方式  | 目录                                                                                                                                                                                                                                                                                                  | <strong>描述</strong>                                      |
| --------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------- |
| 直调    | [kernel_launch_method_by_direct](./kernel_launch_method_by_direct)       | host侧的核函数调用程序，包含CPU侧和NPU侧两种运行验证方法。 |
| 框架调用 | [kernel_launch_method_by_framework](./kernel_launch_method_by_framework) | 通过aclnn调用的方式调用MatmulAsync算子。                        |

## 样例支持的产品型号为：
- Atlas A2训练系列产品/Atlas 800I A2推理产品

## 目录结构
| 目录                  | 描述                   |
|---------------------|----------------------|
| [kernel_launch_method_by_direct](./kernel_launch_method_by_direct)       | 通过kernel直调的方式调用自定义算子工程样例目录 |
| [kernel_launch_method_by_framework](./kernel_launch_method_by_framework) | 通过aclnn调用的方式调用自定义算子工程样例目录 |
| [host_tiling](./host_tiling)       | 通过aclnn调用的方式调用自定义算子工程样例所需的host侧tiling实现 |
| [kernel_impl](./kernel_impl)       | 本样例kernel侧代码实现 |

## 算子描述
MatmulAsync单算子，对输入的A B矩阵做矩阵乘和加bias偏置。

MatmulAsync算子规格
<table>
<tr><td rowspan="1" align="center">算子类型(OpType)</td><td colspan="5" align="center">MatmulAsyncCustom</td></tr>
</tr>
<tr><td rowspan="4" align="center">算子输入</td><td align="center">name</td><td align="center">shape</td><td align="center">data type</td><td align="center">format</td><td align="center">isTrans</td></tr>
<tr><td align="center">a</td><td align="center">-</td><td align="center">float16</td><td align="center">ND</td><td align="center">true</td></tr>
<tr><td align="center">b</td><td align="center">-</td><td align="center">float16</td><td align="center">ND</td><td align="center">false</td></tr>
<tr><td align="center">bias</td><td align="center">-</td><td align="center">float</td><td align="center">ND</td><td align="center">-</td></tr>
</tr>
</tr>
<tr><td rowspan="1" align="center">算子输出</td><td align="center">c</td><td align="center">-</td><td align="center">float</td><td align="center">ND</td><td align="center">-</td></tr>
</tr>
<tr><td rowspan="1" align="center">核函数名</td><td colspan="5" align="center">matmul_async_custom</td></tr>
</table>

## 算子实现介绍

Framework调用样例中实现的是固定shape为[M, N, K] = [640, 1024, 512], bias = [1024]的MatmulAsync算子。

- 约束条件：
  - Iterate&GetTensorC的异步实现场景，仅支持输出到VECIN
  - IterateAll的异步实现场景，仅支持输出到GM

- kernel实现
  - 计算逻辑是：Ascend C提供一组Matmul高阶API，方便用户快速实现Matmul矩阵乘法的运算操作。MatMul的计算公式为：C = A * B + Bias。
    - A、B为源操作数，A为左矩阵，形状为[M, K]；B为右矩阵，形状为[K, N]。
    - C为目的操作数，存放矩阵乘结果的矩阵，形状为[M, N]。
    - Bias为矩阵乘偏置，形状为[1, N]。对A*B结果矩阵的每一行都采用该bias进行偏置。
  - Iterate&GetTensorC的异步实现MatMul矩阵乘运算的具体步骤如下：
    - 创建Matmul对象。
    - 初始化操作。在Iterate接口之前调用SetWorkspace接口设置临时空间，使用一块临时Global空间缓存Iterate计算结果，否则会覆盖计算结果，调用GetTensorC时，会在该临时空间中获取C的矩阵分片。
    - 设置左矩阵A、右矩阵B、Bias。
    - 调用Iterate。
    - 可以先执行其他操作，待需要获取结果时再调用GetTensorC等待Iterate异步接口返回。
    - 结束矩阵乘操作。
  - IterateAll的异步实现MatMul矩阵乘运算的具体步骤如下：
    - 创建Matmul对象。
    - 初始化操作。
    - 设置左矩阵A、右矩阵B、Bias。
    - 调用IterateAll。
    - 可以先执行其他操作，待需要获取结果时再调用WaitIterateAll等待IterateAll异步接口返回。
    - 结束矩阵乘操作。

- tiling实现
    - Ascend C提供一组Matmul Tiling API，方便用户获取MatMul kernel计算时所需的Tiling参数。只需要传入A/B/C矩阵等信息，调用API接口，即可获取到TCubeTiling结构体中的相关参数。
    - 获取Tiling参数的流程如下：
      - 创建一个Tiling对象。
      - 设置A、B、C、Bias的参数类型信息；M、N、Ka、Kb形状信息等。
      - 调用GetTiling接口，获取Tiling信息。