/*
 * Copyright (c) 2025 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

#include <string>
#include "tiling/tiling_api.h"
#include "tiling/platform/platform_ascendc.h"
#include "../host_tiling/matmul_async_custom_tiling.h"
#include "testcase_params.h"

uint8_t *GetTilingBuf(optiling::TCubeTiling *tilingData)
{
    uint32_t tilingSize = tilingData->GetDataSize();
    uint8_t *buf = (uint8_t *)malloc(tilingSize);
    tilingData->SaveToBuffer(buf, tilingSize);
    return buf;
}

uint8_t *GenerateTiling(const char *socVersion)
{
    matmul_tiling::TPosition cPos;
    if (TESTCASE_PARAMS.mode == ComputeMode::ITERATE_ALL) {
        // use IterateAll, only support CTYPE::TPosition is GM
        cPos = matmul_tiling::TPosition::GM;
    } else if (TESTCASE_PARAMS.mode == ComputeMode::ITERATE) {
        // use Iterate, only support CTYPE::TPosition is VECIN
        cPos = matmul_tiling::TPosition::VECIN;
    } else {
        cPos = matmul_tiling::TPosition::GM;
    }

    optiling::TCubeTiling tilingData;
    auto ascendcPlatform = platform_ascendc::PlatformAscendCManager::GetInstance(socVersion);
    auto aivCoreNum = platform_ascendc::PlatformAscendCManager::GetInstance()->GetCoreNum();
    matmul_tiling::MultiCoreMatmulTiling cubeTiling(*ascendcPlatform);
    bool res = ComputeTiling(tilingData, &cubeTiling, TESTCASE_PARAMS.m, TESTCASE_PARAMS.n, TESTCASE_PARAMS.k,
        aivCoreNum, TESTCASE_PARAMS.hasBias, cPos);
    if (!res) {
        std::cout << "gen tiling failed" << std::endl;
    }
    return GetTilingBuf(&tilingData);
}
