/*
 * Copyright (c) 2025 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */
#ifndef EXAMPLES_MATRIX_MATMUL_ASYNC_TESTCASE_PARAMS_H
#define EXAMPLES_MATRIX_MATMUL_ASYNC_TESTCASE_PARAMS_H

enum class ComputeMode {
    ITERATE_ALL = 0,
    ITERATE = 1,
};

class TestcaseParams
{
public:
    TestcaseParams() {}
    constexpr TestcaseParams(const uint32_t mIn, const uint32_t nIn, const uint32_t kIn,
        const bool hasBiasIn, const ComputeMode modeIn)
        : m(mIn),
          n(nIn),
          k(kIn),
          hasBias(hasBiasIn),
          mode(modeIn)
    {}

    uint32_t m = 0;
    uint32_t n = 0;
    uint32_t k = 0;
    bool hasBias = false;
    ComputeMode mode = ComputeMode::ITERATE_ALL;
};

// use IterateAll, only support CTYPE::TPosition is GM
constexpr TestcaseParams ASYNC_ITERATE_ALL_CASE = TestcaseParams(640, 1024, 512, false, ComputeMode::ITERATE_ALL);
// use Iterate, only support CTYPE::TPosition is VECIN
constexpr TestcaseParams ASYNC_ITERATE_CASE = TestcaseParams(640, 1024, 512, false, ComputeMode::ITERATE);

constexpr TestcaseParams TESTCASE_PARAMS = ASYNC_ITERATE_ALL_CASE;
#endif // EXAMPLES_MATRIX_MATMUL_ASYNC_TESTCASE_PARAMS_H