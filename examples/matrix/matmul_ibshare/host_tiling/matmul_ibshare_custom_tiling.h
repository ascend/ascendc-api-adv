/*
 * Copyright (c) 2025 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

#ifndef EXAMPLES_MATRIX_MATMUL_IBSHARE_CUSTOM_TILING_H
#define EXAMPLES_MATRIX_MATMUL_IBSHARE_CUSTOM_TILING_H
#include "register/tilingdata_base.h"
#include "tiling/tiling_api.h"

constexpr int32_t MIX_RATIO = 2;  // AIC:AIV = 1:2

namespace optiling {
BEGIN_TILING_DATA_DEF(MatmulIbshareCustomTilingData)
  TILING_DATA_FIELD_DEF_STRUCT(TCubeTiling, cubeTilingData);
END_TILING_DATA_DEF;

REGISTER_TILING_DATA_CLASS(MatmulIbshareCustom, MatmulIbshareCustomTilingData)
}

bool ComputeTiling(optiling::TCubeTiling& tiling, matmul_tiling::MultiCoreMatmulTiling* cubeTiling,
    const int32_t M, const int32_t N, const int32_t K, int32_t blockDim, const bool isBias, const bool ibshareA, const bool ibshareB)
{
    if (ibshareA && ibshareB) {
        blockDim /= MIX_RATIO;
    }
    cubeTiling->SetDim(blockDim);
    cubeTiling->SetAType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    cubeTiling->SetBType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    cubeTiling->SetCType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    cubeTiling->SetBiasType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    cubeTiling->SetShape(M, N, K);
    cubeTiling->SetOrgShape(M, N, K);
    cubeTiling->EnableBias(isBias);
    cubeTiling->SetBufferSpace(-1, -1, -1);
    if (cubeTiling->GetTiling(tiling) == -1) {
        return false;
    }
    return true;
}
#endif // EXAMPLES_MATRIX_MATMUL_IBSHARE_CUSTOM_TILING_H