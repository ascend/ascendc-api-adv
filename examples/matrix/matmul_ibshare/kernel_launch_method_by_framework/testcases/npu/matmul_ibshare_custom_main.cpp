/*
 * Copyright (c) 2025 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

#include "aclnn_matmul_ibshare_custom.h"
#include "acl/acl_rt.h"
#include "acl/acl.h"
#include <stdio.h>
#include <stdlib.h>
#include "../../../../../common/data_utils.h"
#include "../../testcase_params.h"

constexpr uint32_t INDEX_ZERO = 0;
constexpr uint32_t INDEX_ONE = 1;
constexpr uint32_t INDEX_TWO = 2;
constexpr uint32_t INDEX_THREE = 3;

aclrtStream CreateStream(int32_t device)
{
    if (aclInit(nullptr) != ACL_SUCCESS) {
        printf("acl init failed\n");
        return nullptr;
    }
    if (aclrtSetDevice(device) != ACL_SUCCESS) {
        printf("Set device failed\n");
        (void)aclFinalize();
        return nullptr;
    }
    aclrtStream stream = nullptr;
    if (aclrtCreateStream(&stream) != ACL_SUCCESS) {
        printf("Create stream failed\n");
        (void)aclFinalize();
        return nullptr;
    }
    return stream;
}

void DestroyStream(aclrtStream stream, int32_t device)
{
    (void)aclrtDestroyStream(stream);
    if (aclrtResetDevice(device) != ACL_SUCCESS) {
        printf("Reset device failed\n");
    }
    if (aclFinalize() != ACL_SUCCESS) {
        printf("Finalize acl failed\n");
    }
}

void DestroyTensor(aclTensor *tensors[], void *devMem[], int32_t tensorCount)
{
    for (auto i = 0; i < tensorCount; i++) {
        if (!tensors[i]) {
            continue;
        }
        if (devMem[i]) {
            CHECK_ACL(aclrtFree(devMem[i]));
        }
        aclDestroyTensor(tensors[i]);
    }
}

struct tensorInfo {
    int64_t *dims;
    int64_t dimCnt;
    aclDataType dtype;
    aclFormat fmt;
};

int64_t GetDataSize(struct tensorInfo *desc)
{
    if (!desc->dims) {
        return 0;
    }
    int64_t size = 1;
    for (auto i = 0; i < desc->dimCnt; i++) {
        size *= desc->dims[i];
    }
    return size * sizeof(float);
}

int64_t CompareResult(void *outputData, int64_t outSize)
{
    void *goldenData;
    CHECK_ACL(aclrtMallocHost((void**)(&goldenData), outSize));
    size_t goldenSize = outSize;
    bool ret = ReadFile("../output/golden.bin", goldenSize, goldenData, goldenSize);
    if (ret) {
        printf("ReadFile golden success!\n");
    } else {
        CHECK_ACL(aclrtFreeHost(goldenData));
        return -1;
    }
    constexpr float EPS = 1e-3;
    int64_t wrongNum = 0;

    for (auto i = 0; i < outSize / sizeof(float); i++) {
        float a = ((float*)outputData)[i];
        float b = ((float*)goldenData)[i];
        float ae = std::abs(a - b);
        float re = ae / abs(b);
        if (ae > EPS && re > EPS) {
            printf("CompareResult failed output is %lf, golden is %lf\n", a, b);
            wrongNum++;
        }
    }
    CHECK_ACL(aclrtFreeHost(goldenData));
    return wrongNum;
}

int32_t main(void)
{
    aclrtStream stream;
    int64_t shapeA[] = {TESTCASE_PARAMS.m, TESTCASE_PARAMS.k};
    int64_t shapeB[] = {TESTCASE_PARAMS.k, TESTCASE_PARAMS.n};
    int64_t shapeBias[] = {TESTCASE_PARAMS.n};
    int64_t shapeC[] = {TESTCASE_PARAMS.m, TESTCASE_PARAMS.n};
    struct tensorInfo tensorDesc[] = {{shapeA, 2, ACL_FLOAT16, ACL_FORMAT_ND},
                                      {shapeB, 2, ACL_FLOAT16, ACL_FORMAT_ND},
                                      {shapeBias, 1, ACL_FLOAT, ACL_FORMAT_ND},
                                      {shapeC, 2, ACL_FLOAT, ACL_FORMAT_ND},
    };
    stream = CreateStream(0);
    int32_t tensorCount = sizeof(tensorDesc) / sizeof(struct tensorInfo);
    aclTensor *tensors[tensorCount];
    void *devMem[tensorCount];
    for (auto i = 0; i < tensorCount; i++) {
        void *data;
        struct tensorInfo *info = &(tensorDesc[i]);
        int64_t size = GetDataSize(info);
        if (size == 0) {
            tensors[i] = nullptr;
            devMem[i] = nullptr;
            continue;
        }
        CHECK_ACL(aclrtMalloc(&data, size, ACL_MEM_MALLOC_HUGE_FIRST));
        // read input
        if (i == INDEX_ZERO) {
            size_t inputSize = size;
            void *dataHost;
            CHECK_ACL(aclrtMallocHost((void**)(&dataHost), inputSize));
            ReadFile("../input/input_a.bin", inputSize, dataHost, inputSize);
            CHECK_ACL(aclrtMemcpy(data, size, dataHost, size, ACL_MEMCPY_HOST_TO_DEVICE));
            CHECK_ACL(aclrtFreeHost(dataHost));
        }
        if (i == INDEX_ONE) {
            size_t inputSize = size;
            void *dataHost;
            CHECK_ACL(aclrtMallocHost((void**)(&dataHost), inputSize));
            ReadFile("../input/input_b.bin", inputSize, dataHost, inputSize);
            CHECK_ACL(aclrtMemcpy(data, size, dataHost, size, ACL_MEMCPY_HOST_TO_DEVICE));
            CHECK_ACL(aclrtFreeHost(dataHost));
        }
        if (i == INDEX_TWO) {
            size_t inputSize = size;
            void *dataHost;
            CHECK_ACL(aclrtMallocHost((void**)(&dataHost), inputSize));
            ReadFile("../input/input_bias.bin", inputSize, dataHost, inputSize);
            CHECK_ACL(aclrtMemcpy(data, size, dataHost, size, ACL_MEMCPY_HOST_TO_DEVICE));
            CHECK_ACL(aclrtFreeHost(dataHost));
        }
        devMem[i] = data;
        tensors[i] = aclCreateTensor(info->dims, info->dimCnt, info->dtype, nullptr, 0, info->fmt, info->dims, info->dimCnt, data);
    }
    size_t workspaceSize = 0;
    aclOpExecutor *handle;
    int32_t ret;
    ret = aclnnMatmulIbshareCustomGetWorkspaceSize(tensors[INDEX_ZERO], tensors[INDEX_ONE], tensors[INDEX_TWO],
        tensors[INDEX_THREE], &workspaceSize, &handle);
    if (ret != ACL_SUCCESS) {
        printf("aclnnMatmulIbshareCustomGetWorkspaceSize failed. error code is %u\n", ret);
        DestroyTensor(tensors, devMem, tensorCount);
        DestroyStream(stream, 0);
        return ret;
    }
    printf("aclnnMatmulIbshareCustomGetWorkspaceSize ret %u workspace size %lu\n", ret, workspaceSize);
    void *workspace = nullptr;
    if (workspaceSize != 0) {
        CHECK_ACL(aclrtMalloc(&workspace, workspaceSize, ACL_MEM_MALLOC_HUGE_FIRST));
    }
    ret = aclnnMatmulIbshareCustom(workspace, workspaceSize, handle, stream);
    printf("aclnnMatmulIbshareCustom ret %u\n", ret);
    CHECK_ACL(aclrtSynchronizeStream(stream));

    uint8_t *zHost, *maxHost, *sumHost;
    int64_t zHostSize = GetDataSize(&(tensorDesc[INDEX_THREE]));
    CHECK_ACL(aclrtMallocHost((void**)(&zHost), zHostSize));
    CHECK_ACL(aclrtMemcpy(zHost, zHostSize, devMem[INDEX_THREE], zHostSize, ACL_MEMCPY_DEVICE_TO_HOST));
    WriteFile("../output/output_z.bin", zHost, zHostSize);

    int64_t wrongNum = CompareResult(zHost, zHostSize);
    if (wrongNum != 0 ) {
        printf("test failed!\n");
    } else {
        printf("test pass!\n");
    }
    printf("\n");

    CHECK_ACL(aclrtFreeHost(zHost));

    DestroyTensor(tensors, devMem, tensorCount);
    CHECK_ACL(aclrtFree(workspace));
    DestroyStream(stream, 0);
    return 0;
}
