/*
 * Copyright (c) 2025 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

#include "kernel_operator.h"
#include "lib/matmul_intf.h"
#include "../kernel_impl/matmul_ibshare_custom_impl.h"
#include "testcase_params.h"

__aicore__ inline void CopyTiling(TCubeTiling* tiling, GM_ADDR tilingGM)
{
    uint32_t* ptr = reinterpret_cast<uint32_t*>(tiling);
    auto tiling32 = reinterpret_cast<__gm__ uint32_t*>(tilingGM);

    for (int i = 0; i < sizeof(TCubeTiling) / sizeof(uint32_t); i++, ptr++) {
      *ptr = *(tiling32 + i);
    }
    return;
}

extern "C" __global__ __aicore__ void matmul_ibshare_custom(const GM_ADDR a, const GM_ADDR b, const GM_ADDR c,
    const GM_ADDR workspace, const GM_ADDR tilingGm)
{
    TCubeTiling tiling;
    CopyTiling(&tiling, tilingGm);
    MatmulIbshareKernel<half, half, float, float, TESTCASE_PARAMS.ibshareA, TESTCASE_PARAMS.ibshareB> matmulKernel;
    AscendC::TPipe pipe;
    REGIST_MATMUL_OBJ(&pipe, GetSysWorkSpacePtr(), matmulKernel.matmulObj, &tiling);
    matmulKernel.Init(a, b, nullptr, c, workspace, tiling);
    matmulKernel.Process<TESTCASE_PARAMS.hasBias>(&pipe);
}

#ifndef ASCENDC_CPU_DEBUG
void matmul_ibshare_custom_do(uint32_t blockDim, void* stream, GM_ADDR a, GM_ADDR b,
                    GM_ADDR c, GM_ADDR workspace, GM_ADDR tilingGm)
{
    matmul_ibshare_custom<<<blockDim, nullptr, stream>>>(a, b, c, workspace, tilingGm);
}
#endif