/*
 * Copyright (c) 2025 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

#include "kernel_operator.h"
#include "../../../../../../kernel_impl/matmul_preload_custom_impl.h"

extern "C" __global__ __aicore__ void matmul_preload_custom(
    GM_ADDR a, GM_ADDR b, GM_ADDR bias, GM_ADDR c, GM_ADDR workspace, GM_ADDR tilingGm) {
    GET_TILING_DATA(tilingData, tilingGm);
    AscendC::TPipe pipe;
    if constexpr (TILING_KEY_IS(1)) {
        constexpr int32_t preloadMode = 1;
        CustomMatmulPreload::MatmulPreloadKernel<half, half, float, float, preloadMode> matmulPreloadKernel;
        matmulPreloadKernel.Init(a, b, bias, c, workspace, tilingData.cubeTilingData);
        matmulPreloadKernel.Process(&pipe);
    } else if constexpr (TILING_KEY_IS(2)) {
        constexpr int32_t preloadMode = 2;
        CustomMatmulPreload::MatmulPreloadKernel<half, half, float, float, preloadMode> matmulPreloadKernel;
        matmulPreloadKernel.Init(a, b, bias, c, workspace, tilingData.cubeTilingData);
        matmulPreloadKernel.Process(&pipe);
    }
}