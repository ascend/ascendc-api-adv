#!/usr/bin/python3
# coding=utf-8

# Copyright (c) 2025 Huawei Technologies Co., Ltd.
# This file is a part of the CANN Open Software.
# Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
# Please refer to the License for details. You may not use this file except in compliance with the License.
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
# INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
# See LICENSE in the root of the software repository for the full text of the License.
# ======================================================================================================================

import numpy as np
import os


def gen_golden_data(preload_mode):
    block_dim = 24
    m, n, k, is_bias = 512 * block_dim, 256, 128, True
    if preload_mode == "N":
        m, n, k, is_bias = 134, 1338 * block_dim, 128, True

    input_a = np.random.randint(1, 10, [m, k]).astype(np.float16)
    input_b = np.random.randint(1, 10, [k, n]).astype(np.float16)
    input_bias = np.random.randint(1, 10, [n, ]).astype(np.float32)
    if is_bias:
        golden = (np.matmul(input_a.astype(np.float32), input_b.astype(np.float32),
                            dtype=np.float32) + input_bias).astype(np.float32)
    else:
        golden = np.matmul(input_a.astype(np.float32), input_b.astype(np.float32),
                           dtype=np.float32).astype(np.float32)
    if not os.path.exists("input"):
        os.mkdir("input")
    if not os.path.exists("output"):
        os.mkdir("output")

    input_a.tofile("./input/input_a.bin")
    input_b.tofile("./input/input_b.bin")
    input_bias.tofile("./input/input_bias.bin")
    golden.tofile("./output/golden.bin")


def main():
    preload_mode = os.environ.get("PRELOAD_MODE", "M")
    assert preload_mode in ("M", "N"), f"preload mode should be in ['M', 'N'], actual is {preload_mode}"
    gen_golden_data(preload_mode)


if __name__ == "__main__":
    main()
