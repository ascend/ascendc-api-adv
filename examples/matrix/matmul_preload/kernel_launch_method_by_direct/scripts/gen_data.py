#!/usr/bin/python3
# coding=utf-8

# Copyright (c) 2025 Huawei Technologies Co., Ltd.
# This file is a part of the CANN Open Software.
# Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
# Please refer to the License for details. You may not use this file except in compliance with the License.
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
# INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
# See LICENSE in the root of the software repository for the full text of the License.
# ======================================================================================================================

import numpy as np
import os
import sys


def gen_golden_data(preload_mode):
    block_dim = 24
    m, n, k, is_bias = 512 * block_dim, 256, 128, True
    if preload_mode == "N":
        m, n, k, is_bias = 134, 1338 * block_dim, 128, True

    x1_gm = np.random.randint(1, 10, [m, k]).astype(np.float16)
    x2_gm = np.random.randint(1, 10, [k, n]).astype(np.float16)
    bias_gm = np.random.randint(1, 10, [n, ]).astype(np.float32)
    if is_bias:
        golden = (np.matmul(x1_gm.astype(np.float32), x2_gm.astype(np.float32),
                            dtype=np.float32) + bias_gm).astype(np.float32)
    else:
        golden = np.matmul(x1_gm.astype(np.float32), x2_gm.astype(np.float32), dtype=np.float32)
    os.system("mkdir -p input")
    os.system("mkdir -p output")
    x1_gm.tofile("./input/x1_gm.bin")
    x2_gm.tofile("./input/x2_gm.bin")
    bias_gm.tofile("./input/bias_gm.bin")
    golden.tofile("./output/golden.bin")


def main():
    preload_mode = "M"
    args = sys.argv
    if len(args) > 1:
        preload_mode = str(args[1])
        assert preload_mode in ("M", "N"), f"preload mode should be in ['M', 'N'], actual is {preload_mode}"
    gen_golden_data(preload_mode)


if __name__ == "__main__":
    main()
