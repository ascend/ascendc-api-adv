/*
 * Copyright (c) 2025 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

#include <iostream>
#include "tiling/tiling_api.h"
#include "tiling/platform/platform_ascendc.h"
#include "../host_tiling/matmul_preload_custom_tiling.h"


void GenerateTiling(const char* socVersion, const int32_t M, const int32_t N, const int32_t K,
    const int32_t blockDim, const bool isBias, uint8_t* tilingBuffer)
{
    optiling::TCubeTiling tilingData;
    auto ascendcPlatform = platform_ascendc::PlatformAscendCManager::GetInstance(socVersion);
    matmul_tiling::MultiCoreMatmulTiling cubeTiling(*ascendcPlatform);
    bool res = ComputeTiling(tilingData, &cubeTiling, M, N, K, blockDim, isBias);
    if (!res) {
        std::cout << "gen tiling failed" << std::endl;
    }
    uint32_t tilingSize = tilingData.GetDataSize();
    tilingData.SaveToBuffer(tilingBuffer, tilingSize);
}
