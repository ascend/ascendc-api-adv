/*
 * Copyright (c) 2025 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

#include "kernel_operator.h"
#include "../kernel_impl/matmul_preload_custom_impl.h"

#if defined(ASCENDC_CPU_DEBUG)
#define SET_G_CORE_TYPE_IS_AIV int g_coreType = 2;
#define SET_G_CORE_TYPE_IS_AIC int g_coreType = 1;
#else
#define SET_G_CORE_TYPE_IS_AIV
#define SET_G_CORE_TYPE_IS_AIC
#endif

#if defined(CUSTOM_PRELOAD_N)
constexpr int32_t PRELOAD_MODE = 2;
#else
constexpr int32_t PRELOAD_MODE = 1;
#endif

__aicore__ inline void CopyTiling(TCubeTiling* tiling, GM_ADDR tilingGM)
{
    uint32_t* ptr = reinterpret_cast<uint32_t*>(tiling);
    auto tiling32 = reinterpret_cast<__gm__ uint32_t*>(tilingGM);

    for (int i = 0; i < sizeof(TCubeTiling) / sizeof(uint32_t); i++, ptr++) {
      *ptr = *(tiling32 + i);
    }
    return;
}

extern "C" __global__ __aicore__ void matmul_preload_custom(
    GM_ADDR a, GM_ADDR b, GM_ADDR bias, GM_ADDR c, GM_ADDR workspace, GM_ADDR tilingGm)
{
#if defined(ASCENDC_CPU_DEBUG)
    if (g_coreType == AscendC::AIV) {
        return;
    }
#endif
    TCubeTiling tiling;
    CopyTiling(&tiling, tilingGm);
    CustomMatmulPreload::MatmulPreloadKernel<half, half, float, float, PRELOAD_MODE> matmulPreloadKernel;
    AscendC::TPipe pipe;
    matmulPreloadKernel.Init(a, b, bias, c, workspace, tiling);
    matmulPreloadKernel.Process(&pipe);
}

#ifndef ASCENDC_CPU_DEBUG
void matmul_preload_custom_do(uint32_t blockDim, void* stream, GM_ADDR a, GM_ADDR b,
                    GM_ADDR bias, GM_ADDR c, GM_ADDR workspace, GM_ADDR tilingGm)
{
    matmul_preload_custom<<<blockDim, nullptr, stream>>>(a, b, bias, c, workspace, tilingGm);
}
#endif