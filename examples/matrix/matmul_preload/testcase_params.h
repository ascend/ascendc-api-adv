/*
 * Copyright (c) 2025 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */
#ifndef EXAMPLE_MATRIX_MATMUL_PRELOAD_TESTCASE_PARAM_H
#define EXAMPLE_MATRIX_MATMUL_PRELOAD_TESTCASE_PARAM_H

struct TestcaseParams {
public:
    TestcaseParams() {}
    constexpr TestcaseParams(const uint32_t mIn, const uint32_t nIn, const uint32_t kIn,
        const uint32_t blockDim, const bool isBiasIn)
        : m(mIn),
          n(nIn),
          k(kIn),
          blockDim(blockDim),
          isBias(isBiasIn) {}

    uint32_t m = 0;
    uint32_t n = 0;
    uint32_t k = 0;
    uint32_t blockDim = 1;
    bool isBias = false;
};

constexpr TestcaseParams PRELOAD_M_CASE = TestcaseParams(12288, 256, 128, 24, true);
constexpr TestcaseParams PRELOAD_N_CASE = TestcaseParams(134, 32112, 128, 24, true);

#endif // EXAMPLES_MATRIX_MATMUL_PRELOAD_TESTCASE_PARAMS_H