/**
 * Copyright (c) Huawei Technologies Co., Ltd. 2024. All rights reserved.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

#include "../host_tiling/basic_block_matmul_custom_tiling.h"
#include <cassert>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include "tiling/tiling_api.h"
#include "tiling/platform/platform_ascendc.h"

constexpr int32_t USED_CORE_NUM = 4;

uint8_t *GetTilingBuf(optiling::TCubeTiling *tilingData)
{
    if (tilingData == nullptr) {
        return nullptr;
    }
    uint32_t tilingSize = tilingData->GetDataSize();
    if (tilingSize <= 0) {
        return nullptr;
    }
    uint8_t *buf = (uint8_t *)malloc(tilingSize);
    if (buf == nullptr) {
        return nullptr;
    }
    tilingData->SaveToBuffer(buf, tilingSize);
    return buf;
}

uint8_t *GenerateTiling(int32_t M, int32_t N, int32_t K, int32_t baseM, int32_t baseN, int32_t baseK, const char *socVersion)
{
    optiling::TCubeTiling tilingData;
    auto ascendcPlatform = platform_ascendc::PlatformAscendCManager::GetInstance(socVersion);
    matmul_tiling::MultiCoreMatmulTiling cubeTiling(*ascendcPlatform);
    bool res = ComputeTiling(tilingData, &cubeTiling, M, N, K,
                baseM, baseN, baseK, USED_CORE_NUM, false);
    if (!res) {
        std::cout << "gen tiling failed" << std::endl;
    }
    return GetTilingBuf(&tilingData);
}
