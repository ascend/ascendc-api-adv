/*
 * Copyright (c) 2025 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

#include "aclnn_matmul_mndb_custom.h"
#include "acl/acl_rt.h"
#include "acl/acl.h"
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include "../../../../../common/data_utils.h"
#include "../../../testcase_params.h"


aclrtStream CreateStream(const int32_t device)
{
    if (aclInit(nullptr) != ACL_SUCCESS) {
        printf("acl init failed\n");
        return nullptr;
    }
    if (aclrtSetDevice(device) != ACL_SUCCESS) {
        printf("Set device failed\n");
        CHECK_ACL(aclFinalize());
        return nullptr;
    }
    aclrtStream stream = nullptr;
    if (aclrtCreateStream(&stream) != ACL_SUCCESS) {
        printf("Create stream failed\n");
        CHECK_ACL(aclFinalize());
        return nullptr;
    }
    return stream;
}

void DestroyStream(aclrtStream stream, const int32_t device)
{
    CHECK_ACL(aclrtDestroyStream(stream));
    if (aclrtResetDevice(device) != ACL_SUCCESS) {
        printf("Reset device failed\n");
    }
    if (aclFinalize() != ACL_SUCCESS) {
        printf("Finalize acl failed\n");
    }
}

void DestroyTensor(aclTensor* tensors[], void* devMem[], const int32_t tensorCount)
{
    for (auto i = 0; i < tensorCount; i++) {
        if (!tensors[i]) {
            continue;
        }
        if (devMem[i]) {
            CHECK_ACL(aclrtFree(devMem[i]));
        }
        CHECK_ACL(aclDestroyTensor(tensors[i]));
    }
}

struct tensorInfo {
    int64_t* dims;
    int64_t dimCnt;
    aclDataType dtype;
    aclFormat fmt;
};

int64_t GetDataSize(struct tensorInfo* desc)
{
    if (!desc->dims) {
        return 0;
    }
    int64_t size = 1;
    for (auto i = 0; i < desc->dimCnt; i++) {
        size *= desc->dims[i];
    }
    return size * sizeof(float);
}

int64_t CompareResult(void* outputData, const int64_t outSize)
{
    void* goldenData;
    CHECK_ACL(aclrtMallocHost((void**)(&goldenData), outSize));
    size_t goldenSize = outSize;
    bool ret = ReadFile("../output/golden.bin", goldenSize, goldenData, goldenSize);
    if (ret) {
        printf("ReadFile golden success!\n");
    } else {
        CHECK_ACL(aclrtFreeHost(goldenData));
        return -1;
    }
    constexpr float EPS = 1e-4;
    int64_t wrongNum = 0;
    int64_t maxPrintNums = 10;
    for (auto i = 0; i < outSize / sizeof(float); i++) {
        float a = ((float*)outputData)[i];
        float b = ((float*)goldenData)[i];
        float ae = std::abs(a - b);
        float re = ae / abs(b);
        if (ae > EPS && re > EPS) {
            printf("CompareResult index %d failed output is %lf, golden is %lf\n", i, a, b);
            wrongNum++;
        }
        if (wrongNum > maxPrintNums) {
            break;
        }
    }
    CHECK_ACL(aclrtFreeHost(goldenData));
    return wrongNum;
}

int32_t main(void)
{
    const char* mndbModeEnv = std::getenv("MNDB_MODE");
    const char* configModeEnv = std::getenv("CONFIG_MODE");
    const char* mndbModeChar = (mndbModeEnv != nullptr) ? mndbModeEnv : "M";
    const char* configModeChar = (configModeEnv != nullptr) ? configModeEnv : "NORM";
    if (mndbModeEnv != nullptr) {
        if (strcmp(mndbModeChar, "M") == 0 && strcmp(mndbModeChar, "N") == 0) {
            printf("check failed!, MNDB MODE should be M/N, actual is %s\n", mndbModeChar);
            return -1;
        }
    }
    if (configModeEnv != nullptr) {
        if (strcmp(configModeChar, "NORM") == 0 && strcmp(configModeChar, "MDL") == 0) {
            printf("check failed!, CONFIG MODE should be NORM/MDL, actual is %s\n", configModeChar);
            return -1;
        }
    }
    bool isBias = MNDB_M_NORM_CASE.isBias;
    int32_t blockDim = MNDB_M_NORM_CASE.blockDim;
    int32_t M = MNDB_M_NORM_CASE.m;
    int32_t N = MNDB_M_NORM_CASE.n;
    int32_t K = MNDB_M_NORM_CASE.k;
    constexpr int32_t M_NORM_MODE = 1;
    constexpr int32_t N_NORM_MODE = 2;
    constexpr int32_t M_MDL_MODE = 3;
    constexpr int32_t N_MDL_MODE = 4;
    int32_t mode = M_NORM_MODE;
    if (strcmp(mndbModeChar, "N") == 0 && strcmp(configModeChar, "NORM") == 0) {
        M = MNDB_N_NORM_CASE.m;
        N = MNDB_N_NORM_CASE.n;
        K = MNDB_N_NORM_CASE.k;
        isBias = MNDB_N_NORM_CASE.isBias;
        blockDim = MNDB_N_NORM_CASE.blockDim;
        mode = N_NORM_MODE;
    } else if (strcmp(mndbModeChar, "M") == 0 && strcmp(configModeChar, "MDL") == 0) {
        M = MNDB_M_MDL_CASE.m;
        N = MNDB_M_MDL_CASE.n;
        K = MNDB_M_MDL_CASE.k;
        isBias = MNDB_M_MDL_CASE.isBias;
        blockDim = MNDB_M_MDL_CASE.blockDim;
        mode = M_MDL_MODE;
    } else if (strcmp(mndbModeChar, "N") == 0 && strcmp(configModeChar, "MDL") == 0) {
        M = MNDB_N_MDL_CASE.m;
        N = MNDB_N_MDL_CASE.n;
        K = MNDB_N_MDL_CASE.k;
        isBias = MNDB_N_MDL_CASE.isBias;
        blockDim = MNDB_N_MDL_CASE.blockDim;
        mode = N_MDL_MODE;
    }

    int64_t shapeA[] = {M, K};
    int64_t shapeB[] = {K, N};
    int64_t shapeBias[] = {N};
    int64_t shapeC[] = {M, N};
    struct tensorInfo tensorDesc[] = {{shapeA, 2, ACL_FLOAT, ACL_FORMAT_ND},
                                      {shapeB, 2, ACL_FLOAT, ACL_FORMAT_ND},
                                      {shapeBias, 1, ACL_FLOAT, ACL_FORMAT_ND},
                                      {shapeC, 2, ACL_FLOAT, ACL_FORMAT_ND},
    };
    aclrtStream stream = CreateStream(0);
    if (stream == nullptr) {
        return -1;
    }
    int32_t tensorCount = sizeof(tensorDesc) / sizeof(struct tensorInfo);
    aclTensor* tensors[tensorCount];
    void* devMem[tensorCount];
    int32_t a_index = 0;
    int32_t b_index = 1;
    int32_t bias_index = 2;
    int32_t c_index = 3;
    for (auto i = 0; i < tensorCount; i++) {
        void* data;
        struct tensorInfo* info = &(tensorDesc[i]);
        int64_t size = GetDataSize(info);
        if (size == 0) {
            tensors[i] = nullptr;
            devMem[i] = nullptr;
            continue;
        }
        CHECK_ACL(aclrtMalloc(&data, size, ACL_MEM_MALLOC_HUGE_FIRST));
        // read input
        if (i == a_index) {
            size_t inputSize = size;
            void* dataHost;
            CHECK_ACL(aclrtMallocHost((void**)(&dataHost), inputSize));
            ReadFile("../input/input_a.bin", inputSize, dataHost, inputSize);
            CHECK_ACL(aclrtMemcpy(data, size, dataHost, size, ACL_MEMCPY_HOST_TO_DEVICE));
            CHECK_ACL(aclrtFreeHost(dataHost));
        }
        if (i == b_index) {
            size_t inputSize = size;
            void* dataHost;
            CHECK_ACL(aclrtMallocHost((void**)(&dataHost), inputSize));
            ReadFile("../input/input_b.bin", inputSize, dataHost, inputSize);
            CHECK_ACL(aclrtMemcpy(data, size, dataHost, size, ACL_MEMCPY_HOST_TO_DEVICE));
            CHECK_ACL(aclrtFreeHost(dataHost));
        }
        if (i == bias_index) {
            size_t inputSize = size;
            void* dataHost;
            CHECK_ACL(aclrtMallocHost((void**)(&dataHost), inputSize));
            ReadFile("../input/input_bias.bin", inputSize, dataHost, inputSize);
            CHECK_ACL(aclrtMemcpy(data, size, dataHost, size, ACL_MEMCPY_HOST_TO_DEVICE));
            CHECK_ACL(aclrtFreeHost(dataHost));
        }
        devMem[i] = data;
        tensors[i] = aclCreateTensor(info->dims, info->dimCnt, info->dtype, nullptr, 0, info->fmt, info->dims, info->dimCnt, data);
    }
    size_t workspaceSize = 0;
    aclOpExecutor* handle;
    int32_t ret;
    ret = aclnnMatmulMndbCustomGetWorkspaceSize(tensors[a_index], tensors[b_index], tensors[bias_index], isBias, mode,
                                            tensors[c_index],
                                            &workspaceSize,
                                            &handle);
    if (ret != ACL_SUCCESS) {
        printf("aclnnMatmulMndbCustomGetWorkspaceSize failed. error code is %u\n", ret);
        DestroyTensor(tensors, devMem, tensorCount);
        DestroyStream(stream, 0);
        return ret;
    }
    printf("aclnnMatmulMndbCustomGetWorkspaceSize ret %u workspace size %lu\n", ret, workspaceSize);
    void* workspace = nullptr;
    if (workspaceSize != 0) {
        CHECK_ACL(aclrtMalloc(&workspace, workspaceSize, ACL_MEM_MALLOC_HUGE_FIRST));
    }
    ret = aclnnMatmulMndbCustom(workspace, workspaceSize, handle, stream);
    printf("aclnnMatmulMndbCustom ret %u\n", ret);
    CHECK_ACL(aclrtSynchronizeStream(stream));

    uint8_t* zHost;
    int64_t zHostSize = GetDataSize(&(tensorDesc[c_index]));
    CHECK_ACL(aclrtMallocHost((void**)(&zHost), zHostSize));
    CHECK_ACL(aclrtMemcpy(zHost, zHostSize, devMem[c_index], zHostSize, ACL_MEMCPY_DEVICE_TO_HOST));
    WriteFile("../output/output_z.bin", zHost, zHostSize);

    int64_t wrongNum = CompareResult(zHost, zHostSize);
    if (wrongNum != 0 ) {
        printf("test failed!\n");
    } else {
        printf("test pass!\n");
    }

    if (workspaceSize != 0) {
        CHECK_ACL(aclrtFree(workspace));
    }
    CHECK_ACL(aclrtFreeHost(zHost));

    DestroyTensor(tensors, devMem, tensorCount);
    DestroyStream(stream, 0);
    return 0;
}
