/*
 * Copyright (c) 2025 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

#include "matmul_mndb_custom_tiling.h"
#include "register/op_def_registry.h"
#include "tiling/platform/platform_ascendc.h"
#include "../../testcase_params.h"

namespace optiling {
static ge::graphStatus TilingFunc(gert::TilingContext* context)
{
    auto ascendcPlatform = platform_ascendc::PlatformAscendC(context->GetPlatformInfo());
    auto shape_a = context->GetInputShape(0)->GetStorageShape();
    auto shape_b = context->GetInputShape(1)->GetStorageShape();
    int32_t M = shape_a.GetDim(0);
    int32_t N = shape_b.GetDim(1);
    int32_t K = shape_a.GetDim(1);
    matmul_tiling::MultiCoreMatmulTiling cubeTiling(ascendcPlatform);
    const gert::RuntimeAttrs* attrs = context->GetAttrs();

    const int64_t aivCoreNum = ascendcPlatform.GetCoreNum();
    const uint32_t blockDim = aivCoreNum / 2;
    const bool isBias = *(attrs->GetAttrPointer<bool>(0));
    const int64_t mode = *(attrs->GetAttrPointer<int64_t>(1));

    constexpr int32_t M_DB_NORM_MODE = 1;
    constexpr int32_t N_DB_NORM_MODE = 2;
    constexpr int32_t M_DB_MDL_MODE = 3;
    constexpr int32_t N_DB_MDL_MODE = 4;

    MatmulMndbCustomTilingData tiling;
    bool res = ComputeTiling(tiling.cubeTilingData, &cubeTiling, M, N, K, blockDim, isBias);
    if (!res) {
        return ge::GRAPH_FAILED;
    }
    context->SetTilingKey(mode);
    TestcaseParams caseParams;
    if (mode == M_DB_NORM_MODE) {
        caseParams = MNDB_M_NORM_CASE;
    } else if (mode == N_DB_NORM_MODE) {
        caseParams = MNDB_N_NORM_CASE;
    } else if (mode == M_DB_MDL_MODE) {
        caseParams = MNDB_M_MDL_CASE;
    } else if (mode == N_DB_MDL_MODE) {
        caseParams = MNDB_N_MDL_CASE;
    }
    tiling.cubeTilingData.set_singleCoreM(caseParams.singleCoreM);
    tiling.cubeTilingData.set_singleCoreN(caseParams.singleCoreN);
    tiling.cubeTilingData.set_singleCoreK(caseParams.singleCoreK);
    tiling.cubeTilingData.set_baseM(caseParams.baseM);
    tiling.cubeTilingData.set_baseN(caseParams.baseN);
    tiling.cubeTilingData.set_baseK(caseParams.baseK);
    tiling.cubeTilingData.set_depthA1(caseParams.depthA1);
    tiling.cubeTilingData.set_depthB1(caseParams.depthB1);
    tiling.cubeTilingData.set_stepM(caseParams.stepM);
    tiling.cubeTilingData.set_stepN(caseParams.stepN);
    tiling.cubeTilingData.set_stepKa(caseParams.stepKa);
    tiling.cubeTilingData.set_stepKb(caseParams.stepKb);
    tiling.cubeTilingData.set_iterateOrder(caseParams.iterateOrder);
    context->SetBlockDim(blockDim);

    tiling.SaveToBuffer(context->GetRawTilingData()->GetData(), context->GetRawTilingData()->GetCapacity());
    context->GetRawTilingData()->SetDataSize(tiling.GetDataSize());
    size_t userWorkspaceSize = 0;
    size_t systemWorkspaceSize = static_cast<size_t>(ascendcPlatform.GetLibApiWorkSpaceSize());
    size_t *currentWorkspace = context->GetWorkspaceSizes(1);
    currentWorkspace[0] = userWorkspaceSize + systemWorkspaceSize;

    return ge::GRAPH_SUCCESS;
}
}

namespace ops {
class MatmulMndbCustom : public OpDef {
public:
    explicit MatmulMndbCustom(const char* name) : OpDef(name)
    {
        this->Input("a")
            .ParamType(REQUIRED)
            .DataType({ge::DT_FLOAT})
            .Format({ge::FORMAT_ND})
            .UnknownShapeFormat({ge::FORMAT_ND});
        this->Input("b")
            .ParamType(REQUIRED)
            .DataType({ge::DT_FLOAT})
            .Format({ge::FORMAT_ND})
            .UnknownShapeFormat({ge::FORMAT_ND});
        this->Input("bias")
            .ParamType(REQUIRED)
            .DataType({ge::DT_FLOAT})
            .Format({ge::FORMAT_ND})
            .UnknownShapeFormat({ge::FORMAT_ND});
        this->Output("c")
            .ParamType(REQUIRED)
            .DataType({ge::DT_FLOAT})
            .Format({ge::FORMAT_ND})
            .UnknownShapeFormat({ge::FORMAT_ND});
        this->Attr("isBias").Bool();
        this->Attr("mode").Int();
        this->AICore()
            .SetTiling(optiling::TilingFunc);
        this->AICore().AddConfig("ascend910b");
    }
};

OP_ADD(MatmulMndbCustom);
}