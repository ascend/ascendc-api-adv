#!/usr/bin/python3
# coding=utf-8
# Copyright (c) 2025 Huawei Technologies Co., Ltd.
# This file is a part of the CANN Open Software.
# Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
# Please refer to the License for details. You may not use this file except in compliance with the License.
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
# INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
# See LICENSE in the root of the software repository for the full text of the License.
# ======================================================================================================================

import numpy as np
import os


def gen_golden_data(mndb_mode, config_mode):
    core_num = 24
    m = 128
    n = 1024 * core_num
    k = 128
    is_bias = True
    if mndb_mode == "N" and config_mode == "NORM":
        m = 1024 * core_num
        n = 128
        k = 128
    elif mndb_mode == "M" and config_mode == "MDL":
        m = 16 * core_num
        n = 1920
        k = 16
    elif mndb_mode == "N" and config_mode == "MDL":
        m = 1920 * core_num
        n = 16
        k = 16

    input_a = np.random.randint(1, 10, [m, k]).astype(np.float32)
    input_b = np.random.randint(1, 10, [k, n]).astype(np.float32)
    input_bias = np.random.randint(1, 10, [n, ]).astype(np.float32)
    if is_bias:
        golden = (np.matmul(input_a.astype(np.float32), input_b.astype(np.float32),
                            dtype=np.float32) + input_bias).astype(np.float32)
    else:
        golden = np.matmul(input_a.astype(np.float32), input_b.astype(np.float32),
                           dtype=np.float32).astype(np.float32)
    if not os.path.exists("input"):
        os.mkdir("input")
    if not os.path.exists("output"):
        os.mkdir("output")

    input_a.tofile("./input/input_a.bin")
    input_b.tofile("./input/input_b.bin")
    input_bias.tofile("./input/input_bias.bin")
    golden.tofile("./output/golden.bin")


def main():
    mndb_mode = os.environ.get("MNDB_MODE", "M")
    config_mode = os.environ.get("CONFIG_MODE", "NORM")
    assert mndb_mode in ("M", "N"), f"mndb mode should be in ['M', 'N'], actual is {mndb_mode}"
    assert config_mode in ("NORM", "MDL"), f"config mode should be in ['NORM', 'MDL'], actual is {config_mode}"
    gen_golden_data(mndb_mode, config_mode)


if __name__ == "__main__":
    main()
