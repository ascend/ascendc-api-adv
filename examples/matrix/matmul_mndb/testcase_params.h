/*
 * Copyright (c) 2025 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */
#ifndef EXAMPLE_MATRIX_MATMUL_MNDB_TESTCASE_PARAM_H
#define EXAMPLE_MATRIX_MATMUL_MNDB_TESTCASE_PARAM_H

struct TestcaseParams {
public:
    TestcaseParams() {}
    constexpr TestcaseParams(const uint32_t mIn, const uint32_t nIn, const uint32_t kIn,
        const uint32_t sM, const uint32_t sN, const uint32_t sK, const uint32_t baseMIn,
        const uint32_t baseNIn, const uint32_t baseKIn, const uint32_t dpA, const uint32_t dpB,
        const uint32_t stepMIn, const uint32_t stepNIn, const uint32_t stepKaIn, const uint32_t stepKbIn,
        const uint32_t iterateOrder,
        const uint32_t blockDim, const bool isBiasIn)
        : m(mIn),
          n(nIn),
          k(kIn),
          singleCoreM(sM),
          singleCoreN(sN),
          singleCoreK(sK),
          baseM(baseMIn),
          baseN(baseNIn),
          baseK(baseKIn),
          depthA1(dpA),
          depthB1(dpB),
          stepM(stepMIn),
          stepN(stepNIn),
          stepKa(stepKaIn),
          stepKb(stepKbIn),
          iterateOrder(iterateOrder),
          blockDim(blockDim),
          isBias(isBiasIn) {}

    uint32_t m = 0;
    uint32_t n = 0;
    uint32_t k = 0;
    uint32_t singleCoreM = 0;
    uint32_t singleCoreN = 0;
    uint32_t singleCoreK = 0;
    uint32_t baseM = 0;
    uint32_t baseN = 0;
    uint32_t baseK = 0;
    uint32_t depthA1 = 0;
    uint32_t depthB1 = 0;
    uint32_t stepM = 0;
    uint32_t stepN = 0;
    uint32_t stepKa = 0;
    uint32_t stepKb = 0;
    uint32_t iterateOrder = 0;
    uint32_t blockDim = 1;
    bool isBias = false;
};

constexpr TestcaseParams MNDB_M_NORM_CASE = TestcaseParams(
    128, 1024 * 24, 128, 128, 1024, 128, 64, 256, 32, 4, 8, 1, 2, 4, 4, 0, 24, true);
constexpr TestcaseParams MNDB_N_NORM_CASE = TestcaseParams(
    24 * 1024, 128, 128, 1024, 128, 128, 128, 64, 32, 8, 2, 4, 1, 2, 2, 1, 24, true);
constexpr TestcaseParams MNDB_M_MDL_CASE = TestcaseParams(
    24 * 16, 1920, 16, 16, 1920, 16, 16, 128, 32, 1, 4, 1, 4, 1, 1, 0, 24, true);
constexpr TestcaseParams MNDB_N_MDL_CASE = TestcaseParams(
    24 * 1920, 16, 16, 1920, 16, 16, 128, 16, 16, 4, 1, 4, 1, 1, 1, 1, 24, true);

#endif // EXAMPLES_MATRIX_MATMUL_MNDB_TESTCASE_PARAMS_H