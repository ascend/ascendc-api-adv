#!/usr/bin/python3
# coding=utf-8

# Copyright (c) 2025 Huawei Technologies Co., Ltd.
# This file is a part of the CANN Open Software.
# Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
# Please refer to the License for details. You may not use this file except in compliance with the License.
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
# INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
# See LICENSE in the root of the software repository for the full text of the License.
# ======================================================================================================================

import numpy as np
import os
import sys


def gen_golden_data(mndb_mode, config_mode):
    x1_gm_type = np.float32
    x2_gm_type = np.float32
    bias_type = np.float32

    core_num = 24
    m = 128
    n = 1024 * core_num
    k = 128
    is_bias = True
    if mndb_mode == "N" and config_mode == "NORM":
        m = 1024 * core_num
        n = 128
        k = 128
    elif mndb_mode == "M" and config_mode == "MDL":
        m = 16 * core_num
        n = 1920
        k = 16
    elif mndb_mode == "N" and config_mode == "MDL":
        m = 1920 * core_num
        n = 16
        k = 16

    x1_gm = np.random.randint(1, 10, [m, k]).astype(x1_gm_type)
    x2_gm = np.random.randint(1, 10, [k, n]).astype(x2_gm_type)
    bias_gm = np.random.randint(1, 10, [n, ]).astype(bias_type)
    if is_bias:
        golden = (np.matmul(x1_gm.astype(np.float32), x2_gm.astype(np.float32),
                            dtype=np.float32) + bias_gm).astype(np.float32)
    else:
        golden = np.matmul(x1_gm.astype(np.float32), x2_gm.astype(np.float32), dtype=np.float32)
    os.system("mkdir -p input")
    os.system("mkdir -p output")
    x1_gm.tofile("./input/x1_gm.bin")
    x2_gm.tofile("./input/x2_gm.bin")
    bias_gm.tofile("./input/bias_gm.bin")
    golden.tofile("./output/golden.bin")

def main():
    mndb_mode = "M"
    config_mode = "NORM"
    args = sys.argv
    if len(args) > 1:
        mndb_mode = str(args[1])
        assert mndb_mode in ("M", "N"), f"mndb mode should be in ['M', 'N'], actual is {mndb_mode}"
    if len(args) > 2:
        config_mode = str(args[2])
        assert config_mode in ("NORM", "MDL"), f"config mode should be in ['NORM', 'MDL'], actual is {config_mode}"
    gen_golden_data(mndb_mode, config_mode)


if __name__ == "__main__":
    main()
