/*
 * Copyright (c) 2025 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

#include <iostream>
#include "tiling/tiling_api.h"
#include "tiling/platform/platform_ascendc.h"
#include "../host_tiling/matmul_mndb_custom_tiling.h"
#include "../testcase_params.h"
#include "./matmul_mndb_custom_tiling.h"


namespace MatmulMndb {
void GenerateTiling(const char* socVersion, TestcaseParams& caseParams, std::uint8_t* tilingBuffer)
{
    optiling::TCubeTiling tilingData;
    auto ascendcPlatform = platform_ascendc::PlatformAscendCManager::GetInstance(socVersion);
    matmul_tiling::MultiCoreMatmulTiling cubeTiling(*ascendcPlatform);
    bool res = ComputeTiling(tilingData, &cubeTiling, caseParams.m, caseParams.n, caseParams.k,
        caseParams.blockDim, caseParams.isBias);
    if (!res) {
        std::cout << "gen tiling failed" << std::endl;
    }
    // reset tiling params because cubeTiling does not have Set api with stepN
    // mndb stepN should be > 1 or step M should be >1
    tilingData.set_singleCoreM(caseParams.singleCoreM);
    tilingData.set_singleCoreN(caseParams.singleCoreN);
    tilingData.set_singleCoreK(caseParams.singleCoreK);
    tilingData.set_baseM(caseParams.baseM);
    tilingData.set_baseN(caseParams.baseN);
    tilingData.set_baseK(caseParams.baseK);
    tilingData.set_depthA1(caseParams.depthA1);
    tilingData.set_depthB1(caseParams.depthB1);
    tilingData.set_stepM(caseParams.stepM);
    tilingData.set_stepN(caseParams.stepN);
    tilingData.set_stepKa(caseParams.stepKa);
    tilingData.set_stepKb(caseParams.stepKb);
    tilingData.set_usedCoreNum(caseParams.blockDim);
    tilingData.set_iterateOrder(caseParams.iterateOrder);

    uint32_t tilingSize = tilingData.GetDataSize();
    tilingData.SaveToBuffer(tilingBuffer, tilingSize);
}
}
