/*
 * Copyright (c) 2025 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

#include "kernel_operator.h"
#include "../kernel_impl/matmul_mndb_custom_impl.h"

#if ASCENDC_CPU_DEBUG
#define SET_G_CORE_TYPE_IS_AIV int g_coreType = 2;
#define SET_G_CORE_TYPE_IS_AIC int g_coreType = 1;
#else
#define SET_G_CORE_TYPE_IS_AIV
#define SET_G_CORE_TYPE_IS_AIC
#endif

#if !defined(CUSTOM_MNDB_N) && !defined(CUSTOM_CONFIG_MDL)
constexpr int32_t MNDB_MODE = 1;
#elif CUSTOM_MNDB_N && !defined(CUSTOM_CONFIG_MDL)
constexpr int32_t MNDB_MODE = 2;
#elif !defined(CUSTOM_MNDB_N) && CUSTOM_CONFIG_MDL
constexpr int32_t MNDB_MODE = 3;
#else
constexpr int32_t MNDB_MODE = 4;
#endif


__aicore__ inline void CopyTiling(TCubeTiling* tiling, GM_ADDR tilingGM)
{
    uint32_t* ptr = reinterpret_cast<uint32_t*>(tiling);
    auto tiling32 = reinterpret_cast<__gm__ uint32_t*>(tilingGM);

    for (int i = 0; i < sizeof(TCubeTiling) / sizeof(uint32_t); i++, ptr++) {
      *ptr = *(tiling32 + i);
    }
    return;
}

extern "C" __global__ __aicore__ void matmul_mndb_custom(GM_ADDR a, GM_ADDR b, GM_ADDR bias, GM_ADDR c, GM_ADDR workspace, GM_ADDR tilingGm)
{
#if ASCENDC_CPU_DEBUG
    if (g_coreType == AscendC::AIV) {
        return;
    }
#endif
    TCubeTiling tiling;
    CopyTiling(&tiling, tilingGm);
    CustomMatmulMndb::MatmulMndbKernel<float, float, float, float, MNDB_MODE> matmulMndbKernel;
    AscendC::TPipe pipe;
    matmulMndbKernel.Init(a, b, bias, c, workspace, tiling);
    matmulMndbKernel.Process(&pipe);
}

#ifndef ASCENDC_CPU_DEBUG
void matmul_mndb_custom_do(uint32_t blockDim, void* stream, GM_ADDR a, GM_ADDR b,
                    GM_ADDR bias, GM_ADDR c, GM_ADDR workspace, GM_ADDR tilingGm)
{
    matmul_mndb_custom<<<blockDim, nullptr, stream>>>(a, b, bias, c, workspace, tilingGm);
}
#endif