<!--声明：本文使用[Creative Commons License version 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)许可协议，转载、引用或修改等操作请遵循此许可协议。-->
## 概述

本样例介绍了调用Matmul高阶API实现batchMatmul单算子，并按照不同的算子调用方式分别给出了对应的端到端实现。

- 直调：使用核函数直调batchMatmul自定义算子。

  核函数的基础调用方式，开发者完成算子核函数的开发和Tiling实现后，即可通过AscendCL运行时接口，完成算子的调用。

- 框架调用：使用框架调用batchMatmul自定义算子。

  按照工程创建->算子实现->编译部署->算子调用的流程完成算子开发。整个过程都依赖于算子工程：基于工程代码框架完成算子核函数的开发和Tiling实现，通过工程编译脚本完成算子的编译部署，继而实现单算子调用或第三方框架中的算子调用。

本样例中包含如下调用方式：

| 调用方式  | 目录                                                                                                                                                                                                                                                                                                  | <strong>描述</strong>                                      |
| --------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------- |
| 直调    | [kernel_launch_method_by_direct](./kernel_launch_method_by_direct)       | host侧的核函数调用程序，包含CPU侧和NPU侧两种运行验证方法。 |
| 框架调用 | [kernel_launch_method_by_framework](./kernel_launch_method_by_framework) | 通过aclnn调用的方式调用batchMatmul算子。                        |

## 样例支持的产品型号为：
- Atlas A2训练系列产品/Atlas 800I A2推理产品

## 目录结构
| 目录                  | 描述                   |
|---------------------|----------------------|
| [kernel_launch_method_by_direct](./kernel_launch_method_by_direct)       | 通过kernel直调的方式调用自定义算子工程样例目录 |
| [kernel_launch_method_by_framework](./kernel_launch_method_by_framework) | 通过aclnn调用的方式调用自定义算子工程样例目录 |
| [host_tiling](./host_tiling)       | 通过aclnn调用的方式调用自定义算子工程样例所需的host侧tiling实现 |
| [kernel_impl](./kernel_impl)       | 本样例kernel侧代码实现 |

## 算子描述
batchMatmul单算子，对输入的A B矩阵做矩阵乘和加bias偏置

batchMatmul算子规格
<table>
<tr><td rowspan="1" align="center">算子类型(OpType)</td><td colspan="6" align="center">BatchMatmulCustom</td></tr>
</tr>
<tr><td rowspan="4" align="center">算子输入</td><td align="center">name</td><td align="center">shape</td><td align="center">data type</td><td align="center">format</td><td align="center">isTrans</td><td align="center">layout</td></tr>
<tr><td align="center">a</td><td align="center">-</td><td align="center">float16</td><td align="center">ND</td><td align="center">false</td><td align="center">BSNGD</td></tr>
<tr><td align="center">b</td><td align="center">-</td><td align="center">float16</td><td align="center">ND</td><td align="center">true</td><td align="center">BSNGD</td></tr>
<tr><td align="center">bias</td><td align="center">-</td><td align="center">float</td><td align="center">ND</td><td align="center">-</td><td align="center">-</td></tr>
</tr>
</tr>
<tr><td rowspan="1" align="center">算子输出</td><td align="center">c</td><td align="center">-</td><td align="center">float</td><td align="center">ND</td><td align="center">-</td><td align="center">BSNGD</td></tr>
</tr>
<tr><td rowspan="1" align="center">核函数名</td><td colspan="6" align="center">batch_matmul_custom</td></tr>
</table>

## 算子实现介绍

Framework调用样例中实现的是固定shape为[M, N, K] = [192, 1536, 64], bias = [1536]的原始矩阵，A矩阵按照[B, S, N, G, D] = [2, 32, 1, 3, 64]输入, B矩阵按照[B, S, N, G, D] = [2, 256, 1, 3, 64]输入，bias矩阵按照[B, S, N, G, D] = [2, 1, 1, 3, 256]输入，C矩阵按照[B, S, N, G, D] = [2, 32, 1, 3, 256]输出，batchNum = 3的batchMatmul算子。

- kernel实现
  - 一次完成batchNum个Matmul矩阵乘法的运算。单次MatMul的计算公式为：C = A * B + Bias。
    - A、B为源操作数，A为左矩阵，形状为[M, K]；B为右矩阵，形状为[K, N]。
    - C为目的操作数，存放矩阵乘结果的矩阵，形状为[M, N]。
    - Bias为矩阵乘偏置，形状为[1, N]。对A*B结果矩阵的每一行都采用该bias进行偏置。
  - 实现MatMul矩阵乘运算的具体步骤如下：
    - 创建Matmul对象。
    - 初始化操作。
    - 设置左矩阵A 、右矩阵B、Bias。
    - 完成多batch矩阵乘操作。
    - 结束矩阵乘操作。

- tiling实现
  - Ascend C提供一组Matmul Tiling API，方便用户获取MatMul kernel计算时所需的 Tiling参数。只需要传入A/B/C矩阵等信息，调用API接口，即可获取到TCubeTiling结构体中的相关参数。
  - 获取Tiling参数的流程如下：
    - 创建一个Tiling对象。
    - 根据输入输出LayOut设置单核计算的A、B、C、Bias的参数类型信息；M、N、Ka、Kb形状信息等。
    - 调用SetALayout、SetBLayout、SetCLayout、SetBatchNum设置A/B/C的Layout轴信息和最大BatchNum数。
    - 调用GetTiling接口，获取Tiling信息。