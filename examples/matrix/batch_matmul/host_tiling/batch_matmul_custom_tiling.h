/**
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

#ifndef EXAMPLES_MATRIX_BATCH_MATMUL_CUSTOM_TILING_H
#define EXAMPLES_MATRIX_BATCH_MATMUL_CUSTOM_TILING_H
#include "register/tilingdata_base.h"
#include "tiling/tiling_api.h"

namespace optiling {
BEGIN_TILING_DATA_DEF(MatmulCustomTilingData)
  TILING_DATA_FIELD_DEF_STRUCT(TCubeTiling, cubeTilingData);
END_TILING_DATA_DEF;

REGISTER_TILING_DATA_CLASS(BatchMatmulCustom, MatmulCustomTilingData)
}

bool ComputeTiling(optiling::TCubeTiling& tiling, matmul_tiling::MultiCoreMatmulTiling* cubeTiling, bool isBias)
{
    int32_t M = 32;
    int32_t N = 256;
    int32_t K = 64;
    int32_t baseM = 32;
    int32_t baseN = 32;
    cubeTiling->SetDim(1);
    cubeTiling->SetAType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    cubeTiling->SetBType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16, true);
    cubeTiling->SetCType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    cubeTiling->SetBiasType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    cubeTiling->SetShape(M, N, K);
    cubeTiling->SetOrgShape(M, N, K);
    cubeTiling->SetFixSplit(baseM, baseN, -1);
    cubeTiling->EnableBias(isBias);
    cubeTiling->SetBufferSpace(-1, -1, -1);

    constexpr int32_t A_BNUM = 2;
    constexpr int32_t A_SNUM = 32;
    constexpr int32_t A_GNUM = 3;
    constexpr int32_t A_DNUM = 64;
    constexpr int32_t B_BNUM = 2;
    constexpr int32_t B_SNUM = 256;
    constexpr int32_t B_GNUM = 3;
    constexpr int32_t B_DNUM = 64;
    constexpr int32_t C_BNUM = 2;
    constexpr int32_t C_SNUM = 32;
    constexpr int32_t C_GNUM = 3;
    constexpr int32_t C_DNUM = 256;
    constexpr int32_t BATCH_NUM = 3;
    cubeTiling->SetALayout(A_BNUM, A_SNUM, 1, A_GNUM, A_DNUM);
    cubeTiling->SetBLayout(B_BNUM, B_SNUM, 1, B_GNUM, B_DNUM);
    cubeTiling->SetCLayout(C_BNUM, C_SNUM, 1, C_GNUM, C_DNUM);
    cubeTiling->SetBatchNum(BATCH_NUM);
    cubeTiling->SetBufferSpace(-1, -1, -1);
    if (cubeTiling->GetTiling(tiling) == -1) {
        return false;
    }
    return true;
}
#endif // EXAMPLES_MATRIX_BATCH_MATMUL_CUSTOM_TILING_H