#!/bin/bash
# Copyright (c) 2024 Huawei Technologies Co., Ltd.
# This file is a part of the CANN Open Software.
# Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
# Please refer to the License for details. You may not use this file except in compliance with the License.
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
# INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
# See LICENSE in the root of the software repository for the full text of the License.
# ======================================================================================================================

script_path=$(realpath $(dirname $0))

source $ASCEND_HOME_DIR/bin/setenv.bash
cp -rf ../host_tiling/* op_host/
rm -rf ./cmake/util
ln -s $ASCEND_HOME_DIR/tools/op_project_templates/ascendc/customize/cmake/util/ ./cmake/util
mkdir -p build_out
rm -rf build_out/*
cd build_out

cmake_version=$(cmake --version | grep "cmake version" | awk '{print $3}')
if [ "$cmake_version" \< "3.19.0" ] ; then
    opts=$(python3 $script_path/cmake/util/preset_parse.py $script_path/CMakePresets.json)
    echo $opts
    cmake .. $opts
else
    cmake .. --preset=default
fi
target=package
if [ "$1"x != ""x ]; then target=$1; fi

cmake --build . --target $target -j16
if [ $? -ne 0 ]; then exit 1; fi

if [ $target = "package" ]; then
  if test -d ./op_kernel/binary ; then
    ./cust*.run
    if [ $? -ne 0 ]; then exit 1; fi
    cmake --build . --target binary -j16
    if [ $? -ne 0 ]; then exit 1; fi
    cmake --build . --target $target -j16
  fi
fi

# for debug
# cd build_out
# make
# cpack
# verbose append -v
