<!--声明：本文使用[Creative Commons License version 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)许可协议，转载、引用或修改等操作请遵循此许可协议。-->

## 算子开发样例

<table>
    <td> 目录名称 </td>
    <td> 算子样例 </td>
    <td> 功能描述 </td>
    <tr>
        <th rowspan="6"><a href="./activation"> activation</th>
        <td><a href="./activation/fastergelu"> fastergelu</td>
        <td> 对输入tensor做FasterGelu计算。</td>
    </tr>
    <tr>
        <td><a href="./activation/simplesoftmax"> simplesoftmax</td>
        <td> 对输入tensor按行做softmax计算，计算过程中不包含按行reduce计算max和sum，所需的max和sum由外部输入。 </td>
    </tr>
    <tr>
        <td><a href="./activation/softmax"> softmax</td>
        <td> 对输入tensor按行做softmax计算，计算过程包含按行reduce计算max和sum，同时会输出max和sum。 </td>
    </tr>
    <tr>
        <td><a href="./activation/softmaxflash"> softmaxflash</td>
        <td> softmax增强版本，除了可以对输入tensor做softmaxflash计算，还可以根据上一次softmax计算的sum和max来更新本次softmax计算结果。 </td>
    </tr>
    <tr>
        <td><a href="./activation/softmaxgrad"> softmaxgrad</td>
        <td> 对输入tensor按行做如下公式的计算：zi = ∑(xi * yi)，其中∑为按行reduce求和。</td>
    </tr>
    <tr>
        <td><a href="./activation/softmaxgradfront"> softmaxgradfront</td>
        <td> 对输入tensor按行做如下公式的计算：zi = (xi - ∑(xi * yi)) * yi，其中∑为按行reduce求和。</td>
    </tr>
    <tr>
        <th rowspan="7"><a href="./matrix"> matrix</th>
        <td><a href="./matrix/basic_block_matmul"> basic_block_matmul</td>
        <td> 实现无尾块且tiling的base块大小固定的场景下的Matmul矩阵乘法，计算公式为：C = A * B + Bias。 </td>
    </tr>
    <tr>
        <td><a href="./matrix/batch_matmul"> batch_matmul</td>
        <td> 一次完成BatchNum个Matmul矩阵乘法，单次Matmul计算公式为：C = A * B + Bias。 </td>
    </tr>
    <tr>
        <td><a href="./matrix/matmul"> matmul</td>
        <td> 实现Matmul矩阵乘法，计算公式为：C = A * B + Bias。 </td>
    </tr>
    <tr>
        <td><a href="./matrix/matmul_async"> matmul_async</td>
        <td> 实现异步场景下的Matmul矩阵乘法，计算公式为：C = A * B + Bias。 </td>
    </tr>
    <tr>
        <td><a href="./matrix/matmul_ibshare"> matmul_ibshare</td>
        <td> 实现A矩阵或B矩阵GM地址相同，共享L1 Buffer场景下的Matmul矩阵乘法，计算公式为：C = A * B + Bias。 </td>
    </tr>
    <tr>
        <td><a href="./matrix/matmul_mndb"> matmul_mndb</td>
        <td> 实现M或N轴方向流水并行场景下的Matmul矩阵乘法，计算公式为：C = A * B + Bias。 </td>
    </tr>
    <tr>
        <td><a href="./matrix/matmul_preload"> matmul_preload</td>
        <td> 实现MDL模板使能M或N方向预加载功能场景下的Matmul矩阵乘法，计算公式为：C = A * B + Bias。 </td>
    </tr>
    <tr>
        <th rowspan="6"><a href="./normalization"> normalization</th>
        <td><a href="./normalization/layernorm"> layernorm</td>
        <td> 将输入数据收敛到[0, 1]之间，每个tensor的特征值减去该特征的均值然后除以该特征的标准差，实现数据归一化。 </td>
    </tr>
    <tr>
        <td><a href="./normalization/layernorm_grad"> layernorm_grad</td>
        <td> 计算layernorm的反向传播梯度。 </td>
    </tr>
    <tr>
        <td><a href="./normalization/layernorm_v2"> layernorm_v2</td>
        <td> 将shape为[A, R]的输入数据收敛到[0, 1]之间，计算输入数据的标准差的倒数rstd与归一化结果y。 </td>
    </tr>
    <tr>
        <td><a href="./normalization/normalize"> normalize </td>
        <td> 已知均值和方差，计算shape为[A, R]的输入数据的标准差倒数rstd和归一化结果y的方法。 </td>
    </tr>
    <tr>
        <td><a href="./normalization/welford_update"> welford_update </td>
        <td> Welford算法的前处理，一种在线计算均值和方差的方法。 </td>
    </tr>
    <tr>
        <td><a href="./normalization/welford_finalize"> welford_finalize </td>
        <td> Welford算法的后处理，一种在线计算均值和方差的方法。 </td>
    </tr>
    <tr>
        <th rowspan="1"><a href="./pad"> pad</th>
        <td><a href="./pad/broadcast"> broadcast</td>
        <td> 对输入tensor的shape进行广播。 </td>
    </tr>
    <tr>
        <th rowspan="2"><a href="./quantization"> quantization</th>
        <td><a href="./quantization/quant"> quant</td>
        <td> 对输入tensor按元素做量化计算，将float数据类型量化为int8_t数据类型。
 </td>
    </tr>
    <tr>
        <td><a href="./quantization/dequant"> dequant</td>
        <td> 对输入tensor按元素做反量化计算，将int32_t数据类型反量化为float数据类型。
 </td>
    </tr>
    <tr>
        <th rowspan="1"><a href="./sort"> sort</th>
        <td><a href="./sort/sort"> sort</td>
        <td> 对输入tensor做Sort计算，按照数值大小进行降序排序。 </td>
    </tr>
    <tr>
        <th rowspan="1"><a href="./utils"> utils</th>
        <td><a href="./utils/init_global_memory"> init_global_memory</td>
        <td> 将Global Memory上的数据初始化为指定值。 </td>
    </tr>
</table>






