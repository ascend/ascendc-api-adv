/**
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */
#include "broadcast_custom_tiling.h"
#include "register/op_def_registry.h"

namespace optiling {
static ge::graphStatus TilingFunc(gert::TilingContext *context)
{
    auto dt = context->GetInputTensor(0)->GetDataType();
    uint32_t dtypeSize;
    if (dt == ge::DT_FLOAT16) {
        dtypeSize = 2;
    } else if (dt == ge::DT_FLOAT) {
        dtypeSize = 4;
    } else {
        dtypeSize = 1;
    }
    const auto &srcStorageShape = context->GetInputShape(0)->GetStorageShape();
    const auto &outputStorageShape = context->GetOutputShape(0)->GetStorageShape();
    ge::Shape inputShape;
    ge::Shape outputShape;
    BroadcastTilingData tiling;
    const uint32_t dim = srcStorageShape.GetDimNum();
    if (dim == 1) {
        inputShape = ge::Shape({srcStorageShape.GetDim(0)});
        outputShape = ge::Shape({outputStorageShape.GetDim(0)});
    } else {
        inputShape = ge::Shape({srcStorageShape.GetDim(0), srcStorageShape.GetDim(1)});
        outputShape = ge::Shape({outputStorageShape.GetDim(0), outputStorageShape.GetDim(1)});
    }

    ComputeTiling(inputShape, outputShape, dtypeSize, tiling);

    context->SetBlockDim(1);
    context->SetTilingKey(1);
    tiling.SaveToBuffer(context->GetRawTilingData()->GetData(), context->GetRawTilingData()->GetCapacity());
    context->GetRawTilingData()->SetDataSize(tiling.GetDataSize());
    size_t *currentWorkSpace = context->GetWorkspaceSizes(1);
    currentWorkSpace[0] = 0;
    return ge::GRAPH_SUCCESS;
}
}  // namespace optiling

namespace ge {
static graphStatus InferShape(gert::InferShapeContext *context)
{
    return GRAPH_SUCCESS;
}
}  // namespace ge

namespace ops {
class BroadcastCustom : public OpDef {
public:
    BroadcastCustom(const char *name) : OpDef(name)
    {
        this->Input("x")
            .ParamType(REQUIRED)
            .DataType({ge::DT_FLOAT16, ge::DT_FLOAT, ge::DT_INT8, ge::DT_UINT8})
            .Format({ge::FORMAT_ND, ge::FORMAT_ND, ge::FORMAT_ND, ge::FORMAT_ND})
            .UnknownShapeFormat({ge::FORMAT_ND, ge::FORMAT_ND, ge::FORMAT_ND, ge::FORMAT_ND});
        this->Output("y")
            .ParamType(REQUIRED)
            .DataType({ge::DT_FLOAT16, ge::DT_FLOAT, ge::DT_INT8, ge::DT_UINT8})
            .Format({ge::FORMAT_ND, ge::FORMAT_ND, ge::FORMAT_ND, ge::FORMAT_ND})
            .UnknownShapeFormat({ge::FORMAT_ND, ge::FORMAT_ND, ge::FORMAT_ND, ge::FORMAT_ND});
        this->SetInferShape(ge::InferShape);
        this->AICore().SetTiling(optiling::TilingFunc);
        this->AICore().AddConfig("ascend910b");
        this->AICore().AddConfig("ascend310p");
    }
};

OP_ADD(BroadcastCustom);
}  // namespace ops