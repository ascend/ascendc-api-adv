/**
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */
#include "../kernel_impl/broadcast_custom.h"
struct BroadcastTilingData {
    uint32_t dim{0};
    uint32_t axis{0};
    uint32_t srcFirstDim{0};
    uint32_t srcLastDim{0};
    uint32_t dstFirstDim{0};
    uint32_t dstLastDim{0};
};

__aicore__ inline void CopyTiling(BroadcastTilingData *tiling, GM_ADDR tilingGM)
{
    uint32_t *ptr = reinterpret_cast<uint32_t *>(tiling);
    auto tiling32 = reinterpret_cast<__gm__ uint32_t *>(tilingGM);

    for (int i = 0; i < sizeof(BroadcastTilingData) / sizeof(uint32_t); i++, ptr++) {
        *ptr = *(tiling32 + i);
    }
    return;
}

extern "C" __global__ __aicore__ void broadcast_custom(GM_ADDR x, GM_ADDR y, GM_ADDR workspace, GM_ADDR tiling)
{
    BroadcastTilingData tilingData;
    CopyTiling(&tilingData, tiling);
    uint32_t axis = tilingData.axis;
    uint32_t dim = tilingData.dim;

    if (dim == 1) {
        const uint32_t srcShape[] = {tilingData.srcFirstDim};
        const uint32_t dstShape[] = {tilingData.dstFirstDim};
        KernelBroadcastCustom<float, 1, 0> op;
        op.Init(x, y, tilingData.srcFirstDim, tilingData.dstFirstDim, srcShape, dstShape);
        op.Process();
    } else {
        const uint32_t srcShape[] = {tilingData.srcFirstDim, tilingData.srcLastDim};
        const uint32_t dstShape[] = {tilingData.dstFirstDim, tilingData.dstLastDim};

        if (axis == 0) {
            KernelBroadcastCustom<float, 2, 0> op;
            op.Init(x,
                y,
                tilingData.srcFirstDim * tilingData.srcLastDim,
                tilingData.dstFirstDim * tilingData.dstLastDim,
                srcShape,
                dstShape);
            op.Process();
        } else {
            KernelBroadcastCustom<float, 2, 1> op;
            op.Init(x,
                y,
                tilingData.srcFirstDim * tilingData.srcLastDim,
                tilingData.dstFirstDim * tilingData.dstLastDim,
                srcShape,
                dstShape);
            op.Process();
        }
    }
}

#ifndef ASCENDC_CPU_DEBUG
// call of kernel function
void broadcast_custom_do(
    uint32_t blockDim, void *l2ctrl, void *stream, uint8_t *x, uint8_t *y, uint8_t *workspace, uint8_t *tiling)
{
    broadcast_custom<<<blockDim, l2ctrl, stream>>>(x, y, workspace, tiling);
}
#endif