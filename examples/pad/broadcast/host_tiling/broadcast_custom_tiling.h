/**
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */
#ifndef EXAMPLES_PAD_BROADCAST_CUSTOM_TILING_H
#define EXAMPLES_PAD_BROADCAST_CUSTOM_TILING_H
#include "graph/tensor.h"
#include "register/tilingdata_base.h"

namespace optiling {
BEGIN_TILING_DATA_DEF(BroadcastTilingData)
    TILING_DATA_FIELD_DEF(uint32_t, dim);
    TILING_DATA_FIELD_DEF(uint32_t, axis);
    TILING_DATA_FIELD_DEF(uint32_t, srcFirstDim);
    TILING_DATA_FIELD_DEF(uint32_t, srcLastDim);
    TILING_DATA_FIELD_DEF(uint32_t, dstFirstDim);
    TILING_DATA_FIELD_DEF(uint32_t, dstLastDim);
END_TILING_DATA_DEF;

REGISTER_TILING_DATA_CLASS(BroadcastCustom, BroadcastTilingData)
}

void ComputeTiling(const ge::Shape &inputShape, const ge::Shape &outputShape, uint32_t dtypeSize,
    optiling::BroadcastTilingData &tiling)
{
    int32_t axis = 0;
    const uint32_t dim = inputShape.GetDimNum();
    if (dim == 1) {
        tiling.set_srcFirstDim(inputShape.GetDim(0));
        tiling.set_srcLastDim(1);
        tiling.set_dstFirstDim(outputShape.GetDim(0));
        tiling.set_dstLastDim(1);
    } else {
        tiling.set_srcFirstDim(inputShape.GetDim(0));
        tiling.set_srcLastDim(inputShape.GetDim(1));
        tiling.set_dstFirstDim(outputShape.GetDim(0));
        tiling.set_dstLastDim(outputShape.GetDim(1));
        if (inputShape.GetDim(1) == 1) {
            axis = 1;
        }
    }

    tiling.set_axis(axis);
    tiling.set_dim(dim);
    return;
}

#endif // EXAMPLES_PAD_BROADCAST_CUSTOM_TILING_H