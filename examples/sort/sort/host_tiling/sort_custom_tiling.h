/**
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

#ifndef EXAMPLES_SORT_SORT_CUSTOM_TILING_H
#define EXAMPLES_SORT_SORT_CUSTOM_TILING_H
#include "register/tilingdata_base.h"
#include "tiling/tiling_api.h"
#include "tiling/platform/platform_ascendc.h"

namespace optiling {
BEGIN_TILING_DATA_DEF(SortCustomTilingData)
    TILING_DATA_FIELD_DEF(uint32_t, elementCount);
END_TILING_DATA_DEF;
REGISTER_TILING_DATA_CLASS(SortCustom, SortCustomTilingData)
} // namespace optiling

void ComputeTiling(uint32_t elementCount, optiling::SortCustomTilingData &tiling)
{
    tiling.set_elementCount(elementCount);
}

#endif // EXAMPLES_SORT_SORT_CUSTOM_TILING_H
