/**
 * Copyright (c) 2025 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

#ifndef EXAMPLES_INIT_GLOBAL_MEMORY_INIT_GLOBAL_MEMORY_CUSTOM_TILING_H
#define EXAMPLES_INIT_GLOBAL_MEMORY_INIT_GLOBAL_MEMORY_CUSTOM_TILING_H
#include "register/tilingdata_base.h"
#include "tiling/tiling_api.h"
#include "tiling/platform/platform_ascendc.h"


namespace optiling {
BEGIN_TILING_DATA_DEF(InitGlobalMemoryCustomTilingData)
TILING_DATA_FIELD_DEF(uint32_t, k);
END_TILING_DATA_DEF;
REGISTER_TILING_DATA_CLASS(InitGlobalMemoryCustom, InitGlobalMemoryCustomTilingData)
} // namespace optiling

#endif // EXAMPLES_INIT_GLOBAL_MEMORY_INIT_GLOBAL_MEMORY_CUSTOM_TILING_H
