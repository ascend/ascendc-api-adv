/**
 * Copyright (c) 2025 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

#include "../../../common/data_utils.h"
#ifndef ASCENDC_CPU_DEBUG
#include "acl/acl.h"
extern void init_global_memory_custom_do(uint32_t blockDim, void *l2ctrl, void *stream, uint8_t *x, uint8_t *y,
    uint8_t *z, uint8_t *workspace, uint8_t *tiling);
#else
#include "tikicpulib.h"
extern "C" __global__ __aicore__ void init_global_memory_custom(GM_ADDR x, GM_ADDR y, GM_ADDR z, uint8_t *workspace,
    uint8_t *tiling);
#endif

constexpr uint8_t BLOCK_DIM = 1;
constexpr uint32_t INIT_SIZE = 256;
constexpr uint32_t TILINGDATA_SIZE = 1;
constexpr uint32_t WORKSPACE_SIZE = 1;

// extern uint8_t *GenerateTiling(uint32_t k, uint32_t outter, uint32_t inner, uint32_t n, bool isLargest,
//     const char *socVersion);

static bool CompareResult(const void *outputData, int64_t outSize, std::string goldenName)
{
    void *goldenData;
#ifdef ASCENDC_CPU_DEBUG
    goldenData = (uint8_t *)AscendC::GmAlloc(outSize);
#else
    CHECK_ACL(aclrtMallocHost((void **)(&goldenData), outSize));
#endif
    size_t goldenSize = outSize;
    bool ret = ReadFile("../output/golden_" + goldenName + ".bin", goldenSize, goldenData, goldenSize);
    if (ret) {
        printf("ReadFile golden_%s.bin success!\n", goldenName.c_str());
    } else {
        printf("test failed!\n");
        return false;
    }
    constexpr float EPS = 1e-4;
    int64_t wrongNum = 0;

    for (int i = 0; i < outSize / sizeof(float); i++) {
        float a = (reinterpret_cast<const float *>(outputData))[i];
        float b = (reinterpret_cast<const float *>(goldenData))[i];
        float ae = std::abs(a - b);
        float re = ae / abs(b);
        if (ae > EPS && re > EPS) {
            printf("CompareResult golden_%s.bin failed output is %lf, golden is %lf\n", goldenName.c_str(), a, b);
            wrongNum++;
        }
    }
#ifdef ASCENDC_CPU_DEBUG
    AscendC::GmFree((void *)goldenData);
#else
    CHECK_ACL(aclrtFreeHost(goldenData));
#endif
    if (wrongNum != 0) {
        return false;
    } else {
        printf("CompareResult golden_%s.bin success!\n", goldenName.c_str());
        return true;
    }
}

int32_t main(int32_t argc, char *argv[])
{
    uint32_t blockDim = BLOCK_DIM;
    size_t inputSize_x = INIT_SIZE * sizeof(float);
    size_t inputSize_y = INIT_SIZE * sizeof(float);
    size_t outputSize_z = INIT_SIZE * sizeof(float);

    size_t workspaceSize = WORKSPACE_SIZE;
    size_t tilingFileSize = TILINGDATA_SIZE;

#ifdef ASCENDC_CPU_DEBUG
    const char *socVersion = SOC_VERSION;
    uint8_t *input_x = (uint8_t *)AscendC::GmAlloc(inputSize_x);
    uint8_t *input_y = (uint8_t *)AscendC::GmAlloc(inputSize_y);
    uint8_t *output_z = (uint8_t *)AscendC::GmAlloc(outputSize_z);

    uint8_t *workspace = (uint8_t *)AscendC::GmAlloc(workspaceSize);
    uint8_t *tiling = (uint8_t *)AscendC::GmAlloc(tilingFileSize);

    ReadFile("../input/input_x.bin", inputSize_x, input_x, inputSize_x);
    ReadFile("../input/input_y.bin", inputSize_y, input_y, inputSize_y);

    AscendC::SetKernelMode(KernelMode::AIV_MODE);
    ICPU_RUN_KF(init_global_memory_custom, blockDim, input_x, input_y, output_z, workspace, tiling);

    WriteFile("../output/output_z.bin", output_z, outputSize_z);

    bool goldenResult = true;
    goldenResult &= CompareResult(output_z, outputSize_z, "z");

    if (goldenResult) {
        printf("test pass!\n");
    } else {
        printf("test failed!\n");
    }

    AscendC::GmFree((void *)input_x);
    AscendC::GmFree((void *)input_y);
    AscendC::GmFree((void *)output_z);
    AscendC::GmFree((void *)workspace);
    AscendC::GmFree((void *)tiling);

#else
    const char *socVersion = nullptr;
    CHECK_ACL(aclInit(nullptr));
    aclrtContext context;
    int32_t deviceId = 0;
    CHECK_ACL(aclrtSetDevice(deviceId));
    CHECK_ACL(aclrtCreateContext(&context, deviceId));
    aclrtStream stream = nullptr;
    CHECK_ACL(aclrtCreateStream(&stream));

    uint8_t *inputXHost, *inputYHost, *outputZHost;
    uint8_t *inputXDevice, *inputYDevice, *outputZDevice, *workspaceDevice, *tilingDevice;
    
    CHECK_ACL(aclrtMallocHost((void **)(&inputXHost), inputSize_x));
    CHECK_ACL(aclrtMallocHost((void **)(&inputYHost), inputSize_y));
    CHECK_ACL(aclrtMallocHost((void **)(&outputZHost), outputSize_z));

    CHECK_ACL(aclrtMalloc((void **)&inputXDevice, inputSize_x, ACL_MEM_MALLOC_HUGE_FIRST));
    CHECK_ACL(aclrtMalloc((void **)&inputYDevice, inputSize_y, ACL_MEM_MALLOC_HUGE_FIRST));
    CHECK_ACL(aclrtMalloc((void **)&outputZDevice, outputSize_z, ACL_MEM_MALLOC_HUGE_FIRST));
    CHECK_ACL(aclrtMalloc((void **)&workspaceDevice, workspaceSize, ACL_MEM_MALLOC_HUGE_FIRST));
    CHECK_ACL(aclrtMalloc((void **)&tilingDevice, tilingFileSize, ACL_MEM_MALLOC_HUGE_FIRST));

    ReadFile("../input/input_x.bin", inputSize_x, inputXHost, inputSize_x);
    ReadFile("../input/input_y.bin", inputSize_y, inputYHost, inputSize_y);

    CHECK_ACL(aclrtMemcpy(inputXDevice, inputSize_x, inputXHost, inputSize_x, ACL_MEMCPY_HOST_TO_DEVICE));
    CHECK_ACL(aclrtMemcpy(inputYDevice, inputSize_y, inputYHost, inputSize_y, ACL_MEMCPY_HOST_TO_DEVICE));

    init_global_memory_custom_do(blockDim, nullptr, stream, inputXDevice, inputYDevice, outputZDevice,
        workspaceDevice, tilingDevice);

    CHECK_ACL(aclrtSynchronizeStream(stream));
    
    CHECK_ACL(aclrtMemcpy(outputZHost, outputSize_z, outputZDevice, outputSize_z, ACL_MEMCPY_DEVICE_TO_HOST));

    WriteFile("../output/output_z.bin", outputZHost, outputSize_z);

    bool goldenResult = true;
    goldenResult &= CompareResult(outputZHost, outputSize_z, "z");
    if (goldenResult) {
        printf("test pass!\n");
    } else {
        printf("test failed!\n");
    }

    CHECK_ACL(aclrtFree(inputXDevice));
    CHECK_ACL(aclrtFree(inputYDevice));
    CHECK_ACL(aclrtFree(outputZDevice));
    CHECK_ACL(aclrtFree(workspaceDevice));
    CHECK_ACL(aclrtFree(tilingDevice));

    CHECK_ACL(aclrtFreeHost(inputXHost));
    CHECK_ACL(aclrtFreeHost(inputYHost));
    CHECK_ACL(aclrtFreeHost(outputZHost));

    CHECK_ACL(aclrtDestroyStream(stream));
    CHECK_ACL(aclrtDestroyContext(context));
    CHECK_ACL(aclrtResetDevice(deviceId));
    CHECK_ACL(aclFinalize());
#endif
    return 0;
}
