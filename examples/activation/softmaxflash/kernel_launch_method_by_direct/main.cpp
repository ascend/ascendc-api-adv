/**
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

#include "../../../common/data_utils.h"
#ifndef ASCENDC_CPU_DEBUG
#include "acl/acl.h"
extern void softmaxflash_custom_do(uint32_t coreDim, void* l2ctrl, void* stream, uint8_t* x, uint8_t* max, uint8_t* sum,
    uint8_t* workspace, uint8_t* tiling);
#else
#include "tikicpulib.h"
extern "C" __global__ __aicore__ void softmaxflash_custom(GM_ADDR x, GM_ADDR max, GM_ADDR sum, GM_ADDR workspace,
    GM_ADDR tiling);
#endif

constexpr uint32_t ROW_NUM = 960;
constexpr uint32_t COLUMN_NUM = 960;
constexpr uint32_t USED_CORE_NUM = 40;
constexpr uint32_t WORKSPACE_SIZE = 1024;
constexpr uint32_t TILINGDATA_SIZE = 32; // Element count of struct SoftmaxflashCustomTilingData
constexpr uint32_t FLOAT_NUM_PER_BLOCK = 8;

extern void GenerateTiling(const uint32_t m, const uint32_t k, const uint32_t coreNum, const uint32_t tilingSize,
                           uint8_t* tilingData);

static int64_t CompareResult(void* outputData, const int64_t outSize)
{
    void* goldenData;
#ifdef ASCENDC_CPU_DEBUG
    goldenData = (uint8_t*)AscendC::GmAlloc(outSize);
#else
    CHECK_ACL(aclrtMallocHost((void**)(&goldenData), outSize));
#endif
    size_t goldenSize = outSize;
    bool ret = ReadFile("../output/golden_sum.bin", goldenSize, goldenData, goldenSize);
    if (ret) {
        printf("ReadFile golden success!\n");
    } else {
#ifdef ASCENDC_CPU_DEBUG
        AscendC::GmFree((void*)goldenData);
#else
        CHECK_ACL(aclrtFreeHost(goldenData));
#endif
        return -1;
    }
    constexpr float EPS = 1e-5;
    int64_t wrongNum = 0;

    for (int i = 0; i < outSize / sizeof(float); i++) {
        float a = ((float*)outputData)[i];
        float b = ((float*)goldenData)[i];
        float ae = std::abs(a - b);
        float re = ae / abs(b);
        if (ae > EPS && re > EPS) {
            printf("CompareResult failed output is %lf, golden is %lf\n", a, b);
            wrongNum++;
        }
    }
#ifdef ASCENDC_CPU_DEBUG
    AscendC::GmFree((void*)goldenData);
#else
    CHECK_ACL(aclrtFreeHost(goldenData));
#endif
    return wrongNum;
}

int32_t main(int32_t argc, char* argv[])
{
    size_t inputSize = ROW_NUM * ROW_NUM * sizeof(float);
    size_t workspaceSize = WORKSPACE_SIZE * sizeof(float);
    size_t tilingSize = TILINGDATA_SIZE * sizeof(uint32_t);
    size_t outputMaxSize = ROW_NUM * FLOAT_NUM_PER_BLOCK * sizeof(float);
    size_t outputSumSize = ROW_NUM * FLOAT_NUM_PER_BLOCK * sizeof(float);
    int64_t wrongNum = -1;
#ifdef ASCENDC_CPU_DEBUG
    uint8_t* x = (uint8_t*)AscendC::GmAlloc(inputSize);
    uint8_t* max = (uint8_t*)AscendC::GmAlloc(outputMaxSize);
    uint8_t* sum = (uint8_t*)AscendC::GmAlloc(outputSumSize);
    uint8_t* workspace = (uint8_t*)AscendC::GmAlloc(workspaceSize);
    uint8_t* tiling = (uint8_t*)AscendC::GmAlloc(tilingSize);

    ReadFile("../input/input_x.bin", inputSize, x, inputSize);
    ReadFile("../input/workspace.bin", workspaceSize, workspace, workspaceSize);

    GenerateTiling(ROW_NUM, COLUMN_NUM, USED_CORE_NUM, tilingSize, tiling);

    AscendC::SetKernelMode(KernelMode::AIV_MODE);  // run in aiv mode
    ICPU_RUN_KF(softmaxflash_custom, USED_CORE_NUM, x, max, sum, workspace, tiling); // use this macro for cpu debug

    WriteFile("../output/output_max.bin", max, outputMaxSize);
    WriteFile("../output/output_sum.bin", sum, outputSumSize);

    wrongNum = CompareResult(sum, outputSumSize);

    AscendC::GmFree((void*)x);
    AscendC::GmFree((void*)max);
    AscendC::GmFree((void*)sum);
    AscendC::GmFree((void*)workspace);
    AscendC::GmFree((void*)tiling);
#else
    // Initialize resources
    CHECK_ACL(aclInit(nullptr));
    aclrtContext context;
    int32_t deviceId = 0;
    CHECK_ACL(aclrtSetDevice(deviceId));
    CHECK_ACL(aclrtCreateContext(&context, deviceId));
    aclrtStream stream = nullptr;
    CHECK_ACL(aclrtCreateStream(&stream));

    uint8_t *xHost, *maxHost, *sumHost, *workspaceHost, *tilingHost;
    uint8_t *xDevice, *maxDevice, *sumDevice, *workspaceDevice, *tilingDevice;

    // Allocate host memory and device memory
    CHECK_ACL(aclrtMallocHost((void**)(&xHost), inputSize));
    CHECK_ACL(aclrtMallocHost((void**)(&maxHost), outputMaxSize));
    CHECK_ACL(aclrtMallocHost((void**)(&sumHost), outputSumSize));
    CHECK_ACL(aclrtMallocHost((void**)(&workspaceHost), workspaceSize));
    CHECK_ACL(aclrtMallocHost((void**)(&tilingHost), tilingSize));
    CHECK_ACL(aclrtMalloc((void**)&xDevice, inputSize, ACL_MEM_MALLOC_HUGE_FIRST));
    CHECK_ACL(aclrtMalloc((void**)&maxDevice, outputMaxSize, ACL_MEM_MALLOC_HUGE_FIRST));
    CHECK_ACL(aclrtMalloc((void**)&sumDevice, outputSumSize, ACL_MEM_MALLOC_HUGE_FIRST));
    CHECK_ACL(aclrtMalloc((void**)&workspaceDevice, workspaceSize, ACL_MEM_MALLOC_HUGE_FIRST));
    CHECK_ACL(aclrtMalloc((void**)&tilingDevice, tilingSize, ACL_MEM_MALLOC_HUGE_FIRST));

    ReadFile("../input/input_x.bin", inputSize, xHost, inputSize);
    ReadFile("../input/workspace.bin", workspaceSize, workspaceHost, workspaceSize);

    GenerateTiling(ROW_NUM, COLUMN_NUM, USED_CORE_NUM, tilingSize, tilingHost);

    // Copy host memory to device memory
    CHECK_ACL(aclrtMemcpy(xDevice, inputSize, xHost, inputSize, ACL_MEMCPY_HOST_TO_DEVICE));
    CHECK_ACL(aclrtMemcpy(workspaceDevice, workspaceSize, workspaceHost, workspaceSize, ACL_MEMCPY_HOST_TO_DEVICE));
    CHECK_ACL(aclrtMemcpy(tilingDevice, tilingSize, tilingHost, tilingSize, ACL_MEMCPY_HOST_TO_DEVICE));

    // Execute the kernel
    softmaxflash_custom_do(USED_CORE_NUM, nullptr, stream, xDevice, maxDevice, sumDevice, workspaceDevice,
                           tilingDevice);

    // Wait for the stop event to complete
    CHECK_ACL(aclrtSynchronizeStream(stream));

    // Copy result to host memory and write to output file
    CHECK_ACL(aclrtMemcpy(maxHost, outputMaxSize, maxDevice, outputMaxSize, ACL_MEMCPY_DEVICE_TO_HOST));
    CHECK_ACL(aclrtMemcpy(sumHost, outputSumSize, sumDevice, outputSumSize, ACL_MEMCPY_DEVICE_TO_HOST));
    WriteFile("../output/output_max.bin", maxHost, outputMaxSize);
    WriteFile("../output/output_sum.bin", sumHost, outputSumSize);

    // Compare the result with the golden result
    wrongNum = CompareResult(sumHost, outputSumSize); // softmaxflash just need to compare sum

    // Clean up memory
    CHECK_ACL(aclrtFree(xDevice));
    CHECK_ACL(aclrtFree(maxDevice));
    CHECK_ACL(aclrtFree(sumDevice));
    CHECK_ACL(aclrtFree(workspaceDevice));
    CHECK_ACL(aclrtFree(tilingDevice));
    CHECK_ACL(aclrtFreeHost(xHost));
    CHECK_ACL(aclrtFreeHost(maxHost));
    CHECK_ACL(aclrtFreeHost(sumHost));
    CHECK_ACL(aclrtFreeHost(workspaceHost));
    CHECK_ACL(aclrtFreeHost(tilingHost));

    CHECK_ACL(aclrtDestroyStream(stream));
    CHECK_ACL(aclrtDestroyContext(context));
    CHECK_ACL(aclrtResetDevice(deviceId));
    CHECK_ACL(aclFinalize());
#endif
    if (wrongNum != 0) {
        printf("test failed!\n");
    } else {
        printf("test pass!\n");
    }
    return 0;
}
