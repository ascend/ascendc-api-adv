/**
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

#ifndef EXAMPLES_ACTIVATION_SOFTMAXFLASH_CUSTOM_TILING_H
#define EXAMPLES_ACTIVATION_SOFTMAXFLASH_CUSTOM_TILING_H
#include "register/tilingdata_base.h"
#include "tiling/tiling_api.h"
namespace optiling {
BEGIN_TILING_DATA_DEF(SoftmaxflashCustomTilingData)
  TILING_DATA_FIELD_DEF(uint32_t, columnLength);
  TILING_DATA_FIELD_DEF(uint32_t, rowLength);
  TILING_DATA_FIELD_DEF(uint32_t, sharedTmpBufferSize);
  TILING_DATA_FIELD_DEF(uint32_t, usedBlockDim);
  TILING_DATA_FIELD_DEF(uint32_t, coreRowNum);
  TILING_DATA_FIELD_DEF(uint32_t, tailCoreRowNum);
  TILING_DATA_FIELD_DEF(uint32_t, singleLoopCoreRowNum);
  TILING_DATA_FIELD_DEF(uint32_t, singleCoreLoopCount);
  TILING_DATA_FIELD_DEF(uint32_t, singleCoreLoopTail);
  TILING_DATA_FIELD_DEF(uint32_t, tailCoreSingleLoopCoreRowNum);
  TILING_DATA_FIELD_DEF(uint32_t, tailCoreSingleCoreLoopCount);
  TILING_DATA_FIELD_DEF(uint32_t, tailCoreSingleCoreLoopTail);
  TILING_DATA_FIELD_DEF(uint32_t, splitK);
  TILING_DATA_FIELD_DEF(uint32_t, loopK);
  TILING_DATA_FIELD_DEF(uint32_t, tailK);
  TILING_DATA_FIELD_DEF(uint32_t, reserved); // for alignment
  TILING_DATA_FIELD_DEF_STRUCT(SoftMaxTiling, softmaxTilingData);
END_TILING_DATA_DEF;

REGISTER_TILING_DATA_CLASS(SoftmaxflashCustom, SoftmaxflashCustomTilingData)
}
namespace SoftmaxflashCustomTiling {
constexpr uint32_t SHARED_TMP_BUFFER_SIZE = 61440; // reserved tmpbuffer 60K for softmax compute

struct SingleCoreLoopParam {
    uint32_t singleLoopCoreRowNum{ 0 };  // row num processed in single loop
    uint32_t singleCoreLoopCount{ 0 };   // loop count in single loop
    uint32_t singleCoreLoopTail{ 0 };    // row num of last loop in single core
    uint32_t splitK{ 0 };                // single split num in k direction
    uint32_t loopK{ 0 };                 // loop count in k direction
    uint32_t tailK{ 0 };                 // tail num in k direction
};

SingleCoreLoopParam GetSingleCoreLoopParam(const uint32_t colNum, const uint32_t coreRowNum)
{
    //  Determine the params of single core based on the reduce axis length
    SingleCoreLoopParam singleCoreLoopParam;
    if (colNum >= 1024) {  // reduce axis length >= 1024, set slice factor to 8
        singleCoreLoopParam.splitK = 1024;
        singleCoreLoopParam.loopK = colNum / 1024;
        singleCoreLoopParam.tailK = colNum % 1024;
        singleCoreLoopParam.singleLoopCoreRowNum = 8;
        singleCoreLoopParam.singleCoreLoopCount = coreRowNum / 8;
        singleCoreLoopParam.singleCoreLoopTail = coreRowNum % 8;
    } else if (colNum >= 512) {  // reduce axis length < 1024 && >= 512, set slice factor to 16
        singleCoreLoopParam.splitK = 512;
        singleCoreLoopParam.loopK = colNum / 512;
        singleCoreLoopParam.tailK = colNum % 512;
        singleCoreLoopParam.singleLoopCoreRowNum = 16;
        singleCoreLoopParam.singleCoreLoopCount = coreRowNum / 16;
        singleCoreLoopParam.singleCoreLoopTail = coreRowNum % 16;
    } else if (colNum >= 128) {  // reduce axis length < 512 && >= 128, set slice factor to 32
        singleCoreLoopParam.splitK = 128;
        singleCoreLoopParam.loopK = colNum / 128;
        singleCoreLoopParam.tailK = colNum % 128;
        singleCoreLoopParam.singleLoopCoreRowNum = 32;
        singleCoreLoopParam.singleCoreLoopCount = coreRowNum / 32;
        singleCoreLoopParam.singleCoreLoopTail = coreRowNum % 32;
    } else {  // reduce axis length < 128 && >= 0, set slice factor to 64
        singleCoreLoopParam.splitK = colNum;
        singleCoreLoopParam.loopK = 1;
        singleCoreLoopParam.tailK = 0;
        singleCoreLoopParam.singleLoopCoreRowNum = 64;
        singleCoreLoopParam.singleCoreLoopCount = coreRowNum / 64;
        singleCoreLoopParam.singleCoreLoopTail = coreRowNum % 64;
    }
    return singleCoreLoopParam;
}

void ComputeTiling(const uint32_t rowNum, const uint32_t colNum, const uint32_t coreNum,
    optiling::SoftmaxflashCustomTilingData& tiling)
{
    uint32_t localworkspaceSize = SHARED_TMP_BUFFER_SIZE;

    auto alignedRowNum = (rowNum + coreNum - 1) / coreNum * coreNum;
    auto coreRowNum = alignedRowNum / coreNum;  // each core equal distribution
    auto tailCoreRowNum = rowNum % coreRowNum;  // last core process the tail rownum
    auto usedBlockDim = rowNum / coreRowNum;    // the core num used actually

    SingleCoreLoopParam mainCoreLoopParam = GetSingleCoreLoopParam(colNum, coreRowNum);
    SingleCoreLoopParam tailCoreLoopParam;
    if (usedBlockDim == coreNum && tailCoreRowNum == 0) {
        tailCoreLoopParam = GetSingleCoreLoopParam(colNum, coreRowNum);
    } else {
        tailCoreLoopParam = GetSingleCoreLoopParam(colNum, tailCoreRowNum);
    }

    ge::Shape softmaxComputeShape({ mainCoreLoopParam.singleLoopCoreRowNum, mainCoreLoopParam.splitK });
    uint32_t apiNeedMinTmpSize = 0;
    if (mainCoreLoopParam.singleLoopCoreRowNum % 8 == 0 && mainCoreLoopParam.splitK % 64 == 0) { // enable basicBlock
        apiNeedMinTmpSize =
            AscendC::GetSoftMaxFlashV2MinTmpSize(softmaxComputeShape, sizeof(float), sizeof(float), true, true);
    } else {
        apiNeedMinTmpSize =
            AscendC::GetSoftMaxFlashV2MinTmpSize(softmaxComputeShape, sizeof(float), sizeof(float), true, false);
    }
    if (apiNeedMinTmpSize > SHARED_TMP_BUFFER_SIZE) {
        localworkspaceSize = apiNeedMinTmpSize;
    } else {
        localworkspaceSize = SHARED_TMP_BUFFER_SIZE;
    }
    // get SoftMax Tiling
    if (mainCoreLoopParam.singleLoopCoreRowNum % 8 == 0 && mainCoreLoopParam.splitK % 64 == 0) { // enable basicBlock
        AscendC::SoftMaxFlashV2TilingFunc(softmaxComputeShape, sizeof(float), sizeof(float), localworkspaceSize,
            tiling.softmaxTilingData, true, true);
    } else {
        AscendC::SoftMaxFlashV2TilingFunc(softmaxComputeShape, sizeof(float), sizeof(float), localworkspaceSize,
            tiling.softmaxTilingData, true, false);
    }

    tiling.set_columnLength(colNum);
    tiling.set_rowLength(rowNum);
    tiling.set_sharedTmpBufferSize(localworkspaceSize);
    tiling.set_usedBlockDim(usedBlockDim);
    tiling.set_coreRowNum(coreRowNum);
    tiling.set_tailCoreRowNum(tailCoreRowNum);
    tiling.set_splitK(mainCoreLoopParam.splitK);
    tiling.set_loopK(mainCoreLoopParam.loopK);
    tiling.set_tailK(mainCoreLoopParam.tailK);

    tiling.set_singleLoopCoreRowNum(mainCoreLoopParam.singleLoopCoreRowNum);
    tiling.set_singleCoreLoopCount(mainCoreLoopParam.singleCoreLoopCount);
    tiling.set_singleCoreLoopTail(mainCoreLoopParam.singleCoreLoopTail);
    tiling.set_tailCoreSingleLoopCoreRowNum(tailCoreLoopParam.singleLoopCoreRowNum);
    tiling.set_tailCoreSingleCoreLoopCount(tailCoreLoopParam.singleCoreLoopCount);
    tiling.set_tailCoreSingleCoreLoopTail(tailCoreLoopParam.singleCoreLoopTail);
    tiling.set_reserved(0);
}
}
#endif // EXAMPLES_ACTIVATION_SOFTMAXFLASH_CUSTOM_TILING_H