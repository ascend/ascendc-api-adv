#!/usr/bin/python3
# coding=utf-8

# Copyright (c) 2024 Huawei Technologies Co., Ltd.
# This file is a part of the CANN Open Software.
# Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
# Please refer to the License for details. You may not use this file except in compliance with the License.
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
# INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
# See LICENSE in the root of the software repository for the full text of the License.
# ======================================================================================================================

import numpy as np
import os

def softmax_grad_float(grad, src, isFront = None):
    muls_r = grad * src
    muls_r = muls_r.sum(axis=-1, keepdims=True)
    if isFront :
        return muls_r
    sub_r = grad - muls_r
    res = sub_r * src
    return res

def gen_golden_data_simple():
    x_shape = (960, 960)
    x = np.random.uniform(-1, 1, x_shape).astype(np.float32)
    y = np.random.uniform(-1, 1, x_shape).astype(np.float32)

    golden = softmax_grad_float(x, y)

    os.system("mkdir -p input")
    os.system("mkdir -p output")
    x.tofile("./input/input_x.bin")
    y.tofile("./input/input_y.bin")
    golden.tofile("./output/golden.bin")

if __name__ == "__main__":
    gen_golden_data_simple()

