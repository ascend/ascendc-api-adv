/**
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

#ifndef EXAMPLES_QUANTIZATION_DEQUANT_CUSTOM_H
#define EXAMPLES_QUANTIZATION_DEQUANT_CUSTOM_H
#include "kernel_operator.h"

#define INFO_LOG(fmt, args...) fprintf(stdout, "[INFO]  " fmt "\n", ##args)

namespace MyCustomKernel {
struct VecTiling{
    uint32_t m;
    uint32_t n;
    uint32_t calCount;
    uint32_t sharedTmpBufferSize;
};

template <typename dstT, typename scaleT> class KernelDequant {
public:
    __aicore__ inline KernelDequant() {}
    __aicore__ inline void Init(GM_ADDR srcGm, GM_ADDR dstGm, GM_ADDR deqScaleGm,  uint32_t m, uint32_t n, uint32_t calCount)
    {
        rowLen = m;
        colLen = n;
        dataSize = m*n;
        scaleSize = calCount;

        srcGlobal.SetGlobalBuffer(reinterpret_cast<__gm__ int32_t *>(srcGm), dataSize);
        deqScaleGlobal.SetGlobalBuffer(reinterpret_cast<__gm__ scaleT *>(deqScaleGm), scaleSize);
        dstGlobal.SetGlobalBuffer(reinterpret_cast<__gm__ dstT *>(dstGm), dataSize);

        pipe.InitBuffer(inQueueX, 1, dataSize * sizeof(int32_t));
        pipe.InitBuffer(inQueueDeqScale, 1, scaleSize * sizeof(scaleT));
        pipe.InitBuffer(outQueue, 1, dataSize * sizeof(dstT));
    }
    __aicore__ inline void Process()
    {
        CopyIn();
        Compute();
        CopyOut();
    }

private:
    __aicore__ inline void CopyIn()
    {
        AscendC::LocalTensor<int32_t> srcLocal = inQueueX.AllocTensor<int32_t>();
        AscendC::DataCopy(srcLocal, srcGlobal, dataSize);
        inQueueX.EnQue(srcLocal);
        AscendC::LocalTensor<scaleT> deqScaleLocal = inQueueDeqScale.AllocTensor<scaleT>();
        AscendC::DataCopy(deqScaleLocal, deqScaleGlobal, scaleSize);
        inQueueDeqScale.EnQue(deqScaleLocal);
    }
    __aicore__ inline void Compute()
    {
        AscendC::LocalTensor<dstT> dstLocal = outQueue.AllocTensor<dstT>();
        AscendC::LocalTensor<int32_t> srcLocal = inQueueX.DeQue<int32_t>();
        AscendC::LocalTensor<scaleT> deqScaleLocal = inQueueDeqScale.DeQue<scaleT>();
        AscendC::AscendDequant(dstLocal, srcLocal, deqScaleLocal, {rowLen, colLen, deqScaleLocal.GetSize()});
        outQueue.EnQue<dstT>(dstLocal);
        inQueueX.FreeTensor(srcLocal);
        inQueueDeqScale.FreeTensor(deqScaleLocal);
    }
    __aicore__ inline void CopyOut()
    {
        AscendC::LocalTensor<dstT> dstLocal = outQueue.DeQue<dstT>();
        AscendC::DataCopy(dstGlobal, dstLocal, dataSize);
        outQueue.FreeTensor(dstLocal);
    }

private:
    AscendC::GlobalTensor<int32_t> srcGlobal;
    AscendC::GlobalTensor<scaleT> deqScaleGlobal;
    AscendC::GlobalTensor<dstT> dstGlobal;
    AscendC::TPipe pipe;
    AscendC::TQue<AscendC::TPosition::VECIN, 1> inQueueX;
    AscendC::TQue<AscendC::TPosition::VECIN, 1> inQueueDeqScale;
    AscendC::TQue<AscendC::TPosition::VECOUT, 1> outQueue;
    uint32_t dataSize = 0;
    uint32_t scaleSize = 0;
    uint32_t rowLen = 0;
    uint32_t colLen = 0;
};

}

#endif // EXAMPLES_QUANTIZATION_DEQUANT_CUSTOM_H



