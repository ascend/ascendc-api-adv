#!/usr/bin/python3
# coding=utf-8

# Copyright (c) 2024 Huawei Technologies Co., Ltd.
# This file is a part of the CANN Open Software.
# Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
# Please refer to the License for details. You may not use this file except in compliance with the License.
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
# INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
# See LICENSE in the root of the software repository for the full text of the License.
# ======================================================================================================================

import numpy as np
import os

def gen_golden_data_simple():
    shape=[4,8]
    scale_size = [8]
    inp = np.random.randint(low=-10, high=10, size=shape).astype(np.int32)
    scale = np.random.uniform(low=-100, high=100, size=scale_size).astype(np.float32)
    golden = np.zeros(shape)
    for i in range(shape[1]):
        for j in range(shape[0]):
            golden[j][i]=inp[j][i]*scale[i]
    golden = golden.astype(np.float32)
    os.system("mkdir -p input")
    os.system("mkdir -p output")
    inp.tofile("./input/input.bin")
    scale.tofile("./input/scale.bin")
    golden.tofile("./output/golden.bin")

if __name__ == "__main__":
    gen_golden_data_simple()