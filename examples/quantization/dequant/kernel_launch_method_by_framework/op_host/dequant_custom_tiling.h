/**
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

#ifndef EXAMPLES_QUANTIZATION_DEQUANT_CUSTOM_TILING_H
#define EXAMPLES_QUANTIZATION_DEQUANT_CUSTOM_TILING_H
#include "register/tilingdata_base.h"
#include "tiling/tiling_api.h"

namespace optiling {
BEGIN_TILING_DATA_DEF(DequantCustomTilingData)
  TILING_DATA_FIELD_DEF(uint32_t, m);
  TILING_DATA_FIELD_DEF(uint32_t, n);
  TILING_DATA_FIELD_DEF(uint32_t, calCount);
  TILING_DATA_FIELD_DEF(uint32_t, sharedTmpBufferSize);
END_TILING_DATA_DEF;

REGISTER_TILING_DATA_CLASS(DequantCustom, DequantCustomTilingData)
} // namespace optiling

void ComputeTiling(const uint32_t m, const uint32_t n, const uint32_t calCount, optiling::DequantCustomTilingData &tiling){
    std::vector<int64_t> shapeVec = {m, n};
    ge::Shape srcShape(shapeVec);
    uint32_t typeSize = sizeof(int32_t);
    uint32_t maxTmpSize;
    uint32_t minTmpSize;
    AscendC::GetAscendDequantMaxMinTmpSize(srcShape, typeSize, maxTmpSize, minTmpSize);
    uint32_t localWorkspaceSize = minTmpSize;
    tiling.set_m(m);
    tiling.set_n(n);
    tiling.set_calCount(calCount);
    tiling.set_sharedTmpBufferSize(localWorkspaceSize);
}

#endif // EXAMPLES_QUANTIZATION_DEQUANT_CUSTOM_TILING_H
