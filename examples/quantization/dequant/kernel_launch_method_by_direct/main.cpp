/**
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

#include "../../../common/data_utils.h"
#ifndef ASCENDC_CPU_DEBUG
#include "acl/acl.h"
extern void dequant_custom_do(uint32_t blockDim, void *l2ctrl, void *stream, uint8_t *srcGm, uint8_t *dstGm,
    uint8_t *deqScaleGm,  uint8_t *workspace, uint8_t *tiling);
#else
#include "tikicpulib.h"
extern "C" __global__ __aicore__ void dequant_custom(GM_ADDR srcGm, GM_ADDR dstGm, GM_ADDR deqScaleGm,
     GM_ADDR workspace, GM_ADDR tiling);
#endif

constexpr uint32_t BLOCK_DIM = 1;
constexpr uint32_t M = 4;
constexpr uint32_t N = 8;
constexpr uint32_t CAL_COUNT = 8;
constexpr uint32_t SCALE_SIZE = 8;
constexpr uint32_t TILINGDATA_SIZE = 4;
constexpr uint32_t WORKSPACE_SIZE = 16*512*512;

extern uint8_t *GenerateTiling(uint32_t m, uint32_t n, uint32_t calCount);

static bool CompareResult(const void *outputData, int64_t outSize)
{
    void *goldenData;
#ifdef ASCENDC_CPU_DEBUG
    goldenData = (uint8_t *)AscendC::GmAlloc(outSize);
#else
    CHECK_ACL(aclrtMallocHost((void **)(&goldenData), outSize));
#endif
    size_t goldenSize = outSize;
    bool ret = ReadFile("../output/golden.bin", goldenSize, goldenData, goldenSize);
    if (ret) {
        printf("ReadFile golden.bin success!\n");
    } else {
        printf("test failed!\n");
        return false;
    }
    constexpr float EPS = 1e-5;
    int64_t wrongNum = 0;

    for (int i = 0; i < outSize / sizeof(float); i++) {
        float a = (reinterpret_cast<const float *>(outputData))[i];
        float b = (reinterpret_cast<const float *>(goldenData))[i];
        float ae = std::abs(a - b);
        float re = ae / abs(b);
        if (ae > EPS && re > EPS) {
            printf("CompareResult golden.bin failed. Output[%d] is %lf, golden is %lf\n", i, a, b);
            wrongNum++;
        }
    }
#ifdef ASCENDC_CPU_DEBUG
    AscendC::GmFree((void *)goldenData);
#else
    CHECK_ACL(aclrtFreeHost(goldenData));
#endif
    if (wrongNum != 0) {
        return false;
    } else {
        printf("CompareResult golden.bin success!\n");
        return true;
    }
}

int32_t main(int32_t argc, char *argv[])
{
    uint32_t blockDim = BLOCK_DIM;
    size_t m = M;
    size_t n = N;
    size_t calCount = CAL_COUNT;
    size_t scaleSize = SCALE_SIZE * sizeof(float);
    size_t inpSize = M * N * sizeof(int32_t);
    size_t outSize = M * N * sizeof(float);
    size_t tilingFileSize = TILINGDATA_SIZE * sizeof(uint32_t);
    size_t workspaceSize = WORKSPACE_SIZE;
    uint8_t *tilingBuf = GenerateTiling(m,n,calCount);

#ifdef ASCENDC_CPU_DEBUG
    uint8_t *input = (uint8_t *)AscendC::GmAlloc(inpSize);
    uint8_t *scale = (uint8_t *)AscendC::GmAlloc(scaleSize);
    uint8_t *output = (uint8_t *)AscendC::GmAlloc(outSize);
    uint8_t *tiling = (uint8_t *)AscendC::GmAlloc(tilingFileSize);
    uint8_t *workspace = (uint8_t *)AscendC::GmAlloc(workspaceSize);

    ReadFile("../input/input.bin", inpSize, input, inpSize);
    ReadFile("../input/scale.bin", scaleSize, scale, scaleSize);
    memcpy_s(tiling, tilingFileSize, tilingBuf, tilingFileSize);
    AscendC::SetKernelMode(KernelMode::AIV_MODE);

    ICPU_RUN_KF(dequant_custom, blockDim, input, output, scale, workspace, tiling); // use this macro for cpu debug

    WriteFile("../output/output.bin", output, outSize);
    bool goldenResult = CompareResult(output, outSize);
    if (goldenResult) {
        printf("test pass!\n");
    } else {
        printf("test failed!\n");
    }

    AscendC::GmFree((void *)input);
    AscendC::GmFree((void *)scale);
    AscendC::GmFree((void *)output);
    AscendC::GmFree((void *)tiling);
    AscendC::GmFree((void *)workspace);
#else
    CHECK_ACL(aclInit(nullptr));
    aclrtContext context;
    int32_t deviceId = 0;
    CHECK_ACL(aclrtSetDevice(deviceId));
    CHECK_ACL(aclrtCreateContext(&context, deviceId));
    aclrtStream stream = nullptr;
    CHECK_ACL(aclrtCreateStream(&stream));

    uint8_t *inputHost, *outputHost, *scaleHost, *workspaceHost, *tilingHost;
    uint8_t *inputDevice, *outputDevice, *scaleDevice, *workspaceDevice, *tilingDevice;

    CHECK_ACL(aclrtMallocHost((void **)(&inputHost), inpSize));
    CHECK_ACL(aclrtMallocHost((void **)(&outputHost), outSize));
    CHECK_ACL(aclrtMallocHost((void **)(&scaleHost), scaleSize));
    CHECK_ACL(aclrtMallocHost((void **)(&workspaceHost), workspaceSize));
    CHECK_ACL(aclrtMallocHost((void **)(&tilingHost), tilingFileSize));
    CHECK_ACL(aclrtMalloc((void **)&inputDevice, inpSize, ACL_MEM_MALLOC_HUGE_FIRST));
    CHECK_ACL(aclrtMalloc((void **)&outputDevice, outSize, ACL_MEM_MALLOC_HUGE_FIRST));
    CHECK_ACL(aclrtMalloc((void **)&scaleDevice, scaleSize, ACL_MEM_MALLOC_HUGE_FIRST));
    CHECK_ACL(aclrtMalloc((void **)&workspaceDevice, workspaceSize, ACL_MEM_MALLOC_HUGE_FIRST));
    CHECK_ACL(aclrtMalloc((void **)&tilingDevice, tilingFileSize, ACL_MEM_MALLOC_HUGE_FIRST));

    ReadFile("../input/input.bin", inpSize, inputHost, inpSize);
    ReadFile("../input/scale.bin", scaleSize, scaleHost, scaleSize);
    CHECK_ACL(aclrtMemcpy(tilingDevice, tilingFileSize, tilingBuf,
        tilingFileSize, ACL_MEMCPY_HOST_TO_DEVICE));
    CHECK_ACL(aclrtMemcpy(inputDevice, inpSize, inputHost, inpSize, ACL_MEMCPY_HOST_TO_DEVICE));
    CHECK_ACL(aclrtMemcpy(scaleDevice, scaleSize, scaleHost, scaleSize, ACL_MEMCPY_HOST_TO_DEVICE));

    dequant_custom_do(blockDim, nullptr, stream, inputDevice, outputDevice, scaleDevice, workspaceDevice, tilingDevice);
    CHECK_ACL(aclrtSynchronizeStream(stream));
    CHECK_ACL(aclrtMemcpy(outputHost, outSize, outputDevice, outSize, ACL_MEMCPY_DEVICE_TO_HOST));

    WriteFile("../output/output.bin", outputHost, outSize);

    bool goldenResult = CompareResult(outputHost, outSize);
    if (goldenResult) {
        printf("test pass!\n");
    } else {
        printf("test failed!\n");
    }

    CHECK_ACL(aclrtFree(inputDevice));
    CHECK_ACL(aclrtFree(scaleDevice));
    CHECK_ACL(aclrtFree(outputDevice));
    CHECK_ACL(aclrtFree(tilingDevice));
    CHECK_ACL(aclrtFreeHost(inputHost));
    CHECK_ACL(aclrtFreeHost(scaleHost));
    CHECK_ACL(aclrtFreeHost(outputHost));
    CHECK_ACL(aclrtFreeHost(tilingHost));
    CHECK_ACL(aclrtFree(workspaceDevice));
    CHECK_ACL(aclrtFreeHost(workspaceHost));

    CHECK_ACL(aclrtDestroyStream(stream));
    CHECK_ACL(aclrtDestroyContext(context));
    CHECK_ACL(aclrtResetDevice(deviceId));
    CHECK_ACL(aclFinalize());
#endif
    free(tilingBuf);
    return 0;
}
