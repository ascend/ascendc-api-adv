/**
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

#ifndef EXAMPLES_QUANTIZATION_QUANT_CUSTOM_TILING_H
#define EXAMPLES_QUANTIZATION_QUANT_CUSTOM_TILING_H
#include "register/tilingdata_base.h"
#include "tiling/tiling_api.h"

namespace optiling {
BEGIN_TILING_DATA_DEF(QuantCustomTilingData)
  TILING_DATA_FIELD_DEF(uint32_t, dataLength);
  TILING_DATA_FIELD_DEF(uint32_t, sharedTmpBufferSize);
END_TILING_DATA_DEF;

REGISTER_TILING_DATA_CLASS(QuantCustom, QuantCustomTilingData)
} // namespace optiling

void ComputeTiling(const uint32_t dataLength, optiling::QuantCustomTilingData &tiling){
    std::vector<int64_t> shapeVec = {dataLength};
    ge::Shape srcShape(shapeVec);
    uint32_t typeSize = sizeof(float);
    uint32_t maxTmpSize;
    uint32_t minTmpSize;
    AscendC::GetAscendQuantMaxMinTmpSize(srcShape, typeSize, maxTmpSize, minTmpSize);
    uint32_t localWorkspaceSize = minTmpSize;
    tiling.set_dataLength(dataLength);
    tiling.set_sharedTmpBufferSize(localWorkspaceSize);
}

#endif // EXAMPLES_QUANTIZATION_QUANT_CUSTOM_TILING_H
