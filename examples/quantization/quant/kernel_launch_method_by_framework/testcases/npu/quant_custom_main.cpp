/**
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */
#include "aclnn_quant_custom.h"
#include "acl/acl_rt.h"
#include "acl/acl.h"
#include <stdio.h>
#include <stdlib.h>
#include "../../../../../common/data_utils.h"

aclrtStream CreateStream(int device)
{
    if (aclInit(NULL) != ACL_SUCCESS) {
        printf("acl init failed\n");
        return NULL;
    }
    if (aclrtSetDevice(device) != ACL_SUCCESS) {
        printf("Set device failed\n");
        (void)aclFinalize();
        return NULL;
    }
    aclrtStream stream = nullptr;
    if (aclrtCreateStream(&stream) != ACL_SUCCESS) {
        printf("Create stream failed\n");
        return NULL;
    }
    return stream;
}

void DestroyStream(aclrtStream stream, int device)
{
    (void)aclrtDestroyStream(stream);
    if (aclrtResetDevice(device) != ACL_SUCCESS) {
        printf("Reset device failed\n");
    }
    if (aclFinalize() != ACL_SUCCESS) {
        printf("Finalize acl failed\n");
    }
}

struct tensorInfo {
    int64_t *dims;
    int64_t dimCnt;
    aclDataType dtype;
    aclFormat fmt;
};

int64_t GetDataSize(struct tensorInfo *desc)
{
    if (!desc->dims)
        return 0;
    int64_t size = 1;
    for (auto i = 0; i < desc->dimCnt; i++) {
        size *= desc->dims[i];
    }
    return size *aclDataTypeSize(desc->dtype);
}

static bool CompareResult(const void *outputData, int64_t outSize)
{
    void *goldenData;
    CHECK_ACL(aclrtMallocHost((void **)(&goldenData), outSize));
    size_t goldenSize = outSize;
    bool ret = ReadFile("../output/golden.bin", goldenSize, goldenData, goldenSize);
    if (ret) {
        printf("ReadFile golden.bin success!\n");
    } else {
        printf("test failed!\n");
        return false;
    }
    constexpr float EPS = 1e-6;
    int64_t wrongNum = 0;

    for (int i = 0; i <  outSize / sizeof(int8_t); i++) {
        float a = (reinterpret_cast<const int8_t *>(outputData))[i];
        float b = (reinterpret_cast<const int8_t *>(goldenData))[i];
        float ae = std::abs(a - b);
        float re = ae / abs(b);
        if (ae > EPS && re > EPS) {
            printf("CompareResult golden.bin failed output is %lf, golden is %lf\n", a, b);
            wrongNum++;
        }
    }
    CHECK_ACL(aclrtFreeHost(goldenData));

    if (wrongNum != 0) {
        return false;
    } else {
        printf("CompareResult golden.bin success\n");
        return true;
    }
}

int main(void)
{
    aclrtStream stream;
    int64_t input[] = {1024};
    int64_t output[] = {1024};
    struct tensorInfo tensorDesc[] = {{input, 1, ACL_FLOAT, ACL_FORMAT_ND},
                                      {output, 1, ACL_INT8, ACL_FORMAT_ND},
                                     };
    stream = CreateStream(0);
    aclTensor *tensors[sizeof(tensorDesc) / sizeof(struct tensorInfo)];
    void *devMem[sizeof(tensorDesc) / sizeof(struct tensorInfo)];
    for (auto i = 0; i < sizeof(tensorDesc) / sizeof(struct tensorInfo); i++) {
        void *data;
        struct tensorInfo *info = &(tensorDesc[i]);
        int64_t size = GetDataSize(info);
        if (size == 0) {
            tensors[i] = NULL;
            devMem[i] = NULL;
            continue;
        }
        CHECK_ACL(aclrtMalloc(&data, size, ACL_MEM_MALLOC_HUGE_FIRST));
        // read input
        if (i == 0) {
            size_t inputSize = size;
            void *dataHost;
            CHECK_ACL(aclrtMallocHost((void **)(&dataHost), inputSize));
            ReadFile("../input/input.bin", inputSize, dataHost, inputSize);
            CHECK_ACL(aclrtMemcpy(data, size, dataHost, size, ACL_MEMCPY_HOST_TO_DEVICE));
            CHECK_ACL(aclrtFreeHost(dataHost));
        }
        devMem[i] = data;
        tensors[i] =
            aclCreateTensor(info->dims, info->dimCnt, info->dtype, NULL, 0, info->fmt, info->dims, info->dimCnt, data);
    }

    size_t workspaceSize = 0;
    aclOpExecutor *handle;
    int32_t ret;
    ret = aclnnQuantCustomGetWorkspaceSize(tensors[0], tensors[1], &workspaceSize, &handle);
    printf("aclnnQuantCustomGetWorkspaceSize ret %d workspace size %lu\n", ret, workspaceSize);
    void *workspace = NULL;
    if (workspaceSize != 0) {
        CHECK_ACL(aclrtMalloc(&workspace, workspaceSize, ACL_MEM_MALLOC_HUGE_FIRST));
    }
    ret = aclnnQuantCustom(workspace, workspaceSize, handle, stream);
    printf("aclnnQuantCustom ret %u\n", ret);
    if (aclrtSynchronizeStreamWithTimeout(stream, 5000) != ACL_SUCCESS) {
        printf("Synchronize stream failed\n");
    }

    uint8_t *outputHost;
    int64_t outputHostSize = GetDataSize(&(tensorDesc[1]));

    CHECK_ACL(aclrtMallocHost((void **)(&outputHost), outputHostSize));
    CHECK_ACL(aclrtMemcpy(outputHost, outputHostSize, devMem[1], outputHostSize, ACL_MEMCPY_DEVICE_TO_HOST));
    WriteFile("../output/output.bin", outputHost, outputHostSize);
    bool goldenResult = CompareResult(outputHost, outputHostSize);
    if (goldenResult) {
        printf("test pass!\n");
    } else {
        printf("test failed!\n");
    }

    CHECK_ACL(aclrtFreeHost(outputHost));

    for (auto i = 0; i < sizeof(tensorDesc) / sizeof(struct tensorInfo); i++) {
        if (!tensors[i])
            continue;
        if (devMem[i]) {
            CHECK_ACL(aclrtFree(devMem[i]));
        }
        aclDestroyTensor(tensors[i]);
    }
    DestroyStream(stream, 0);
    return 0;
}
