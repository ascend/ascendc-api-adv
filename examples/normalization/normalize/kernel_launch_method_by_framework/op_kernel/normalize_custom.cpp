/**
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

#include "../../../../../../kernel_impl/normalize_custom.h"

extern "C" __global__ __aicore__ void normalize_custom(GM_ADDR srcGm, GM_ADDR srcMeanGm, GM_ADDR srcVarGm,
    GM_ADDR srcGammaGm, GM_ADDR srcBetaGm, GM_ADDR outGm, GM_ADDR outRstdGm, GM_ADDR workspace, GM_ADDR tiling)
{
    GET_TILING_DATA(tilingData, tiling);
    NormalizeCustomKernel::NormalizeTiling vecTiling = *reinterpret_cast<NormalizeCustomKernel::NormalizeTiling*>(&tilingData);
    if (TILING_KEY_IS(1)) {
        NormalizeCustomKernel::KernelNormalize<float, float, false> op;
        op.Init(srcGm, srcMeanGm, srcVarGm, srcGammaGm, srcBetaGm, outGm, outRstdGm, vecTiling);
        op.Process();
    }
}
