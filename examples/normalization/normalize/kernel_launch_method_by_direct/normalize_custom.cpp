/**
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */
#include "kernel_operator.h"
#include "../kernel_impl/normalize_custom.h"

__aicore__ inline void CopyTiling(NormalizeCustomKernel::NormalizeTiling* tiling, GM_ADDR tilingGM)
{
    uint32_t* ptr = reinterpret_cast<uint32_t*>(tiling);
    auto tiling32 = reinterpret_cast<__gm__ uint32_t*>(tilingGM);

    for (uint32_t i = 0; i < sizeof(NormalizeCustomKernel::NormalizeTiling) / sizeof(uint32_t); i++, ptr++) {
        *ptr = *(tiling32 + i);
    }
    return;
}

extern "C" __global__ __aicore__ void normalize_custom(GM_ADDR srcGm, GM_ADDR inMeanGm, GM_ADDR inVarGm,
    GM_ADDR inGammaGm, GM_ADDR inBetaGm, GM_ADDR outGm, GM_ADDR outRstdGm, GM_ADDR workspace, GM_ADDR tiling)
{
    if ASCEND_IS_AIC {
        return;
    }
    NormalizeCustomKernel::KernelNormalize<float, float, false> op;
    NormalizeCustomKernel::NormalizeTiling tilingData;
    CopyTiling(&tilingData, tiling);
    op.Init(srcGm, inMeanGm, inVarGm, inGammaGm, inBetaGm, outGm, outRstdGm, tilingData);
    op.Process();
}

#ifndef ASCENDC_CPU_DEBUG
// call of kernel function
void normalize_custom_do(uint32_t blockDim, void *l2ctrl, void *stream, uint8_t *srcGm, uint8_t *inMeanGm,
    uint8_t *inVarGm, uint8_t *inGammaGm, uint8_t *inBetaGm, uint8_t *outGm, uint8_t *outRstdGm, uint8_t *workspace,
    uint8_t *tiling)
{
    normalize_custom<<<blockDim, l2ctrl, stream>>>(srcGm, inMeanGm, inVarGm, inGammaGm, inBetaGm, outGm, outRstdGm,
        workspace, tiling);
}
#endif
