/**
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

#ifndef EXAMPLES_NORMALIZATION_WELFORDUPDATE_CUSTOM_TILING_H
#define EXAMPLES_NORMALIZATION_WELFORDUPDATE_CUSTOM_TILING_H
#include "register/tilingdata_base.h"
#include "tiling/tiling_api.h"
#include "tiling/platform/platform_ascendc.h"

namespace optiling {
BEGIN_TILING_DATA_DEF(WelfordUpdateCustomTilingData)
    TILING_DATA_FIELD_DEF(bool, inplace);
    TILING_DATA_FIELD_DEF(uint32_t, nLength);
    TILING_DATA_FIELD_DEF(uint32_t, rLength);
    TILING_DATA_FIELD_DEF(uint32_t, abComputeLength);
    TILING_DATA_FIELD_DEF(float, nRec);
    TILING_DATA_FIELD_DEF(uint32_t, tmpLocalSize);
END_TILING_DATA_DEF;
REGISTER_TILING_DATA_CLASS(WelfordUpdateCustom, WelfordUpdateCustomTilingData)
} // namespace optiling

constexpr bool ISREUSESOURCE = false;
constexpr bool ISINPLACE = true;

void ComputeTiling(bool inplace, uint32_t nLength, uint32_t rLength, uint32_t abComputeLength,
    float nRec, optiling::WelfordUpdateCustomTilingData &tiling)
{
    std::vector<int64_t> shapeVec = {nLength, rLength};
    ge::Shape srcShape(shapeVec);
    uint32_t maxsize = 0;
    uint32_t minsize = 0;
    uint32_t dtypesizeT = 2;  // half类型
    uint32_t dtypesizeU = 4;  // float类型

    tiling.set_inplace(inplace);
    tiling.set_nLength(nLength);
    tiling.set_rLength(rLength);
    tiling.set_abComputeLength(abComputeLength);
    tiling.set_nRec(nRec);

    AscendC::GetWelfordUpdateMaxMinTmpSize(srcShape, dtypesizeT, dtypesizeU, ISREUSESOURCE, ISINPLACE, maxsize,
        minsize);
    tiling.set_tmpLocalSize(minsize);
}

#endif // EXAMPLES_NORMALIZATION_WELFORDUPDATE_CUSTOM_TILING_H
