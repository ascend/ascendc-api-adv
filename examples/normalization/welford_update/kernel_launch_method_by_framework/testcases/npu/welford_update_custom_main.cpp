/**
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

#include <cstdio>
#include <cstdlib>
#include "acl/acl_rt.h"
#include "acl/acl.h"
#include "aclnn_welford_update_custom.h"
#include "../../../../../common/data_utils.h"

constexpr uint8_t SRC_SIZE = 3;
constexpr uint16_t TIMEOUT = 5000;
constexpr uint8_t INDEX_IN_VAR = 2;
constexpr uint8_t INDEX_OUT_MEAN = 3;
constexpr uint8_t INDEX_OUT_VAR = 4;
constexpr uint8_t RN_SIZE = 1;
constexpr uint32_t AB_SIZE = 64;

aclrtStream CreateStream(int device)
{
    if (aclInit(NULL) != ACL_SUCCESS) {
        printf("acl init failed\n");
        return NULL;
    }
    if (aclrtSetDevice(device) != ACL_SUCCESS) {
        printf("Set device failed\n");
        (void)aclFinalize();
        return NULL;
    }
    aclrtStream stream = nullptr;
    if (aclrtCreateStream(&stream) != ACL_SUCCESS) {
        printf("Create stream failed\n");
        return NULL;
    }
    return stream;
}

void DestroyStream(aclrtStream stream, int device)
{
    (void)aclrtDestroyStream(stream);
    if (aclrtResetDevice(device) != ACL_SUCCESS) {
        printf("Reset device failed\n");
    }
    if (aclFinalize() != ACL_SUCCESS) {
        printf("Finalize acl failed\n");
    }
}

struct tensorInfo {
    int64_t *dims;
    int64_t dimCnt;
    aclDataType dtype;
    aclFormat fmt;
};

int64_t GetDataSize(struct tensorInfo *desc)
{
    if (!desc->dims) {
        return 0;
    }
    int64_t size = 1;
    for (auto i = 0; i < desc->dimCnt; i++) {
        size *= desc->dims[i];
    }
    return size * sizeof(float);
}

static bool CompareResult(const void *outputData, int64_t outSize, std::string goldenName)
{
    void *goldenData;
    CHECK_ACL(aclrtMallocHost((void **)(&goldenData), outSize));
    size_t goldenSize = outSize;
    bool ret = ReadFile("../output/golden_" + goldenName + ".bin", goldenSize, goldenData, goldenSize);
    if (ret) {
        printf("ReadFile golden_%s.bin success!\n", goldenName.c_str());
    } else {
        printf("test failed!\n");
        return false;
    }
    constexpr float EPS = 1e-4;
    int64_t wrongNum = 0;

    for (int i = 0; i < outSize / sizeof(float); i++) {
        float a = (reinterpret_cast<const float *>(outputData))[i];
        float b = (reinterpret_cast<const float *>(goldenData))[i];
        float ae = std::abs(a - b);
        float re = ae / abs(b);
        if (ae > EPS && re > EPS) {
            printf("CompareResult golden_output_%s.bin failed output is %lf, golden is %lf\n", goldenName.c_str(), a,
            b);
            wrongNum++;
        }
    }
    CHECK_ACL(aclrtFreeHost(goldenData));

    if (wrongNum != 0) {
        return false;
    } else {
        printf("CompareResult golden_output_%s.bin success\n", goldenName.c_str());
        return true;
    }
}

int main(void)
{
    aclrtStream stream;

    int64_t srcGm[] = {RN_SIZE * AB_SIZE};
    int64_t inMeanGm[] = {RN_SIZE * AB_SIZE};
    int64_t inVarGm[] = {RN_SIZE * AB_SIZE};
    int64_t outMeanGm[] = {RN_SIZE * AB_SIZE};
    int64_t outVarGm[] = {RN_SIZE * AB_SIZE};
    struct tensorInfo tensorDesc[] = {{srcGm, 1, ACL_FLOAT16, ACL_FORMAT_ND},
                                        {inMeanGm, 1, ACL_FLOAT, ACL_FORMAT_ND},
                                        {inVarGm, 1, ACL_FLOAT, ACL_FORMAT_ND},
                                        {outMeanGm, 1, ACL_FLOAT, ACL_FORMAT_ND},
                                        {outVarGm, 1, ACL_FLOAT, ACL_FORMAT_ND},
                                        };

    std::string ParamNames[] = {
        "srcGm",
        "inMeanGm",
        "inVarGm",
        "outMeanGm",
        "outVarGm",
    };
    stream = CreateStream(0);

    aclTensor *tensors[sizeof(tensorDesc) / sizeof(struct tensorInfo)];
    void *devMem[sizeof(tensorDesc) / sizeof(struct tensorInfo)];
    for (auto i = 0; i < sizeof(tensorDesc) / sizeof(struct tensorInfo); i++) {
        void *data;
        struct tensorInfo *info = &(tensorDesc[i]);
        int64_t size = GetDataSize(info);
        if (size == 0) {
            tensors[i] = NULL;
            devMem[i] = NULL;
            continue;
        }
        CHECK_ACL(aclrtMalloc(&data, size, ACL_MEM_MALLOC_HUGE_FIRST));
        // read input
        if (i < SRC_SIZE) {
            size_t inputSize = size;
            void *dataHost;
            CHECK_ACL(aclrtMallocHost((void **)(&dataHost), inputSize));
            ReadFile("../input/input_" + ParamNames[i] + ".bin", inputSize, dataHost, inputSize);
            CHECK_ACL(aclrtMemcpy(data, size, dataHost, size, ACL_MEMCPY_HOST_TO_DEVICE));
            CHECK_ACL(aclrtFreeHost(dataHost));
        }
        devMem[i] = data;
        tensors[i] =
            aclCreateTensor(info->dims, info->dimCnt, info->dtype, NULL, 0, info->fmt, info->dims, info->dimCnt, data);
    }

    size_t workspaceSize = 0;
    aclOpExecutor *handle;
    int32_t ret = 0;
    ret = aclnnWelfordUpdateCustomGetWorkspaceSize(tensors[0], tensors[1], tensors[INDEX_IN_VAR],
        tensors[INDEX_OUT_MEAN], tensors[INDEX_OUT_VAR], &workspaceSize, &handle);
    printf("aclnnWelfordUpdateCustomGetWorkspaceSize ret %u workspace size %lu\n", ret, workspaceSize);
    void *workspace = nullptr;
    if (workspaceSize != 0) {
        CHECK_ACL(aclrtMalloc(&workspace, workspaceSize, ACL_MEM_MALLOC_HUGE_FIRST));
    }
    ret = aclnnWelfordUpdateCustom(workspace, workspaceSize, handle, stream);
    printf("aclnnWelfordUpdateCustom ret %u\n", ret);
    if (aclrtSynchronizeStreamWithTimeout(stream, TIMEOUT) != ACL_SUCCESS) {
        printf("Synchronize stream failed\n");
    }

    uint8_t *outMeanHost, *outVarHost;
    int64_t outMeanHostSize = GetDataSize(&(tensorDesc[INDEX_OUT_MEAN]));
    int64_t outVarHostSize = GetDataSize(&(tensorDesc[INDEX_OUT_VAR]));

    CHECK_ACL(aclrtMallocHost((void **)(&outMeanHost), outMeanHostSize));
    CHECK_ACL(aclrtMallocHost((void **)(&outVarHost), outVarHostSize));

    CHECK_ACL(aclrtMemcpy(outMeanHost, outMeanHostSize, devMem[INDEX_OUT_MEAN], outMeanHostSize,
        ACL_MEMCPY_DEVICE_TO_HOST));
    CHECK_ACL(aclrtMemcpy(outVarHost, outVarHostSize, devMem[INDEX_OUT_VAR], outVarHostSize,
        ACL_MEMCPY_DEVICE_TO_HOST));

    WriteFile("../output/output_outMeanGm.bin", outMeanHost, outMeanHostSize);
    WriteFile("../output/output_outVarGm.bin", outVarHost, outVarHostSize);

    bool goldenResult = true;
    goldenResult &= CompareResult(outMeanHost, outMeanHostSize, ParamNames[INDEX_OUT_MEAN]);
    goldenResult &= CompareResult(outVarHost, outVarHostSize, ParamNames[INDEX_OUT_VAR]);
    if (goldenResult) {
        printf("test pass!\n");
    } else {
        printf("test failed!\n");
    }

    CHECK_ACL(aclrtFreeHost(outMeanHost));
    CHECK_ACL(aclrtFreeHost(outVarHost));

    for (auto i = 0; i < sizeof(tensorDesc) / sizeof(struct tensorInfo); i++) {
        if (!tensors[i])
            continue;
        if (devMem[i]) {
            CHECK_ACL(aclrtFree(devMem[i]));
        }
        aclDestroyTensor(tensors[i]);
    }
    DestroyStream(stream, 0);
    return 0;
}
