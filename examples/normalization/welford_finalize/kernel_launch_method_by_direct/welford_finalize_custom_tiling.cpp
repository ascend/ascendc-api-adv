/**
 * Copyright (c) 2025 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

#include <cassert>
#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include "tiling/tiling_api.h"
#include "../host_tiling/welford_finalize_custom_tiling.h"

uint8_t *GetTilingBuf(optiling::WelfordFinalizeCustomTilingData *tilingData) {
    uint32_t tilingSize = sizeof(optiling::WelfordFinalizeCustomTilingData);
    uint8_t *buf = (uint8_t *)malloc(tilingSize);
    tilingData->SaveToBuffer(buf, tilingSize);
    return buf;
}
uint8_t* GenerateTiling(uint32_t rnLength, uint32_t abLength, uint32_t head, uint32_t headLength, uint32_t tail, uint32_t tailLength)
{
    optiling::WelfordFinalizeCustomTilingData tiling;
    ComputeTiling(rnLength, abLength, head, headLength, tail, tailLength, tiling);
    return GetTilingBuf(&tiling);
}