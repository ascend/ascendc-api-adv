#!/usr/bin/python3
# coding=utf-8

# Copyright (c) 2025 Huawei Technologies Co., Ltd.
# This file is a part of the CANN Open Software.
# Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
# Please refer to the License for details. You may not use this file except in compliance with the License.
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
# INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
# See LICENSE in the root of the software repository for the full text of the License.
# ======================================================================================================================

import os
import numpy as np

RN_SIZE = 1
AB_SIZE = 1024

def gen_golden_data_simple():
    x1 = np.ones([RN_SIZE * AB_SIZE]).astype(np.int32)
    x2 = np.random.uniform(-6000, 6000, [RN_SIZE * AB_SIZE]).astype(np.float32)
    x3 = np.random.uniform(0, 6000, [RN_SIZE * AB_SIZE]).astype(np.float32)
    print(x1)
    print(x2)
    print(x3)
    os.system("mkdir -p ./input")
    x1.tofile("./input/input_countsGm.bin")
    x2.tofile("./input/input_inMeanGm.bin")
    x3.tofile("./input/input_inVarGm.bin")
    
    golden1 = np.mean(x2)
    for i in range(AB_SIZE):
        x3[i] += np.power((x2[i] - golden1), 2, dtype=np.float32) * RN_SIZE
    middle = np.divide(x3, RN_SIZE * AB_SIZE, out=np.zeros_like(x3, dtype=np.float32))
    golden2 = np.sum(middle, dtype=np.float32)

    out1 = np.concatenate((np.array([golden1]), np.zeros(7, dtype=np.float32)))
    out2 = np.concatenate((np.array([golden2]), np.zeros(7, dtype=np.float32)))

    print(out1)
    print(out2)
    os.system("mkdir -p ./output")
    out1.tofile("./output/golden_outMeanGm.bin")
    out2.tofile("./output/golden_outVarGm.bin")

if __name__ == "__main__":
    gen_golden_data_simple()