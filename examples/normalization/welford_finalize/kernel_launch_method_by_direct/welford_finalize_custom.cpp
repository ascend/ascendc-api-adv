/**
 * Copyright (c) 2025 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */
#include "kernel_operator.h"
#include "../kernel_impl/welford_finalize_custom.h"

__aicore__ inline void CopyTiling(MyCustomKernel::VecTiling* tiling, GM_ADDR tilingGM)
{
    uint32_t* ptr = reinterpret_cast<uint32_t*>(tiling);
    auto tiling32 = reinterpret_cast<__gm__ uint32_t*>(tilingGM);

    for (uint32_t i = 0; i < sizeof(MyCustomKernel::VecTiling) / sizeof(uint32_t); i++, ptr++) {
        *ptr = *(tiling32 + i);
    }
    return;
}

extern "C" __global__ __aicore__ void welford_finalize_custom(GM_ADDR inMeanGm, GM_ADDR inVarGm, GM_ADDR countsGm, 
    GM_ADDR outMeanGm, GM_ADDR outVarGm, GM_ADDR workspace, GM_ADDR tiling)
{
    if ASCEND_IS_AIC {
        return;
    }
    MyCustomKernel::KernelWelfordFinalize<float> op;
    MyCustomKernel::VecTiling tilingData;
    CopyTiling(&tilingData, tiling);
    op.Init(inMeanGm, inVarGm, countsGm, outMeanGm, outVarGm, tilingData);
    op.Process();
}

#ifndef ASCENDC_CPU_DEBUG
// call of kernel function
void welford_finalize_custom_do(uint32_t blockDim, void *l2ctrl, void *stream, uint8_t *inMeanGm,
    uint8_t *inVarGm, uint8_t *countsGm, uint8_t *outMeanGm, uint8_t *outVarGm, uint8_t *workspace, uint8_t *tiling)
{
    welford_finalize_custom<<<blockDim, l2ctrl, stream>>>(inMeanGm, inVarGm, countsGm, outMeanGm, outVarGm, workspace,
        tiling);
}
#endif
