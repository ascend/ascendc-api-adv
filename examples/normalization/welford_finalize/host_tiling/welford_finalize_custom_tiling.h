/**
 * Copyright (c) 2025 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

#ifndef EXAMPLES_NORMALIZATION_WELFORDFINALIZE_CUSTOM_TILING_H
#define EXAMPLES_NORMALIZATION_WELFORDFINALIZE_CUSTOM_TILING_H
#include "register/tilingdata_base.h"
#include "tiling/tiling_api.h"
#include "tiling/platform/platform_ascendc.h"

constexpr uint32_t MIN_BUFFER_SIZE = 1024;

namespace optiling {
BEGIN_TILING_DATA_DEF(WelfordFinalizeCustomTilingData)
    TILING_DATA_FIELD_DEF(uint32_t, rnLength);
    TILING_DATA_FIELD_DEF(uint32_t, abLength);
    TILING_DATA_FIELD_DEF(uint32_t, head);
    TILING_DATA_FIELD_DEF(uint32_t, headLength);
    TILING_DATA_FIELD_DEF(uint32_t, tail);
    TILING_DATA_FIELD_DEF(uint32_t, tailLength);
    TILING_DATA_FIELD_DEF(uint32_t, tmpLocalSize);
END_TILING_DATA_DEF;
REGISTER_TILING_DATA_CLASS(WelfordFinalizeCustom, WelfordFinalizeCustomTilingData)
} // namespace optiling

constexpr bool ISREUSESOURCE = false;

void ComputeTiling(uint32_t rnLength, uint32_t abLength, uint32_t head, uint32_t headLength, uint32_t tail,
    uint32_t tailLength, optiling::WelfordFinalizeCustomTilingData &tiling)
{
    std::vector<int64_t> shapeVec = {abLength};
    ge::Shape srcShape(shapeVec);
    uint32_t maxsize = 0;
    uint32_t minsize = 0;
    uint32_t dtypesize = 4;  // float类型

    tiling.set_rnLength(rnLength);
    tiling.set_abLength(abLength);
    tiling.set_head(head);
    tiling.set_headLength(headLength);
    tiling.set_tail(tail);
    tiling.set_tailLength(tailLength);

    AscendC::GetWelfordFinalizeMaxMinTmpSize(srcShape, dtypesize, ISREUSESOURCE, maxsize,
        minsize);
    tiling.set_tmpLocalSize(minsize);
}

#endif // EXAMPLES_NORMALIZATION_WELFORDFINALIZE_CUSTOM_TILING_H
