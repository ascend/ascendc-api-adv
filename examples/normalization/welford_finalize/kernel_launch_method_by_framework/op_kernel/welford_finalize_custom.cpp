/**
 * Copyright (c) 2025 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

#include "../../../../../../kernel_impl/welford_finalize_custom.h"

extern "C" __global__ __aicore__ void welford_finalize_custom(GM_ADDR inMeanGm, GM_ADDR inVarGm, GM_ADDR countsGm, 
    GM_ADDR outMeanGm, GM_ADDR outVarGm, GM_ADDR workspace, GM_ADDR tiling)
{
    GET_TILING_DATA(tilingData, tiling);
    MyCustomKernel::VecTiling vecTiling = *reinterpret_cast<MyCustomKernel::VecTiling*>(&tilingData);
    if (TILING_KEY_IS(1)) {
        MyCustomKernel::KernelWelfordFinalize<float> op;
        op.Init(inMeanGm, inVarGm, countsGm, outMeanGm, outVarGm, vecTiling);
    op.Process();
    }
}