# ascendc-api-adv

ascendc-api-adv，是昇腾硬件上面向算子开发场景的编程语言Ascend C的高阶类库。

## 概述

面向算子开发场景的编程语言Ascend C，原生支持C和C++标准规范，最大化匹配用户开发习惯；通过多层接口抽象、自动并行计算、孪生调试等关键技术，极大提高算子开发效率，助力AI开发者低成本完成算子开发和模型调优部署。

Ascend C算子采用标准C++语法和一组类库API进行编程，您可以在核函数的实现中根据自己的需求选择合适的API。Ascend C编程类库API分为基础API和高阶API。其中，基础API提供基础功能API，实现对硬件能力的抽象，开放芯片的能力，保证完备性和兼容性。

本代码仓是Ascend C 高阶API的源码仓。高阶API一般是基于单核对常见算法的抽象和封装，用于提高编程开发效率，通常会调用多种基础API实现。高阶API包括数学库、Matmul、Softmax等API，详细API列表请见[Ascend C高阶API列表](./docs/README.md)。开发者可根据实际业务需要，修改高阶API或开发其他高阶API，并编译部署到CANN软件环境中使用。

## 版本配套说明
- 本源码仓会适配CANN软件版本创建相应的标签并发行，关于CANN软件版本与本源码仓中标签的配套关系可参见"[开放项目与CANN版本配套表](https://gitee.com/ascend/cann-community/blob/master/README.md#cannversionmap)"。**需要注意，为确保您的源码定制开发顺利进行，请选择配套的CANN版本与Gitee标签源码，使用master分支可能存在版本不匹配的风险。**

- 本源码仓支持的固件驱动版本与配套CANN软件支持的固件驱动版本相同，开发者可通过“[昇腾社区-固件与驱动](https://www.hiascend.com/hardware/firmware-drivers/community?product=2&model=28)”页面根据产品型号与CANN软件版本获取配套的固件与驱动。

## 目录结构说明
本代码仓目录结构如下：

```
├── .gitee                        # 用于全局配置Issue模板
├── cmake                         # Ascend C高阶API编译工程文件
├── docs                          # Ascend C高阶API列表
├── examples                      # Ascend C高阶API样例工程
├── impl                          # Ascend C高阶API接口实现源代码
├── lib                           # Ascend C高阶API接口声明源代码
├── tests                         # Ascend C高阶API的UT用例
```

## 环境准备

ascendc-api-adv支持由源码编译，进行源码编译前，请根据如下步骤完成相关环境准备。

1. **获取CANN开发套件包**

   请参见“[开放项目与CANN版本配套表](https://gitee.com/ascend/cann-community/blob/master/README.md#cannversionmap)”获取对应的CANN开发套件包`Ascend-cann-toolkit_<cann_version>_linux-<arch>.run`，CANN开发套件包支持的安装方式及操作系统请参见配套版本的[用户手册](https://hiascend.com/document/redirect/CannCommunityInstSoftware)。

2. **安装依赖**

   以下所列仅为ascendc-api-adv源码编译用到的依赖，其中python、gcc、cmake的安装方法请参见配套版本的[用户手册](https://hiascend.com/document/redirect/CannCommunityInstDepend)，选择安装场景后，参见“安装CANN > 安装依赖”章节进行相关依赖的安装。

   - python >= 3.7.0

   - gcc >= 7.3.0

   - cmake >= 3.16.0

   - googletest（可选，仅执行UT时依赖，建议版本release-1.11.0）

     下载[googletest源码](https://github.com/google/googletest.git)后，执行以下命令安装：

     ```bash
     mkdir temp && cd temp                 # 在googletest源码根目录下创建临时目录并进入
     cmake .. -DCMAKE_CXX_FLAGS="-fPIC -D_GLIBCXX_USE_CXX11_ABI=0"
     make
     make install                         # root用户安装googletest
     # sudo make install                  # 非root用户安装googletest
     ```

3. **安装CANN开发套件包**

   执行安装命令时，请确保安装用户对软件包具有可执行权限。

   - 使用默认路径安装

     ```bash
     # CANN开发套件包安装命令示例：
     ./Ascend-cann-toolkit_<cann_version>_linux-<arch>.run --install
     ```

     - 若使用root用户安装，安装完成后相关软件存储在`/usr/local/Ascend/ascend-toolkit/latest`路径下。
     - 若使用非root用户安装，安装完成后相关软件存储在`$HOME/Ascend/ascend-toolkit/latest`路径下。

   - 指定路径安装

     ```bash
     # CANN开发套件包安装命令示例：
     ./Ascend-cann-toolkit_<cann_version>_linux-<arch>.run --install --install-path=${install_path}
     ```

     安装完成后，相关软件存储在\${install_path}指定路径下。

4. **设置环境变量**

   - 默认路径，root用户安装

     ```bash
     source /usr/local/Ascend/ascend-toolkit/set_env.sh
     ```

   - 默认路径，非root用户安装

     ```bash
     source $HOME/Ascend/ascend-toolkit/set_env.sh
     ```

   - 指定路径安装

     ```bash
     source ${install_path}/ascend-toolkit/set_env.sh
     ```

   **注意：若环境中已安装多个版本的CANN软件包，设置上述环境变量时，请确保${install_path}/ascend-toolkit/latest目录指向的是配套版本的软件包。**



## 源码下载

执行如下命令，下载ascendc-api-adv仓源码：

```bash
git clone -b ${tag_version} https://gitee.com/ascend/ascendc-api-adv.git
```
${tag_version}请替换为具体的标签名称，本源码仓与CANN版本的配套关系可参见"[开放项目与CANN版本配套表](https://gitee.com/ascend/cann-community/blob/master/README.md#cannversionmap)"。


## 编译安装

1. 编译

   ascendc-api-adv仓提供一键式编译安装能力，进入本源码仓代码根目录，执行如下命令：

   ```bash
   bash build.sh
   ```

   编译完成后会在`output`目录下生成CANN-ascendc_api_adv-\<cann_version>-linux.\<arch>.run软件包。

2. 安装

   在源码仓根目录下执行下列命令，根据设置的环境变量路径，将编译生成的run包安装到CANN包的装包路径，同时会覆盖原CANN包中的高阶API内容。

   ```bash
   # 设置CANN开发套件包环境变量，以root用户默认路径为例，如已设置，则可忽略该操作
   source /usr/local/Ascend/ascend-toolkit/set_env.sh
   # 切换到run包生成路径下
   cd output
   # 安装run包
   ./CANN-ascendc_api_adv-<cann_version>-linux.<arch>.run
   ```

## UT测试（可选）

在源码仓根目录执行下列命令之一，将依次批跑tests目录下的用例，得到结果日志，用于看护编译是否正常。

```    bash
bash build.sh -t
```

或

```    bash
bash build.sh --test
```

执行UT用例依赖googletest单元测试框架，关于googletest更多功能请参见[googletest官网](https://google.github.io/googletest/advanced.html#running-a-subset-of-the-tests)。

## 样例运行验证（可选）

开发者调用高阶API实现自定义算子后，可通过单算子调用的方式验证算子功能。本代码仓提供部分算子实现及其调用样例，具体请参考[examples](./examples)目录下的样例。

## 回滚

回滚是指在CANN包基础上安装了ascendc-api-adv的run包之后，需要将ascendc-api-adv包回退，恢复到本次安装ascendc-api-adv的run包之前的操作。如果开发者定制修改代码时出现功能问题，可执行回滚操作，将环境恢复到本次安装ascendc-api-adv的run包之前的环境。

**说明：** 回滚只支持一次，重复执行会报错。

在`output`目录下执行如下命令：

```    bash
./CANN-ascendc_api_adv-<cann_version>-linux.<arch>.run --rollback
```

若提示如下信息，则说明回滚成功。
```    bash
package rollback successfully!
```
## 贡献指南

ascendc-api-adv仓欢迎广大开发者体验并参与贡献，在参与社区贡献前，请参见[cann-community](https://gitee.com/ascend/cann-community/blob/master/README.md)了解行为准则，进行CLA协议签署，了解贡献的详细流程。

针对本仓，开发者准备本地代码与提交PR时需要重点关注如下几点：

1. 提交PR时，请按照PR模板仔细填写本次PR的业务背景、目的、方案等信息。
2. 若您的修改不是简单的bug修复，而是涉及到新增特性、新增接口、新增配置参数或者修改代码流程等，请务必先通过Issue进行方案讨论，以避免您的代码被拒绝合入。若您不确定本次修改是否可被归为“简单的bug修复”，亦可通过提交Issue进行方案讨论。

## 许可证
[CANN Open Software License Agreement Version 1.0](LICENSE)
