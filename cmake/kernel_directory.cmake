set(ASCENDC_INSTALL_BASE_PATH ${CMAKE_INSTALL_PREFIX}/lib)

file(MAKE_DIRECTORY  ${ASCENDC_INSTALL_BASE_PATH}/tikcpp/tikcfw)
file(CREATE_LINK ../../ascendc/include/highlevel_api/lib ${ASCENDC_INSTALL_BASE_PATH}/tikcpp/tikcfw/lib SYMBOLIC)
file(CREATE_LINK ../../ascendc/include/highlevel_api/kernel_tiling ${ASCENDC_INSTALL_BASE_PATH}/tikcpp/tikcfw/kernel_tiling SYMBOLIC)

