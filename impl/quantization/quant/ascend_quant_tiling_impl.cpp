/**
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

/*!
 * \file ascend_quant_tiling_impl.cpp
 * \brief
 */
#include "lib/quantization/ascend_quant_tiling.h"

#include <cstdint>

#include "graph/tensor.h"
#include "impl/host_log.h"
namespace AscendC {
namespace {
constexpr uint32_t ASCEND_QUANT_TWO_TIMES = 2;
constexpr uint32_t ASCEND_QUANT_ONE_REPEAT_BYTE_SIZE = 256;
constexpr uint32_t ASCEND_QUANT_MEMORY_CALC = 2;

inline uint32_t GetAscendQuantMaxTmpSize(const uint32_t inputSize)
{
    constexpr uint32_t blkSize = 32;
    uint32_t maxVal = std::max(inputSize * ASCEND_QUANT_MEMORY_CALC,
        ASCEND_QUANT_TWO_TIMES * ASCEND_QUANT_ONE_REPEAT_BYTE_SIZE);
    return (maxVal + blkSize - 1) / blkSize * blkSize;
}

inline uint32_t GetAscendQuantMinTmpSize()
{
    return ASCEND_QUANT_TWO_TIMES * ASCEND_QUANT_ONE_REPEAT_BYTE_SIZE;
}
} // namespace

void GetAscendQuantMaxMinTmpSize(const ge::Shape& srcShape, const uint32_t typeSize, uint32_t& maxValue,
    uint32_t& minValue)
{
    (void)typeSize;
    const uint32_t inputSize = srcShape.GetShapeSize();
    ASCENDC_HOST_ASSERT(inputSize > 0, return, "Input Shape size must be greater than 0.");

    maxValue = GetAscendQuantMaxTmpSize(inputSize);
    minValue = GetAscendQuantMinTmpSize();
}
} // namespace AscendC