/**
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

/* !
 * \file ascend_quant_v200_impl.h
 * \brief
 */
#ifndef IMPL_QUANTIZATION_QUANT_ASCEND_QUANT_V200_IMPL_H
#define IMPL_QUANTIZATION_QUANT_ASCEND_QUANT_V200_IMPL_H
#include "ascend_quant_pre_impl.h"

namespace AscendC {
// per tensor intrinsics
__aicore__ inline void AscendQuantIntrinsicsImpl(const LocalTensor<int8_t>& dstTensor,
    const LocalTensor<half>& srcTensor, const LocalTensor<half>& stackBuffer, half scale, half offset)
{
    UnaryRepeatParams unaryParams;
    UnaryRepeatParams f162s8Params;
    f162s8Params.dstRepStride = HALF_DEFAULT_REPEAT_STRIDE;
    Muls<half, false>(stackBuffer, srcTensor, scale, MASK_PLACEHOLDER, 1, unaryParams);
    PipeBarrier<PIPE_V>();
    Adds<half, false>(stackBuffer, stackBuffer, offset, MASK_PLACEHOLDER, 1, unaryParams);
    PipeBarrier<PIPE_V>();
    Cast<int8_t, half, false>(dstTensor, stackBuffer, RoundMode::CAST_NONE, MASK_PLACEHOLDER, 1, f162s8Params);
    PipeBarrier<PIPE_V>();
}
__aicore__ inline void AscendQuantIntrinsicsImpl(const LocalTensor<int8_t>& dstTensor,
    const LocalTensor<float>& srcTensor, const LocalTensor<half>& stackBuffer, half scale, half offset)
{
    UnaryRepeatParams unaryParams;
    UnaryRepeatParams f162s8Params;
    f162s8Params.dstRepStride = HALF_DEFAULT_REPEAT_STRIDE;
    Cast<half, float, false>(stackBuffer, srcTensor, RoundMode::CAST_NONE, MASK_PLACEHOLDER, 1, f162s8Params);
    PipeBarrier<PIPE_V>();
    Muls<half, false>(stackBuffer, stackBuffer, scale, MASK_PLACEHOLDER, 1, unaryParams);
    PipeBarrier<PIPE_V>();
    Adds<half, false>(stackBuffer, stackBuffer, offset, MASK_PLACEHOLDER, 1, unaryParams);
    PipeBarrier<PIPE_V>();
    Cast<int8_t, half, false>(dstTensor, stackBuffer, RoundMode::CAST_NONE, MASK_PLACEHOLDER, 1, f162s8Params);
    PipeBarrier<PIPE_V>();
}

// api impl
template <typename T, bool isReuseSource = false, const AscendQuantConfig& config = ASCEND_QUANT_DEFAULT_CFG>
__aicore__ inline void AscendQuantImpl(const LocalTensor<int8_t>& dstTensor, const LocalTensor<T>& srcTensor,
    const LocalTensor<uint8_t>& sharedTmpBuffer, const float scale, const float offset, const uint32_t calCount)
{
    IsQuantValid<T, config>(srcTensor, sharedTmpBuffer, calCount);

    SetMaskCount();
    LocalTensor<half> tmpBuffer = sharedTmpBuffer.ReinterpretCast<half>();
    if constexpr(config.workLocalSize != 0 && config.calcCount != 0) {
        constexpr uint32_t splitSize = config.workLocalSize / sizeof(half) / ONE_BLK_SIZE * ONE_BLK_SIZE;
        ASCENDC_ASSERT((splitSize != 0), {
            KERNEL_LOG(KERNEL_ERROR, "splitSize should not be 0!");
        });
        constexpr uint32_t loopCount = config.calcCount / splitSize;
        SetVectorMask<T, MaskMode::COUNTER>(0, splitSize);
        for (uint32_t i = 0; i < loopCount; ++i) {
            AscendQuantIntrinsicsImpl(dstTensor[splitSize * i], srcTensor[splitSize * i],
                tmpBuffer, static_cast<half>(scale), static_cast<half>(offset));
        }
        if constexpr(config.calcCount % splitSize > 0) {
            SetVectorMask<T, MaskMode::COUNTER>(0, config.calcCount % splitSize);
            AscendQuantIntrinsicsImpl(dstTensor[splitSize * loopCount], srcTensor[splitSize * loopCount],
                tmpBuffer, static_cast<half>(scale), static_cast<half>(offset));
        }
    } else {
        uint32_t splitSize = sharedTmpBuffer.GetSize() / sizeof(half) / ONE_BLK_SIZE * ONE_BLK_SIZE;
        ASCENDC_ASSERT((splitSize != 0), {
            KERNEL_LOG(KERNEL_ERROR, "splitSize should not be 0!");
        });
        uint32_t loopCount = calCount / splitSize;
        SetVectorMask<T, MaskMode::COUNTER>(0, splitSize);
        for (uint32_t i = 0; i < loopCount; ++i) {
            AscendQuantIntrinsicsImpl(dstTensor[splitSize * i], srcTensor[splitSize * i],
                sharedTmpBuffer.ReinterpretCast<half>(), static_cast<half>(scale), static_cast<half>(offset));
        }
        if (calCount % splitSize > 0) {
            SetVectorMask<T, MaskMode::COUNTER>(0, calCount % splitSize);
            AscendQuantIntrinsicsImpl(dstTensor[splitSize * loopCount], srcTensor[splitSize * loopCount],
                sharedTmpBuffer.ReinterpretCast<half>(),
                static_cast<half>(scale), static_cast<half>(offset));
        }
    }

    SetMaskNorm();
    ResetMask();
}

template <typename T, bool isReuseSource = false, const AscendQuantConfig& config = ASCEND_QUANT_DEFAULT_CFG>
__aicore__ inline void AscendQuantImpl(const LocalTensor<int8_t>& dstTensor, const LocalTensor<T>& srcTensor,
    const LocalTensor<uint8_t>& sharedTmpBuffer, const LocalTensor<T>& scaleTensor,
    const T offset, const uint32_t scaleCount, const uint32_t calCount)
{
    IsQuantParamValid(dstTensor, srcTensor, sharedTmpBuffer, scaleTensor, offset, scaleCount, calCount);
    IsQuantConfigValid<T, config>(srcTensor, sharedTmpBuffer, scaleTensor);
    SetMaskCount();

    if constexpr(config.scaleCount != 0 && config.calcCount != 0) {
        AscendQuantImplStatic<T, config>(dstTensor, srcTensor, sharedTmpBuffer, scaleTensor, offset);
    } else {
        uint32_t N = calCount / scaleCount;
        if constexpr (IsSameType<T, float>::value) {
            // source vector of scale is reused
            LocalTensor<half> halfScaleTensor =  scaleTensor.template ReinterpretCast<half>();
            UnaryRepeatParams f162s8Param;
            f162s8Param.dstRepStride = HALF_DEFAULT_REPEAT_STRIDE;
            SetVectorMask<half, MaskMode::COUNTER>(0, scaleCount);
            Cast<half, float, false>(halfScaleTensor, scaleTensor, RoundMode::CAST_NONE, MASK_PLACEHOLDER, 1,
                f162s8Param);
            PipeBarrier<PIPE_V>();
            for (uint32_t i = 0; i < N; ++i) {
                AscendQuantPerChannelImpl<T, config>(dstTensor[i * scaleCount], srcTensor[i * scaleCount],
                    sharedTmpBuffer, halfScaleTensor, static_cast<half>(offset), scaleCount);
            }
        } else {
            for (uint32_t i = 0; i < N; ++i) {
                AscendQuantPerChannelImpl<T, config>(dstTensor[i * scaleCount], srcTensor[i * scaleCount],
                    sharedTmpBuffer, scaleTensor, static_cast<half>(offset), scaleCount);
            }
        }
    }

    SetMaskNorm();
    ResetMask();
}

template <typename T, bool isReuseSource = false, const AscendQuantConfig& config = ASCEND_QUANT_DEFAULT_CFG>
__aicore__ inline void AscendQuantImpl(const LocalTensor<int8_t>& dstTensor, const LocalTensor<T>& srcTensor,
    const LocalTensor<uint8_t>& sharedTmpBuffer, const LocalTensor<T>& scaleTensor,
    const LocalTensor<T>& offsetTensor, const uint32_t scaleCount, const uint32_t offsetCount,
    const uint32_t calCount)
{
    IsQuantParamValid(dstTensor, srcTensor, sharedTmpBuffer, scaleTensor, offsetTensor,
        scaleCount, offsetCount, calCount);
    IsQuantConfigValid<T, config>(srcTensor, sharedTmpBuffer, scaleTensor, offsetTensor);
    SetMaskCount();

    if constexpr(config.scaleCount != 0 && config.calcCount != 0) {
        AscendQuantImplStatic<T, config>(dstTensor, srcTensor, sharedTmpBuffer, scaleTensor, offsetTensor);
    } else {
        uint32_t N = calCount / scaleCount;
        if constexpr (IsSameType<T, float>::value) {
            SetVectorMask<half, MaskMode::COUNTER>(0, scaleCount);
            UnaryRepeatParams f162s8Param;
            f162s8Param.dstRepStride = HALF_DEFAULT_REPEAT_STRIDE;
            // source vector of scale is reused
            LocalTensor<half> halfScaleTensor =  scaleTensor.template ReinterpretCast<half>();
            Cast<half, float, false>(halfScaleTensor, scaleTensor, RoundMode::CAST_NONE, MASK_PLACEHOLDER, 1,
                f162s8Param);
            // source vector of offset is reused
            LocalTensor<half> halfOffsetTensor =  offsetTensor.template ReinterpretCast<half>();
            Cast<half, float, false>(halfOffsetTensor, offsetTensor, RoundMode::CAST_NONE,
                MASK_PLACEHOLDER, 1, f162s8Param);

            for (uint32_t i = 0; i < N; ++i) {
                AscendQuantPerChannelImpl<T, config>(dstTensor[i * scaleCount], srcTensor[i * scaleCount],
                    sharedTmpBuffer, halfScaleTensor, halfOffsetTensor, scaleCount);
            }
        } else {
            for (uint32_t i = 0; i < N; ++i) {
                AscendQuantPerChannelImpl<T, config>(dstTensor[i * scaleCount], srcTensor[i * scaleCount],
                    sharedTmpBuffer, scaleTensor, offsetTensor, scaleCount);
            }
        }
    }

    SetMaskNorm();
    ResetMask();
}
} //  namespace AscendC
#endif // IMPL_QUANTIZATION_QUANT_ASCEND_QUANT_V200_IMPL_H
