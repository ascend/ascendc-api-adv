/**
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

/* !
 * \file ascend_antiquant_tiling_impl.cpp
 * \brief
 */
#include "graph/tensor.h"
#include "graph/types.h"
#include "lib/quantization/ascend_antiquant_tiling.h"

namespace AscendC {
constexpr uint32_t ANTI_QUANT_MIN_TMP_SIZE = 1024;
constexpr uint32_t ASCEND_ANTIQUANT_TWO = 2;
constexpr uint32_t ASCEND_ANTIQUANT_SINGE_N_SIZE = 64;

uint32_t GetScaleSize(const ge::Shape &scaleShape)
{
    auto shapeDims = scaleShape.GetDims();
    uint32_t scaleSize = 1;
    for (uint32_t i = 0; i < shapeDims.size(); i++) {
        scaleSize *= shapeDims[i];
    }
    return scaleSize;
}

uint32_t GetAscendAntiQuantMaxTmpSize(const ge::Shape &srcShape, const ge::Shape &scaleShape, bool isTranspose,
    ge::DataType inputDataType, ge::DataType outputDataType)
{
    (void)inputDataType;
    if (outputDataType == ge::DT_FLOAT16) {
        return 0;
    }

    uint32_t scaleSize = GetScaleSize(scaleShape);
    auto shapeDims = srcShape.GetDims();
    uint32_t srcSize = 1;
    for (uint32_t i = 0; i < shapeDims.size(); i++) {
        srcSize *= shapeDims[i];
    }
    bool isPerChannel = (scaleSize == 1) ? false : true;

    if (isTranspose && isPerChannel) {
        uint32_t tmpTensorScaleSize = 8 * scaleSize * sizeof(float);     // 8  * N FP32
        uint32_t tmpTensorOffsetSize = 8 * scaleSize * sizeof(float);    // 8  * N FP32
        uint32_t tmpTensorInputSize = 64 * scaleSize * sizeof(float);    // 64 * N FP32
        return tmpTensorScaleSize + tmpTensorOffsetSize + tmpTensorInputSize;
    } else if (isTranspose && (!isPerChannel)) {
        return srcSize * sizeof(float);
    } else if ((!isTranspose) && isPerChannel) {
        uint32_t k = srcShape.GetDims()[0];
        return scaleSize * ASCEND_ANTIQUANT_TWO * sizeof(float) + ASCEND_ANTIQUANT_SINGE_N_SIZE * k * sizeof(float);
    } else {
        return srcSize * sizeof(float);
    }
}

uint32_t GetAscendAntiQuantMinTmpSize(const ge::Shape &srcShape, const ge::Shape &scaleShape, bool isTranspose,
    ge::DataType inputDataType, ge::DataType outputDataType)
{
    (void)inputDataType;
    if (outputDataType == ge::DT_FLOAT16) {
        return 0;
    }

    uint32_t scaleSize = GetScaleSize(scaleShape);
    bool isPerChannel = (scaleSize == 1) ? false : true;
    if (!isPerChannel) {
        return ANTI_QUANT_MIN_TMP_SIZE;
    }

    auto shapeDims = srcShape.GetDims();
    uint32_t srcSize = 1;
    for (uint32_t i = 0; i < shapeDims.size(); i++) {
        srcSize *= shapeDims[i];
    }

    if (isTranspose) {
        uint32_t tmpTensorScaleSize = 8 * scaleSize * sizeof(float);     // 8  * N FP32
        uint32_t tmpTensorOffsetSize = 8 * scaleSize * sizeof(float);    // 8  * N FP32
        uint32_t tmpTensorInputSize = 64 * scaleSize * sizeof(float);    // 64 * N FP32
        return tmpTensorScaleSize + tmpTensorOffsetSize + tmpTensorInputSize;
    } else {
        uint32_t k = srcShape.GetDims()[0];
        return scaleSize * ASCEND_ANTIQUANT_TWO * sizeof(float) + ASCEND_ANTIQUANT_SINGE_N_SIZE * k * sizeof(float);
    }
}

void GetAscendAntiQuantMaxMinTmpSize(const ge::Shape &srcShape, const ge::Shape &scaleShape, bool isTranspose,
    ge::DataType inputDataType, ge::DataType outputDataType, uint32_t &maxValue, uint32_t &minValue)
{
    maxValue = GetAscendAntiQuantMaxTmpSize(srcShape, scaleShape, isTranspose, inputDataType, outputDataType);
    minValue = GetAscendAntiQuantMinTmpSize(srcShape, scaleShape, isTranspose, inputDataType, outputDataType);
}
} // namespace AscendC