/**
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

#include "lib/normalization/deepnorm_tiling.h"

namespace optiling {
REGISTER_TILING_DATA_CLASS(DeepNormTilingOpApi, DeepNormTiling);
} // namespace optiling
namespace AscendC {
namespace {
constexpr uint32_t ONE_BLK_SIZE = 32;
constexpr uint32_t ONE_BLOCK_NUM = ONE_BLK_SIZE / sizeof(float);  // 1 block = 32 bytes, contains 8 FP32
constexpr uint32_t DEEPNORM_THREE_TIMES = 3;                      // normal constant 3
constexpr uint32_t DEEPNORM_TWO_TIMES = 2;                        // normal constant 2
constexpr uint32_t DEEPNORM_MAX_REPEAT = 255;                     // uint8_t range [0, 255]
constexpr float DEEPNORM_LAST_DIM_INIT_VALUE = 1.0;               // reciprocal base

constexpr uint32_t BASIC_BLK_HLENGTH = 64;            // basic block H must be 64
constexpr uint32_t BASIC_BLK_BSLENGTH = 8;            // basic block B*S must be divisible by 64

// ceil(dataAmount / blockSize) * blockSize      blocknum * blockSize needed to fill in dataAmount
uint32_t GetFinalBlockSize(uint32_t dataAmount, uint32_t blockSize)
{
    return (dataAmount + blockSize - 1) / blockSize * blockSize;
}

uint32_t GetDeepNormMaxTmpSize(const ge::Shape& srcShape, const uint32_t typeSize, const bool isReuseSource)
{
    std::vector<int64_t> shapeDims = srcShape.GetDims();
    const uint32_t bLength = static_cast<uint32_t>(shapeDims[0]);
    const uint32_t sLength = static_cast<uint32_t>(shapeDims[1]);
    const uint32_t hLength = static_cast<uint32_t>(shapeDims[2]);

    // applied tensors are all FP32
    uint32_t inputBSsize = bLength * sLength * sizeof(float);
    uint32_t inputBSHsize = bLength * sLength * hLength * sizeof(float);
    inputBSsize = GetFinalBlockSize(inputBSsize, ONE_BLK_SIZE);
    inputBSHsize = GetFinalBlockSize(inputBSHsize, ONE_BLK_SIZE);

    // copy one temp BSH tensor to output Tensor
    if (isReuseSource && (typeSize == sizeof(float))) {
        return DEEPNORM_TWO_TIMES * inputBSHsize + DEEPNORM_TWO_TIMES * inputBSsize;
    }
    return DEEPNORM_THREE_TIMES * inputBSHsize + DEEPNORM_TWO_TIMES * inputBSsize;
}

uint32_t GetDeepNormMinTmpSize(const ge::Shape& srcShape, const uint32_t typeSize, const bool isReuseSource,
    const bool isBasicBlock)
{
    (void)isBasicBlock;
    std::vector<int64_t> shapeDims = srcShape.GetDims();
    const uint32_t bLength = static_cast<uint32_t>(shapeDims[0]);
    const uint32_t sLength = static_cast<uint32_t>(shapeDims[1]);
    const uint32_t hLength = static_cast<uint32_t>(shapeDims[2]);

    // applied tensors are all FP32
    uint32_t inputBSsize = bLength * sLength;
    uint32_t inputHsize = hLength;
    inputBSsize = GetFinalBlockSize(inputBSsize * sizeof(float), ONE_BLK_SIZE);
    inputHsize = GetFinalBlockSize(inputHsize * sizeof(float), ONE_BLK_SIZE);

    if (isReuseSource && (typeSize == sizeof(float))) {
        return DEEPNORM_TWO_TIMES * inputHsize + DEEPNORM_TWO_TIMES * inputBSsize;
    }
    return DEEPNORM_THREE_TIMES * inputHsize + DEEPNORM_TWO_TIMES * inputBSsize;
}
} // namespace

bool GetDeepNormMaxMinTmpSize(const ge::Shape& srcShape, const uint32_t typeSize, const bool isReuseSource,
    const bool isBasicBlock, uint32_t& maxValue, uint32_t& minValue)
{
    std::vector<int64_t> shapeDims = srcShape.GetDims();
    const uint32_t bLength = static_cast<uint32_t>(shapeDims[0]);
    const uint32_t sLength = static_cast<uint32_t>(shapeDims[1]);
    const uint32_t hLength = static_cast<uint32_t>(shapeDims[2]);

    // if basic block case   check   1. hlength must be divisible by 64   2. B*S must be divisible by 8
    bool hDivBy64 = (hLength % BASIC_BLK_HLENGTH == 0) && (hLength > 0);
    bool bsDivBy8 = (bLength * sLength) % BASIC_BLK_BSLENGTH == 0;
    if (isBasicBlock && !(hDivBy64 && bsDivBy8)) {
        return false;
    }

    maxValue = GetDeepNormMaxTmpSize(srcShape, typeSize, isReuseSource);
    minValue = GetDeepNormMinTmpSize(srcShape, typeSize, isReuseSource, isBasicBlock);
    return true;
}

bool GetDeepNormTilingInfo(const ge::Shape& srcShape, const ge::Shape& originSrcShape, const uint32_t stackBufferSize,
    const uint32_t typeSize, const bool isReuseSource, const bool isBasicBlock, optiling::DeepNormTiling& tiling)
{
    uint32_t maxSize = 0;
    uint32_t minSize = 0;
    // allocated buffer must be at least minSize
    const bool res = GetDeepNormMaxMinTmpSize(srcShape, typeSize, isReuseSource, isBasicBlock, maxSize, minSize);
    if (!res || stackBufferSize < minSize) {
        return false;
    }

    std::vector<int64_t> shapeDims = srcShape.GetDims();
    const uint32_t bLength = static_cast<uint32_t>(shapeDims[0]);
    const uint32_t sLength = static_cast<uint32_t>(shapeDims[1]);
    const uint32_t hLength = static_cast<uint32_t>(shapeDims[2]);            // H after alignment

    std::vector<int64_t> shapeDimsOri = originSrcShape.GetDims();
    const uint32_t bLengthOri = static_cast<uint32_t>(shapeDimsOri[0]);
    const uint32_t sLengthOri = static_cast<uint32_t>(shapeDimsOri[1]);
    const uint32_t originalHLength = static_cast<uint32_t>(shapeDimsOri[2]); // H before alignment

    bool bsCheck = (bLength == bLengthOri) && (sLength == sLengthOri);       // check originb = b, origins = s
    bool hAlign = (hLength * typeSize % ONE_BLK_SIZE) == 0;                  // Hlength must align with 32 bytes
    bool hCheckOri = (0u < originalHLength  && originalHLength <= hLength);  // oriHLength must be in range (0, Hlength]
    bool basicblkOriH = !(isBasicBlock && originalHLength != hLength);       // when basicblock, it must be oriH = H
    bool hLengthCheck = (hLength <= DEEPNORM_MAX_REPEAT * 8);      // one addimpl has uint8_t repeatStride = hLength / 8

    if (!(bsCheck && hAlign && hCheckOri && basicblkOriH && hLengthCheck)) {
        return false;
    }

    const uint32_t inputXSize = bLength * sLength * hLength;
    const uint32_t meanVarSize = bLength * sLength;

    // if reuse, one tmp buffer can be replaced by output tensor
    uint32_t numberOfTmpBuf = (isReuseSource && (typeSize == sizeof(float)))? DEEPNORM_TWO_TIMES: DEEPNORM_THREE_TIMES;

    // mean and variance are both B * S
    constexpr uint32_t meanTmpTensorPos = 0;
    const uint32_t meanTmpTensorSize = GetFinalBlockSize(meanVarSize, ONE_BLOCK_NUM);
    const uint32_t varianceTmpTensorPos = meanTmpTensorSize;
    const uint32_t varianceTmpTensorSize = meanTmpTensorSize;

    uint32_t meanVarTotalSize = (typeSize == sizeof(float))? 0 : meanTmpTensorSize + varianceTmpTensorSize;
    const uint32_t tmpBufSize = stackBufferSize / sizeof(float);    // how many FP32 tmpBuffer can store in total
    uint32_t oneTmpSize = (tmpBufSize - meanVarTotalSize) / numberOfTmpBuf;  // FP32 num for each H tensor tmpTensorABC
    // basicBlock also means tmpBuffer must be n * hLength
    oneTmpSize = oneTmpSize / hLength * hLength;                             // floor(hLength)
    oneTmpSize = (oneTmpSize > inputXSize) ? inputXSize : oneTmpSize;
    // assume that tmpSize is 9*n, for basicblock, use 8*n is better
    if (isBasicBlock && (oneTmpSize / (BASIC_BLK_BSLENGTH * hLength) > 0)) {
        oneTmpSize = oneTmpSize / (BASIC_BLK_BSLENGTH * hLength) * (BASIC_BLK_BSLENGTH * hLength);
    }

    // stackBufferSize cannot allocate enough space for tmpTensor A, B, C
    if (oneTmpSize == 0) {
        return false;
    }

    const uint32_t firstTmpStartPos = meanVarTotalSize;                  // tmpTensorA
    const uint32_t secondTmpStartPos = firstTmpStartPos + oneTmpSize;    // tmpTensorB
    const uint32_t thirdTmpStartPos = secondTmpStartPos + oneTmpSize;    // tmpTensorC

    const uint32_t loopRound = inputXSize / oneTmpSize;
    const uint32_t inputTailSize = inputXSize % oneTmpSize;
    const uint32_t inputTailPos = inputXSize - inputTailSize;
    const uint32_t inputRoundSize = oneTmpSize;                      // B * S * H every round
    const uint32_t meanVarRoundSize = inputRoundSize / hLength;      // B * S every round
    const uint32_t meanVarTailSize = inputTailSize / hLength;
    const uint32_t meanVarTailPos = meanVarSize - meanVarTailSize;

    const uint32_t bshCurLength = inputRoundSize;
    const uint32_t bsCurLength = meanVarRoundSize;

    const float lastDimValueBack = DEEPNORM_LAST_DIM_INIT_VALUE / static_cast<float>(originalHLength);

    tiling.set_bLength(bLength);
    tiling.set_sLength(sLength);
    tiling.set_hLength(hLength);
    tiling.set_originalHLength(originalHLength);
    tiling.set_inputXSize(inputXSize);
    tiling.set_meanVarSize(meanVarSize);
    tiling.set_numberOfTmpBuf(numberOfTmpBuf);
    tiling.set_meanTmpTensorPos(meanTmpTensorPos);
    tiling.set_meanTmpTensorSize(meanTmpTensorSize);
    tiling.set_varianceTmpTensorPos(varianceTmpTensorPos);
    tiling.set_varianceTmpTensorSize(varianceTmpTensorSize);
    tiling.set_tmpBufSize(tmpBufSize);
    tiling.set_oneTmpSize(oneTmpSize);
    tiling.set_firstTmpStartPos(firstTmpStartPos);
    tiling.set_secondTmpStartPos(secondTmpStartPos);
    tiling.set_thirdTmpStartPos(thirdTmpStartPos);
    tiling.set_loopRound(loopRound);
    tiling.set_inputRoundSize(inputRoundSize);
    tiling.set_inputTailSize(inputTailSize);
    tiling.set_inputTailPos(inputTailPos);
    tiling.set_meanVarRoundSize(meanVarRoundSize);
    tiling.set_meanVarTailSize(meanVarTailSize);
    tiling.set_meanVarTailPos(meanVarTailPos);
    tiling.set_bshCurLength(bshCurLength);
    tiling.set_bsCurLength(bsCurLength);
    tiling.set_lastDimValueBack(lastDimValueBack);
    return true;
}
} // namespace AscendC