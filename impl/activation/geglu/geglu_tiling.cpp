/**
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

/* !
 * \file geglu_tiling.cpp
 * \brief
 */
#include "lib/activation/geglu_tiling.h"

#include <cstdint>

#include "graph/tensor.h"
#include "impl/host_log.h"
namespace AscendC {
namespace {
constexpr uint32_t GEGLU_ONE_REPEAT_BYTE_SIZE = 256;
constexpr uint32_t GEGLU_HALF_CALC_FAC = 4;
constexpr uint32_t GEGLU_FLOAT_CALC_FAC = 0;

inline uint32_t GetGeGLUMaxTmpSize(const uint32_t inputSize, const uint32_t typeSize)
{
    const uint32_t calcPro = typeSize == sizeof(float) ? GEGLU_FLOAT_CALC_FAC : GEGLU_HALF_CALC_FAC;
    return calcPro * std::max(inputSize * typeSize, GEGLU_ONE_REPEAT_BYTE_SIZE);
}

inline uint32_t GetGeGLUMinTmpSize(const uint32_t typeSize)
{
    return typeSize == sizeof(float) ? GEGLU_FLOAT_CALC_FAC * GEGLU_ONE_REPEAT_BYTE_SIZE :
                                       GEGLU_HALF_CALC_FAC * GEGLU_ONE_REPEAT_BYTE_SIZE;
}
} // namespace

void GetGeGLUMaxMinTmpSize(const ge::Shape &srcShape, const uint32_t typeSize, const bool isReuseSource,
    uint32_t &maxValue, uint32_t &minValue)
{
    (void)isReuseSource;
    const uint32_t inputSize = srcShape.GetShapeSize();
    ASCENDC_HOST_ASSERT(inputSize > 0, return, "Input Shape size must be greater than 0.");

    minValue = GetGeGLUMinTmpSize(typeSize);
    maxValue = GetGeGLUMaxTmpSize(inputSize, typeSize);
}

void GetGeGLUTmpBufferFactorSize(const uint32_t typeSize, uint32_t &maxLiveNodeCnt, uint32_t &extraBuf)
{
    extraBuf = 0;
    maxLiveNodeCnt = (typeSize == sizeof(float)) ? GEGLU_FLOAT_CALC_FAC : GEGLU_HALF_CALC_FAC;
}
} // namespace AscendC
