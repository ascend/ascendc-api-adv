/**
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

/* !
 * \file softmax_tiling.cpp
 * \brief
 */
#include "lib/activation/softmax_tilingdata.h"
#include "lib/activation/softmax_tiling.h"
#include "impl/host_log.h"

namespace optiling {
REGISTER_TILING_DATA_CLASS(SoftMaxTilingOpApi, SoftMaxTiling)
}
namespace AscendC {
constexpr uint32_t SOFTMAX_DEFAULT_BLK_SIZE = 32;
constexpr uint32_t SOFTMAX_TMPBUFFER_COUNT = 2;
constexpr uint32_t SOFTMAX_TMPFLASHUPDATE_COUNT = 4;
constexpr uint32_t SOFTMAX_HALF_SIZE = 2;
constexpr uint32_t SOFTMAX_FLOAT_SIZE = 4;
constexpr uint32_t SOFTMAXGRAD_TMPBUFFER_COUNT = 3;
constexpr uint32_t BASIC_TILE_NUM = SOFTMAX_DEFAULT_BLK_SIZE / SOFTMAX_FLOAT_SIZE;
constexpr uint32_t SOFTMAX_BASICBLOCK_MIN_SIZE = 256;
constexpr uint32_t SOFTMAX_BASICBLOCK_UNIT = 64;
constexpr uint32_t SOFTMAX_SPECIAL_BASICBLOCK_LEN = SOFTMAX_BASICBLOCK_MIN_SIZE * SOFTMAX_FLOAT_SIZE;
constexpr uint32_t SOFTMAXV3_TMPBUFFER_COUNT = 5;
#define UNUSED __attribute__((unused))

inline std::vector<uint32_t> GetLastAxisShapeND(const ge::Shape& srcShape)
{
    std::vector<uint32_t> ret;
    std::vector<int64_t> shapeDims = srcShape.GetDims();
    uint32_t calculateSize = 1;
    for (uint32_t i = 0; i < shapeDims.size(); i++) {
        calculateSize *= shapeDims[i];
    }

    const uint32_t srcK = shapeDims.back();
    uint32_t srcM = calculateSize / srcK;
    ret = { srcM, srcK };
    return ret;
}

inline void AdjustToBasicBlockBaseM(uint32_t& baseM, const uint32_t srcM, const uint32_t srcK)
{
    if (baseM > BASIC_TILE_NUM && srcM % BASIC_TILE_NUM == 0 && srcK % SOFTMAX_BASICBLOCK_UNIT == 0) { // basicblock
        baseM = baseM / BASIC_TILE_NUM * BASIC_TILE_NUM;
        while (srcM % baseM != 0) {
            baseM -= BASIC_TILE_NUM;
        }
        // max repeat only support 255
        while (baseM * srcK >= SOFTMAX_BASICBLOCK_UNIT * SOFTMAX_BASICBLOCK_MIN_SIZE) {
            baseM = baseM / SOFTMAX_HALF_SIZE;
        }
    }
}

uint32_t GetSoftMaxMaxTmpSize(const ge::Shape& srcShape, const uint32_t dataTypeSize, UNUSED const bool isReuseSource)
{
    std::vector<uint32_t> retVec = GetLastAxisShapeND(srcShape);
    // the softmax shape size must be 2
    if (retVec.size() <= 1 || dataTypeSize == 0) {
        return 0;
    }
    const uint32_t srcM = retVec[0];
    const uint32_t srcK = retVec[1];
    const uint32_t elementNumPerBlk = SOFTMAX_DEFAULT_BLK_SIZE / dataTypeSize;
    uint32_t needSize = srcM * (elementNumPerBlk + srcK + SOFTMAX_BASICBLOCK_UNIT);
    return needSize * SOFTMAX_FLOAT_SIZE;
}

uint32_t GetSoftMaxMinTmpSize(const ge::Shape& srcShape, const uint32_t dataTypeSize, UNUSED const bool isReuseSource)
{
    std::vector<uint32_t> retVec = GetLastAxisShapeND(srcShape);
    // the softmax shape size must be 2
    if (retVec.size() <= 1 || dataTypeSize == 0) {
        return 0;
    }
    const uint32_t srcK = retVec[1];
    const uint32_t elementNumPerBlk = SOFTMAX_DEFAULT_BLK_SIZE / dataTypeSize;
    uint32_t needSize = elementNumPerBlk + srcK + SOFTMAX_BASICBLOCK_UNIT;
    return needSize * SOFTMAX_FLOAT_SIZE;
}

void SoftMaxTilingFunc(const ge::Shape& srcShape, const uint32_t dataTypeSize, const uint32_t localWorkSpaceSize,
    optiling::SoftMaxTiling& softmaxTiling)
{
    std::vector<uint32_t> retVec = GetLastAxisShapeND(srcShape);
    if (retVec.size() <= 1 || dataTypeSize == 0) {
        return;
    }
    const uint32_t elementNumPerBlk = SOFTMAX_DEFAULT_BLK_SIZE / dataTypeSize;
    const uint32_t workLocalSize = localWorkSpaceSize / SOFTMAX_FLOAT_SIZE;
    const uint32_t srcK = retVec[1];
    const uint32_t srcM = retVec[0];
    uint32_t baseM = std::min(workLocalSize / (elementNumPerBlk + srcK + SOFTMAX_BASICBLOCK_UNIT), srcM);
    if (baseM < srcM && baseM > BASIC_TILE_NUM) {
        baseM = baseM / BASIC_TILE_NUM * BASIC_TILE_NUM;
    }

    AdjustToBasicBlockBaseM(baseM, srcM, srcK);

    softmaxTiling.set_srcM(srcM);
    softmaxTiling.set_srcK(srcK);
    softmaxTiling.set_srcSize(srcM * srcK);

    softmaxTiling.set_outMaxM(srcM);             // output dstMax
    softmaxTiling.set_outMaxK(elementNumPerBlk); // output dstMax
    softmaxTiling.set_outMaxSize(srcM * elementNumPerBlk);

    softmaxTiling.set_splitM(baseM);
    softmaxTiling.set_splitK(srcK);
    softmaxTiling.set_splitSize(baseM * srcK);

    softmaxTiling.set_reduceM(baseM);
    softmaxTiling.set_reduceK(elementNumPerBlk);
    softmaxTiling.set_reduceSize(baseM * elementNumPerBlk);

    const uint32_t range = srcM / baseM;
    uint32_t tail = srcM % baseM;
    softmaxTiling.set_rangeM(range);
    softmaxTiling.set_tailM(tail);

    softmaxTiling.set_tailSplitSize(tail * srcK);
    softmaxTiling.set_tailReduceSize(tail * elementNumPerBlk);
}

uint32_t GetSoftMaxFlashMaxTmpSize(const ge::Shape& srcShape, const uint32_t dataTypeSize, const bool isUpdate,
    UNUSED const bool isReuseSource)
{
    std::vector<uint32_t> retVec = GetLastAxisShapeND(srcShape);
    if (retVec.size() <= 1 || dataTypeSize == 0) {
        return 0;
    }
    const uint32_t srcM = retVec[0];
    const uint32_t srcK = retVec[1];
    const uint32_t elementNumPerBlk = SOFTMAX_DEFAULT_BLK_SIZE / dataTypeSize;
    uint32_t needSize = 0;
    needSize = !isUpdate ? srcM * (elementNumPerBlk + srcK + SOFTMAX_BASICBLOCK_UNIT) :
                           srcM * (elementNumPerBlk * SOFTMAX_TMPFLASHUPDATE_COUNT + srcK * SOFTMAX_TMPBUFFER_COUNT);

    return needSize * SOFTMAX_FLOAT_SIZE;
}

uint32_t GetSoftMaxFlashMinTmpSize(const ge::Shape& srcShape, const uint32_t dataTypeSize, const bool isUpdate,
    UNUSED const bool isReuseSource)
{
    std::vector<uint32_t> retVec = GetLastAxisShapeND(srcShape);
    if (retVec.size() <= 1 || dataTypeSize == 0) {
        return 0;
    }
    const uint32_t srcK = retVec[1];
    const uint32_t elementNumPerBlk = SOFTMAX_DEFAULT_BLK_SIZE / dataTypeSize;
    uint32_t needSize = 0;

    needSize = !isUpdate ? elementNumPerBlk + srcK + SOFTMAX_BASICBLOCK_UNIT :
                           elementNumPerBlk * SOFTMAX_TMPFLASHUPDATE_COUNT + srcK * SOFTMAX_TMPBUFFER_COUNT;

    return needSize * SOFTMAX_FLOAT_SIZE;
}

void SoftMaxFlashTilingFunc(const ge::Shape& srcShape, const uint32_t dataTypeSize, const uint32_t localWorkSpaceSize,
    optiling::SoftMaxTiling& softmaxFlashTiling, const bool isUpdate)
{
    std::vector<uint32_t> retVec = GetLastAxisShapeND(srcShape);
    if (retVec.size() <= 1 || dataTypeSize == 0) {
        return;
    }
    const uint32_t elementNumPerBlk = SOFTMAX_DEFAULT_BLK_SIZE / dataTypeSize;
    const uint32_t workLocalSize = localWorkSpaceSize / SOFTMAX_FLOAT_SIZE;
    const uint32_t srcK = retVec[1];
    const uint32_t srcM = retVec[0];
    uint32_t baseM = 0;
    baseM = !isUpdate ?
        workLocalSize / (elementNumPerBlk + srcK + SOFTMAX_BASICBLOCK_UNIT) :
        workLocalSize / (elementNumPerBlk * SOFTMAX_TMPFLASHUPDATE_COUNT + srcK * SOFTMAX_TMPBUFFER_COUNT);
    baseM = std::min(baseM, srcM);
    if (baseM < srcM && baseM > BASIC_TILE_NUM) {
        baseM = baseM / BASIC_TILE_NUM * BASIC_TILE_NUM;
    }

    AdjustToBasicBlockBaseM(baseM, srcM, srcK);

    softmaxFlashTiling.set_srcM(srcM);
    softmaxFlashTiling.set_srcK(srcK);
    softmaxFlashTiling.set_srcSize(srcM * srcK);

    softmaxFlashTiling.set_outMaxM(srcM);             // output dstMax
    softmaxFlashTiling.set_outMaxK(elementNumPerBlk); // output dstMax
    softmaxFlashTiling.set_outMaxSize(srcM * elementNumPerBlk);

    softmaxFlashTiling.set_splitM(baseM);
    softmaxFlashTiling.set_splitK(srcK);
    softmaxFlashTiling.set_splitSize(baseM * srcK);

    softmaxFlashTiling.set_reduceM(baseM);
    softmaxFlashTiling.set_reduceK(elementNumPerBlk);
    softmaxFlashTiling.set_reduceSize(baseM * elementNumPerBlk);

    uint32_t range = srcM / baseM;
    uint32_t tail = srcM % baseM;
    softmaxFlashTiling.set_rangeM(range);
    softmaxFlashTiling.set_tailM(tail);

    softmaxFlashTiling.set_tailSplitSize(tail * srcK);
    softmaxFlashTiling.set_tailReduceSize(tail * elementNumPerBlk);
}

uint32_t GetSoftMaxGradMaxTmpSize(const ge::Shape& srcShape, const uint32_t dataTypeSize, const bool isFront,
    UNUSED const bool isReuseSource)
{
    std::vector<uint32_t> retVec = GetLastAxisShapeND(srcShape);
    if (retVec.size() <= 1 || dataTypeSize == 0) {
        return 0;
    }
    const uint32_t srcM = retVec[0];
    const uint32_t srcK = retVec[1];
    const uint32_t elementNumPerBlk = SOFTMAX_DEFAULT_BLK_SIZE / dataTypeSize;
    uint32_t needSize = 0;
    if (dataTypeSize == SOFTMAX_HALF_SIZE) {
        needSize = srcM *
            (elementNumPerBlk * SOFTMAX_TMPBUFFER_COUNT + srcK * SOFTMAXGRAD_TMPBUFFER_COUNT + SOFTMAX_BASICBLOCK_UNIT);
    } else {
        needSize = isFront ? srcM * (elementNumPerBlk + srcK + SOFTMAX_BASICBLOCK_UNIT) :
                             srcM * (elementNumPerBlk * SOFTMAX_TMPBUFFER_COUNT + srcK + SOFTMAX_BASICBLOCK_UNIT);
    }
    return needSize * SOFTMAX_FLOAT_SIZE;
}

uint32_t GetSoftMaxGradMinTmpSize(const ge::Shape& srcShape, const uint32_t dataTypeSize, const bool isFront,
    UNUSED const bool isReuseSource)
{
    std::vector<uint32_t> retVec = GetLastAxisShapeND(srcShape);
    if (retVec.size() <= 1 || dataTypeSize == 0) {
        return 0;
    }
    const uint32_t srcK = retVec[1];
    const uint32_t elementNumPerBlk = SOFTMAX_DEFAULT_BLK_SIZE / dataTypeSize;
    uint32_t needSize = 0;
    if (dataTypeSize == SOFTMAX_HALF_SIZE) {
        needSize =
            elementNumPerBlk * SOFTMAX_TMPBUFFER_COUNT + srcK * SOFTMAXGRAD_TMPBUFFER_COUNT + SOFTMAX_BASICBLOCK_UNIT;
    } else {
        needSize = isFront ? elementNumPerBlk + srcK + SOFTMAX_BASICBLOCK_UNIT :
                             elementNumPerBlk * SOFTMAX_TMPBUFFER_COUNT + srcK + SOFTMAX_BASICBLOCK_UNIT;
    }
    return needSize * SOFTMAX_FLOAT_SIZE;
}

void SoftMaxGradTilingFunc(const ge::Shape& srcShape, const uint32_t dataTypeSize, const uint32_t localWorkSpaceSize,
    optiling::SoftMaxTiling& softmaxGradTiling, const bool isFront)
{
    std::vector<uint32_t> retVec = GetLastAxisShapeND(srcShape);
    if (retVec.size() <= 1 || dataTypeSize == 0) {
        return;
    }
    const uint32_t elementNumPerBlk = SOFTMAX_DEFAULT_BLK_SIZE / dataTypeSize;
    const uint32_t workLocalSize = localWorkSpaceSize / SOFTMAX_FLOAT_SIZE;
    const uint32_t srcK = retVec[1];
    const uint32_t srcM = retVec[0];
    uint32_t baseM = 0;
    if (dataTypeSize == SOFTMAX_HALF_SIZE) {
        baseM = workLocalSize /
            (elementNumPerBlk * SOFTMAX_TMPBUFFER_COUNT + srcK * SOFTMAXGRAD_TMPBUFFER_COUNT + SOFTMAX_BASICBLOCK_UNIT);
    } else {
        baseM = isFront ? workLocalSize / (elementNumPerBlk + srcK + SOFTMAX_BASICBLOCK_UNIT) :
                          workLocalSize / (elementNumPerBlk * SOFTMAX_TMPBUFFER_COUNT + srcK + SOFTMAX_BASICBLOCK_UNIT);
    }

    baseM = std::min(baseM, srcM);
    if (baseM < srcM && baseM > BASIC_TILE_NUM) {
        baseM = baseM / BASIC_TILE_NUM * BASIC_TILE_NUM;
    }

    AdjustToBasicBlockBaseM(baseM, srcM, srcK);

    softmaxGradTiling.set_srcM(srcM);
    softmaxGradTiling.set_srcK(srcK);
    softmaxGradTiling.set_srcSize(srcM * srcK);

    softmaxGradTiling.set_outMaxM(srcM);
    softmaxGradTiling.set_outMaxK(elementNumPerBlk);
    softmaxGradTiling.set_outMaxSize(srcM * elementNumPerBlk);

    softmaxGradTiling.set_splitM(baseM);
    softmaxGradTiling.set_splitK(srcK);
    softmaxGradTiling.set_splitSize(baseM * srcK);

    softmaxGradTiling.set_reduceM(baseM);
    softmaxGradTiling.set_reduceK(elementNumPerBlk);
    softmaxGradTiling.set_reduceSize(baseM * elementNumPerBlk);

    uint32_t range = srcM / baseM;
    const uint32_t tail = srcM % baseM;
    softmaxGradTiling.set_rangeM(range);
    softmaxGradTiling.set_tailM(tail);

    softmaxGradTiling.set_tailSplitSize(tail * srcK);
    softmaxGradTiling.set_tailReduceSize(tail * elementNumPerBlk);
}

bool IsBasicBlockInSoftMax(optiling::SoftMaxTiling& tiling, const uint32_t dataTypeSize)
{
    constexpr uint32_t SOFTMAX_BASICBLOCK_MAX_SIZE = 2048;
    if (dataTypeSize == 0) {
        return false;
    }
    const uint32_t srcK = tiling.get_srcK();
    const uint32_t tailM = tiling.get_tailM();
    if (tailM == 0 && srcK >= (SOFTMAX_BASICBLOCK_MIN_SIZE / dataTypeSize) && srcK < SOFTMAX_BASICBLOCK_MAX_SIZE &&
        srcK % SOFTMAX_BASICBLOCK_UNIT == 0) {
        return true;
    } else {
        return false;
    }
}

uint32_t GetSoftMaxFlashV2MaxTmpSize(const ge::Shape& srcShape, const uint32_t dataTypeSize1,
    const uint32_t dataTypeSize2, UNUSED const bool isUpdate, const bool isBasicBlock,
    UNUSED const bool isFlashOutputBrc)
{
    std::vector<uint32_t> retVec = GetLastAxisShapeND(srcShape);
    if (retVec.size() <= 1 || dataTypeSize1 == 0 || dataTypeSize2 == 0) {
        return 0;
    }
    uint32_t srcM = retVec[0];
    const uint32_t srcK = retVec[1];
    const uint32_t elementNumPerBlk = SOFTMAX_DEFAULT_BLK_SIZE / dataTypeSize2;
    uint32_t needMaxSize = 0;

    if (isBasicBlock && srcK % SOFTMAX_BASICBLOCK_UNIT == 0 && srcM % BASIC_TILE_NUM == 0) {
        // max repeat only support 255
        while (srcM * srcK >= SOFTMAX_BASICBLOCK_UNIT * SOFTMAX_BASICBLOCK_MIN_SIZE) {
            srcM = srcM / SOFTMAX_HALF_SIZE;
        }
        if (dataTypeSize1 == SOFTMAX_HALF_SIZE) {
            needMaxSize = (srcK == SOFTMAX_SPECIAL_BASICBLOCK_LEN) ?
                srcM * (elementNumPerBlk + srcK + SOFTMAX_BASICBLOCK_UNIT * SOFTMAX_HALF_SIZE) :
                srcM * (elementNumPerBlk + srcK + SOFTMAX_BASICBLOCK_UNIT);
        } else {
            needMaxSize = (srcK == SOFTMAX_SPECIAL_BASICBLOCK_LEN) ?
                srcM * (elementNumPerBlk + SOFTMAX_BASICBLOCK_UNIT * SOFTMAX_HALF_SIZE) :
                srcM * (elementNumPerBlk + SOFTMAX_BASICBLOCK_UNIT);
        }
    } else {
        needMaxSize = (dataTypeSize1 == SOFTMAX_HALF_SIZE) ?
            srcM * (elementNumPerBlk * SOFTMAX_TMPBUFFER_COUNT + srcK + SOFTMAX_BASICBLOCK_UNIT) :
            srcM * (elementNumPerBlk + SOFTMAX_BASICBLOCK_UNIT);
    }

    return needMaxSize * SOFTMAX_FLOAT_SIZE;
}

uint32_t GetSoftMaxFlashV2MinTmpSize(const ge::Shape& srcShape, const uint32_t dataTypeSize1,
    const uint32_t dataTypeSize2, UNUSED const bool isUpdate, const bool isBasicBlock, const bool isFlashOutputBrc)
{
    std::vector<uint32_t> retVec = GetLastAxisShapeND(srcShape);
    if (retVec.size() <= 1 || dataTypeSize1 == 0 || dataTypeSize2 == 0) {
        return 0;
    }
    const uint32_t srcM = retVec[0];
    const uint32_t srcK = retVec[1];
    const uint32_t elementNumPerBlk = SOFTMAX_DEFAULT_BLK_SIZE / dataTypeSize2;
    uint32_t needMinSize = 0;

    if (isBasicBlock && srcK % SOFTMAX_BASICBLOCK_UNIT == 0) {
        if (dataTypeSize1 == SOFTMAX_HALF_SIZE) {
            needMinSize = (srcK == SOFTMAX_SPECIAL_BASICBLOCK_LEN) ?
                BASIC_TILE_NUM * (elementNumPerBlk + srcK + SOFTMAX_BASICBLOCK_UNIT * SOFTMAX_HALF_SIZE) :
                BASIC_TILE_NUM * (elementNumPerBlk + srcK + SOFTMAX_BASICBLOCK_UNIT);
        } else {
            needMinSize = (srcK == SOFTMAX_SPECIAL_BASICBLOCK_LEN) ?
                BASIC_TILE_NUM * (elementNumPerBlk + SOFTMAX_BASICBLOCK_UNIT * SOFTMAX_HALF_SIZE) :
                BASIC_TILE_NUM * (elementNumPerBlk + SOFTMAX_BASICBLOCK_UNIT);
        }
    } else {
        needMinSize = (dataTypeSize1 == SOFTMAX_HALF_SIZE) ?
            elementNumPerBlk * SOFTMAX_TMPBUFFER_COUNT + srcK + SOFTMAX_BASICBLOCK_UNIT :
            elementNumPerBlk + SOFTMAX_BASICBLOCK_UNIT;
    }

    if (isFlashOutputBrc) {
        if (isBasicBlock && srcK % SOFTMAX_BASICBLOCK_UNIT == 0) {
            needMinSize = (needMinSize / BASIC_TILE_NUM) * std::min((SOFTMAX_DEFAULT_BLK_SIZE / dataTypeSize1), srcM);
        } else {
            needMinSize = needMinSize * std::min((SOFTMAX_DEFAULT_BLK_SIZE / dataTypeSize1), srcM);
        }
    }

    return needMinSize * SOFTMAX_FLOAT_SIZE;
}

void SoftMaxFlashV2TilingFunc(const ge::Shape& srcShape, const uint32_t dataTypeSize1, const uint32_t dataTypeSize2,
    const uint32_t localWorkSpaceSize, optiling::SoftMaxTiling& softmaxFlashTiling, UNUSED const bool isUpdate,
    const bool isBasicBlock, const bool isFlashOutputBrc)
{
    std::vector<uint32_t> retVec = GetLastAxisShapeND(srcShape);
    if (retVec.size() <= 1 || dataTypeSize1 == 0 || dataTypeSize2 == 0) {
        return;
    }
    const uint32_t elementNumPerBlk = SOFTMAX_DEFAULT_BLK_SIZE / dataTypeSize2;
    const uint32_t workLocalSize = localWorkSpaceSize / SOFTMAX_FLOAT_SIZE;
    const uint32_t srcK = retVec[1];
    const uint32_t srcM = retVec[0];
    uint32_t baseM = 0;

    if (isBasicBlock && srcK % SOFTMAX_BASICBLOCK_UNIT == 0 && srcM % BASIC_TILE_NUM == 0) {
        if (dataTypeSize1 == SOFTMAX_HALF_SIZE) {
            baseM = (srcK == SOFTMAX_SPECIAL_BASICBLOCK_LEN) ?
                workLocalSize / (elementNumPerBlk + srcK + SOFTMAX_BASICBLOCK_UNIT * SOFTMAX_HALF_SIZE) :
                workLocalSize / (elementNumPerBlk + srcK + SOFTMAX_BASICBLOCK_UNIT);
        } else {
            baseM = (srcK == SOFTMAX_SPECIAL_BASICBLOCK_LEN) ?
                workLocalSize / (elementNumPerBlk + SOFTMAX_BASICBLOCK_UNIT * SOFTMAX_HALF_SIZE) :
                workLocalSize / (elementNumPerBlk + SOFTMAX_BASICBLOCK_UNIT);
        }

        baseM = std::min(baseM, srcM);
        AdjustToBasicBlockBaseM(baseM, srcM, srcK);
    } else {
        baseM = (dataTypeSize1 == SOFTMAX_HALF_SIZE) ?
            workLocalSize / (elementNumPerBlk * SOFTMAX_TMPBUFFER_COUNT + srcK + SOFTMAX_BASICBLOCK_UNIT) :
            workLocalSize / (elementNumPerBlk + SOFTMAX_BASICBLOCK_UNIT);
    }

    uint32_t softmaxBasicTileNum = BASIC_TILE_NUM;
    if (isFlashOutputBrc && dataTypeSize1 == SOFTMAX_HALF_SIZE) {
        softmaxBasicTileNum = SOFTMAX_DEFAULT_BLK_SIZE / SOFTMAX_HALF_SIZE;
    }

    baseM = std::min(baseM, srcM);
    if (baseM < srcM && baseM > softmaxBasicTileNum) {
        baseM = baseM / softmaxBasicTileNum * softmaxBasicTileNum;
    }

    softmaxFlashTiling.set_srcM(srcM);
    softmaxFlashTiling.set_srcK(srcK);
    softmaxFlashTiling.set_srcSize(srcM * srcK);

    softmaxFlashTiling.set_outMaxM(srcM);
    softmaxFlashTiling.set_outMaxK(elementNumPerBlk);
    softmaxFlashTiling.set_outMaxSize(srcM * elementNumPerBlk);

    softmaxFlashTiling.set_splitM(baseM);
    softmaxFlashTiling.set_splitK(srcK);
    softmaxFlashTiling.set_splitSize(baseM * srcK);

    softmaxFlashTiling.set_reduceM(baseM);
    softmaxFlashTiling.set_reduceK(elementNumPerBlk);
    softmaxFlashTiling.set_reduceSize(baseM * elementNumPerBlk);

    uint32_t tail = srcM % baseM;
    softmaxFlashTiling.set_rangeM(srcM / baseM);
    softmaxFlashTiling.set_tailM(tail);

    softmaxFlashTiling.set_tailSplitSize(tail * srcK);
    softmaxFlashTiling.set_tailReduceSize(tail * elementNumPerBlk);

    if (isFlashOutputBrc && (softmaxFlashTiling.get_rangeM() > 1 || softmaxFlashTiling.get_tailM() != 0)) {
        ASCENDC_HOST_ASSERT((softmaxFlashTiling.get_reduceM() % (SOFTMAX_DEFAULT_BLK_SIZE / dataTypeSize1) == 0), return,
            "When dataTypeSize1(%d) is float(or half), softmaxFlashTiling.reduceM(%d) must be a multiple of 8(or 16), "
            "Adjust the input parameter -> localWorkSpaceSize.\n", dataTypeSize1, softmaxFlashTiling.get_reduceM());        
    }
}

void GetSoftMaxFlashV3MaxMinTmpSize(const ge::Shape &srcShape, const uint32_t dataTypeSize1,
                                    const uint32_t dataTypeSize2, uint32_t &maxValue, uint32_t &minValue, UNUSED const bool isUpdate,
                                    UNUSED const bool isBasicBlock)
{
    ASCENDC_HOST_ASSERT(dataTypeSize1 == SOFTMAX_HALF_SIZE, return, "dataTypeSize1 must be sizeof(half).");
    ASCENDC_HOST_ASSERT(dataTypeSize2 > 0, return, "dataTypeSize2 must be greater than 0.");
    std::vector<uint32_t> retVec = GetLastAxisShapeND(srcShape);
    ASCENDC_HOST_ASSERT(retVec.size() > 1, return, "retVec must be greater than 1.");

    const uint32_t srcM = retVec[0];
    const uint32_t srcK = retVec[1];
    if (dataTypeSize2 == 0)
    {
        return;
    }
    const uint32_t elementNumPerBlk = SOFTMAX_DEFAULT_BLK_SIZE / dataTypeSize2;

    minValue = elementNumPerBlk * SOFTMAXV3_TMPBUFFER_COUNT + SOFTMAX_TMPBUFFER_COUNT * srcK + SOFTMAX_BASICBLOCK_UNIT;
    minValue = minValue * SOFTMAX_FLOAT_SIZE;
    maxValue = srcM * minValue;
}

void SoftMaxFlashV3TilingFunc(const ge::Shape &srcShape, const uint32_t dataTypeSize1, const uint32_t dataTypeSize2,
                              const uint32_t localWorkSpaceSize, optiling::SoftMaxTiling &softmaxFlashV3Tiling, UNUSED const bool isUpdate,
                              UNUSED const bool isBasicBlock)
{
    ASCENDC_HOST_ASSERT(dataTypeSize1 == SOFTMAX_HALF_SIZE, return, "dataTypeSize1 must be sizeof(half).");
    ASCENDC_HOST_ASSERT(dataTypeSize2 > 0, return, "dataTypeSize2 must be greater than 0.");

    std::vector<uint32_t> retVec = GetLastAxisShapeND(srcShape);
    ASCENDC_HOST_ASSERT(retVec.size() > 1, return, "retVec must be greater than 1.");
    if (dataTypeSize2 == 0)
    {
        return;
    }
    const uint32_t elementNumPerBlk = SOFTMAX_DEFAULT_BLK_SIZE / dataTypeSize2;
    const uint32_t workLocalSize = localWorkSpaceSize / SOFTMAX_FLOAT_SIZE;
    const uint32_t srcK = retVec[1];
    const uint32_t srcM = retVec[0];
    uint32_t baseM = 0;

    baseM = workLocalSize / (elementNumPerBlk * SOFTMAXV3_TMPBUFFER_COUNT + SOFTMAX_TMPBUFFER_COUNT * srcK + SOFTMAX_BASICBLOCK_UNIT);
    baseM = std::min(baseM, srcM);
    if (baseM < srcM && baseM > BASIC_TILE_NUM)
    {
        baseM = baseM / BASIC_TILE_NUM * BASIC_TILE_NUM;
    }

    softmaxFlashV3Tiling.set_srcM(srcM);
    softmaxFlashV3Tiling.set_srcK(srcK);
    softmaxFlashV3Tiling.set_srcSize(srcM * srcK);

    softmaxFlashV3Tiling.set_outMaxM(srcM);
    softmaxFlashV3Tiling.set_outMaxK(elementNumPerBlk);
    softmaxFlashV3Tiling.set_outMaxSize(srcM * elementNumPerBlk);

    softmaxFlashV3Tiling.set_splitM(baseM);
    softmaxFlashV3Tiling.set_splitK(srcK);
    softmaxFlashV3Tiling.set_splitSize(baseM * srcK);

    softmaxFlashV3Tiling.set_reduceM(baseM);
    softmaxFlashV3Tiling.set_reduceK(elementNumPerBlk);
    softmaxFlashV3Tiling.set_reduceSize(baseM * elementNumPerBlk);

    if (baseM != 0)
    {
        uint32_t tail = srcM % baseM;
        softmaxFlashV3Tiling.set_rangeM(srcM / baseM);
        softmaxFlashV3Tiling.set_tailM(tail);
        softmaxFlashV3Tiling.set_tailSplitSize(tail * srcK);
        softmaxFlashV3Tiling.set_tailReduceSize(tail * elementNumPerBlk);
    }
}
}