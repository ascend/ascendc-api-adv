/* Copyright (c) Huawei Technologies Co., Ltd. 2024. All rights reserved.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

/* !
 * \file simple_softmax_impl.h
 * \brief
 */
#ifndef IMPL_ACTIVATION_SOFTMAX_V220_SIMPLE_SOFTMAX_IMPL_H
#define IMPL_ACTIVATION_SOFTMAX_V220_SIMPLE_SOFTMAX_IMPL_H

#include "softmax_common_impl.h"
#include "../common/simple_softmax_common_impl.h"

#endif // IMPL_ACTIVATION_SOFTMAX_V220_SIMPLE_SOFTMAX_IMPL_H