/**
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

/*!
 * \file sigmoid_tiling_impl.cpp
 * \brief
 */
#include "lib/activation/sigmoid_tiling.h"

#include <cstdint>

#include "graph/tensor.h"
#include "impl/host_log.h"
namespace AscendC {
namespace {
constexpr uint32_t SIGMOID_MEMORY_SIZE = 256;

inline uint32_t GetSigmoidMaxTmpSize(const uint32_t inputSize, const uint32_t typeSize)
{
    return inputSize * typeSize > SIGMOID_MEMORY_SIZE ? inputSize * typeSize : SIGMOID_MEMORY_SIZE;
}
} // namespace

void GetSigmoidMaxMinTmpSize(const ge::Shape& srcShape, const uint32_t typeSize, bool isReuseSource,
    uint32_t& maxValue, uint32_t& minValue)
{
    (void)isReuseSource;
    const uint32_t inputSize = srcShape.GetShapeSize();
    ASCENDC_HOST_ASSERT(inputSize > 0, return, "Input Shape size must be greater than 0.");

    maxValue = GetSigmoidMaxTmpSize(inputSize, typeSize);
    minValue = SIGMOID_MEMORY_SIZE;
}
} // namespace AscendC