/**
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

/*!
 * \file load_to_l0b.h
 * \brief
 */
#ifndef IMPL_MATMUL_MODULES_STAGE_SPLIT_LOAD_TO_L0B_H
#define IMPL_MATMUL_MODULES_STAGE_SPLIT_LOAD_TO_L0B_H

#include "load_to_l0b_loadInstr.h"
#include "load_to_l0b_basic.h"
#include "load_to_l0b_load2d.h"

#endif // IMPL_MATMUL_MODULES_STAGE_SPLIT_LOAD_TO_L0B_H