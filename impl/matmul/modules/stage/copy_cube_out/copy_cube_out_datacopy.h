/**
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

/*!
 * \file copy_cube_out_datacopy.h
 * \brief
 */

#ifndef IMPL_MATMUL_MODULES_STAGE_COPY_CUBE_OUT_COPY_CUBE_OUT_DATACOPY_H
#define IMPL_MATMUL_MODULES_STAGE_COPY_CUBE_OUT_COPY_CUBE_OUT_DATACOPY_H

#include "../../matmul_module.h"
#include "../../matmul_param.h"
#include "../../resource/cube_out_buffer/cube_out_buffer.h"
#include "copy_cube_out_intf.h"

namespace AscendC {
namespace Impl {
namespace Detail {

constexpr int DOUBLE_SPACE = 2;
constexpr int TRIPLE_SPACE = 3;
constexpr int TWO_TIMES = 2;
constexpr int EIGHT_TIMES = 8;
constexpr int SHIFT_16_BIT = 16;
constexpr int SHIFT_32_BIT = 32;
constexpr int SHIFT_48_BIT = 48;
constexpr uint32_t MAX_REPEAT_STRIDE = 255;
constexpr int PATTERN_SIZE = 8;
constexpr int PATTERN_OFFSET = 2;
/*
    CopyCubeOut is considered entirely experimental.
    We retain the freedom to make incompatible changes, but do not guarantee the stability.
    CopyCubeOut is only for internal usage, does not support extension or customized specialization!
*/
template <typename IMPL, class A_TYPE, class B_TYPE, class C_TYPE, const auto& MM_CFG>
class CopyCubeOut<IMPL, A_TYPE, B_TYPE, C_TYPE, MM_CFG, enable_if_t<(MatmulFeatureTrait<MM_CFG>::IsNeedUB())>>
{
    using SrcType = typename A_TYPE::T;
    using DstT = typename C_TYPE::T;
    using SrcT = typename GetDstType<typename A_TYPE::T>::Type;

    MATMUL_USE_MODULE(CubeOutBuffer);
    MATMUL_USE_MODULE(MatmulQuantProcessor);
    MATMUL_USE_MODULE(MatmulShapeInfo);
    MATMUL_USE_MODULE(MatmulShapeTiling);
    MATMUL_USE_MODULE(LocalWorkspace);

public:
    template <bool enSequentialWrite = false, typename ScheduleContext = int>
    __aicore__ inline void Copy(const GlobalTensor<DstT>& gm, const LocalTensor<SrcT>& co1Local, int curRow,
                                   int curCol, int32_t baseHeight, int32_t baseWidth, int32_t baseBlockHeight,
                                   int32_t baseBlockWidth, const ScheduleContext& context = 0)
    {
        CopyOutImpl<enSequentialWrite>(gm, co1Local, curRow, curCol, baseHeight, baseWidth, baseBlockHeight,
                                       baseBlockWidth);
    }

    template <bool enSequentialWrite = false, typename ScheduleContext = int>
    __aicore__ inline void Copy(const LocalTensor<DstT>& co2Local, const LocalTensor<SrcT>& co1Local, int curRow,
                                   int curCol, int32_t baseHeight, int32_t baseWidth, int32_t baseBlockHeight,
                                   int32_t baseBlockWidth, const ScheduleContext& context = 0)
    {
        CopyOutImpl<enSequentialWrite>(co2Local, co1Local, curRow, curCol, baseHeight, baseWidth, baseBlockHeight,
                                       baseBlockWidth);
    }

    template <bool enSequentialWrite = false, typename ScheduleContext = int>
    __aicore__ inline void Copy(const GlobalTensor<DstT>& gm, const LocalTensor<DstT>& co2Local,
                                   const LocalTensor<SrcT>& co1Local, int curRow, int curCol, int32_t baseHeight,
                                   int32_t baseWidth, int32_t baseBlockHeight, int32_t baseBlockWidth,
                                   const ScheduleContext& context = 0)
    {
        CopyOutImpl<enSequentialWrite>(gm, co2Local, co1Local, curRow, curCol, baseHeight, baseWidth, baseBlockHeight,
                                       baseBlockWidth);
    }

private:
    template <bool enSequentialWrite, class T>
    __aicore__ inline void CopyOutImpl(const T& dst, const LocalTensor<SrcT>& co1Local, int curRow, int curCol,
                                       int32_t baseHeight, int32_t baseWidth, int32_t baseBlockHeight,
                                       int32_t baseBlockWidth)
    {
        if constexpr (C_TYPE::format == CubeFormat::ND || C_TYPE::format == CubeFormat::ND_ALIGN) {
            CopyOutNZ2ND<enSequentialWrite>(dst, co1Local, curRow, curCol, baseHeight, baseWidth, baseBlockHeight,
                                            baseBlockWidth);
        } else if constexpr (C_TYPE::format == CubeFormat::NZ) {
            CopyOutNZ2NZ<enSequentialWrite>(dst, co1Local, curRow, curCol, baseHeight, baseWidth, baseBlockHeight,
                                            baseBlockWidth);
        } else {
            ASCENDC_ASSERT(false, { KERNEL_LOG(KERNEL_ERROR, "Copy: unsupport Matmul format type."); });
        }
    }

    template <bool enSequentialWrite, class T>
    __aicore__ inline void CopyOutImpl(const T& dst, const LocalTensor<DstT>& co2Local,
        const LocalTensor<SrcT>& co1Local, int curRow, int curCol, int32_t baseHeight,
        int32_t baseWidth, int32_t baseBlockHeight, int32_t baseBlockWidth)
    {
        if constexpr(C_TYPE::format == CubeFormat::ND || C_TYPE::format == CubeFormat::ND_ALIGN) {
            CopyOutNZ2ND<enSequentialWrite>(dst, co2Local, co1Local, curRow, curCol, baseHeight, baseWidth,
                                            baseBlockHeight, baseBlockWidth);
        } else if constexpr (C_TYPE::format == CubeFormat::NZ) {
            CopyOutNZ2NZ<enSequentialWrite>(dst, co2Local, co1Local, curRow, curCol, baseHeight, baseWidth,
                                            baseBlockHeight, baseBlockWidth);
        } else {
            ASCENDC_ASSERT(false, { KERNEL_LOG(KERNEL_ERROR, "Copy: unsupport Matmul format type."); });
        }
    }

    template <bool enSequentialWrite>
    __aicore__ inline void CopyOutNZ2NZ(const LocalTensor<DstT>& co2Local, const LocalTensor<SrcT>& co1Local,
                                        int curRow, int curCol, int32_t baseHeight, int32_t baseWidth,
                                        int32_t baseBlockHeight, int32_t baseBlockWidth)
    {
        ASCENDC_ASSERT((MATMUL_CAST_TO_IMPL()->M_ >= MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetBaseM()), {
            KERNEL_LOG(KERNEL_ERROR, "M_ is %d , which should be not less than baseM %d",
            MATMUL_CAST_TO_IMPL()->M_, MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetBaseM());
        });

        DataCopyParams dataCopyInfo;
        dataCopyInfo.blockCount = baseBlockWidth;
        dataCopyInfo.blockLen = baseBlockHeight;
        dataCopyInfo.srcStride = 0;
        DataCopyEnhancedParams enhancedParams;
        enhancedParams.blockMode = BlockMode::BLOCK_MODE_MATRIX;
        if constexpr (enSequentialWrite) {
            dataCopyInfo.dstStride = 0;
            CopyCo12Co2WithQuant(co2Local, co1Local, curCol, baseBlockHeight, baseBlockWidth, dataCopyInfo, enhancedParams);
        } else {
            dataCopyInfo.dstStride = (Ceil(MATMUL_MODULE(MatmulShapeInfo)->GetSingleCoreM(), BLOCK_CUBE) * BLOCK_CUBE -
                baseBlockHeight * BLOCK_CUBE) * BLOCK_CUBE * sizeof(DstT) /
                ONE_BLK_SIZE;
            int dstOffset = curRow * MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetBaseM() * BLOCK_CUBE +
                curCol * MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetBaseN() * MATMUL_CAST_TO_IMPL()->M_;
            CopyCo12Co2WithQuant(co2Local[dstOffset], co1Local, curCol, baseBlockHeight, baseBlockWidth, dataCopyInfo, enhancedParams);
        }
    }

    template <bool enSequentialWrite>
    __aicore__ inline void CopyOutNZ2NZ(const GlobalTensor<DstT>& gm, const LocalTensor<SrcT>& co1Local, int curRow,
                                        int curCol, int32_t baseHeight, int32_t baseWidth, int32_t baseBlockHeight,
                                        int32_t baseBlockWidth)
    {
        event_t eventIDMte3ToV = static_cast<event_t>(GetTPipePtr()->FetchEventID(HardEvent::MTE3_V));
        SetFlag<HardEvent::MTE3_V>(eventIDMte3ToV);
        WaitFlag<HardEvent::MTE3_V>(eventIDMte3ToV);

        LocalTensor<DstT> localBuf = GetLocalBuf();
        CopyCo12Local(localBuf, co1Local, curCol, baseBlockHeight, baseBlockWidth);

        event_t eventIDVToMte3 = static_cast<event_t>(GetTPipePtr()->FetchEventID(HardEvent::V_MTE3));
        SetFlag<HardEvent::V_MTE3>(eventIDVToMte3);
        WaitFlag<HardEvent::V_MTE3>(eventIDVToMte3);

        CopyLocal2GMNZ2NZ<enSequentialWrite>(gm, localBuf, curRow, curCol, baseHeight, baseWidth, baseBlockHeight, baseBlockWidth);
    }

    template <bool enSequentialWrite>
    __aicore__ inline void CopyOutNZ2NZ(const GlobalTensor<DstT>& gm, const LocalTensor<DstT>& co2Local,
        const LocalTensor<SrcT>& co1Local, int curRow, int curCol, int32_t baseHeight, int32_t baseWidth,
        int32_t baseBlockHeight, int32_t baseBlockWidth)
    {
        CopyOutNZ2NZ<enSequentialWrite>(co2Local, co1Local, curRow, curCol, baseHeight, baseWidth, baseBlockHeight, baseBlockWidth);
        CopyLocal2GMNZ2NZ<enSequentialWrite>(gm, co2Local, curRow, curCol, baseHeight, baseWidth, baseBlockHeight, baseBlockWidth);
    }

    template <bool enSequentialWrite>
    __aicore__ inline void CopyOutNZ2ND(const LocalTensor<DstT>& co2Local, const LocalTensor<SrcT>& co1Local,
                                        int curRow, int curCol, int32_t baseHeight, int32_t baseWidth,
                                        int32_t baseBlockHeight, int32_t baseBlockWidth)
    {
        if constexpr (A_TYPE::format == CubeFormat::VECTOR) {
            ASCENDC_ASSERT((MATMUL_CAST_TO_IMPL()->M_ == 1),
                { KERNEL_LOG(KERNEL_ERROR, "M_ is %d, which should be equal with 1.", MATMUL_CAST_TO_IMPL()->M_); });

            DataCopyParams dataCopyInfo;
            dataCopyInfo.blockCount = 1;
            dataCopyInfo.blockLen = baseBlockHeight * baseBlockWidth;
            DataCopyEnhancedParams enhancedParams;
            enhancedParams.blockMode = BlockMode::BLOCK_MODE_VECTOR;

            if constexpr (enSequentialWrite) {
                DataCopy(co2Local, co1Local, dataCopyInfo, enhancedParams);
            } else {
                int dstOffset = curCol * MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetBaseN();
                DataCopy(co2Local[dstOffset], co1Local, dataCopyInfo, enhancedParams);
            }
        } else {
            ASCENDC_ASSERT((!IsSameType<DstT, int8_t>::value && !IsSameType<DstT, uint8_t>::value),
                { KERNEL_LOG(KERNEL_ERROR, "Data format should be NZ if GetTensorC to UB when output is int8_t."); });

            LocalTensor<DstT> trans = GetLocalBuf();

            CopyCo12Co2WithoutQuant(trans, co1Local, curCol, baseBlockHeight, baseBlockWidth);

            if constexpr(enSequentialWrite) {
                TransNZ2NDForDstUB(co2Local, trans, MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetBaseN(), baseHeight, baseBlockWidth, baseBlockHeight);
            } else {
                uint32_t dimN = (MATMUL_CAST_TO_IMPL()->Kc_ != 0) ? MATMUL_CAST_TO_IMPL()->Kc_ : MATMUL_CAST_TO_IMPL()->N_;
                int dstOffset = curRow * MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetBaseM() * dimN +
                    curCol * MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetBaseN();
                constexpr int blockCount = sizeof(DstT) == B32_BYTE_SIZE ? BLOCK_CUBE : ONE_BLK_SIZE / sizeof(DstT);
                TransNZ2NDForDstUB(co2Local[dstOffset], trans, Ceil(dimN, blockCount) * blockCount, baseHeight, baseBlockWidth, baseBlockHeight);
            }
        }
    }

    template <bool enSequentialWrite>
    __aicore__ inline void CopyOutNZ2ND(const GlobalTensor<DstT>& gm, const LocalTensor<SrcT>& co1Local, int curRow,
                                        int curCol, int32_t baseHeight, int32_t baseWidth, int32_t baseBlockHeight,
                                        int32_t baseBlockWidth)
    {
        event_t eventIDMte3ToV = static_cast<event_t>(GetTPipePtr()->FetchEventID(HardEvent::MTE3_V));
        SetFlag<HardEvent::MTE3_V>(eventIDMte3ToV);
        WaitFlag<HardEvent::MTE3_V>(eventIDMte3ToV);

        LocalTensor<DstT> localBuf = GetLocalBuf();
        CopyCo12Local(localBuf, co1Local, curCol, baseBlockHeight, baseBlockWidth);

        event_t eventIDVToMte3 = static_cast<event_t>(GetTPipePtr()->FetchEventID(HardEvent::V_MTE3));
        SetFlag<HardEvent::V_MTE3>(eventIDVToMte3);
        WaitFlag<HardEvent::V_MTE3>(eventIDVToMte3);

        if constexpr (A_TYPE::format == CubeFormat::VECTOR) {
            CopyLocal2GMNZ2NZ<enSequentialWrite>(gm, localBuf, curRow, curCol, baseHeight, baseWidth, baseBlockHeight,
                                                 baseBlockWidth);
        } else if (C_TYPE::format == CubeFormat::ND || C_TYPE::format == CubeFormat::ND_ALIGN) {
            if constexpr (!ToMatmulConfig(MM_CFG).enVecND2NZ || IsSameType<typename A_TYPE::T, half>::value &&
                                                                    IsSameType<typename B_TYPE::T, int8_t>::value) {
                CopyLocal2GMNZ2NDOnTheFly<enSequentialWrite>(gm, localBuf, curRow, curCol, baseHeight, baseWidth,
                                                             baseBlockHeight, baseBlockWidth);
            } else {
                CopyLocal2GMNZ2NDByVec<enSequentialWrite>(gm, localBuf, curRow, curCol, baseHeight, baseWidth,
                                                          baseBlockHeight, baseBlockWidth);
            }
        }
    }

    __aicore__ inline void CopyOutNZ2ND(const GlobalTensor<DstT>& gm, const LocalTensor<DstT>& co2Local,
        const LocalTensor<SrcT>& co1Local, int curRow, int curCol, int32_t baseHeight, int32_t baseWidth,
        int32_t baseBlockHeight, int32_t baseBlockWidth, bool enSequentialWrite)
    {
        if constexpr (A_TYPE::format == CubeFormat::VECTOR) {
            CopyOutNZ2ND(co2Local, co1Local, curRow, curCol, baseHeight, baseWidth, baseBlockHeight, baseBlockWidth, enSequentialWrite);
            CopyLocal2GMNZ2NZ(gm, co2Local, curRow, curCol, baseHeight, baseWidth, baseBlockHeight, baseBlockWidth, enSequentialWrite);
        } else if (C_TYPE::format == CubeFormat::ND || C_TYPE::format == CubeFormat::ND_ALIGN) {
            LocalTensor<DstT> localBuf = GetLocalBuf();
            CopyCo12Co2WithoutQuant(localBuf, co1Local, curCol, baseBlockHeight, baseBlockWidth);

            if (enSequentialWrite) {
                TransNZ2NDForDstUB(co2Local, localBuf, MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetBaseN(), baseHeight, baseBlockWidth, baseBlockHeight);
            } else {
                uint32_t dimN = (MATMUL_CAST_TO_IMPL()->Kc_ != 0) ? MATMUL_CAST_TO_IMPL()->Kc_ : MATMUL_CAST_TO_IMPL()->N_;
                int dstOffset = curRow * MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetBaseM() * dimN +
                    curCol * MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetBaseN();
                constexpr int blockCount = sizeof(DstT) == B32_BYTE_SIZE ? BLOCK_CUBE : ONE_BLK_SIZE / sizeof(DstT);
                TransNZ2NDForDstUB(co2Local[dstOffset], localBuf, Ceil(dimN, blockCount) * blockCount, baseHeight, baseBlockWidth, baseBlockHeight);
            }

            if constexpr (!ToMatmulConfig(MM_CFG).enVecND2NZ ||
                IsSameType<typename A_TYPE::T, half>::value && IsSameType<typename B_TYPE::T, int8_t>::value) {
                CopyLocal2GMNZ2NDOnTheFly(gm, localBuf, curRow, curCol, baseHeight, baseWidth, baseBlockHeight, baseBlockWidth, enSequentialWrite);
            } else {
                CopyLocal2GMNZ2NDByVec(gm, localBuf, curRow, curCol, baseHeight, baseWidth, baseBlockHeight, baseBlockWidth, enSequentialWrite);
            }
        }
    }

    __aicore__ inline void CopyCo12Co2WithQuant(const LocalTensor<DstT>& dst, const LocalTensor<SrcT>& src,
        int curCol, int baseBlockHeight, int baseBlockWidth, DataCopyParams& dataCopyInfo, DataCopyEnhancedParams& enhancedParams)
    {
        if constexpr (IsSameType<SrcType, int8_t>::value) {
            UpdateDataCopyParamForQuant(enhancedParams, curCol);
            uint64_t alignedHeight = baseBlockHeight * BLOCK_CUBE;
            if (MATMUL_MODULE(MatmulQuantProcessor)->GetMatmulQuantMode() == QuantMode_t::VREQ8) {
                dataCopyInfo.blockLen = baseBlockHeight;
                uint64_t addr = enhancedParams.deqTensorAddr;
                for (int i = 0; i < Ceil(baseBlockWidth, TWO_TIMES); ++i) {
                    for (int storeMode = 0; storeMode < TWO_TIMES; ++storeMode) {
                        if (baseBlockWidth % TWO_TIMES != 0 &&
                            i == Ceil(baseBlockWidth, TWO_TIMES) - 1 &&
                            storeMode == 1) {
                            continue;
                        }
                        enhancedParams.deqTensorAddr = addr + i * ONE_BLK_SIZE * ONE_BYTE_BIT_SIZE + storeMode * BLOCK_CUBE * ONE_BYTE_BIT_SIZE;
                        enhancedParams.sidStoreMode = (uint8_t)storeMode;
                        DataCopy(dst[i * ONE_BLK_SIZE * alignedHeight],
                            src[i * ONE_BLK_SIZE * alignedHeight + storeMode * BLOCK_CUBE * alignedHeight],
                            dataCopyInfo, enhancedParams);
                    }
                }
            } else if (MATMUL_MODULE(MatmulQuantProcessor)->GetMatmulQuantMode() == QuantMode_t::REQ8) {
                dataCopyInfo.blockLen = baseBlockHeight;
                uint64_t addr = enhancedParams.deqTensorAddr;
                for (int i = 0; i < Ceil(baseBlockWidth, TWO_TIMES); ++i) {
                    for (int storeMode = 0; storeMode < TWO_TIMES; ++storeMode) {
                        if (baseBlockWidth % TWO_TIMES != 0 &&
                            i == Ceil(baseBlockWidth, TWO_TIMES) - 1 &&
                            storeMode == 1) {
                            continue;
                        }
                        enhancedParams.sidStoreMode = (uint8_t)storeMode;
                        DataCopy(dst[i * ONE_BLK_SIZE * alignedHeight],
                            src[i * ONE_BLK_SIZE * alignedHeight + storeMode * BLOCK_CUBE * alignedHeight],
                            dataCopyInfo, enhancedParams);
                    }
                }
            } else if (MATMUL_MODULE(MatmulQuantProcessor)->GetMatmulQuantMode() == QuantMode_t::VDEQF16) {
                dataCopyInfo.blockCount = 1;
                dataCopyInfo.blockLen = baseBlockHeight;
                dataCopyInfo.dstStride = 0;
                uint64_t addr = enhancedParams.deqTensorAddr;
                for (int i = 0; i < baseBlockWidth; ++i) {
                    constexpr int DEQ_OFFSET = 128;
                    enhancedParams.deqTensorAddr = addr + i * DEQ_OFFSET;
                    DataCopy(dst[i * BLOCK_CUBE * alignedHeight], src[i * BLOCK_CUBE * alignedHeight], dataCopyInfo, enhancedParams);
                }
            } else {
                DataCopy(dst, src, dataCopyInfo, enhancedParams);
            }
        } else {
            DataCopy(dst, src, dataCopyInfo, enhancedParams);
        }
    }

    __aicore__ inline void CopyCo12Co2WithoutQuant(const LocalTensor<DstT>& dst, const LocalTensor<SrcT>& src, int curCol,
        int baseBlockHeight, int baseBlockWidth)
    {
        DataCopyParams dataCopyInfo;
        dataCopyInfo.blockCount = baseBlockWidth;
        dataCopyInfo.blockLen = baseBlockHeight;
        dataCopyInfo.srcStride = 0;
        dataCopyInfo.dstStride = 0;
        DataCopyEnhancedParams enhancedParams;
        enhancedParams.blockMode = BlockMode::BLOCK_MODE_MATRIX;
        if constexpr (IsSameType<SrcType, int8_t>::value) {
            UpdateDataCopyParamForQuant(enhancedParams, curCol);
        }
        DataCopy(dst, src, dataCopyInfo, enhancedParams);
    }

    __aicore__ inline void CopyCo12Local(const LocalTensor<DstT>& localBuf, const LocalTensor<SrcT>& co1Local, int curCol, int baseBlockHeight, int baseBlockWidth)
    {
        DataCopyParams dataCopyInfo;
        dataCopyInfo.blockCount = 1;
        dataCopyInfo.blockLen = baseBlockHeight * baseBlockWidth;
        DataCopyEnhancedParams enhancedParams;
        if constexpr (A_TYPE::format == CubeFormat::VECTOR) {
            enhancedParams.blockMode = BlockMode::BLOCK_MODE_VECTOR;
        } else {
            enhancedParams.blockMode = BlockMode::BLOCK_MODE_MATRIX;
            ASCENDC_ASSERT((localBuf.GetSize() >= dataCopyInfo.blockLen * CUBE_MAX_SIZE), {
                KERNEL_LOG(KERNEL_ERROR, "copy len is %d, which should be less than dst size %d",
                    dataCopyInfo.blockLen * CUBE_MAX_SIZE, localBuf.GetSize());
            });
        }
        CopyCo12Co2WithQuant(localBuf, co1Local, curCol, baseBlockHeight, baseBlockWidth, dataCopyInfo, enhancedParams);
    }

    __aicore__ inline void CopyLocal2GMNZ2NZNotSeq(const GlobalTensor<DstT>& gm, const LocalTensor<DstT>& localBuf,
        int curRow, int curCol, int32_t baseHeight, int32_t baseWidth, int32_t baseBlockHeight, int32_t baseBlockWidth)
    {
        ASCENDC_ASSERT((MATMUL_CAST_TO_IMPL()->M_ >= baseHeight), {
            KERNEL_LOG(KERNEL_ERROR, "M_ is %d, baseUseM_ is %d, M_ should be no less than baseUseM_",
                MATMUL_CAST_TO_IMPL()->M_, baseHeight);
        });
        int64_t alignM;
        int alignBaseUseM;
        if constexpr (C_TYPE::format == CubeFormat::NZ) {
            // nz2nz
            alignM = Ceil(MATMUL_CAST_TO_IMPL()->M_, BLOCK_CUBE) * BLOCK_CUBE;
            alignBaseUseM = Ceil(baseHeight, BLOCK_CUBE) * BLOCK_CUBE;
        } else {
            // nz2nd A is vector
            alignM = MATMUL_CAST_TO_IMPL()->M_;
            alignBaseUseM = baseHeight;
        }

        int64_t dstOffset;
        int64_t dstStride;
        int blockLen;
        int blockCount;
        if constexpr (IsSameType<DstT, int8_t>::value || IsSameType<DstT, uint8_t>::value) {
            dstOffset = curCol * MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetBaseN() * alignM +
                curRow * MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetBaseM() * ONE_BLK_SIZE;
            dstStride = (alignM - alignBaseUseM) * sizeof(DstT);
            blockLen = baseBlockHeight * BLOCK_CUBE * sizeof(DstT);
            blockCount = Ceil(baseBlockWidth, TWO_TIMES);
        } else {
            dstOffset = curCol * MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetBaseN() * alignM +
                curRow * MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetBaseM() * BLOCK_CUBE;
            dstStride = (alignM - alignBaseUseM) * sizeof(DstT) * BLOCK_CUBE / ONE_BLK_SIZE;
            blockLen = baseBlockHeight * BLOCK_CUBE * sizeof(DstT) *
                BLOCK_CUBE / ONE_BLK_SIZE;
            blockCount = baseBlockWidth;
        }

        if (dstStride >= UINT16_MAX) {
            int srcStride;
            if constexpr (IsSameType<DstT, int8_t>::value || IsSameType<DstT, uint8_t>::value) {
                dstStride = alignM * ONE_BLK_SIZE;
                srcStride= baseHeight * ONE_BLK_SIZE;
            } else {
                dstStride = alignM * BLOCK_CUBE;
                srcStride = baseHeight * BLOCK_CUBE;
            }
            for (int i = 0; i < blockCount; ++i) {
                DataCopy(gm[dstOffset + i * dstStride], localBuf[i * srcStride], { 1, static_cast<uint16_t>(blockLen), 0, 0 });
            }
        } else {
            DataCopy(gm[dstOffset], localBuf, { static_cast<uint16_t>(blockCount), static_cast<uint16_t>(blockLen), 0,
                static_cast<uint16_t>(dstStride) });
        }
    }

    __aicore__ inline void CopyLocal2GMNZ2NZSeq(const GlobalTensor<DstT>& gm, const LocalTensor<DstT>& localBuf, int baseHeight, int baseBlockWidth)
    {
        int blockLen = baseHeight * BLOCK_CUBE * sizeof(DstT) / ONE_BLK_SIZE;
        DataCopy(gm, localBuf, { static_cast<uint16_t>(baseBlockWidth),
            static_cast<uint16_t>(blockLen), 0, 0 });
    }

    template <bool enSequentialWrite>
    __aicore__ inline void CopyLocal2GMNZ2NZ(const GlobalTensor<DstT>& gm, const LocalTensor<DstT>& localBuf,
        int curRow, int curCol, int32_t baseHeight, int32_t baseWidth, int32_t baseBlockHeight, int32_t baseBlockWidth)
    {
        if constexpr (enSequentialWrite) {
            CopyLocal2GMNZ2NZSeq(gm, localBuf, baseHeight, baseBlockWidth);
        } else {
            CopyLocal2GMNZ2NZNotSeq(gm, localBuf, curRow, curCol, baseHeight, baseWidth, baseBlockHeight, baseBlockWidth);
        }
    }

    __aicore__ inline void TransNZ2NDForDstUB(const LocalTensor<DstT>& co2Local, const LocalTensor<DstT>& trans,
        int dstStride, int baseHeight, int baseBlockWidth, int baseBlockHeight)
    {
        constexpr int blockCount = sizeof(DstT) == B32_BYTE_SIZE ? BLOCK_CUBE : ONE_BLK_SIZE / sizeof(DstT);
        DataCopyParams dataCopyInfo {
            static_cast<uint16_t>(baseBlockWidth),
            static_cast<uint16_t>(blockCount * sizeof(DstT) / ONE_BLK_SIZE),
            static_cast<uint16_t>((baseBlockHeight * BLOCK_CUBE * blockCount -
                blockCount) * sizeof(DstT) / ONE_BLK_SIZE),
            0
        };
        for (int i = 0; i < baseHeight; i++) {
            DataCopy(co2Local[i * dstStride], trans[i * blockCount], dataCopyInfo);
        }
    }

    template <bool enSequentialWrite>
    __aicore__ inline void CopyLocal2GMNZ2NDByVec(const GlobalTensor<DstT>& gm, const LocalTensor<DstT>& localBuf,
        int curRow, int curCol, int32_t baseHeight, int32_t baseWidth, int32_t baseBlockHeight, int32_t baseBlockWidth)
    {
        uint32_t dimN = (MATMUL_CAST_TO_IMPL()->Kc_ != 0) ? MATMUL_CAST_TO_IMPL()->Kc_ : MATMUL_CAST_TO_IMPL()->N_;
        constexpr int blockCount = sizeof(DstT) == B32_BYTE_SIZE ? BLOCK_CUBE : ONE_BLK_SIZE / sizeof(DstT);

        LocalTensor<DstT> trans = MATMUL_MODULE(LocalWorkspace)->template
            GetWorkspaceWithOffset<ToMatmulConfig(MM_CFG).enableUBReuse>(
            MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetTransLength())
            .template ReinterpretCast<DstT>();
        int transSize = localBuf.GetSize();
        if constexpr (IsSameType<DstT, int8_t>::value || IsSameType<DstT, uint8_t>::value) {
            if (baseBlockWidth % TWO_TIMES != 0) {
                transSize += baseBlockHeight * CUBE_MAX_SIZE;
            }
        }
        trans.SetSize(transSize);

        int dstOffset;
        int dstStride;
        int offset;
        bool isGmAligned;
        if constexpr (enSequentialWrite) {
            dstOffset = 0;
            dstStride = 0;
            offset = baseWidth;
            isGmAligned = ((baseWidth % blockCount) == 0);
        } else {
            int width = baseBlockWidth * blockCount;
            if constexpr (IsSameType<DstT, int8_t>::value || IsSameType<DstT, uint8_t>::value) {
                width = width / TWO_TIMES;
            }
            ASCENDC_ASSERT((dimN >= width),
                { KERNEL_LOG(KERNEL_ERROR, "dimN is %d, width is %d, dimN should be no less than width", dimN, width); });
            if constexpr (C_TYPE::format == CubeFormat::ND_ALIGN) {
                isGmAligned = 1;
            } else {
                isGmAligned = ((dimN % blockCount) == 0 && (MATMUL_MODULE(MatmulShapeInfo)->GetSingleCoreN() % blockCount) == 0);
            }

            dstOffset = curRow * MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetBaseM() * dimN + curCol * MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetBaseN();
            dstStride = (dimN - width) * sizeof(DstT) / ONE_BLK_SIZE;
            offset = dimN;
        }
        bool isTargetAligned = (baseWidth % blockCount) == 0;
        const bool isComputeLineByLine = (!isGmAligned || dstStride >= UINT16_MAX);

        // 1 if target is not aligned, must copy the unalign data to trans UB
        if constexpr (IsSameType<SrcType, int8_t>::value) {
            bool isOdd = false;
            if constexpr (IsSameType<DstT, int8_t>::value || IsSameType<DstT, uint8_t>::value) {
                if (baseWidth % TWO_TIMES > 0) {
                    isOdd = true;
                }
            }
            bool isSingleCore = MATMUL_CAST_TO_IMPL()->M_ <= MATMUL_MODULE(MatmulShapeInfo)->GetSingleCoreM() && dimN <= MATMUL_MODULE(MatmulShapeInfo)->GetSingleCoreN();
            bool isMutiCoreNeedPad = !isSingleCore && !isComputeLineByLine;
            if (!isTargetAligned && (isSingleCore || isMutiCoreNeedPad) && !isOdd) {
                PadUnalignedToTrans<enSequentialWrite>(trans, gm, dstOffset, isComputeLineByLine, baseHeight, baseWidth,
                                                       baseBlockHeight, baseBlockWidth);
            }
        } else {
            if (!isTargetAligned) {
                PadUnalignedToTrans<enSequentialWrite>(trans, gm, dstOffset, isComputeLineByLine, baseHeight, baseWidth,
                                                       baseBlockHeight, baseBlockWidth);
            }
        }

        // 2. trans nz buffer to nd buffer
        TransNZ2NDByVec(trans, localBuf, baseBlockHeight, baseBlockWidth, (DstT)1.0, baseHeight, baseWidth, baseBlockWidth);

        // 3. copy trans buffer to gm
        int blockLen = baseBlockWidth * (blockCount * sizeof(DstT) / ONE_BLK_SIZE);
        if constexpr (IsSameType<DstT, int8_t>::value || IsSameType<DstT, uint8_t>::value) {
            blockLen = Ceil(blockLen, TWO_TIMES);
        }
        if (isComputeLineByLine) {
            if constexpr (IsSameType<SrcType, int8_t>::value) {
                if constexpr (!enSequentialWrite) {
                    dstOffset = curRow * MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetBaseM() * MATMUL_CAST_TO_IMPL()->N_ + curCol * MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetBaseN();
                    offset = MATMUL_CAST_TO_IMPL()->N_;
                }
                int newBlockCount;
                if constexpr (IsSameType<DstT, float>::value || IsSameType<DstT, int32_t>::value) {
                    newBlockCount = BLOCK_CUBE;
                } else {
                    newBlockCount = ONE_BLK_SIZE / sizeof(DstT);
                }
                if (isTargetAligned) {
                    CopyTrans2GMByVecByLineAlign(gm[dstOffset], trans, baseHeight, blockLen, newBlockCount, offset);
                } else {
                    if (blockLen == 1) {
                        CopyTrans2GMByVecByLineUnalignOneBlock(gm[dstOffset], trans, baseHeight, baseWidth, blockLen, newBlockCount, offset);
                    } else {
                        if constexpr (IsSameType<DstT, int8_t>::value || IsSameType<DstT, uint8_t>::value) {
                            CopyTrans2GMByVecByLineUnalign<true>(gm[dstOffset], trans, baseHeight, baseWidth, baseBlockHeight, baseBlockWidth, blockLen, newBlockCount, offset);
                        } else {
                            CopyTrans2GMByVecByLineUnalign<false>(gm[dstOffset], trans, baseHeight, baseWidth, baseBlockHeight, baseBlockWidth, blockLen, newBlockCount, offset);
                        }
                    }
                }
            } else {
                CopyTrans2GMByVecByLineAlign(gm[dstOffset], trans, baseHeight, blockLen, ONE_BLK_SIZE / sizeof(DstT), offset);
            }
        } else {
            CopyTrans2GMByVecNormal(gm[dstOffset], trans, baseHeight, blockLen, dstStride);
        }
    }

    template <bool enSequentialWrite>
    __aicore__ inline void PadUnalignedToTrans(const LocalTensor<DstT>& trans, const GlobalTensor<DstT>& gm,
        int dstOffset, bool isComputeLineByLine, int32_t baseHeight, int32_t baseWidth, int32_t baseBlockHeight, int32_t baseBlockWidth)
    {
        constexpr int blockCount = sizeof(DstT) == B32_BYTE_SIZE ? BLOCK_CUBE : ONE_BLK_SIZE / sizeof(DstT);
        int32_t alignedSize;
        if constexpr (IsSameType<DstT, int8_t>::value || IsSameType<DstT, uint8_t>::value) {
            alignedSize = GetC0Size<SrcType>();
        } else {
            alignedSize = BLOCK_CUBE;
        }
        int baseUseN = Ceil(baseWidth, alignedSize) * alignedSize;
        int gmTailOffset = dstOffset + baseUseN - blockCount;
        int transTailOffset = baseUseN - blockCount;

        auto enQueEvtID = static_cast<event_t>(GetTPipePtr()->FetchEventID(HardEvent::MTE3_MTE2));
        SetFlag<HardEvent::MTE3_MTE2>(enQueEvtID);
        WaitFlag<HardEvent::MTE3_MTE2>(enQueEvtID);

        if (isComputeLineByLine) {
            if constexpr (enSequentialWrite) {
                PadUnalignedToTransByLine(trans[transTailOffset], gm[gmTailOffset], baseUseN, baseUseN, baseHeight);
            } else {
                PadUnalignedToTransByLine(trans[transTailOffset], gm[gmTailOffset], baseUseN, MATMUL_CAST_TO_IMPL()->N_, baseHeight);
            }
        } else {
            PadUnalignedToTransWithStride(trans[transTailOffset], gm[gmTailOffset], baseHeight, baseWidth, baseBlockWidth);
        }

        // if copy gm to ub, must add the set/wait flag to wait the UB has be writed;
        event_t eventIDMte2ToV = static_cast<event_t>(GetTPipePtr()->FetchEventID(HardEvent::MTE2_V));
        SetFlag<HardEvent::MTE2_V>(eventIDMte2ToV);
        WaitFlag<HardEvent::MTE2_V>(eventIDMte2ToV);
    }

    __aicore__ inline void PadUnalignedToTransByLine(const LocalTensor<DstT>& trans, const GlobalTensor<DstT>& gm,
        int transStride, int gmStride, int baseHeight)
    {
        constexpr int blockCount = sizeof(DstT) == B32_BYTE_SIZE ? BLOCK_CUBE : ONE_BLK_SIZE / sizeof(DstT);
        // copy gm to trans one line by one line
        for (int i = 0; i < baseHeight; ++i) {
            DataCopy(trans[i * transStride], gm[i * gmStride], { static_cast<uint16_t>(1),
                static_cast<uint16_t>(blockCount * sizeof(DstT) / ONE_BLK_SIZE), 0, 0 });
        }
    }

    __aicore__ inline void PadUnalignedToTransWithStride(const LocalTensor<DstT>& trans, const GlobalTensor<DstT>& gm, int baseHeight,
        int baseWidth, int baseBlockWidth)
    {
        constexpr int blockCount = sizeof(DstT) == B32_BYTE_SIZE ? BLOCK_CUBE : ONE_BLK_SIZE / sizeof(DstT);
        // copy gm to trans with stride
        DataCopy(trans, gm, { static_cast<uint16_t>(baseHeight), static_cast<uint16_t>(1),
            static_cast<uint16_t>(MATMUL_CAST_TO_IMPL()->N_ / blockCount - 1), static_cast<uint16_t>(baseWidth / blockCount) });
    }

    __aicore__ inline auto TransNZ2NDByVec(const LocalTensor<DstT>& trans, const LocalTensor<DstT>& localBuf,
        int blockHigh, int blockWidth, DstT scalar, int baseHeight, int baseWidth, int32_t baseBlockWidth)
    {
        event_t eventIDMte3ToV = static_cast<event_t>(GetTPipePtr()->FetchEventID(HardEvent::MTE3_V));
        SetFlag<HardEvent::MTE3_V>(eventIDMte3ToV);
        WaitFlag<HardEvent::MTE3_V>(eventIDMte3ToV);
        // B32's block count is 16
        constexpr int blockCount = sizeof(DstT) == B32_BYTE_SIZE ? BLOCK_CUBE : ONE_BLK_SIZE / sizeof(DstT);
        ASCENDC_ASSERT(((blockWidth * blockCount * sizeof(DstT) / ONE_BLK_SIZE) <= MAX_REPEAT_TIMES), {
            KERNEL_LOG(KERNEL_ERROR, "blockWidth is %d, blockCount is %d, repeat time exceed max time %d", blockWidth,
                blockCount, MAX_REPEAT_TIMES);
        });
        if constexpr (IsSameType<DstT, int8_t>::value || IsSameType<DstT, uint8_t>::value) {
            struct UnaryRepeatParams intriParams;
            int widthAlign = TWO_TIMES;
            int offsetWidth = Ceil(blockWidth, widthAlign) * widthAlign;
            intriParams.dstBlkStride = Ceil(baseWidth, ONE_BLK_SIZE);
            intriParams.srcBlkStride = 1;
            uint32_t dstRepStride = Ceil(baseWidth * sizeof(DstT), ONE_BLK_SIZE) * EIGHT_TIMES;
            intriParams.dstRepStride = dstRepStride;
            bool isBeyondMaxStride = false;
            if (dstRepStride > MAX_REPEAT_STRIDE) {
                isBeyondMaxStride = true;
            }
            intriParams.srcRepStride = (blockCount * sizeof(DstT) / ONE_BLK_SIZE) * EIGHT_TIMES;
            int dstOffset = 0;
            int srcOffset = 0;
            int highBlock = MAX_REPEAT_TIMES;
            int highBlocks = (blockHigh * BLOCK_CUBE) / EIGHT_TIMES / highBlock;
            int highTail = (blockHigh * BLOCK_CUBE) / EIGHT_TIMES % highBlock;
            uint64_t mask[2] = {uint64_t(-1), uint64_t(-1)};
            // mov src to dst width aligned
            LocalTensor<int16_t> tmpSrc = localBuf.template ReinterpretCast<int16_t>();
            LocalTensor<int16_t> tmpDst = trans.template ReinterpretCast<int16_t>();
            SetVectorMask<int16_t>(mask[1], mask[0]);
            constexpr int64_t srcOffsetStride = BLOCK_CUBE * EIGHT_TIMES;
            const int64_t dstOffsetStride = baseBlockWidth * BLOCK_CUBE * EIGHT_TIMES /
                TWO_TIMES;
            for (int i = 0; i < Ceil(blockWidth, TWO_TIMES); ++i) {
                if constexpr (C_TYPE::format != CubeFormat::ND_ALIGN) {
                    // if the baseWidth is not aligned, set the mask value;
                    if (i == (Ceil(blockWidth, TWO_TIMES) - 1) && (baseWidth % blockCount != 0)) {
                        uint64_t masktail = (1 << (Ceil(baseWidth % blockCount, TWO_TIMES))) - 1;
                        mask[0] = masktail + (masktail << SHIFT_16_BIT) + (masktail << SHIFT_32_BIT) + (masktail << SHIFT_48_BIT);
                        mask[1] = mask[0];
                        SetVectorMask<int16_t>(mask[1], mask[0]);
                    }
                }
                int dstMulsOffset = dstOffset;
                for (int j = 0; j < highBlocks; ++j) {
                    Muls<int16_t, false>(tmpDst[dstMulsOffset], tmpSrc[srcOffset], (int16_t)scalar, mask,
                        highBlock, intriParams);
                    srcOffset += highBlock * BLOCK_CUBE;
                    dstMulsOffset += blockWidth * blockCount * highBlock;
                }
                if (highTail) {
                    if (isBeyondMaxStride) {
                        int tmpSrcOffset = srcOffset;
                        for (int j = 0; j < highTail; j++) {
                            Muls<int16_t, false>(tmpDst[dstMulsOffset],
                                tmpSrc[tmpSrcOffset], (int16_t)scalar, mask, 1, intriParams);
                            dstMulsOffset += dstOffsetStride;
                            tmpSrcOffset += srcOffsetStride;
                        }
                    } else {
                        Muls<int16_t, false>(tmpDst[dstMulsOffset], tmpSrc[srcOffset], (int16_t)scalar, mask,
                            highTail, intriParams);
                    }
                    srcOffset += highTail * BLOCK_CUBE * EIGHT_TIMES;
                }
                dstOffset += BLOCK_CUBE;
            }
        } else {
            struct UnaryRepeatParams intriParams;

            int dstOffset = 0;
            int srcOffset = 0;
            int highBlock = MAX_REPEAT_TIMES;
            int highBlocks = 0;
            int highTail = 0;
            int32_t srcStride = highBlock * blockCount;
            int32_t dstStride = blockWidth * blockCount * highBlock;
            bool isBeyondMaxStride = false;
            uint64_t mask[2] = {uint64_t(-1), uint64_t(-1)};

            if constexpr (sizeof(DstT) == B32_BYTE_SIZE) {
                intriParams.dstBlkStride = 1;
                intriParams.srcBlkStride = 1;
                intriParams.dstRepStride = blockWidth * blockCount * sizeof(DstT) / ONE_BLK_SIZE;
                intriParams.srcRepStride = blockCount * sizeof(DstT) / ONE_BLK_SIZE;
                highBlocks = (blockHigh * blockCount) / highBlock;
                highTail = (blockHigh * blockCount) % highBlock;
                mask[0] = static_cast<uint64_t>((1<< blockCount) - 1);
                mask[1] = 0;
            } else {
                intriParams.dstBlkStride = blockWidth;
                intriParams.srcBlkStride = 1;
                uint32_t dstRepStride = (blockWidth * blockCount * sizeof(DstT) / ONE_BLK_SIZE) * EIGHT_TIMES;
                intriParams.dstRepStride = dstRepStride;
                if (dstRepStride > MAX_REPEAT_STRIDE) {
                    isBeyondMaxStride = true;
                }
                intriParams.srcRepStride = (blockCount * sizeof(DstT) / ONE_BLK_SIZE) * EIGHT_TIMES;
                highBlocks = (blockHigh * blockCount) / EIGHT_TIMES / highBlock;
                highTail = (blockHigh * blockCount) / EIGHT_TIMES % highBlock;
                srcStride *= EIGHT_TIMES;
                dstStride *= EIGHT_TIMES;
            }
            SetVectorMask<DstT>(mask[1], mask[0]);
            for (int i = 0; i < blockWidth; ++i) {
                if constexpr (C_TYPE::format != CubeFormat::ND_ALIGN) {
                    // if the baseWidth is not aligned, set the mask value;
                    if (i == (blockWidth - 1) && (baseWidth % blockCount != 0)) {
                        uint64_t masktail = (1 << (baseWidth % blockCount)) - 1;
                        mask[0] = masktail + (masktail << SHIFT_16_BIT) + (masktail << SHIFT_32_BIT) + (masktail << SHIFT_48_BIT);
                        mask[1] = mask[0];
                        SetVectorMask<DstT>(mask[1], mask[0]);
                    }
                }
                int dstMulsOffset = dstOffset;
                for (int j = 0; j < highBlocks; ++j) {
                    Muls<DstT, false>(trans[dstMulsOffset], localBuf[srcOffset], scalar, mask, highBlock, intriParams);
                    srcOffset += srcStride;
                    dstMulsOffset += dstStride;
                }
                if (highTail) {
                    if (isBeyondMaxStride) {
                        const int64_t srcOffsetStride = blockCount * EIGHT_TIMES;
                        const int64_t dstOffsetStride = baseBlockWidth * BLOCK_CUBE * EIGHT_TIMES;
                        for (int j = 0; j < highTail; j++) {
                            Muls<DstT, false>(trans[dstMulsOffset + j * dstOffsetStride],
                                localBuf[srcOffset + j * srcOffsetStride], scalar, mask, 1, intriParams);
                        }
                    } else {
                        Muls<DstT, false>(trans[dstMulsOffset], localBuf[srcOffset], scalar, mask, highTail, intriParams);
                    }
                    if constexpr (sizeof(DstT) == B32_BYTE_SIZE) {
                            srcOffset += highTail * blockCount;
                        } else {
                            srcOffset += highTail * blockCount * EIGHT_TIMES;
                    }
                }
                dstOffset += blockCount;
            }
        }
        event_t eventIDVToMte3 = static_cast<event_t>(GetTPipePtr()->FetchEventID(HardEvent::V_MTE3));
        SetFlag<HardEvent::V_MTE3>(eventIDVToMte3);
        WaitFlag<HardEvent::V_MTE3>(eventIDVToMte3);
    }

    __aicore__ inline void CopyTrans2GMByVecByLineAlign(const GlobalTensor<DstT>& gm, const LocalTensor<DstT>& trans, int baseHeight,
        int blockLen, int blockCount, int offset)
    {
        for (int i = 0; i < baseHeight; ++i) {
            DataCopy(gm[i * offset], trans[i * blockLen * blockCount],
                { 1, static_cast<uint16_t>(blockLen), 0, 0 });
            PipeBarrier<PIPE_MTE3>();
        }
    }

    __aicore__ inline void CopyTrans2GMByVecByLineUnalignOneBlock(const GlobalTensor<DstT>& gm, const LocalTensor<DstT>& trans,
        int baseHeight, int baseWidth, int blockLen, int blockCount, int offset)
    {
        auto eventIDVToS = GetTPipePtr()->FetchEventID(HardEvent::V_S);
        SetFlag<HardEvent::V_S>(eventIDVToS);
        WaitFlag<HardEvent::V_S>(eventIDVToS);
        int padLen = (ONE_BLK_SIZE - baseWidth * sizeof(DstT)) / sizeof(DstT);
        SetAtomicAdd<int16_t>();
        for (int i = 0; i < baseHeight; ++i) {
            LocalTensor<DstT> transAligin = MATMUL_MODULE(LocalWorkspace)->template
                GetWorkspaceWithOffset<ToMatmulConfig(MM_CFG).enableUBReuse>(0)
                .template ReinterpretCast<DstT>();
            int transIndex = i * blockLen * blockCount;
            for (int j = 0; j < baseWidth; ++j) {
                transAligin.SetValue(j, trans.GetValue(transIndex + j));
            }
            for (int j = baseWidth; j < blockCount; ++j) {
                transAligin.SetValue(j, 0);
            }
            DataCopy(gm[i * offset], transAligin, { 1, 1, 0, 0 });
            auto eventIDMTE3ToS = GetTPipePtr()->FetchEventID(HardEvent::MTE3_S);
            SetFlag<HardEvent::MTE3_S>(eventIDMTE3ToS);
            WaitFlag<HardEvent::MTE3_S>(eventIDMTE3ToS);
        }
        SetAtomicNone();
    }

    template <bool isDstB8>
    __aicore__ inline auto CopyTrans2GMByVecByLineUnalign(const GlobalTensor<DstT>& gm, const LocalTensor<DstT>& trans,
        int baseHeight, int baseWidth, int baseBlockHeight, int baseBlockWidth, int blockLen, int blockCount, int offset) -> enable_if_t<isDstB8, void>
    {
        LocalTensor<uint16_t> transAligin = MATMUL_MODULE(LocalWorkspace)->template
            GetWorkspaceWithOffset<ToMatmulConfig(MM_CFG).enableUBReuse>(0).template ReinterpretCast<uint16_t>();
        int remainLen = (baseWidth % blockCount) / TWO_TIMES;
        auto eventIDVToS = GetTPipePtr()->FetchEventID(HardEvent::V_S);
        SetFlag<HardEvent::V_S>(eventIDVToS);
        WaitFlag<HardEvent::V_S>(eventIDVToS);
        LocalTensor<uint16_t> src1Pattern;
        src1Pattern = MATMUL_MODULE(LocalWorkspace)->template
            GetWorkspaceWithOffset<ToMatmulConfig(MM_CFG).enableUBReuse>(
            MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetTransLength() / TWO_TIMES).template ReinterpretCast<uint16_t>();
        LocalTensor<uint16_t> tmpSrc = trans.template ReinterpretCast<uint16_t>();
        src1Pattern.SetSize(PATTERN_SIZE);
        src1Pattern.SetValue(0, 0xFFFF << remainLen);
        src1Pattern.SetValue(1, (1 << remainLen) - 1);
        for (int i = PATTERN_OFFSET; i < PATTERN_SIZE; ++i) {
            src1Pattern.SetValue(i, 0);
        }
        int orinRemain = baseWidth % blockCount;
        int gmOffset = blockCount * (blockLen - PATTERN_OFFSET);
        for (int i = 0; i < baseHeight; ++i) {
            DataCopy(gm[i * offset], trans[i * blockLen * blockCount],
                { 1, static_cast<uint16_t>(blockLen - 1), 0, 0 });
            if (baseWidth % TWO_TIMES == 0) {
                auto enQueEvtID = GetTPipePtr()->FetchEventID(HardEvent::MTE3_V);
                SetFlag<HardEvent::MTE3_V>(enQueEvtID);
                WaitFlag<HardEvent::MTE3_V>(enQueEvtID);
                GatherMaskParams gatherMaskParams(1, 1, PATTERN_SIZE, PATTERN_SIZE);
                uint64_t rsvdCnt = 0;
                GatherMask<uint16_t>(transAligin, tmpSrc[((i + 1) * blockLen - PATTERN_OFFSET) * BLOCK_CUBE], src1Pattern,
                    false, 0, gatherMaskParams, rsvdCnt);
                LocalTensor<DstT> tmpTrans = transAligin.template ReinterpretCast<DstT>();
                DataCopy(gm[i * offset + gmOffset + remainLen * DOUBLE_SPACE], tmpTrans, { 1, 1, 0, 0 });
                PipeBarrier<PIPE_MTE3>();
            } else {
                auto eventIDMTE3ToS = GetTPipePtr()->FetchEventID(HardEvent::MTE3_S);
                SetFlag<HardEvent::MTE3_S>(eventIDMTE3ToS);
                WaitFlag<HardEvent::MTE3_S>(eventIDMTE3ToS);
                LocalTensor<DstT> tmpTrans = transAligin.template ReinterpretCast<DstT>();
                for (int j = 0; j < ONE_BLK_SIZE; ++j) {
                    tmpTrans.SetValue(j, trans[((i + 1) * blockLen - PATTERN_OFFSET) * blockCount + orinRemain].GetValue(j));
                }
                auto eventIDSToMTE3 = GetTPipePtr()->FetchEventID(HardEvent::S_MTE3);
                SetFlag<HardEvent::S_MTE3>(eventIDSToMTE3);
                WaitFlag<HardEvent::S_MTE3>(eventIDSToMTE3);
                DataCopy(gm[i * offset + gmOffset + orinRemain], tmpTrans, { 1, 1, 0, 0 });
                PipeBarrier<PIPE_MTE3>();
            }
        }
    }

    template <bool isDstB8>
    __aicore__ inline auto CopyTrans2GMByVecByLineUnalign(const GlobalTensor<DstT>& gm, const LocalTensor<DstT>& trans,
        int baseHeight, int baseWidth, int baseBlockHeight, int baseBlockWidth, int blockLen, int blockCount, int offset) -> enable_if_t<!isDstB8, void>
    {
        LocalTensor<DstT> transAligin = MATMUL_MODULE(LocalWorkspace)->template
            GetWorkspaceWithOffset<ToMatmulConfig(MM_CFG).enableUBReuse>(0).template ReinterpretCast<DstT>();
        int remainLen = baseWidth % blockCount;
        auto eventIDVToS = GetTPipePtr()->FetchEventID(HardEvent::V_S);
        SetFlag<HardEvent::V_S>(eventIDVToS);
        WaitFlag<HardEvent::V_S>(eventIDVToS);
        LocalTensor<uint16_t> src1Pattern;
        src1Pattern = MATMUL_MODULE(LocalWorkspace)->template
            GetWorkspaceWithOffset<ToMatmulConfig(MM_CFG).enableUBReuse>(
            MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetTransLength() / TWO_TIMES).template ReinterpretCast<uint16_t>();
        src1Pattern.SetSize(PATTERN_SIZE);
        src1Pattern.SetValue(0, 0xFFFF << remainLen);
        src1Pattern.SetValue(1, (1 << remainLen) - 1);
        for (int i = PATTERN_OFFSET; i < PATTERN_SIZE; ++i) {
            src1Pattern.SetValue(i, 0);
        }
        int gmOffset = blockCount * (blockLen - PATTERN_OFFSET);
        for (int i = 0; i < baseHeight; ++i) {
            DataCopy(gm[i * offset], trans[i * blockLen * blockCount],
                { 1, static_cast<uint16_t>(blockLen - 1), 0, 0 });
            GatherMaskParams gatherMaskParams(1, 1, PATTERN_SIZE, PATTERN_SIZE);
            uint64_t rsvdCnt = 0;
            auto enQueEvtID = GetTPipePtr()->FetchEventID(HardEvent::MTE3_V);
            SetFlag<HardEvent::MTE3_V>(enQueEvtID);
            WaitFlag<HardEvent::MTE3_V>(enQueEvtID);
            GatherMask<DstT>(transAligin, trans[((i + 1) * blockLen - PATTERN_OFFSET) * blockCount],
                src1Pattern, false, 0, gatherMaskParams, rsvdCnt);
            DataCopy(gm[i * offset + gmOffset + remainLen], transAligin, { 1, 1, 0, 0 });
            PipeBarrier<PIPE_MTE3>();
        }
    }

    __aicore__ inline void CopyTrans2GMByVecNormal(const GlobalTensor<DstT>& gm, const LocalTensor<DstT>& trans,
        int baseHeight, int blockLen, int dstStride)
    {
        DataCopy(gm, trans, { static_cast<uint16_t>(baseHeight), static_cast<uint16_t>(blockLen), 0,
            static_cast<uint16_t>(dstStride) });
    }

    template <bool enSequentialWrite>
    __aicore__ inline void CopyLocal2GMNZ2NDOnTheFly(const GlobalTensor<DstT>& gm, const LocalTensor<DstT>& localBuf,
                                                     int curRow, int curCol, int32_t baseHeight, int32_t baseWidth,
                                                     int32_t baseBlockHeight, int32_t baseBlockWidth)
    {
        uint32_t dimN = (MATMUL_CAST_TO_IMPL()->Kc_ != 0) ? MATMUL_CAST_TO_IMPL()->Kc_ : MATMUL_CAST_TO_IMPL()->N_;
        constexpr int oneBlockCount = ONE_BLK_SIZE / sizeof(DstT);
        constexpr int blockCount = sizeof(DstT) == B32_BYTE_SIZE ? BLOCK_CUBE : oneBlockCount;
        int calcWidth = baseWidth / blockCount;
        int dstOffset = curRow * MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetBaseM() * dimN + curCol * MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetBaseN();
        int blockLen = blockCount * sizeof(DstT) / ONE_BLK_SIZE;
        int srcRepeatGap = (baseBlockHeight * BLOCK_CUBE * blockCount - blockCount) * sizeof(DstT) / ONE_BLK_SIZE;
        int tail = baseWidth % blockCount;
        LocalTensor<DstT> trans = MATMUL_MODULE(LocalWorkspace)->GetNZ2NDWorkspace().template ReinterpretCast<DstT>();
        trans.SetSize(blockCount);

        int offset = dimN;
        if constexpr (enSequentialWrite) {
            dstOffset = 0;
            offset = baseWidth;
        }

        if constexpr (C_TYPE::format == CubeFormat::ND_ALIGN) {
            offset = Ceil(offset, blockCount) * blockCount;
            calcWidth = baseBlockWidth;
            tail = 0;
        }

        // Allocate MTE2_MTE3 eventId: eventIDMte3ToMte2
        event_t eventIDMte3ToMte2 = static_cast<event_t>(GetTPipePtr()->AllocEventID<HardEvent::MTE3_MTE2>());

        for (int i = 0; i < baseHeight; i++) {
            if (calcWidth > 0) {
                DataCopy(gm[dstOffset + i * offset], localBuf[i * blockCount],
                        { static_cast<uint16_t>(calcWidth), static_cast<uint16_t>(blockLen),
                            static_cast<uint16_t>(srcRepeatGap), 0 });
                if constexpr (IsSameType<typename A_TYPE::T, half>::value &&
                    IsSameType<typename B_TYPE::T, int8_t>::value) {
                    PipeBarrier<PIPE_MTE3>();
                }
            }

            if (tail != 0) {
                int SrcTypeailOffset = i * blockCount + calcWidth * blockCount * Ceil(baseHeight, blockCount) * blockCount;
                if (baseWidth * sizeof(DstT) > ONE_BLK_SIZE) {
                    int dstTailOffset = dstOffset + i * offset + calcWidth * blockCount;
                    int basicOffset = 0;
                    if (sizeof(DstT) == B32_BYTE_SIZE) {
                        DataCopy(gm[dstTailOffset], localBuf[SrcTypeailOffset], { 1, 1, 0, 0 });
                        basicOffset = oneBlockCount;
                    }

                    // reg_mov
                    SrcTypeailOffset = SrcTypeailOffset + basicOffset -
                        blockCount * Ceil(baseHeight, blockCount) * blockCount + baseWidth % blockCount;
                    dstTailOffset = dstTailOffset + basicOffset + baseWidth % blockCount - blockCount;
                    if constexpr (IsSameType<typename A_TYPE::T, half>::value &&
                        IsSameType<typename B_TYPE::T, int8_t>::value) {
                        event_t eventID = static_cast<event_t>(GetTPipePtr()->FetchEventID(HardEvent::V_S));
                        SetFlag<HardEvent::V_S>(eventID);
                        WaitFlag<HardEvent::V_S>(eventID);
                    }
                    int j = 0;
                    for (int i = 0; i < blockCount - baseWidth % blockCount; j++, i++) {
                        DstT scalar = localBuf.GetValue(SrcTypeailOffset + i);
                        trans.SetValue(j, scalar);
                    }
                    SrcTypeailOffset = i * blockCount + calcWidth * blockCount * Ceil(baseHeight, blockCount) * blockCount;
                    for (int i = 0; i < baseWidth % blockCount; j++, i++) {
                        DstT scalar = localBuf.GetValue(SrcTypeailOffset + i);
                        trans.SetValue(j, scalar);
                    }

                    event_t eventIDSToMte3 = static_cast<event_t>(GetTPipePtr()->FetchEventID(HardEvent::S_MTE3));
                    SetFlag<HardEvent::S_MTE3>(eventIDSToMte3);
                    WaitFlag<HardEvent::S_MTE3>(eventIDSToMte3);
                    // copy the tail from ub to gm
                    DataCopy(gm[dstTailOffset], trans, { 1, 1, 0, 0 });
                    if constexpr (IsSameType<typename A_TYPE::T, half>::value &&
                        IsSameType<typename B_TYPE::T, int8_t>::value) {
                        event_t eventID = static_cast<event_t>(GetTPipePtr()->FetchEventID(HardEvent::MTE3_S));
                        SetFlag<HardEvent::MTE3_S>(eventID);
                        WaitFlag<HardEvent::MTE3_S>(eventID);
                    }
                } else {
                    if (i > 0) {
                        WaitFlag<HardEvent::MTE3_MTE2>(eventIDMte3ToMte2);
                    }
                    if constexpr (IsSameType<typename A_TYPE::T, half>::value &&
                        IsSameType<typename B_TYPE::T, int8_t>::value) {
                        event_t eventID = static_cast<event_t>(GetTPipePtr()->FetchEventID(HardEvent::V_MTE2));
                        SetFlag<HardEvent::V_MTE2>(eventID);
                        WaitFlag<HardEvent::V_MTE2>(eventID);
                    }
                    DataCopy(trans, gm[dstOffset + i * offset + baseWidth], { 1, 1, 0, 0 });
                    event_t eventIDMte2ToMte3 = static_cast<event_t>(GetTPipePtr()->FetchEventID(HardEvent::MTE2_MTE3));
                    SetFlag<HardEvent::MTE2_MTE3>(eventIDMte2ToMte3);
                    WaitFlag<HardEvent::MTE2_MTE3>(eventIDMte2ToMte3);
                    DataCopy(gm[dstOffset + i * offset], localBuf[SrcTypeailOffset], { 1, 1, 0, 0 });
                    PipeBarrier<PIPE_MTE3>();
                    DataCopy(gm[dstOffset + i * offset + baseWidth], trans, { 1, 1, 0, 0 });
                    if (i <  baseHeight - 1) {
                        SetFlag<HardEvent::MTE3_MTE2>(eventIDMte3ToMte2);
                    }
                }
            }
        }
        event_t eventID = static_cast<event_t>(GetTPipePtr()->FetchEventID<HardEvent::MTE3_V>());
        SetFlag<HardEvent::MTE3_V>(eventID);
        WaitFlag<HardEvent::MTE3_V>(eventID);
        // Release MTE2_MTE3 eventId: eventIDMte3ToMte2
        GetTPipePtr()->ReleaseEventID<HardEvent::MTE3_MTE2>(eventIDMte3ToMte2);
    }

    __aicore__ inline LocalTensor<DstT> GetLocalBuf()
    {
        LocalTensor<DstT> localBuf = MATMUL_MODULE(LocalWorkspace)->GetCopy2Co2Workspace().template ReinterpretCast<DstT>();
        localBuf.SetSize(MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetBaseM() * MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetBaseN());
        return localBuf;
    }

    __aicore__ inline void UpdateDataCopyParamForQuant(DataCopyEnhancedParams& enhancedParams, int curCol)
    {
        if constexpr (IsSameType<DstT, half>::value) {
            if (MATMUL_MODULE(MatmulQuantProcessor)->GetMatmulQuantMode() == QuantMode_t::DEQF16) {
                enhancedParams.deqScale = DeqScale::DEQ16;
                enhancedParams.deqValue = MATMUL_MODULE(MatmulQuantProcessor)->GetQuantScalarValue();
            } else if (MATMUL_MODULE(MatmulQuantProcessor)->GetMatmulQuantMode() == QuantMode_t::VDEQF16) {
                enhancedParams.deqScale = DeqScale::VDEQ16;
                LocalTensor<uint64_t> quantLocalTensor;
                MATMUL_MODULE(MatmulQuantProcessor)->CopyQuantTensor(quantLocalTensor, curCol, MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetBaseN());
                enhancedParams.deqTensorAddr = reinterpret_cast<uint64_t>(quantLocalTensor.GetPhyAddr());
            }
        } else if constexpr (IsSameType<DstT, int8_t>::value || IsSameType<DstT, uint8_t>::value) {
            enhancedParams.sidStoreMode = (uint8_t)TWO_TIMES;
            if (MATMUL_MODULE(MatmulQuantProcessor)->GetMatmulQuantMode() == QuantMode_t::QF322B8_PRE ||
                MATMUL_MODULE(MatmulQuantProcessor)->GetMatmulQuantMode() == QuantMode_t::REQ8) {
                enhancedParams.deqScale = DeqScale::DEQ8;
                enhancedParams.deqValue = MATMUL_MODULE(MatmulQuantProcessor)->GetQuantScalarValue();
            } else if (MATMUL_MODULE(MatmulQuantProcessor)->GetMatmulQuantMode() == QuantMode_t::VQF322B8_PRE ||
                MATMUL_MODULE(MatmulQuantProcessor)->GetMatmulQuantMode() == QuantMode_t::VREQ8) {
                enhancedParams.deqScale = DeqScale::VDEQ8;
                LocalTensor<uint64_t> quantLocalTensor;
                MATMUL_MODULE(MatmulQuantProcessor)->CopyQuantTensor(quantLocalTensor, curCol, MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetBaseN());
                enhancedParams.deqTensorAddr = reinterpret_cast<uint64_t>(quantLocalTensor.GetPhyAddr());
            }
        }
    }
};
}  // namespace Detail
}  // namespace Impl
}  // namespace AscendC
#endif // IMPL_MATMUL_MODULES_STAGE_COPY_CUBE_OUT_COPY_CUBE_OUT_DATACOPY_H
