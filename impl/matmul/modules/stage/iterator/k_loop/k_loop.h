/**
* Copyright (c) 2024 Huawei Technologies Co., Ltd.
* This file is a part of the CANN Open Software.
* Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
* Please refer to the License for details. You may not use this file except in compliance with the License.
* THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
* INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
* See LICENSE in the root of the software repository for the full text of the License.
*/
/*!
* \file k_loop.h
* \brief
*/
#ifndef IMPL_MATMUL_MODULES_STAGE_ITERATOR_K_LOOP_K_LOOP_H_
#define IMPL_MATMUL_MODULES_STAGE_ITERATOR_K_LOOP_K_LOOP_H_

#include "k_loop_mdl.h"
#include "k_loop_norm.h"
#include "k_loop_intrablock.h"

#endif // _K_LOOP_H_