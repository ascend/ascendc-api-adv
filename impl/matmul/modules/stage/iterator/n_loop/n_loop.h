/**
 * Copyright (c) 2025 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */
/*!
* \file n_loop.h
* \brief
*/

#ifndef IMPL_MATMUL_MODULES_STAGE_ITERATOR_N_LOOP_N_LOOP_H
#define IMPL_MATMUL_MODULES_STAGE_ITERATOR_N_LOOP_N_LOOP_H

#include "n_loop_mdl.h"
#include "n_loop_norm.h"
#include "n_loop_batch.h"
#include "n_loop_mdl_outer_product.h"
#include "n_loop_basic.h"
#include "n_loop_intrablock.h"
#include "n_loop_batch_db.h"

#endif // IMPL_MATMUL_MODULES_ITERATOR_N_LOOP_N_LOOP_H
