/**
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

/*!
 * \file n_loop_mdl_outer_product.h
 * \brief
 */


#ifndef IMPL_MATMUL_MODULES_STAGE_ITERATOR_N_LOOP_N_LOOP_MDL_OUTER_PRODUCT_H
#define IMPL_MATMUL_MODULES_STAGE_ITERATOR_N_LOOP_N_LOOP_MDL_OUTER_PRODUCT_H

#include "n_loop_intf.h"

namespace AscendC {
namespace Impl {
namespace Detail {
/*
    NLoop is considered entirely experimental.
    We retain the freedom to make incompatible changes, but do not guarantee the stability.
    NLoop is only for internal usage, does not support extension or customized specialization!
*/
template <typename IMPL, class A_TYPE, const auto& MM_CFG>
class NLoop<IMPL, A_TYPE, MM_CFG, enable_if_t<DoMatmulMDL(MM_CFG) &&
    MatmulFeatureTrait<MM_CFG>().IsSupportMNL0DB()>>
{
    MATMUL_USE_MODULE(MatmulShapeTiling);
public:
    __aicore__ inline NLoop() = default;
    __aicore__ inline ~NLoop() = default;

    __aicore__ inline void Init(int32_t singleShape)
    {
        SetSingleShape(singleShape);
    }

    __aicore__ inline void SetSingleShape(int32_t singleShape)
    {
        if constexpr (ToMatmulConfig(MM_CFG).singleCoreN != 0 && ToMatmulConfig(MM_CFG).basicN != 0 &&
            !ToMatmulConfig(MM_CFG).enableSetTail) {
            SetSingleShapeFromCFG();
        } else {
            SetSingleShapeFromTiling(singleShape);
        }
        ASCENDC_ASSERT((totalIter_ > 0), {
            KERNEL_LOG(KERNEL_ERROR, "invalid singleCoreN or baseN, totalIter_ is %d , which should be larger than 0",
                totalIter_);
        });
    }

    __aicore__ inline uint32_t GetTotalIter() const
    {
        return totalIter_;
    }

    __aicore__ inline bool OuterNext()
    {
        if constexpr (IsBasicN(MM_CFG)) {
            return false;
        } else {
            outerIndex_++;
            UpdateOuterParams();
            return !OuterEnd();
        }
    }

    __aicore__ inline void OuterStart()
    {
        outerIndex_ = 0;
        innerIndex_ = 0;
        UpdateOuterParams();
    }

    __aicore__ inline bool OuterEnd()
    {
        if constexpr (IsBasicN(MM_CFG)) {
            return true;
        } else {
            return outerIndex_ + 1 > outerIter_;
        }
    }

    __aicore__ inline uint32_t GetOuterIdx() const
    {
        return outerIndex_;
    }

    __aicore__ inline uint32_t GetOuterIter() const
    {
        return outerIter_;
    }

    __aicore__ inline int32_t GetTileShape() const
    {
        return tileShape_;
    }

    __aicore__ inline int32_t GetTileShapeOf(int32_t outerIdx) const
    {
        return (outerIdx + 1 >= outerIter_) ? tailTileShape_ : mainTileShape_;
    }

    __aicore__ inline int32_t GetTileBlockShape() const
    {
        return tileBlockShape_;
    }

    __aicore__ inline bool InnerNext()
    {
        if constexpr (IsBasicN(MM_CFG)) {
            return false;
        } else {
            if (IsL0DoubleBuffer()) {
                innerIndex_ += DB_FACTOR;
            } else {
                ++innerIndex_;
            }
            UpdateInnerParams();
            CalcDBLoopNum();
            return !InnerEnd();
        }
    }

    __aicore__ inline void InnerStart()
    {
        innerIndex_ = innerStartIdx_;
        CalcDBLoopNum();
        UpdateInnerParams();
    }

    __aicore__ inline bool InnerEnd()
    {
        return innerIndex_ >= innerStartIdx_ + innerIter_;
    }

    __aicore__ inline uint32_t GetInnerIdx() const
    {
        return innerIndex_;
    }

    __aicore__ inline uint32_t GetInnerIter() const
    {
        return innerIter_;
    }

    __aicore__ inline int32_t GetBaseShape() const
    {
        return baseShape_;
    }

    __aicore__ inline int32_t GetBaseBlockShape() const
    {
        return baseBlockShape_;
    }

    __aicore__ inline bool IsL0DoubleBuffer()
    {
        if (outerIndex_ + 1 < outerIter_) {
            return innerIndex_ + DB_FACTOR <= innerStartIdx_ + innerIter_;
        } else {
            return (innerIndex_ + DB_FACTOR < totalIter_) || (innerIndex_ + DB_FACTOR == totalIter_ &&
                tailBaseShape_ == MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetBaseN()); 
        }
    }

    __aicore__ inline uint32_t GetL0DBLoopNum() const
    {
        return l0dbLoopNum;
    }

private:
    __aicore__ inline void SetSingleShapeFromCFG()
    {
        totalIter_ = GetNIter(MM_CFG);
        outerIter_ = Ceil(ToMatmulConfig(MM_CFG).singleCoreN,
                            ToMatmulConfig(MM_CFG).basicN * ToMatmulConfig(MM_CFG).stepN);
        tailBaseShape_ = ToMatmulConfig(MM_CFG).singleCoreN % ToMatmulConfig(MM_CFG).basicN;
        if (tailBaseShape_ == 0) {
            tailBaseShape_ = ToMatmulConfig(MM_CFG).basicN;
        }
        mainTileShape_ = MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetBaseN() *
                        MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetStepN();
        tailTileShape_ = ToMatmulConfig(MM_CFG).singleCoreN % mainTileShape_;
        if (tailTileShape_ == 0) {
            tailTileShape_ = mainTileShape_;
        }
    }

    __aicore__ inline void SetSingleShapeFromTiling(int32_t singleShape)
    {
        auto tilingBaseN = MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetBaseN();
        totalIter_ = singleShape / tilingBaseN;
        tailBaseShape_ = singleShape - totalIter_ * tilingBaseN;
        if (tailBaseShape_ == 0) {
            tailBaseShape_ = tilingBaseN;
        } else {
            totalIter_ += 1;
        }
        mainTileShape_ = tilingBaseN * MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetStepN();
        outerIter_ = Ceil(singleShape, mainTileShape_);
        tailTileShape_ = singleShape % mainTileShape_;
        if (tailTileShape_ == 0) {
            tailTileShape_ = mainTileShape_;
        }
    }

    __aicore__ inline void UpdateOuterParams()
    {
        if constexpr (IsBasicN(MM_CFG)) {
            innerStartIdx_ = 0;
            innerIter_ = 1;
            tileShape_ = tailTileShape_;
        } else {
            innerStartIdx_ = outerIndex_ * MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetStepN();
            innerIter_ = (totalIter_ - innerStartIdx_) > MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetStepN() ?
                MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetStepN() : (totalIter_ - innerStartIdx_);
            tileShape_ = (outerIndex_ + 1 >= outerIter_) ? tailTileShape_ : mainTileShape_;
        }
        tileBlockShape_ = Ceil(tileShape_, BLOCK_CUBE);
    }

    __aicore__ inline void UpdateInnerParams()
    {
        if constexpr (IsBasicN(MM_CFG)) {
            baseShape_ = tailBaseShape_;
        } else {
            baseShape_ = (innerIndex_ + 1 == totalIter_) ? tailBaseShape_ : MATMUL_MODULE(MatmulShapeTiling)->GetTiling().GetBaseN();
        }
        baseBlockShape_ = Ceil(baseShape_, BLOCK_CUBE);
    }

    __aicore__ inline void CalcDBLoopNum()
    {
        l0dbLoopNum = IsL0DoubleBuffer() ? DB_FACTOR : 1;
    }

    uint32_t totalIter_;
    // OuterLoop
    uint32_t outerIndex_ = 0;
    uint32_t outerIter_;
    // InnerLoop
    uint32_t innerIndex_ = 0;
    uint32_t innerIter_;
    uint32_t innerStartIdx_ = 0;
    // Shape
    int32_t mainTileShape_;
    int32_t tailTileShape_;
    int32_t tileShape_;
    int32_t tileBlockShape_;
    int32_t baseShape_;
    int32_t tailBaseShape_;
    int32_t baseBlockShape_;
    // DBLoop
    uint32_t l0dbLoopNum = 1;
};

}  // namespace Detail
}  // namespace Impl
}  // namespace AscendC
#endif // _N_LOOP_MDL_OUTER_PRODUCT_H_
