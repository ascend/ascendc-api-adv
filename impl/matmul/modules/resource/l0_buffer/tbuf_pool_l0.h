/**
 * Copyright (c) 2025 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

/*!
 * \file tbuf_pool_l0.h
 * \brief
 */
#ifndef IMPL_MATMUL_MODULES_RESOURCE_L0_BUFFER_TBUF_POOL_L0_H
#define IMPL_MATMUL_MODULES_RESOURCE_L0_BUFFER_TBUF_POOL_L0_H

#include "tbuf_pool_l0_common.h"
#include "tbuf_pool_l0_a2b2_shared.h"
#include "tbuf_pool_l0_cache.h"

#endif // IMPL_MATMUL_MODULES_RESOURCE_L0_BUFFER_TBUF_POOL_L0_H
