/**
 * Copyright (c) 2025 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

/*!
 * \file tbuf_pool_l0_common.h
 * \brief
 */
#ifndef IMPL_MATMUL_MODULES_RESOURCE_L0_BUFFER_TBUF_POOL_L0_COMMON_H
#define IMPL_MATMUL_MODULES_RESOURCE_L0_BUFFER_TBUF_POOL_L0_COMMON_H

#include "tbuf_pool_l0_intf.h"

namespace AscendC {
namespace Impl {
namespace Detail {

template <typename IMPL, typename A_TYPE, typename B_TYPE, const auto& MM_CFG>
class TBufPoolL0<IMPL, A_TYPE, B_TYPE, MM_CFG,
    enable_if_t<(DoMatmulMDL(MM_CFG) || isNormEnableScheduler<A_TYPE, MM_CFG> ||
        IsBmmEnableScheduler<A_TYPE, MM_CFG> || DoMatmulSpecialMDL(MM_CFG) || IsBasicBlockEnable<MM_CFG> ||
        DoMatmulIBShareNorm(MM_CFG) || IsIntrablock<MM_CFG>) && !IsA2B2Shared(MM_CFG) && !IsL0Cache<A_TYPE, MM_CFG>()>> {
public:
    __aicore__ inline TBufPoolL0() {}
    __aicore__ inline ~TBufPoolL0() {
#if __CCE_AICORE__ <= 200
        WaitFlag<HardEvent::M_MTE1>(eventIdMToMte1Ping_);
        WaitFlag<HardEvent::M_MTE1>(eventIdMToMte1Pong_);
        GetTPipePtr()->ReleaseEventID<HardEvent::M_MTE1>(eventIdMToMte1Ping_);
        GetTPipePtr()->ReleaseEventID<HardEvent::M_MTE1>(eventIdMToMte1Pong_);
#endif
    };
    __aicore__ inline void Init(bool isL0Db = true)
    {
        useL0PingPong_ = static_cast<uint16_t>(isL0Db);
        GetTPipePtr()->InitBuffer(l0aBuf_, L0AUF_SIZE);
        GetTPipePtr()->InitBuffer(l0bBuf_, L0BUF_SIZE);
#if __CCE_AICORE__ <= 200
        eventIdMToMte1Ping_ = static_cast<event_t>(GetTPipePtr()->AllocEventID<HardEvent::M_MTE1>());
        eventIdMToMte1Pong_ = static_cast<event_t>(GetTPipePtr()->AllocEventID<HardEvent::M_MTE1>());
        SetFlag<HardEvent::M_MTE1>(eventIdMToMte1Ping_);
        SetFlag<HardEvent::M_MTE1>(eventIdMToMte1Pong_);
#endif
    }

    __aicore__ inline void SetDBFlag(bool isL0Db = true)
    {
        useL0PingPong_ = static_cast<uint16_t>(isL0Db);
    }

    template <bool IS_INTRA_BLOCK = false>
    __aicore__ inline TBufPoolL0& Allocate() {
        if constexpr (IS_INTRA_BLOCK) {
            return *this;
        } else {
            WaitFlag<HardEvent::M_MTE1>(l0PingPongFlag_);
        }
        return *this;
    }

    template <TPosition Pos, typename T, bool IS_INTRA_BLOCK = false>
    __aicore__ inline LocalTensor<T> GetBuffer(uint8_t subIdx = 0)
    {
        LocalTensor<T> tempTensor;
        if constexpr (Pos == TPosition::A2) {
            tempTensor = l0aBuf_.Get<T>();
            if (l0PingPongFlag_ != 0) {
                if constexpr (IsSameType<T, int4b_t>::value) {
                    tempTensor = tempTensor[L0AUF_SIZE / sizeof(T)];
                } else {
                    tempTensor = tempTensor[L0AUF_SIZE / 2 / sizeof(T)];
                }
            }
        } else {
            tempTensor = l0bBuf_.Get<T>();
            if constexpr (IS_INTRA_BLOCK) {
                if (subIdx != 0) {
                    tempTensor = tempTensor[L0BUF_SIZE / 2 / sizeof(T)];
                }
            } else {
                if (l0PingPongFlag_ != 0) {
                    if constexpr (IsSameType<T, int4b_t>::value) {
                        tempTensor = tempTensor[L0BUF_SIZE / sizeof(T)];
                    } else {
                        tempTensor = tempTensor[L0BUF_SIZE / 2 / sizeof(T)];
                    }
                }
            }
        }
        return tempTensor;
    }

    template <TPosition Pos>
    __aicore__ inline bool Hit(uint32_t pos = 0) {
        return false;
    }

    __aicore__ inline void ResetCache() {}

    __aicore__ inline void EnQue()
    {
        SetFlag<HardEvent::MTE1_M>(l0PingPongFlag_);
    }

    __aicore__ inline void DeQue()
    {
        WaitFlag<HardEvent::MTE1_M>(l0PingPongFlag_);
    }

    __aicore__ inline void Free()
    {
        SetFlag<HardEvent::M_MTE1>(l0PingPongFlag_);
        l0PingPongFlag_ = useL0PingPong_ - l0PingPongFlag_;
    }

private:
    TBuf<TPosition::A2> l0aBuf_;
    TBuf<TPosition::B2> l0bBuf_;
    uint16_t l0PingPongFlag_{0};
    uint16_t useL0PingPong_{1};
#if __CCE_AICORE__ <= 200
    event_t eventIdMToMte1Ping_;
    event_t eventIdMToMte1Pong_;
#endif
};

}  // namespace Detail
}  // namespace Impl
}  // namespace AscendC
#endif // IMPL_MATMUL_MODULES_RESOURCE_L0_BUFFER_TBUF_POOL_L0_COMMON_H