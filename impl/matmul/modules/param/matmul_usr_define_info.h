/**
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

/*!
* \file matmul_usr_define_info.h
* \brief
*/
#ifndef IMPL_MATMUL_MODULES_PARAM_MATMUL_USER_DEFINE_INFO_H_
#define IMPL_MATMUL_MODULES_PARAM_MATMUL_USER_DEFINE_INFO_H_

namespace AscendC {
namespace Impl {
namespace Detail {
template <typename IMPL, const auto &MM_CFG>
class MatmulUserDefineInfo {
public:
    __aicore__ inline uint64_t GetSelfDefineData() const
    {
        return MATMUL_CONST_PARAM_VAR.dataPtr_;
    }

    __aicore__ inline uint64_t GetUserDefineInfo() const
    {
        return MATMUL_CONST_PARAM_VAR.tilingPtr_;
    }
};
}  // namespace Detail
}  // namespace Impl
}  // namespace AscendC
#endif // IMPL_MATMUL_MODULES_PARAM_MATMUL_USER_DEFINE_INFO_H_