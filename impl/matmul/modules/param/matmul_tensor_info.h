/**
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

/*!
 * \file matmul_tensor_info.h
 * \brief matmul variable manager
 */

#ifndef IMPL_MATMUL_MODULES_PARAM_MATMUL_TENSOR_INFO_H
#define IMPL_MATMUL_MODULES_PARAM_MATMUL_TENSOR_INFO_H

#include "../matmul_module.h"

namespace AscendC {
namespace Impl {
namespace Detail {
template <typename IMPL, const auto &MM_CFG, class INPUT_TYPE, typename = void>
class MatmulTensorInfo {
    using SrcT = typename INPUT_TYPE::T;
public:
    template <bool IS_INTRA_BLOCK = false>
    __aicore__ inline GlobalTensor<SrcT> GetGlobalTensor() const
    {
        GlobalTensor<SrcT> globalMatrix;
        if constexpr (IS_INTRA_BLOCK) {
            globalMatrix.SetGlobalBuffer(MATMUL_CONST_INTRA_BLOCK.aGlobal);
        } else {
            globalMatrix.SetGlobalBuffer(MATMUL_CONST_PARAM_VAR.aGlobal_);
        }
        return globalMatrix;
    }

    __aicore__ inline LocalTensor<SrcT> GetLocalTensor() const
    {
        LocalTensor<SrcT> localMatrix;
        localMatrix.SetAddr(MATMUL_CONST_PARAM_VAR.leftMatrix_.address_);
        return localMatrix;
    }

    template <bool IS_INTRA_BLOCK = false>
    __aicore__ inline void SetGlobalTensor(const GlobalTensor<SrcT>& globalMatrix, bool isTranspose)
    {
        if constexpr (IS_INTRA_BLOCK) {
            MATMUL_INTRA_BLOCK.aGlobal = globalMatrix.address_;
            MATMUL_INTRA_BLOCK.isTransposeA = isTranspose;
        } else {
            MATMUL_PARAM_VAR.aGlobal_ = globalMatrix.address_;
            MATMUL_PARAM_VAR.isTransposeA_ = isTranspose;
        }
    }

    __aicore__ inline void SetLocalTensor(const LocalTensor<SrcT>& localMatrix, bool isTranspose)
    {
        MATMUL_PARAM_VAR.leftMatrix_.address_ = localMatrix.address_;
        MATMUL_PARAM_VAR.isTransposeA_ = isTranspose;
    }
};

template <typename IMPL, const auto &MM_CFG, class INPUT_TYPE>
class MatmulTensorInfo<IMPL, MM_CFG, INPUT_TYPE, enable_if_t<INPUT_TYPE::TAG == InputTypeTag::B>> {
    using SrcT = typename INPUT_TYPE::T;
public:
    template <bool IS_INTRA_BLOCK = false>
    __aicore__ inline GlobalTensor<SrcT> GetGlobalTensor() const
    {
        GlobalTensor<SrcT> globalMatrix;
        if constexpr (IS_INTRA_BLOCK) {
            globalMatrix.SetGlobalBuffer(MATMUL_CONST_INTRA_BLOCK.bGlobal);
        } else {
            globalMatrix.SetGlobalBuffer(MATMUL_CONST_PARAM_VAR.bGlobal_);
        }
        return globalMatrix;
    }

    __aicore__ inline LocalTensor<SrcT> GetLocalTensor() const
    {
        LocalTensor<SrcT> localMatrix;
        localMatrix.SetAddr(MATMUL_CONST_PARAM_VAR.rightMatrix_.address_);
        return localMatrix;
    }

    template <bool IS_INTRA_BLOCK = false>
    __aicore__ inline void SetGlobalTensor(const GlobalTensor<SrcT>& globalMatrix, bool isTranspose)
    {
        if constexpr (IS_INTRA_BLOCK) {
            MATMUL_INTRA_BLOCK.bGlobal = globalMatrix.address_;
            MATMUL_INTRA_BLOCK.isTransposeB = isTranspose;
        } else {
            MATMUL_PARAM_VAR.bGlobal_ = globalMatrix.address_;
            MATMUL_PARAM_VAR.isTransposeB_ = isTranspose;
        }
    }

    __aicore__ inline void SetLocalTensor(const LocalTensor<SrcT>& localMatrix, bool isTranspose)
    {
        MATMUL_PARAM_VAR.rightMatrix_.address_ = localMatrix.address_;
        MATMUL_PARAM_VAR.isTransposeB_ = isTranspose;
    }

    __aicore__ inline void SetGlobalSparseIndex(const GlobalTensor<uint8_t>& indexGlobal)
    {
        MATMUL_PARAM_VAR.indexGlobal_ = indexGlobal;
    }

    __aicore__ inline void SetLocalSparseIndex(const LocalTensor<uint8_t>& indexLocal)
    {
        MATMUL_PARAM_VAR.indexLocal_ = indexLocal;
    }

    __aicore__ inline GlobalTensor<uint8_t> GetGlobalSparseIndex()
    {
        return MATMUL_CONST_PARAM_VAR.indexGlobal_;
    }

    __aicore__ inline LocalTensor<uint8_t> GetLocalSparseIndex()
    {
        return MATMUL_CONST_PARAM_VAR.indexLocal_;
    }
};
}  // namespace Detail
}  // namespace Impl
}  // namespace AscendC
#endif // IMPL_MATMUL_MODULES_PARAM_MATMUL_TENSOR_INFO_H
