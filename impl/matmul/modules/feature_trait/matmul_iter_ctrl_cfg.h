/**
* Copyright (c) 2024 Huawei Technologies Co., Ltd.
* This file is a part of the CANN Open Software.
* Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
* Please refer to the License for details. You may not use this file except in compliance with the License.
* THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
* INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
* See LICENSE in the root of the software repository for the full text of the License.
*/

/*!
* \file matmul_iter_ctrl_cfg.h
* \brief
*/
#ifndef IMPL_MATMUL_MODULES_MATMUL_ITER_CTRL_CFG_H
#define IMPL_MATMUL_MODULES_MATMUL_ITER_CTRL_CFG_H

#include "../../../../lib/matmul/tiling.h"
#include "../../../../lib/matmul/constant_tiling.h"

namespace AscendC {
namespace Impl {
namespace Detail {

struct MatmulIterCtrlCfg {
    bool isFixedStep;
    int32_t stepM;
    int32_t stepN;
    IterateOrder iterOrder;
};

}  // namespace Detail
}  // namespace Impl
}  // namespace AscendC
#endif // _MATMUL_ITER_CTRL_CFG_H_