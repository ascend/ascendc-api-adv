/**
* Copyright (c) 2024 Huawei Technologies Co., Ltd.
* This file is a part of the CANN Open Software.
* Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
* Please refer to the License for details. You may not use this file except in compliance with the License.
* THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
* INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
* See LICENSE in the root of the software repository for the full text of the License.
*/

/*!
* \file matmul_macro_def.h
* \brief
*/
#ifndef IMPL_MATMUL_MATMUL_MACRO_DEF_H
#define IMPL_MATMUL_MATMUL_MACRO_DEF_H
#include "matmul_utils.h"
#include "matmul_macro_utils.h"
#include "matmul_macro_v220_impl.h"
#include "matmul_macro_v220_l0cache_impl.h"
#include "matmul_macro_v220_basic_impl.h"
#include "matmul_macro_v200_impl.h"
#include "modules/matmul_param.h"

namespace AscendC {

/* **************************************************************************************************
 * MatmulMacroImpl                                             *
 * ************************************************************************************************* */

struct MatmulMacroNone {};

template <typename IMPL, class A_TYPE, class B_TYPE, class C_TYPE, class BIAS_TYPE, const auto& MM_CFG, MatmulVersion MM_VER>
struct MatmulMacroImpl {
    __aicore__ inline MatmulMacroImpl() {};
};

#if __CCE_AICORE__ >= 220
// CFG_NORM
template <typename IMPL, class A_TYPE, class B_TYPE, class C_TYPE, class BIAS_TYPE, const auto& MM_CFG>
struct MatmulMacroImpl<IMPL, A_TYPE, B_TYPE, C_TYPE, BIAS_TYPE, MM_CFG, GetMatmulVersion(CFG_NORM)> {
    using L0cT = typename GetDstType<typename A_TYPE::T>::Type;
    __aicore__ inline MatmulMacroImpl() {};
    static constexpr uint16_t GEMV_MODE = (A_TYPE::format == CubeFormat::VECTOR) ? 1 :
        ((A_TYPE::format == CubeFormat::SCALAR) ? 2 : 0);
    using PARAMS = typename AscendC::Conditional<(isNormEnableScheduler<A_TYPE, MM_CFG> || IsIntrablock<MM_CFG>), MatmulMacroNone,
             MacroMatmul<IMPL, L0cT, typename A_TYPE::T, typename B_TYPE::T, typename BIAS_TYPE::T,
        EnUnitFlag(MM_CFG), GEMV_MODE, IsL0Cache<A_TYPE, MM_CFG>(), IsA2B2Shared(MM_CFG)>>::type;
};
// CFG_MDL
template <typename IMPL, class A_TYPE, class B_TYPE, class C_TYPE, class BIAS_TYPE, const auto& MM_CFG>
struct MatmulMacroImpl<IMPL, A_TYPE, B_TYPE, C_TYPE, BIAS_TYPE, MM_CFG, GetMatmulVersion(CFG_MDL)> {
    using L0cT = typename GetDstType<typename A_TYPE::T>::Type;
    __aicore__ inline MatmulMacroImpl() {};
    static constexpr uint16_t GEMV_MODE = (A_TYPE::format == CubeFormat::VECTOR) ? 1 :
        ((A_TYPE::format == CubeFormat::SCALAR) ? 2 : 0);
    using PARAMS = typename AscendC::Conditional<DoMatmulMDL(MM_CFG), MatmulMacroNone,
        MacroMatmul<IMPL, L0cT, typename A_TYPE::T, typename B_TYPE::T, typename BIAS_TYPE::T,
        EnUnitFlag(MM_CFG), GEMV_MODE, IsL0Cache<A_TYPE, MM_CFG>(), IsA2B2Shared(MM_CFG)>>::type;
};

// CFG_IBSHARE_NORM
template <typename IMPL, class A_TYPE, class B_TYPE, class C_TYPE, class BIAS_TYPE, const auto& MM_CFG>
struct MatmulMacroImpl<IMPL, A_TYPE, B_TYPE, C_TYPE, BIAS_TYPE, MM_CFG, GetMatmulVersion(CFG_IBSHARE_NORM)> {
    using L0cT = typename GetDstType<typename A_TYPE::T>::Type;
    __aicore__ inline MatmulMacroImpl() {};
    static constexpr uint16_t GEMV_MODE = (A_TYPE::format == CubeFormat::VECTOR) ? 1 :
        ((A_TYPE::format == CubeFormat::SCALAR) ? 2 : 0);
    using PARAMS = MatmulMacroNone;
};
#elif __CCE_AICORE__ == 200
template <typename IMPL, class A_TYPE, class B_TYPE, class C_TYPE, class BIAS_TYPE, const auto& MM_CFG>
struct MatmulMacroImpl<IMPL, A_TYPE, B_TYPE, C_TYPE, BIAS_TYPE, MM_CFG, GetMatmulVersion(CFG_NORM)> {
    using L0cT = typename GetDstType<typename A_TYPE::T>::Type;
    __aicore__ inline MatmulMacroImpl() {};
    using PARAMS = typename AscendC::Conditional<isNormEnableScheduler<A_TYPE, MM_CFG> || IsIntrablock<MM_CFG>, MatmulMacroNone,
             MacroMatmulV200<IMPL, L0cT, typename A_TYPE::T, typename A_TYPE::T>>::type;
};
template <typename IMPL, class A_TYPE, class B_TYPE, class C_TYPE, class BIAS_TYPE, const auto& MM_CFG>
struct MatmulMacroImpl<IMPL, A_TYPE, B_TYPE, C_TYPE, BIAS_TYPE, MM_CFG, GetMatmulVersion(CFG_MDL)> {
    using L0cT = typename GetDstType<typename A_TYPE::T>::Type;
    __aicore__ inline MatmulMacroImpl() {};
    using PARAMS = typename AscendC::Conditional<DoMatmulMDL(MM_CFG), MatmulMacroNone,
        MacroMatmulV200<IMPL, L0cT, typename A_TYPE::T, typename A_TYPE::T>>::type;
};
#endif

// MM_CFG_BB
template <typename IMPL, class A_TYPE, class B_TYPE, class C_TYPE, class BIAS_TYPE, const auto& MM_CFG>
struct MatmulMacroImpl<IMPL, A_TYPE, B_TYPE, C_TYPE, BIAS_TYPE, MM_CFG, GetMatmulVersion(MM_CFG_BB)> {
    using L0cT = typename GetDstType<typename A_TYPE::T>::Type;
    __aicore__ inline MatmulMacroImpl() {};
    using PARAMS = MatmulMacroNone;
};

}  // namespace AscendC
#endif // _MATMUL_MACRO_DEF_H_
