/**
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

#ifndef IMPL_MATMUL_MATMUL_UTILS_H
#define IMPL_MATMUL_MATMUL_UTILS_H

namespace AscendC {
template <typename SrcT> __aicore__ inline constexpr int32_t GetC0Size()
{
    if (sizeof(SrcT) == sizeof(float)) {
        return 8;
    } else if (sizeof(SrcT) == sizeof(int8_t)) {
        return 32;
    }
    return 16;
}

template <typename T, typename U>
constexpr bool IsSameTypeV = AscendC::IsSameType<T, U>::value;

template <typename T, typename... Others>
struct IsTypeOneOf {
    static constexpr bool value = false;
};

template <typename T, typename First, typename... Others>
struct IsTypeOneOf<T, First, Others...> {
    static constexpr bool value = IsSameTypeV<T, First> || IsTypeOneOf<T, Others...>::value;
};

template <typename T, typename... Others>
constexpr bool IsTypeOneOfV = IsTypeOneOf<T, Others...>::value;

struct CopyGMParams {
    int dstOffset { 0 };
    int baseUseN { 0 };
    int blockCount { 0 };
    int dstStride { 0 };
    bool isComputeLineByLine { false };
};

struct DataCopyOutParams {
    __aicore__ DataCopyOutParams()
    {
        quantMode = 0;
        cBurstNum = 0;
        burstLen = 0;
        srcStride = 0;
        dstStride = 0;
        oriNSize = 0;
        enUnitFlag = false;
        quantScalar = 0;
        curM = 0;
        curN = 0;
    }
    __aicore__ DataCopyOutParams(const uint16_t count, const uint16_t len,
        const uint16_t srcStrideIn, const uint32_t dstStrideIn, const uint16_t nSize, const bool unitFlag,
        const int curMPos, const int curNPos)
    {
        cBurstNum = count;
        burstLen= len;
        srcStride = srcStrideIn;
        dstStride = dstStrideIn;
        oriNSize = nSize;
        enUnitFlag = unitFlag;
        curM = curMPos;
        curN = curNPos;
    }
    uint8_t quantMode = 0;
    uint16_t cBurstNum = 0;
    uint16_t burstLen = 0;
    uint16_t srcStride = 0;
    uint32_t dstStride = 0;
    uint16_t oriNSize = 0;
    bool enUnitFlag = false;
    uint64_t quantScalar = 0;
    int curM = 0;
    int curN = 0;
    uint64_t cbufWorkspaceAddr = 0;
};

namespace Impl {
#define L0AUF_SIZE 65536
#define L0BUF_SIZE 65536
constexpr int32_t QUEUE_DEPTH = 1;
constexpr int32_t NZ_MASK_VAlUE = 2;
constexpr int32_t FLOAT_FACTOR = 2;
constexpr int32_t B4_C0SIZE = 64;
constexpr int32_t B8_C0SIZE = 32;
constexpr int32_t B32_C0SIZE = 8;
constexpr int32_t B16_C0SIZE = 16;
constexpr int32_t L0_SIZE = 64 * 1024;
constexpr int32_t MAX_BLOCK_COUNT_SIZE = 4095;
#if __CCE_AICORE__ < 200
constexpr int32_t DB_FACTOR = 1;
#else
constexpr int32_t DB_FACTOR = 2;
#endif

constexpr uint8_t UNIT_FLAG_CHECK = 2; 
constexpr uint8_t UNIT_FLAG_SET = 3; 

// the KFC_MESSAGE_LENGTH is 64
// the MAX_MSG_COUNT is 64
// the BIDIRECTION_NUM is 2
// the MAX_MATMUL_OBJ is 8
// the MAX_AIV_NUM is 50
// the TOTAL_UB_SIZE is 192 * 1024; for ascend910b1
// fixpipe vdeqf16 quant tensor Gm offset
// the gm_offset is AllMsgLen + AllCntMsgLen + AllUBMap
//           equal: sizeof(KfcMsg) * 2 * MAX_MSG_COUNT * MAX_AIV_NUM +
//           equal: sizeof(KfcMsg) * MAX_MATMUL_OBJ * MAX_AIV_NUM +
//           equal: TOTAL_UB_SIZE * MAX_AIV_NUM
constexpr int64_t GM_OFFSET = 128 * 2 * 64 * 50 + 128 * 8 * 50 + 192 * 1024 * 50;
}

template <typename T>
const LocalTensor<T> NULL_TENSOR;

template <typename T>
const GlobalTensor<T> GLOBAL_NULL_TENSOR;

template <typename T> struct GetDstType {
    using Type = T;
};

template <> struct GetDstType<float> {
    using Type = float;
};

template <> struct GetDstType<half> {
    using Type = float;
};

template <> struct GetDstType<int8_t> {
    using Type = int32_t;
};

#if __CCE_AICORE__ >= 220
template <> struct GetDstType<bfloat16_t> {
    using Type = float;
};

template <> struct GetDstType<int4b_t> {
    using Type = int32_t;
};
#endif

int32_t constexpr GetNdNzMask(CubeFormat dstFormat, CubeFormat srcFormat)
{
    if ((srcFormat == CubeFormat::ND) && (dstFormat == CubeFormat::NZ)) {
        return 1;
    } else if ((srcFormat == CubeFormat::NZ) && (dstFormat == CubeFormat::ND)) {
        return Impl::NZ_MASK_VAlUE;
    }
    return 0;
}

template <typename SrcT>
__aicore__ inline constexpr static int32_t AuxGetFactor()
{
    if (sizeof(SrcT) == sizeof(float)) {
        return Impl::FLOAT_FACTOR;
    }
    return 1;
}

template <typename SrcT>
__aicore__ inline constexpr static int32_t AuxGetC0Size()
{
    if (sizeof(SrcT) == sizeof(float)) {
        return Impl::B32_C0SIZE;
    } else if (IsSameType<SrcT, int8_t>::value) {
        return Impl::B8_C0SIZE;
    } else if (IsSameType<SrcT, int4b_t>::value) {
        return Impl::B4_C0SIZE;
    }
    return Impl::B16_C0SIZE;
}

__aicore__ constexpr bool DoMatmulNorm(MatmulConfig mmCFG)
{
    return mmCFG.doNorm;
}

__aicore__ constexpr bool DoMatmulNorm(const MatmulApiStaticTiling &mmCFG)
{
    return DoMatmulNorm(mmCFG.cfg);
}

__aicore__ constexpr bool EnUnitFlag(MatmulConfig mmCFG)
{
    return mmCFG.enUnitFlag;
}

__aicore__ constexpr bool EnUnitFlag(const MatmulApiStaticTiling &mmCFG)
{
    return EnUnitFlag(mmCFG.cfg);
}

__aicore__ constexpr bool DoMatmulBasicBlock(MatmulConfig mmCFG)
{
    return mmCFG.doBasicBlock;
}

__aicore__ constexpr bool DoMatmulBasicBlock(const MatmulApiStaticTiling &mmCFG)
{
    return DoMatmulBasicBlock(mmCFG.cfg);
}

__aicore__ constexpr bool DoMatmulSpecialBasicBlock(MatmulConfig mmCFG)
{
    return mmCFG.doSpecialBasicBlock;
}

__aicore__ constexpr bool DoMatmulSpecialBasicBlock(const MatmulApiStaticTiling &mmCFG)
{
    return DoMatmulSpecialBasicBlock(mmCFG.cfg);
}

__aicore__ constexpr bool DoMatmulMDL(MatmulConfig mmCFG)
{
    return mmCFG.doMultiDataLoad;
}

__aicore__ constexpr bool DoMatmulMDL(const MatmulApiStaticTiling &mmCFG)
{
    return DoMatmulMDL(mmCFG.cfg);
}

__aicore__ constexpr bool DoMatmulIBShareNorm(MatmulConfig mmCFG)
{
    return mmCFG.doIBShareNorm;
}

__aicore__ constexpr bool DoMatmulIBShareNorm(const MatmulApiStaticTiling &mmCFG)
{
    return DoMatmulIBShareNorm(mmCFG.cfg);
}

__aicore__ constexpr bool DoMatmulSpecialMDL(MatmulConfig mmCFG)
{
    return mmCFG.doSpecialMDL;
}

__aicore__ constexpr bool DoMatmulSpecialMDL(const MatmulApiStaticTiling &mmCFG)
{
    return DoMatmulSpecialMDL(mmCFG.cfg);
}

__aicore__ constexpr bool IsSharedObj(MatmulConfig mmCFG)
{
    return !mmCFG.enableInit || mmCFG.enableMixDualMaster;
}

__aicore__ constexpr bool IsSharedObj(const MatmulApiStaticTiling &mmCFG)
{
    return IsSharedObj(mmCFG.cfg);
}

__aicore__ constexpr bool IsA2B2Shared(MatmulConfig mmCFG)
{
    return mmCFG.isA2B2Shared;
}

__aicore__ constexpr bool IsA2B2Shared(const MatmulApiStaticTiling &mmCFG)
{
    return IsA2B2Shared(mmCFG.cfg);
}

__aicore__ inline constexpr MatmulConfig ToMatmulConfig(const MatmulConfig &cfg)
{
    return cfg;
}

__aicore__ inline constexpr MatmulConfig ToMatmulConfig(const MatmulApiStaticTiling &cfg)
{
    return cfg.cfg;
}

__aicore__ constexpr MatmulVersion GetMatmulVersion(MatmulConfig mmCFG)
{
    if (DoMatmulNorm(mmCFG)) {
        return MatmulVersion::NORMAL;
    } else if (DoMatmulBasicBlock(mmCFG) || DoMatmulSpecialBasicBlock(mmCFG)) {
        return MatmulVersion::BASIC_BLOCK;
    } else if (DoMatmulMDL(mmCFG) || DoMatmulSpecialMDL(mmCFG)) {
        return MatmulVersion::MULTI_DATA_LOAD;
    } else if (DoMatmulIBShareNorm(mmCFG)) {
        return MatmulVersion::IBSHARE_NORM;
    }
    return MatmulVersion::NORMAL;
}

__aicore__ constexpr MatmulVersion GetMatmulVersion(const MatmulApiStaticTiling &mmCFG)
{
    return GetMatmulVersion(mmCFG.cfg);
}

__aicore__ constexpr bool IsFullStaticTiling(MatmulConfig mmCFG)
{
    return mmCFG.singleCoreM != 0 && mmCFG.basicM != 0;
}

__aicore__ constexpr bool IsFullStaticTiling(const MatmulApiStaticTiling &mmCFG)
{
    return IsFullStaticTiling(mmCFG.cfg);
}

__aicore__ constexpr bool IsStaticTilingEnable(MatmulConfig mmCFG)
{
    return mmCFG.singleCoreM != 0;
}

__aicore__ constexpr bool IsStaticTilingEnable(const MatmulApiStaticTiling &mmCFG)
{
    return IsStaticTilingEnable(mmCFG.cfg);
}

__aicore__ constexpr bool IsStaticPaddingEnable(MatmulConfig mmCFG)
{
    return mmCFG.enableStaticPadZeros;
}

__aicore__ constexpr bool IsStaticPaddingEnable(const MatmulApiStaticTiling &mmCFG)
{
    return IsStaticPaddingEnable(mmCFG.cfg);
}

__aicore__ constexpr int GetMIter(MatmulConfig mmCFG)
{
    return (mmCFG.singleCoreM + mmCFG.basicM - 1) / mmCFG.basicM;
}

__aicore__ constexpr int GetMIter(const MatmulApiStaticTiling &mmCFG)
{
    return GetMIter(mmCFG.cfg);
}

__aicore__ constexpr int GetNIter(MatmulConfig mmCFG)
{
    return (mmCFG.singleCoreN + mmCFG.basicN - 1) / mmCFG.basicN;
}

__aicore__ constexpr int GetNIter(const MatmulApiStaticTiling &mmCFG)
{
    return GetNIter(mmCFG.cfg);
}

__aicore__ constexpr int GetKIter(MatmulConfig mmCFG)
{
    return (mmCFG.singleCoreK + mmCFG.basicK - 1) / mmCFG.basicK;
}

__aicore__ constexpr int GetKIter(const MatmulApiStaticTiling &mmCFG)
{
    return GetKIter(mmCFG.cfg);
}

__aicore__ constexpr bool IsBasicM(MatmulConfig mmCFG)
{
    return (mmCFG.singleCoreM != 0) && (mmCFG.basicM != 0) && (GetMIter(mmCFG) == 1);
}

__aicore__ constexpr bool IsBasicM(const MatmulApiStaticTiling &mmCFG)
{
    return IsBasicM(mmCFG.cfg);
}

__aicore__ constexpr bool IsBasicN(MatmulConfig mmCFG)
{
    return (mmCFG.singleCoreN != 0) && (mmCFG.basicN != 0) && (GetNIter(mmCFG) == 1);
}

__aicore__ constexpr bool IsBasicN(const MatmulApiStaticTiling &mmCFG)
{
    return IsBasicN(mmCFG.cfg);
}

__aicore__ constexpr bool IsBasicK(MatmulConfig mmCFG)
{
    return (mmCFG.singleCoreK != 0) && (mmCFG.basicK != 0) && (GetKIter(mmCFG) == 1);
}

__aicore__ constexpr bool IsBasicK(const MatmulApiStaticTiling &mmCFG)
{
    return IsBasicK(mmCFG.cfg);
}

__aicore__ constexpr bool IsBasic(MatmulConfig mmCFG)
{
    return IsBasicM(mmCFG) && IsBasicN(mmCFG) && IsBasicK(mmCFG);
}

__aicore__ constexpr bool IsBasic(const MatmulApiStaticTiling &mmCFG)
{
    return IsBasic(mmCFG.cfg);
}

__aicore__ constexpr int GetL0PingPong(MatmulConfig mmCFG)
{
    return ((mmCFG.basicM * mmCFG.basicK * Impl::DB_FACTOR) <= Impl::L0_SIZE) &&
        ((mmCFG.basicK * mmCFG.basicN * Impl::DB_FACTOR) <= Impl::L0_SIZE) ? 1 : 0;
}

__aicore__ constexpr int GetL0PingPong(const MatmulApiStaticTiling &mmCFG)
{
    return GetL0PingPong(mmCFG.cfg);
}

__aicore__ constexpr bool NoTailM(MatmulConfig mmCFG)
{
    return (!mmCFG.enableSetTail) && (mmCFG.singleCoreM != 0) && (mmCFG.basicM != 0) &&
        (mmCFG.singleCoreM % mmCFG.basicM == 0);
}

__aicore__ constexpr bool NoTailM(const MatmulApiStaticTiling &mmCFG)
{
    return NoTailM(mmCFG.cfg);
}

__aicore__ constexpr bool NoTailN(MatmulConfig mmCFG)
{
    return (!mmCFG.enableSetTail) && (mmCFG.singleCoreN != 0) && (mmCFG.basicN != 0) &&
        (mmCFG.singleCoreN % mmCFG.basicN == 0);
}

__aicore__ constexpr bool NoTailN(const MatmulApiStaticTiling &mmCFG)
{
    return NoTailN(mmCFG.cfg);
}

__aicore__ constexpr bool NoTailK(MatmulConfig mmCFG)
{
    return (!mmCFG.enableSetTail) && (mmCFG.singleCoreK != 0) && (mmCFG.basicK != 0) &&
        (mmCFG.singleCoreK % mmCFG.basicK == 0);
}

__aicore__ constexpr bool NoTailK(const MatmulApiStaticTiling &mmCFG)
{
    return NoTailK(mmCFG.cfg);
}

template <bool isTensorA, bool isTranspose>
__aicore__ constexpr bool GetDstNzC0Stride(MatmulConfig mmCFG)
{
    if (mmCFG.enableStaticPadZeros) {
        if (mmCFG.doNorm) {
            if constexpr (isTensorA) {
                if constexpr (!isTranspose) {
                    return mmCFG.basicM;
                } else {
                    return mmCFG.basicK;
                }
            } else {
                if constexpr (!isTranspose) {
                    return mmCFG.basicK;
                } else {
                    return mmCFG.basicN;
                }
            }
        }
    }
    return 0;
}

template <bool isTensorA, bool isTranspose>
__aicore__ constexpr bool GetDstNzC0Stride(const MatmulApiStaticTiling &mmCFG)
{
    if (mmCFG.cfg.enableStaticPadZeros) {
        if (mmCFG.cfg.doMultiDataLoad) {
            if constexpr (isTensorA) {
                if constexpr (!isTranspose) {
                    return mmCFG.baseM * mmCFG.stepM;
                } else {
                    return mmCFG.baseK * mmCFG.stepKa;
                }
            } else {
                if constexpr (!isTranspose) {
                    return mmCFG.baseK * mmCFG.stepKb;
                } else {
                    return mmCFG.baseN * mmCFG.stepN;
                }
            }
        }
    }
    return GetDstNzC0Stride<isTensorA, isTranspose>(mmCFG.cfg);
}

template <typename T>
__aicore__ inline T CeilT(T num1, T num2)
{
    ASCENDC_ASSERT((num2 > 0),
        { KERNEL_LOG(KERNEL_ERROR, "num2 is %d , which should be larger than 0", num2); });
    return (num1 + num2 - 1) / num2;
}

template <typename T>
__aicore__ inline T CeilAlignT(T num1, T num2)
{
    ASCENDC_ASSERT((num2 > 0),
        { KERNEL_LOG(KERNEL_ERROR, "num2 is %d , which should be larger than 0", num2); });
    return CeilT(num1, num2) * num2;
}

#if __CCE_AICORE__ == 220
template <class T, class U>
__aicore__ inline void InitKfcClient(T &matmulClient, U *tiling, TPipe *tpipe, KfcCommClient *client, int instIdx,
    GM_ADDR workspace)
{
    ASSERT(workspace != nullptr && "workspace cannot be nullptr when InitKFC");
    ASSERT(instIdx >= 0);
    matmulClient.client = client;
    matmulClient.instIdx = instIdx;
    matmulClient.cubeTiling.SetTiling((TCubeTiling *)tiling);
    matmulClient.mmCntAddr_ = reinterpret_cast<__gm__ KfcMsg*>(GetMatmulIncAddr(workspace, GetBlockIdxImpl(), instIdx));
    matmulClient.InitStatic();
    matmulClient.devEvtID = instIdx * 2 + GetSubBlockIdxImpl();
}
#endif
__aicore__ constexpr bool PhyPosIsL1(TPosition pos)
{
    ASSERT(pos != TPosition::MAX);
    if (pos == TPosition::A1 || pos == TPosition::B1 ||
        pos == TPosition::SHM || pos == TPosition::TSCM) {
        return true;
    }
#if (__CCE_AICORE__ == 220 || __CCE_AICORE__ == 300)
    if (pos == TPosition::C1) {
        return true;
    }
#endif
    return false;
}

__aicore__ constexpr bool PhyPosIsUB(TPosition pos)
{
    ASSERT(pos != TPosition::MAX);
    if (pos == TPosition::GM || pos == TPosition::A1 || pos == TPosition::A2 ||
        pos == TPosition::B1 || pos == TPosition::B2 || pos == TPosition::CO1 ||
        pos == TPosition::SHM || pos == TPosition::TSCM) {
        return false;
    }
#if (__CCE_AICORE__ <= 200)
    if (pos == TPosition::C2) {
        return false;
    }
#elif (__CCE_AICORE__ == 220)
    if (pos == TPosition::C1 || pos == TPosition::C2 || pos == TPosition::CO2 ||
        pos == TPosition::C2PIPE2GM) {
        return false;
    }
#elif (__CCE_AICORE__ == 300)
    if (pos == TPosition::C1 || pos == TPosition::C2) {
        return false;
    }
#endif
    return true;
}

__aicore__ constexpr bool PhyPosIsGM(TPosition pos)
{
    ASSERT(pos != TPosition::MAX);
    if (pos == TPosition::GM) {
        return true;
    }
#if (__CCE_AICORE__ == 220)
    if (pos == TPosition::CO2) {
        return true;
    }
#endif
    return false;
}

template <bool AShare, bool BShare> __aicore__ __inline__ void SyncCubeWithVec()
{
    // Ensure that the Cube starts to process the message after receiving the
    // signals of V0 and V1 in the case of ABshare.
    // This is needed because only V0 will communicate with Cube for kfc during ABshare, to prevent
    // V1 lags far behind V0 then the Cube output is overwritten by the next Cube calculation triggered by V0
    // before being consumed by V1.
#if defined(__DAV_C220_CUBE__)
    if constexpr (AShare && BShare) {
        constexpr uint16_t eventID = 9U;
        WaitEvent(eventID);
        return;
    }
#elif defined(__DAV_C220_VEC__)
    if constexpr (AShare && BShare) {
        constexpr uint16_t eventID = 9U;
        NotifyEvent<PIPE_MTE3>(eventID);
        return;
    }
#else
#endif
}

template <typename T>
__aicore__ constexpr int32_t GetBitSize()
{
    if constexpr (std::is_arithmetic<T>::value) {
        return sizeof(T) * ONE_BYTE_BIT_SIZE;
    }
    if constexpr (IsSameTypeV<T, AscendC::int4b_t>) {
        return ONE_BYTE_BIT_SIZE / 2;
    }
    return ONE_BYTE_BIT_SIZE * 2;
}

template <typename T>
__aicore__ constexpr T CeilNoLog(T num1, T num2)
{
    if (num2 == 0) {
        return 0;
    }
    return (num1 + num2 - 1) / num2;
}

template <typename T>
__aicore__ constexpr T MaxValue(T t)
{
    return t;
}

template <typename T, typename ...Args>
__aicore__ constexpr T MaxValue(T t, Args... args)
{
    T maxValue = MaxValue(args...);
    return t > maxValue ? t : maxValue;
}

template <typename T>
__aicore__ constexpr T MinValue(T t)
{
    return t;
}

template <typename T, typename ...Args>
__aicore__ constexpr T MinValue(T t, Args... args)
{
    T minValue = MinValue(args...);
    return t < minValue ? t : minValue;
}

template <typename T>
__aicore__ constexpr T Align(T num1, T num2)
{
    if (num2 == 0) {
        return 0;
    }
    return (num1 + num2 - 1) / num2 * num2;
}

template <typename T>
__aicore__ constexpr T AlignDown(T num1, T num2)
{
    if (num2 == 0) {
        return 0;
    }
    return (num1 / num2) * num2;
}

template <typename T>
__aicore__ constexpr int32_t GetTypeSize()
{
    if constexpr (std::is_arithmetic<T>::value) {
        return sizeof(T);
    }
    if constexpr (IsSameTypeV<T, AscendC::int4b_t>) {
        return 1;
    }
    return 1;
}

template <typename T>
__aicore__ constexpr void Swap(T& a, T& b)
{
    a = a ^ b;
    b = a ^ b;
    a = a ^ b;
}

template <typename T>
__aicore__ inline T Ceil(T num1, T num2)
{
    ASCENDC_ASSERT((num2 > 0),
        { KERNEL_LOG(KERNEL_ERROR, "num2 is %d , which should be larger than 0", num2); });
    return (num1 + num2 - 1) / num2;
}

template <typename T>
__aicore__ inline T CeilAlign(T num1, T num2)
{
    ASSERT(num2 > 0);
    return Ceil(num1, num2) * num2;
}

struct SplitParams
{
    int16_t axisL1Len;
    int16_t kAxisL1Len;
    int16_t axisL1Offset;
    int16_t kAxisL1Offset;
    int16_t axisL0Len;
};

struct BatchOffsetInfo
{
    int32_t modA;
    int32_t divisorA;
    int32_t alignA;
    int32_t modB;
    int32_t divisorB;
    int32_t alignB;
    int32_t modBias;
    int32_t divisorBias;
    int32_t alignBias;
    bool setBiasFlag {false};
};

struct BatchSchedulerContext
{
    int32_t offsetA;
    int32_t offsetB;
    int32_t offsetBias;
    uint32_t reduceGNum;
    bool isReduceG;
    SplitParams aL0Params;
    SplitParams bL0Params;
};

template <typename T, const auto& MM_CFG>
__aicore__ inline constexpr bool IsL0ACache()
{
    if constexpr (ToMatmulConfig(MM_CFG).scheduleType == ScheduleType::OUTER_PRODUCT) {
        return ToMatmulConfig(MM_CFG).basicM * ToMatmulConfig(MM_CFG).basicK * sizeof(T) * Impl::DB_FACTOR <= L0AUF_SIZE;
    } else {
        return ToMatmulConfig(MM_CFG).singleCoreK <= ToMatmulConfig(MM_CFG).basicK * Impl::DB_FACTOR;
    }
}

template <typename T, const auto& MM_CFG>
__aicore__ inline constexpr bool IsL0BCache()
{
    if constexpr (ToMatmulConfig(MM_CFG).scheduleType == ScheduleType::OUTER_PRODUCT) {
        return ToMatmulConfig(MM_CFG).basicK * ToMatmulConfig(MM_CFG).basicN * sizeof(T) * Impl::DB_FACTOR <= L0BUF_SIZE;
    } else {
        return ToMatmulConfig(MM_CFG).singleCoreK <= ToMatmulConfig(MM_CFG).basicK * Impl::DB_FACTOR;
    }
}

template <typename A_TYPE, const auto& MM_CFG>
__aicore__ inline constexpr bool IsL0Cache()
{
    if constexpr ((!ToMatmulConfig(MM_CFG).doNorm && !ToMatmulConfig(MM_CFG).doMultiDataLoad) ||
        ToMatmulConfig(MM_CFG).intraBlockPartSum || A_TYPE::layout != LayoutMode::NONE ||
        ToMatmulConfig(MM_CFG).isA2B2Shared) {
        return false;
    }
    if constexpr (ToMatmulConfig(MM_CFG).doMultiDataLoad && ToMatmulConfig(MM_CFG).scheduleType == ScheduleType::OUTER_PRODUCT) {
        return false;
    }
    if constexpr (ToMatmulConfig(MM_CFG).singleCoreM <= 0 || ToMatmulConfig(MM_CFG).singleCoreN <= 0 ||
        ToMatmulConfig(MM_CFG).singleCoreK <= 0 || ToMatmulConfig(MM_CFG).basicM <= 0 ||
        ToMatmulConfig(MM_CFG).basicN <= 0 || ToMatmulConfig(MM_CFG).basicK <= 0) {
        return false;
    }
    return IsL0ACache<typename A_TYPE::T, MM_CFG>() || IsL0BCache<typename A_TYPE::T, MM_CFG>();
}

template <typename A_TYPE, const auto& MM_CFG>
constexpr bool isNormEnableScheduler = DoMatmulNorm(MM_CFG) && (A_TYPE::layout == LayoutMode::NONE)
                                   && (ToMatmulConfig(MM_CFG).scheduleType != ScheduleType::OUTER_PRODUCT)
                                   && !ToMatmulConfig(MM_CFG).intraBlockPartSum;

template <typename A_TYPE, const auto& MM_CFG>
constexpr bool isNormDisableScheduler = DoMatmulNorm(MM_CFG) && ((A_TYPE::layout != LayoutMode::NONE)
                                   || (ToMatmulConfig(MM_CFG).scheduleType == ScheduleType::OUTER_PRODUCT)
                                   || ToMatmulConfig(MM_CFG).intraBlockPartSum);

template <typename A_TYPE, const auto& MM_CFG>
constexpr bool IsBmmEnableScheduler = DoMatmulNorm(MM_CFG) &&
    ((A_TYPE::layout != LayoutMode::NONE && ToMatmulConfig(MM_CFG).batchMode == BatchMode::BATCH_LESS_THAN_L1) ||
    (A_TYPE::layout == LayoutMode::NORMAL && ToMatmulConfig(MM_CFG).batchMode == BatchMode::BATCH_LARGE_THAN_L1) ||
    (A_TYPE::layout == LayoutMode::NORMAL && ToMatmulConfig(MM_CFG).batchMode == BatchMode::SINGLE_LARGE_THAN_L1));

template <const auto& MM_CFG>
constexpr bool IsBasicBlockEnable = DoMatmulBasicBlock(MM_CFG) || DoMatmulSpecialBasicBlock(MM_CFG);

template <const auto& MM_CFG>
constexpr bool IsIntrablock = DoMatmulNorm(MM_CFG) && ToMatmulConfig(MM_CFG).intraBlockPartSum;
} // namespace AscendC
#endif
