/**
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

/*!
 * \file matmul_macro_utils.h
 * \brief
 */
#ifndef IMPL_MATMUL_MATMUL_MACRO_UTILS_H
#define IMPL_MATMUL_MATMUL_MACRO_UTILS_H

namespace AscendC {
namespace Impl {
#define HW_N0 16
#define HW_M0 16
#define ALIGN_NUM 16
#define BIAS_BUF_SIZE 1024
#define L0A_PING_D 0
#define L0A_PONG_D (L0AUF_SIZE / 2)
#define L0B_PING_D 0
#define L0B_PONG_D (L0BUF_SIZE / 2)
#define BIAS_PING_D 0
#define BIAS_PONG_D (BIAS_BUF_SIZE / 2)

constexpr int32_t SHIFT_16_BIT = 16;
constexpr int32_t SHIFT_32_BIT = 32;
constexpr int32_t SHIFT_40_BIT = 40;
constexpr int32_t SHIFT_48_BIT = 48;
constexpr int32_t SHIFT_56_BIT = 56;
constexpr int32_t CTRL_51_BIT = 51;
constexpr uint8_t padList[4] = {0, 0, 0, 0};
}

__aicore__ inline uint16_t CeilDiv(uint16_t num1, uint16_t num2)
{
    ASSERT(num2 > 0);
    return (num1 + num2 - 1) / num2;
}

__aicore__ inline uint16_t CeilAlign(uint16_t num1, uint16_t num2)
{
    ASSERT(num2 > 0);
    return CeilDiv(num1, num2) * num2;
}

__aicore__ inline uint32_t GetScalarBitcodeMm(float scalarValue)
{
    union ScalarBitcode {
    __aicore__ ScalarBitcode() {}
        float input;
        uint32_t output;
    } data;

    data.input = scalarValue;

    return data.output;
}
}  // namespace AscendC
#endif