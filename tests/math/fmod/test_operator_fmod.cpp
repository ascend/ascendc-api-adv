/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2025. All rights reserved.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

#include <gtest/gtest.h>
#include "kernel_operator.h"

using namespace AscendC;

enum TestMode {
    NORMAL_MODE,
    CAL_MODE,
    BUF_MODE,
    BUF_CAL_MODE
};

class TEST_FMOD : public testing::Test {
protected:
    void SetUp()
    {
        AscendC::SetGCoreType(2);
    }
    void TearDown()
    {
        AscendC::SetGCoreType(0);
    }
};

template <typename T>
void MainVecFmodDemo(__gm__ uint8_t *__restrict__ dstGm, __gm__ uint8_t *__restrict__ src0Gm,
    __gm__ uint8_t *__restrict__ src1Gm, uint32_t dataSize, TestMode testMode)
{
    TPipe tpipe;
    GlobalTensor<T> input0Global;
    GlobalTensor<T> input1Global;
    GlobalTensor<T> outputGlobal;
    input0Global.SetGlobalBuffer(reinterpret_cast<__gm__ T *>(src0Gm), dataSize);
    input1Global.SetGlobalBuffer(reinterpret_cast<__gm__ T *>(src1Gm), dataSize);
    outputGlobal.SetGlobalBuffer(reinterpret_cast<__gm__ T *>(dstGm), dataSize);

    TBuf<TPosition::VECCALC> tbuf1;
    tpipe.InitBuffer(tbuf1, dataSize * sizeof(T));
    LocalTensor<T> input0Local = tbuf1.Get<T>();   
    
    TBuf<TPosition::VECCALC> tbuf2;
    tpipe.InitBuffer(tbuf2, dataSize * sizeof(T));
    LocalTensor<T> input1Local = tbuf2.Get<T>();

    TBuf<TPosition::VECCALC> tbuf3;
    tpipe.InitBuffer(tbuf3, dataSize * sizeof(T));
    LocalTensor<T> outputLocal = tbuf3.Get<T>();

    DataCopy(input0Local, input0Global, dataSize);
    DataCopy(input1Local, input1Global, dataSize);

    SetFlag<HardEvent::MTE2_V>(EVENT_ID0);
    WaitFlag<HardEvent::MTE2_V>(EVENT_ID0);

    if (testMode == NORMAL_MODE) {
        Fmod<T>(outputLocal, input0Local, input1Local);
    }  else if (testMode == CAL_MODE) {
        Fmod<T>(outputLocal, input0Local, input1Local, dataSize);
    } else {
        TBuf<TPosition::VECCALC> tbuf4;
        if (sizeof(T) == sizeof(float)) {
            tpipe.InitBuffer(tbuf4, dataSize * sizeof(float));
        } else {
            tpipe.InitBuffer(tbuf4, dataSize * sizeof(float) * 4);
        }
        LocalTensor<uint8_t> tmpLocal = tbuf4.Get<uint8_t>();
        if (testMode == BUF_MODE) {
            Fmod<T>(outputLocal, input0Local, input1Local, tmpLocal);
        } else if (testMode == BUF_CAL_MODE) {
            Fmod<T>(outputLocal, input0Local, input1Local, tmpLocal, dataSize);
        }
    }

    SetFlag<HardEvent::V_MTE3>(EVENT_ID0);
    WaitFlag<HardEvent::V_MTE3>(EVENT_ID0);

    DataCopy(outputGlobal, outputLocal, dataSize);

    PipeBarrier<PIPE_ALL>();
}
#define VEC_FMOD_LEVEL2_TESTCASE(DATA_TYPE, TEST_MODE, DATA_SIZE)                         \
    TEST_F(TEST_FMOD, Fmod##DATA_TYPE##TEST_MODE##DATA_SIZE##Case)                       \
    {                                                                                      \
        uint32_t dataSize = DATA_SIZE;                                                    \
        uint8_t input0Gm[dataSize * sizeof(DATA_TYPE)] = {0};                                  \
        uint8_t input1Gm[dataSize * sizeof(DATA_TYPE)] = {0};                                   \
        uint8_t outputGm[dataSize * sizeof(DATA_TYPE)] = {0};                                  \
                                                                                           \
        MainVecFmodDemo<DATA_TYPE>(outputGm, input0Gm, input1Gm , dataSize, TEST_MODE); \
                                                                                           \
        for (uint32_t i = 0; i < dataSize; i++) {                                         \
            EXPECT_EQ(outputGm[i], 0x00);                                                 \
        }                                                                                  \
    }
VEC_FMOD_LEVEL2_TESTCASE(float, NORMAL_MODE, 256);
VEC_FMOD_LEVEL2_TESTCASE(float, CAL_MODE, 256);
VEC_FMOD_LEVEL2_TESTCASE(float, BUF_MODE, 256);
VEC_FMOD_LEVEL2_TESTCASE(float, BUF_CAL_MODE, 256);

VEC_FMOD_LEVEL2_TESTCASE(half, NORMAL_MODE, 256);
VEC_FMOD_LEVEL2_TESTCASE(half, CAL_MODE, 256);
VEC_FMOD_LEVEL2_TESTCASE(half, BUF_MODE, 256);
VEC_FMOD_LEVEL2_TESTCASE(half, BUF_CAL_MODE, 256);
