/**
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */
#include <gtest/gtest.h>
#include "kernel_common.h"
#include "kernel_tpipe_impl.h"
#include "stub_reg.h"

int32_t main(int32_t argc, char** argv)
{
AscendC::StubInit();
    AscendC::TPipe tpipe;
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

__aicore__ AscendC::TPipe* GetTPipePtr()
{
#if __CCE_AICORE__ == 220
#ifdef __DAV_C220_CUBE__
    return g_cubeTPipePtr;
#elif defined(__DAV_C220_VEC__)
    return g_vecTPipePtr;
#else
    return g_tPipePtr;
#endif
#else
    return g_tPipePtr;
#endif
    ASSERT(false && "Only supported ascend910B1, ascend910, ascend310p");
    return nullptr;
}