# Copyright (c) 2024 Huawei Technologies Co., Ltd.
# This file is a part of the CANN Open Software.
# Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
# Please refer to the License for details. You may not use this file except in compliance with the License.
# THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
# INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
# See LICENSE in the root of the software repository for the full text of the License.
# ======================================================================================================================

set(GENERATE_CPP_COV ${ASCENDC_TESTS_DIR}/cmake/scripts/generate_cpp_cov.sh)
set(ASCEND_3RD_LIB_PATH $ENV{ASCEND_3RD_LIB_PATH})

find_package(GTest CONFIG)
if (NOT ${GTest_FOUND})
    if (EXISTS "${ASCEND_3RD_LIB_PATH}/cmake/modules")
        list(APPEND CMAKE_MODULE_PATH ${ASCEND_3RD_LIB_PATH}/cmake/modules)
    endif ()
    if (EXISTS "${ASCEND_3RD_LIB_PATH}/gtest/lib/cmake/GTest")
        list(APPEND CMAKE_PREFIX_PATH ${ASCEND_3RD_LIB_PATH}/gtest/lib/cmake/GTest)
        find_package(GTest CONFIG REQUIRED)
    endif ()
endif ()
if (NOT ${GTest_FOUND})
    message(FATAL_ERROR "Can't find any googletest.")
endif ()