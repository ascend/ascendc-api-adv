/**
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */
#ifndef _PLATFORM_STUB_H_
#define _PLATFORM_STUB_H_

#include <string>

namespace fe {
enum class LocalMemType {
  L0_A = 0,
  L0_B = 1,
  L0_C = 2,
  L1 = 3,
  L2 = 4,
  UB = 5,
  HBM = 6,
  RESERVED
};
class PlatFormInfos {
public:
uint32_t GetCoreNum(void) const;
bool GetPlatformRes(const std::string &label, const std::string &key, std::string &val);
bool GetPlatformResWithLock(const std::string &label, const std::string &key, std::string &val);
void GetLocalMemSize(const LocalMemType &mem_type, uint64_t &size);
void GetLocalMemBw(const LocalMemType &mem_type, uint64_t &bw_size);
};
} // namespace fe

#endif // _PLATFORM_STUB_H_