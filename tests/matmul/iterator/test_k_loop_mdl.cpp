/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024. All rights reserved.
 *
 * @brief KLoop ut for ascend910B1
 *
 */
#include <gtest/gtest.h>
#include "kernel_operator.h"
#include "lib/matmul/tiling.h"
#include "impl/matmul/modules/matmul_param.h"
#include "impl/matmul/modules/matmul_policy.h"
#define private public
#include "impl/matmul/modules/stage/iterator/k_loop/k_loop.h"
#include "impl/matmul/modules/matmul_private_modules.h"

using namespace std;
using namespace AscendC;


namespace {
template <class A_TYPE, class B_TYPE, class C_TYPE, class BIAS_TYPE, const MatmulConfig& MM_CFG, class MM_CB,
MATMUL_POLICY_DEFAULT_OF(MatmulPolicy)>
class MatmulImpl
: MATMUL_IMPORT_MODULE(KLoop)
, MATMUL_IMPORT_MODULE_PRIVATE(MatmulShapeInfo)
, MATMUL_IMPORT_MODULE_PRIVATE(MatmulShapeTiling)
{
    MATMUL_ALLOW_USING_PRIVATE(MatmulShapeInfo);
    MATMUL_ALLOW_USING_PRIVATE(MatmulShapeTiling);
    MATMUL_ALLOW_USING(KLoop);

public:
    using VAR_PARAMS =
        typename Impl::Detail::MatmulParams<A_TYPE, B_TYPE, C_TYPE, BIAS_TYPE, MM_CFG, GetMatmulVersion(MM_CFG)>::PARAMS;

    MatmulImpl() {
        InitVar();
    }

    VAR_PARAMS& GetVar() {
        return var;
    }

    void InitVar() {
        var.tiling_.SetTiling(&tiling);
        var.tpipe_ = &pipe;
    }

    void SetInitParams(int32_t stepKa, int32_t stepKb, int32_t baseK) {
        tiling.stepKa = stepKa;
        tiling.stepKb = stepKb;
        tiling.baseK = baseK;
        tiling.stepM = 1;
        tiling.stepN = 1;
    }

    void SetRuntimeParams(int32_t singleCoreK) {
        var.singleCoreK_ = singleCoreK;
    }

private:
    TCubeTiling tiling;
    TPipe pipe;
    VAR_PARAMS var;
};
}

class test_k_loop_mdl : public testing::Test {
protected:
    void SetUp() {}
    void TearDown() {}

private:
    using A_TYPE = MatmulType<AscendC::TPosition::GM, CubeFormat::ND, half, false>;
    using B_TYPE = MatmulType<AscendC::TPosition::GM, CubeFormat::ND, half, false>;
    using C_TYPE = MatmulType<AscendC::TPosition::GM, CubeFormat::ND, float>;
    using BIAS_TYPE = MatmulType<AscendC::TPosition::GM, CubeFormat::ND, float>;

    MatmulImpl<A_TYPE, B_TYPE, C_TYPE, BIAS_TYPE, CFG_MDL, void> mm;
};

TEST_F(test_k_loop_mdl, get_loop_cnt) {
    int32_t stepKa = 2;
    int32_t stepKb = 4;
    int32_t baseK = 32;
    int32_t singleCoreK = 512;
    mm.SetInitParams(stepKa, stepKb, baseK);
    mm.SetRuntimeParams(singleCoreK);
    mm.Init(singleCoreK);
    ASSERT_EQ(mm.GetTotalIter(), 16);
    int32_t outerIter = 0;
    int32_t innerIter = 0;
    mm.OuterStart();
    do {
        outerIter++;
        ASSERT_EQ(mm.GetTileShapeA(), 64);
        ASSERT_EQ(mm.GetTileShapeB(), 128);
        mm.InnerStart();
        do {
            innerIter++;
            ASSERT_EQ(mm.GetBaseShape(), 32);
        } while (mm.InnerNext());
    } while (mm.OuterNext());
    ASSERT_EQ(outerIter, 8);
    ASSERT_EQ(innerIter, 16);
}

TEST_F(test_k_loop_mdl, loop_with_tail) {
    int32_t stepKa = 2;
    int32_t stepKb = 4;
    int32_t baseK = 32;
    int32_t singleCoreK = 500;
    mm.SetInitParams(stepKa, stepKb, baseK);
    mm.SetRuntimeParams(singleCoreK);
    mm.Init(singleCoreK);
    int32_t outerIter = 0;
    int32_t innerIter = 0;
    mm.OuterStart();
    do {
        outerIter++;
        if (mm.GetOuterIdx() == 7) {
            ASSERT_EQ(mm.GetTileShapeA(), 52);
        } else {
            ASSERT_EQ(mm.GetTileShapeA(), baseK * stepKa);
        }
        if (mm.GetOuterIdx() >= 6) {
            ASSERT_EQ(mm.GetTileShapeB(), 116);
        } else {
            ASSERT_EQ(mm.GetTileShapeB(), baseK * stepKb);
        }
        int32_t innerStartIdx = mm.GetInnerStartIdx();
        mm.InnerStart();
        do {
            innerIter++;
            if (mm.GetOuterIdx() == 7 && mm.GetInnerIdx() == innerStartIdx + 1) {
                ASSERT_EQ(mm.GetBaseShape(), 20);
            } else {
                ASSERT_EQ(mm.GetBaseShape(), baseK);
            }
        } while (mm.InnerNext());
    } while (mm.OuterNext());
    ASSERT_EQ(outerIter, 8);
    ASSERT_EQ(innerIter, 16);
}

TEST_F(test_k_loop_mdl, loop_with_stepka_tail) {
    int32_t stepKa = 6;
    int32_t stepKb = 3;
    int32_t baseK = 32;
    int32_t singleCoreK = 500;
    mm.SetInitParams(stepKa, stepKb, baseK);
    mm.SetRuntimeParams(singleCoreK);
    mm.Init(singleCoreK);
    int32_t outerIter = 0;
    int32_t innerIter = 0;
    mm.OuterStart();
    do {
        outerIter++;
        if (mm.GetOuterIdx() >= 4) {
            ASSERT_EQ(mm.GetTileShapeA(), 116);
        } else {
            ASSERT_EQ(mm.GetTileShapeA(), baseK * stepKa);
        }
        if (mm.GetOuterIdx() == 5) {
            ASSERT_EQ(mm.GetTileShapeB(), 20);
        } else {
            ASSERT_EQ(mm.GetTileShapeB(), baseK * stepKb);
        }
        int32_t innerStartIdx = mm.GetInnerStartIdx();
        mm.InnerStart();
        do {
            innerIter++;
            if (mm.GetOuterIdx() == 5 && mm.GetInnerIdx() == innerStartIdx) {
                ASSERT_EQ(mm.GetBaseShape(), 20);
            } else {
                ASSERT_EQ(mm.GetBaseShape(), baseK);
            }
        } while (mm.InnerNext());
    } while (mm.OuterNext());
    ASSERT_EQ(outerIter, 6);
    ASSERT_EQ(innerIter, 16);
}