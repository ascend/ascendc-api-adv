/**
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */

/*!
 * \file test_m_loop_norm.cpp
 * \brief m loop ut for norm
 */

#include <gtest/gtest.h>
#include "kernel_operator.h"
#include "lib/matmul/tiling.h"
#include "impl/matmul/modules/matmul_param.h"
#include "impl/matmul/modules/matmul_policy.h"
#define private public
#include "impl/matmul/modules/matmul_private_modules.h"
#include "impl/matmul/modules/param/matmul_tensor_info.h"
#include "impl/matmul/modules/param/matmul_shape_tiling.h"
#include "impl/matmul/modules/stage/iterator/m_loop/m_loop.h"
#include "impl/matmul/modules/stage/iterator/m_loop/m_loop_norm.h"

using namespace std;
using namespace AscendC;


namespace {
template <class A_TYPE, class B_TYPE, class C_TYPE, class BIAS_TYPE, const MatmulConfig& MM_CFG,
          class MM_CB, MATMUL_POLICY_DEFAULT_OF(MatmulPolicy)>
class MatmulImpl :
    MATMUL_IMPORT_MODULE(MLoop),
    MATMUL_IMPORT_MODULE_PRIVATE(MatmulShapeInfo),
    MATMUL_IMPORT_MODULE_PRIVATE(MatmulShapeTiling)
{
    MATMUL_ALLOW_USING(MLoop);
    MATMUL_ALLOW_USING_PRIVATE(MatmulShapeInfo);
    MATMUL_ALLOW_USING_PRIVATE(MatmulShapeTiling);

public:
    using VAR_PARAMS =
        typename Impl::Detail::MatmulParams<A_TYPE, B_TYPE, C_TYPE, BIAS_TYPE, MM_CFG, GetMatmulVersion(MM_CFG)>::PARAMS;

    MatmulImpl() {
        InitVar();
    }

    void InitVar() {
        var.tiling_.SetTiling(&tiling);
        var.tpipe_ = &pipe;
    }

    void SetInitParams(int32_t singleCoreM, int32_t baseM, int32_t stepM) {
        var.singleCoreM_ = singleCoreM;
        tiling.singleCoreM = singleCoreM;
        tiling.baseM = baseM;
        tiling.stepM = stepM;
        tiling.iterateOrder = 0;
    }

    int32_t GetSingleShape()
    {
        return var.singleCoreM_;
    }

private:
    TCubeTiling tiling;
    TPipe pipe;
    VAR_PARAMS var;
};
}

class TestMLoopNorm : public testing::Test {
protected:
    void SetUp() {}
    void TearDown() {}

private:
    using A_TYPE = MatmulType<AscendC::TPosition::GM, CubeFormat::ND, half, false>;
    using B_TYPE = MatmulType<AscendC::TPosition::GM, CubeFormat::ND, half, false>;
    using C_TYPE = MatmulType<AscendC::TPosition::GM, CubeFormat::ND, float>;
    using BIAS_TYPE = MatmulType<AscendC::TPosition::GM, CubeFormat::ND, float>;

    MatmulImpl<A_TYPE, B_TYPE, C_TYPE, BIAS_TYPE, CFG_NORM, void> mm;
};

TEST_F(TestMLoopNorm, first_iter) {
    mm.SetInitParams(1793, 128, 4);
    mm.Init(mm.GetSingleShape());           // SetSingleShape
    mm.OuterStart();
    mm.InnerStart();
    // Outer
    EXPECT_EQ(mm.GetTotalIter(), 15);       // totalIter
    EXPECT_EQ(mm.GetOuterIdx(), 0);
    EXPECT_EQ(mm.GetOuterIter(), 4);        // outerIter
    EXPECT_EQ(mm.GetTileShape(), 128);      // mainTileShape
    EXPECT_EQ(mm.GetTileBlockShape(), 8);   // Ceil(mainTileShape, BLOCK_CUBE)
    // Inner
    EXPECT_EQ(mm.GetInnerIdx(), 0);
    EXPECT_EQ(mm.GetInnerIter(), 4);        // stepM
    EXPECT_EQ(mm.GetBaseShape(), 128);      // baseShape
    EXPECT_EQ(mm.GetBaseBlockShape(), 8);   // Ceil(baseShape, BLOCK_CUBE)
}

TEST_F(TestMLoopNorm, inner_end) {
    mm.SetInitParams(1793, 128, 4);
    mm.Init(mm.GetSingleShape());
    mm.OuterStart();
    mm.InnerStart();

    EXPECT_EQ(mm.GetInnerIdx(), 0);
    mm.InnerNext();
    EXPECT_EQ(mm.GetInnerIdx(), 1);
    mm.InnerNext();
    EXPECT_EQ(mm.GetInnerIdx(), 2);
    mm.InnerNext();
    EXPECT_EQ(mm.GetInnerIdx(), 3);
    EXPECT_FALSE(mm.InnerEnd());
    // tail
    mm.InnerNext();
    EXPECT_TRUE(mm.InnerEnd());
}

TEST_F(TestMLoopNorm, outer_end) {
    mm.SetInitParams(1793, 128, 4);
    mm.Init(mm.GetSingleShape());
    mm.OuterStart();
    mm.InnerStart();
    mm.InnerNext();

    EXPECT_EQ(mm.GetOuterIdx(), 0);
    mm.OuterNext();
    EXPECT_EQ(mm.GetOuterIdx(), 1);
    mm.OuterNext();
    EXPECT_EQ(mm.GetOuterIdx(), 2);
    mm.OuterNext();
    EXPECT_EQ(mm.GetOuterIdx(), 3);
    EXPECT_FALSE(mm.OuterEnd());
    mm.OuterNext();
    // tail
    EXPECT_EQ(mm.GetTileShape(), 128);      // tailTileShape
    EXPECT_EQ(mm.GetTileBlockShape(), 8);   // Ceil(tailTileShape, BLOCK_CUBE)
    EXPECT_TRUE(mm.OuterEnd());
}
