/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024. All rights reserved.
 *
 * @brief copy bias in module unit test
 *
 */
#include <gtest/gtest.h>
#include "kernel_operator.h"
#include "lib/matmul/tiling.h"
#include "impl/matmul/modules/matmul_param.h"
#include "impl/matmul/modules/matmul_policy.h"
#include "impl/matmul/modules/matmul_private_modules.h"
#include "impl/matmul/modules/resource/bias_buffer/c1_buffer/c1_buffer.h"
#include "impl/matmul/modules/resource/bias_buffer/c2_buffer/c2_buffer.h"
#include "impl/matmul/modules/stage/copy_bias_in/copy_bias_in.h"
#include "impl/matmul/modules/scheduler/bias_scheduler/bias_scheduler.h"
#include "../../copy_cube_in/base_tiling_struct.h"

using namespace std;
using namespace AscendC;


namespace {
__aicore__ inline constexpr MatmulConfig GetMmConfig()
{
    auto cfg = GetNormalConfig();
    cfg.batchMode = BatchMode::BATCH_LESS_THAN_L1;
    return cfg;
}
constexpr MatmulConfig CFG_NORM_BATCH = GetMmConfig();

template <const auto& MM_CFG, typename IMPL, typename A_TYPE, typename B_TYPE, typename C_TYPE, typename BIAS_TYPE>
class CustomMatmulPolicy : public Impl::Detail::MatmulPolicy<MM_CFG, IMPL, A_TYPE, B_TYPE, C_TYPE, BIAS_TYPE>
{
public:
    using L0cT = typename GetDstType<typename A_TYPE::T>::Type; 
    using CopyBiasIn = Impl::Detail::CopyBiasIn<IMPL, A_TYPE, BIAS_TYPE, MM_CFG>;
    using C1Buffer = Impl::Detail::C1Buffer<IMPL, BIAS_TYPE, A_TYPE, MM_CFG>;
    using C2Buffer = Impl::Detail::C2Buffer<IMPL, L0cT, A_TYPE, MM_CFG>;
    using LoadBias2C2 = Impl::Detail::LoadBias2C2<IMPL, A_TYPE, BIAS_TYPE, MM_CFG>;
    using BiasScheduler = Impl::Detail::BiasScheduler<IMPL, A_TYPE, B_TYPE, BIAS_TYPE, MM_CFG>;
};

template <class A_TYPE, class B_TYPE, class C_TYPE, class BIAS_TYPE, const MatmulConfig& MM_CFG, class MM_CB,
MATMUL_POLICY_DEFAULT_OF(MatmulPolicy)>
class MatmulImpl
: MATMUL_IMPORT_MODULE(CubeOutBuffer)
, MATMUL_IMPORT_MODULE(C1Buffer)
, MATMUL_IMPORT_MODULE(C2Buffer)
, MATMUL_IMPORT_MODULE(CopyBiasIn)
, MATMUL_IMPORT_MODULE(LoadBias2C2)
, MATMUL_IMPORT_MODULE(BiasScheduler)
, MATMUL_IMPORT_MODULE(MLoop)
, MATMUL_IMPORT_MODULE(NLoop)
, MATMUL_IMPORT_MODULE(KLoop)
, MATMUL_IMPORT_MODULE_PRIVATE(LocalWorkspace)
, MATMUL_IMPORT_MODULE_PRIVATE(MatmulShapeTiling)
{
    MATMUL_ALLOW_USING(CubeOutBuffer);
    MATMUL_ALLOW_USING(CopyBiasIn);
    MATMUL_ALLOW_USING(LoadBias2C2);
    MATMUL_ALLOW_USING(C1Buffer);
    MATMUL_ALLOW_USING(C2Buffer);
    MATMUL_ALLOW_USING(BiasScheduler);
    MATMUL_ALLOW_USING(MLoop);
    MATMUL_ALLOW_USING(NLoop);
    MATMUL_ALLOW_USING(KLoop);
    MATMUL_ALLOW_USING_PRIVATE(LocalWorkspace);
    MATMUL_ALLOW_USING_PRIVATE(MatmulShapeTiling);

    using BiasT = typename BIAS_TYPE::T;
    using BiasScheduler::IsBias;
    using BiasScheduler::SetBias;
public:
    using VAR_PARAMS =
        typename Impl::Detail::MatmulParams<A_TYPE, B_TYPE, C_TYPE, BIAS_TYPE, MM_CFG, GetMatmulVersion(MM_CFG)>::PARAMS;
    using IMPL = MatmulImpl<A_TYPE, B_TYPE, C_TYPE, BIAS_TYPE, MM_CFG, MM_CB, MATMUL_POLICY>;

    using CallBack = MM_CB;

    MATMUL_USE_MODULE(CopyBiasIn);
    MATMUL_USE_MODULE(LoadBias2C2);
    MATMUL_USE_MODULE(BiasScheduler);
    MATMUL_USE_MODULE(MLoop);
    MATMUL_USE_MODULE(NLoop);
    MATMUL_USE_MODULE(KLoop);
    MatmulImpl() {}

    VAR_PARAMS& GetVar() {
        return var;
    }

    void InitVar(const TCubeTiling &tiling) {
        var.tiling_.SetTiling(&tiling);
        var.tpipe_ = &pipe;
    }

    void SetRuntimeParams(int32_t batchA = 4, int32_t batchB = 2, int32_t batchOuter = 1) {
        var.mIter_ = var.tiling_.GetSingleCoreM() / var.tiling_.GetBaseM();
        var.kIter_ = var.tiling_.GetSingleCoreK() / var.tiling_.GetBaseK();
        var.nIter_ = var.tiling_.GetSingleCoreN() / var.tiling_.GetBaseN();
        var.singleCoreM_ = var.tiling_.GetSingleCoreM();
        var.singleCoreN_ = var.tiling_.GetSingleCoreN();
        var.singleCoreK_ = var.tiling_.GetSingleCoreK();
        var.tailN_ = var.singleCoreN_ % var.tiling_.GetBaseN();
        if (var.tailN_ == 0) {
            var.tailN_ = var.tiling_.GetBaseN();
        }
        batchOuter_ = batchOuter;
        batchB_ = batchB;
        batchA_ = batchA;
    }

    void SetGMInput() 
    {
        auto batchNum = batchA_ > batchB_ ? batchA_ : batchB_;
        uint8_t biasGlobal[batchNum * var.tiling_.GetN() * sizeof(BiasT)] = {0};
        GlobalTensor<BiasT> fakeInput;
        fakeInput.SetGlobalBuffer(reinterpret_cast<__gm__ BiasT*>(biasGlobal), batchNum * var.tiling_.GetN());
        MATMUL_MODULE(BiasScheduler)->SetInput(fakeInput);
    }

    void RunCase(bool isBias = false, bool isUBIn = false) {
        auto mLoop = MATMUL_MODULE(MLoop);
        auto nLoop = MATMUL_MODULE(NLoop);
        auto kLoop = MATMUL_MODULE(KLoop);
        auto batchNum = batchA_ > batchB_ ? batchA_ : batchB_;
        MATMUL_MODULE(BiasScheduler)->Init(batchNum);
        // not support UB input, need copy to GM
        SetGMInput();

        MATMUL_MODULE(BiasScheduler)->SetBias(isBias);
        nLoop->Init(var.tiling_.GetSingleCoreN());
        mLoop->Init(var.tiling_.GetSingleCoreM());
        kLoop->Init(var.tiling_.GetSingleCoreK());
        
        for (int outer = 0; outer < batchOuter_; ++outer) {
            auto srcOffset = outer * batchNum * var.singleCoreN_;
            auto bias = MATMUL_MODULE(BiasScheduler)->CopyIn(var.singleCoreN_, batchNum, srcOffset);
            for (int m = 0; m < var.mIter_; ++m) {
                for (int n = 0; n < var.nIter_; ++n) {
                    auto baseUseN =  (n + 1 == var.nIter_) ? var.tailN_ : var.tiling_.GetBaseN();
                    for (int k = 0; k < var.kIter_; ++k) {
                         bool biasValid = false;
                        if (k == 0) {
                            biasValid = true;
                            MATMUL_MODULE(BiasScheduler)->SplitLoad(bias, baseUseN);
                        }
                        if (biasValid) {
                            MATMUL_MODULE(BiasScheduler)->Free();
                        }
                    }
                }
            }
            MATMUL_MODULE(BiasScheduler)->Destroy(bias);
        }
    }

private:
    TPipe pipe;
    VAR_PARAMS var;
    int32_t batchA_ = 1;
    int32_t batchB_ = 1;
    int32_t batchOuter_ = 1;
};
}

class TestBatchBiasScheduler : public testing::Test {
protected:
    void SetUp() {}
    void TearDown() {}

private:
    using A_TYPE_BSNGD = MatmulType<AscendC::TPosition::GM, CubeFormat::ND, half, false, LayoutMode::BSNGD>;
    using A_TYPE_NORMAL = MatmulType<AscendC::TPosition::GM, CubeFormat::ND, half, false, LayoutMode::NORMAL>;
    using B_TYPE = MatmulType<AscendC::TPosition::GM, CubeFormat::ND, half, false>;
    using C_TYPE = MatmulType<AscendC::TPosition::GM, CubeFormat::ND, float>;
    using BIAS_TYPE_GM = MatmulType<AscendC::TPosition::GM, CubeFormat::ND, float>;
    using BIAS_TYPE_UB = MatmulType<AscendC::TPosition::VECIN, CubeFormat::ND, float>;

    MatmulImpl<A_TYPE_BSNGD, B_TYPE, C_TYPE, BIAS_TYPE_GM, CFG_NORM_BATCH, void, CustomMatmulPolicy> mm1_;
    MatmulImpl<A_TYPE_NORMAL, B_TYPE, C_TYPE, BIAS_TYPE_UB, CFG_NORM_BATCH, void, CustomMatmulPolicy> mm2_;
};

TEST_F(TestBatchBiasScheduler, batch_scheduler_bias_for_gm) {
    // coreNum, M, N, K, singleCoreM, singleCoreN, singleCoreK, baseM, baseN, baseK, depthA1, depthB1, stepM, stepN,
    // stepKa, stepKb, isBias, iterateOrder, batchNum
    TilingParams tilingParams = {1, 64, 256, 256, 64, 256, 256, 32, 64, 128, 2, 4, 1, 2, 1, 1, 1, 1, 4};
    TCubeTiling tiling;
    tilingParams.GetTiling(tiling);
    mm1_.InitVar(tiling);
    mm1_.SetRuntimeParams();
    mm1_.SetBias(false);
    ASSERT_FALSE(mm1_.IsBias());
    mm1_.RunCase(true, false);
    ASSERT_TRUE(mm1_.IsBias());
}

TEST_F(TestBatchBiasScheduler, batch_scheduler_bias_for_ub) {
    // coreNum, M, N, K, singleCoreM, singleCoreN, singleCoreK, baseM, baseN, baseK, depthA1, depthB1, stepM, stepN,
    // stepKa, stepKb, isBias, iterateOrder
    TilingParams tilingParams = {1, 64, 256, 256, 64, 256, 256, 32, 64, 128, 2, 4, 1, 2, 1, 1, 1, 1};
    TCubeTiling tiling;
    tilingParams.GetTiling(tiling);
    mm2_.InitVar(tiling);
    mm2_.SetRuntimeParams();
    mm2_.SetBias(false);
    ASSERT_FALSE(mm2_.IsBias());
    mm2_.RunCase(true, false);
    ASSERT_TRUE(mm2_.IsBias());
}
