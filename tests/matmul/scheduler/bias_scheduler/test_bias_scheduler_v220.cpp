/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2024. All rights reserved.
 *
 * @brief copy bias in module unit test
 *
 */
#include <gtest/gtest.h>
#include "kernel_operator.h"
#include "lib/matmul/tiling.h"
#include "impl/matmul/modules/matmul_param.h"
#include "impl/matmul/modules/matmul_policy.h"
#include "impl/matmul/modules/matmul_private_modules.h"
#include "impl/matmul/modules/stage/copy_bias_in/copy_bias_in.h"
#include "impl/matmul/modules/scheduler/bias_scheduler/bias_scheduler.h"
#include "../../copy_cube_in/base_tiling_struct.h"

using namespace std;
using namespace AscendC;

namespace {
template <const auto& MM_CFG, typename IMPL, typename A_TYPE, typename B_TYPE, typename C_TYPE, typename BIAS_TYPE>
class CustomMatmulPolicy : public Impl::Detail::MatmulPolicy<MM_CFG, IMPL, A_TYPE, B_TYPE, C_TYPE, BIAS_TYPE>
{
public:
    using BiasScheduler = Impl::Detail::BiasScheduler<IMPL, A_TYPE, B_TYPE, BIAS_TYPE, MM_CFG>;
};

template <class A_TYPE, class B_TYPE, class C_TYPE, class BIAS_TYPE, const MatmulConfig& MM_CFG, class MM_CB,
MATMUL_POLICY_DEFAULT_OF(MatmulPolicy)>
class MatmulImpl
: MATMUL_IMPORT_MODULE(CubeOutBuffer)
, MATMUL_IMPORT_MODULE_PRIVATE(C1Buffer)
, MATMUL_IMPORT_MODULE_PRIVATE(C2Buffer)
, MATMUL_IMPORT_MODULE_PRIVATE(CopyBiasIn)
, MATMUL_IMPORT_MODULE_PRIVATE(LoadBias2C2)
, MATMUL_IMPORT_MODULE(MLoop)
, MATMUL_IMPORT_MODULE(NLoop)
, MATMUL_IMPORT_MODULE(KLoop)
, MATMUL_IMPORT_MODULE(BiasScheduler)
, MATMUL_IMPORT_MODULE_PRIVATE(LocalWorkspace)
, MATMUL_IMPORT_MODULE_PRIVATE(MatmulShapeTiling)
{
    MATMUL_ALLOW_USING(CubeOutBuffer);
    MATMUL_ALLOW_USING(BiasScheduler);
    MATMUL_ALLOW_USING(MLoop);
    MATMUL_ALLOW_USING(NLoop);
    MATMUL_ALLOW_USING(KLoop);
    MATMUL_ALLOW_USING_PRIVATE(C1Buffer);
    MATMUL_ALLOW_USING_PRIVATE(C2Buffer);
    MATMUL_ALLOW_USING_PRIVATE(CopyBiasIn);
    MATMUL_ALLOW_USING_PRIVATE(LoadBias2C2);
    MATMUL_ALLOW_USING_PRIVATE(LocalWorkspace);
    MATMUL_ALLOW_USING_PRIVATE(MatmulShapeTiling);

    using BiasT = typename BIAS_TYPE::T;
    using BiasScheduler::IsBias;
    using BiasScheduler::SetBias;
public:
    using VAR_PARAMS =
        typename Impl::Detail::MatmulParams<A_TYPE, B_TYPE, C_TYPE, BIAS_TYPE, MM_CFG, GetMatmulVersion(MM_CFG)>::PARAMS;
    using IMPL = MatmulImpl<A_TYPE, B_TYPE, C_TYPE, BIAS_TYPE, MM_CFG, MM_CB, MATMUL_POLICY>;

    using CallBack = MM_CB;

    MATMUL_USE_MODULE(BiasScheduler);
    MATMUL_USE_MODULE(MLoop);
    MATMUL_USE_MODULE(NLoop);
    MATMUL_USE_MODULE(KLoop);
    MatmulImpl() {}

    VAR_PARAMS& GetVar() {
        return var;
    }

    void InitVar(const TCubeTiling &tiling) {
        var.tiling_.SetTiling(&tiling);
        var.tpipe_ = &pipe;
    }

    void SetGMInput()
    {
        uint8_t biasGlobal[var.tiling_.GetN() * sizeof(BiasT)] = {0};
        GlobalTensor<BiasT> fakeInput;
        fakeInput.SetGlobalBuffer(reinterpret_cast<__gm__ BiasT*>(biasGlobal), var.tiling_.GetN());
        MATMUL_MODULE(BiasScheduler)->SetInput(fakeInput);
    }

    void RunCase(bool isBias = false, bool isUBIn = false) {
        auto mLoop = MATMUL_MODULE(MLoop);
        auto nLoop = MATMUL_MODULE(NLoop);
        auto kLoop = MATMUL_MODULE(KLoop);
        MATMUL_MODULE(BiasScheduler)->Init();

        nLoop->Init(var.tiling_.GetSingleCoreN());
        mLoop->Init(var.tiling_.GetSingleCoreM());
        kLoop->Init(var.tiling_.GetSingleCoreK());
        SetGMInput();
        MATMUL_MODULE(BiasScheduler)->SetBias(isBias);
        for (nLoop->OuterStart(); !nLoop->OuterEnd(); nLoop->OuterNext()) {
            for (mLoop->OuterStart(); !mLoop->OuterEnd(); mLoop->OuterNext()) {
                for (mLoop->InnerStart(); !mLoop->InnerEnd(); mLoop->InnerNext()) {
                    for (nLoop->InnerStart(); !nLoop->InnerEnd(); nLoop->InnerNext()) {
                        auto baseUseN = nLoop->GetBaseShape();
                        auto srcOffset = nLoop->GetInnerIdx() * var.tiling_.GetBaseN();
                        for (kLoop->OuterStart(); !kLoop->OuterEnd(); kLoop->OuterNext()) {
                            auto bias = MATMUL_MODULE(BiasScheduler)->CopyIn(baseUseN, 1, srcOffset);
                            MATMUL_MODULE(BiasScheduler)->SplitLoad(bias, baseUseN);
                            MATMUL_MODULE(BiasScheduler)->Free(bias);
                            MATMUL_MODULE(BiasScheduler)->Free();
                        }
                    }
                }
            }
        }
        MATMUL_MODULE(BiasScheduler)->End();
    }

private:
    TPipe pipe;
    VAR_PARAMS var;
    TQue<TPosition::VECIN, 2> qidBias_;
};
}

class TestBiasScheduler : public testing::Test {
protected:
    void SetUp() {}
    void TearDown() {}

private:
    using A_TYPE = MatmulType<AscendC::TPosition::GM, CubeFormat::ND, half, false>;
    using B_TYPE = MatmulType<AscendC::TPosition::GM, CubeFormat::ND, half, false>;
    using C_TYPE = MatmulType<AscendC::TPosition::GM, CubeFormat::ND, float>;
    using BIAS_TYPE_GM = MatmulType<AscendC::TPosition::GM, CubeFormat::ND, float>;
    using BIAS_TYPE_UB = MatmulType<AscendC::TPosition::VECIN, CubeFormat::ND, float>;

    // MatmulImpl<A_TYPE, B_TYPE, C_TYPE, BIAS_TYPE_GM, CFG_NORM, void> mm1_;
    // MatmulImpl<A_TYPE, B_TYPE, C_TYPE, BIAS_TYPE_UB, CFG_NORM, void> mm2_;
    MatmulImpl<A_TYPE, B_TYPE, C_TYPE, BIAS_TYPE_GM, CFG_MDL, void, CustomMatmulPolicy> mm1_;
    MatmulImpl<A_TYPE, B_TYPE, C_TYPE, BIAS_TYPE_UB, CFG_MDL, void, CustomMatmulPolicy> mm2_;
};

TEST_F(TestBiasScheduler, copy_bias_from_gm) {
    // coreNum, M, N, K, singleCoreM, singleCoreN, singleCoreK, baseM, baseN, baseK, depthA1, depthB1, stepM, stepN, stepKa, stepKb, isBias, iterateOrder
    TilingParams tilingParams = {1, 64, 256, 256, 64, 256, 256, 32, 64, 128, 2, 4, 1, 1, 1, 1, 1, 1};
    TCubeTiling tiling;
    tilingParams.GetTiling(tiling);
    mm1_.InitVar(tiling);
    mm1_.RunCase(true, false);
    ASSERT_TRUE(mm1_.IsBias());
    mm1_.SetBias(false);
    ASSERT_FALSE(mm1_.IsBias());
}

TEST_F(TestBiasScheduler, copy_bias_from_ub) {
    // coreNum, M, N, K, singleCoreM, singleCoreN, singleCoreK, baseM, baseN, baseK, depthA1, depthB1, stepM, stepN, stepKa, stepKb, isBias, iterateOrder
    TilingParams tilingParams = {1, 64, 256, 256, 64, 256, 256, 32, 64, 128, 2, 4, 1, 1, 1, 1, 1, 1};
    TCubeTiling tiling;
    tilingParams.GetTiling(tiling);
    mm2_.InitVar(tiling);
    mm2_.SetBias(false);
    ASSERT_FALSE(mm2_.IsBias());
    mm2_.RunCase(true, true);
    ASSERT_TRUE(mm2_.IsBias());
}
