/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023. All rights reserved.
 *
 * @brief load data instruction ut for ascend910B1
 *
 */
#include <gtest/gtest.h>
#include "kernel_operator.h"
#include "lib/matmul/tiling.h"
#include "impl/matmul/modules/matmul_param.h"
#include "impl/matmul/modules/matmul_policy.h"
#include "impl/matmul/modules/matmul_private_modules.h"
#define private public
#include "impl/matmul/modules/param/matmul_shape_tiling.h"

using namespace std;
using namespace AscendC;


namespace {
template <class A_TYPE, class B_TYPE, class C_TYPE, class BIAS_TYPE, const MatmulConfig& MM_CFG, class MM_CB,
MATMUL_POLICY_DEFAULT_OF(MatmulPolicy)>
class MatmulImpl
: MATMUL_IMPORT_MODULE_PRIVATE(MatmulShapeTiling)
, MATMUL_IMPORT_MODULE_PRIVATE(MatmulShapeInfo)
, MATMUL_IMPORT_MODULE(MLoop)
, MATMUL_IMPORT_MODULE(KLoop)
, MATMUL_IMPORT_MODULE(NLoop)
{
    MATMUL_ALLOW_USING_PRIVATE(MatmulShapeTiling);
    MATMUL_ALLOW_USING_PRIVATE(MatmulShapeInfo);
    MATMUL_ALLOW_USING(MLoop);
    MATMUL_ALLOW_USING(KLoop);
    MATMUL_ALLOW_USING(NLoop);
    using SrcT = typename A_TYPE::T;
public:
    using VAR_PARAMS =
        typename Impl::Detail::MatmulParams<A_TYPE, B_TYPE, C_TYPE, BIAS_TYPE, MM_CFG, GetMatmulVersion(MM_CFG)>::PARAMS;
    using IMPL = MatmulImpl<A_TYPE, B_TYPE, C_TYPE, BIAS_TYPE, MM_CFG, MM_CB, MATMUL_POLICY>;
    MATMUL_USE_MODULE(MLoop);
    MATMUL_USE_MODULE(KLoop);
    MATMUL_USE_MODULE(NLoop);
    MatmulImpl() {
        InitVar();
    }

    VAR_PARAMS& GetVar() {
        return var;
    }

    void InitVar() {
        tiling.depthA1 = 2;
        tiling.depthB1 = 2;
        tiling.iterateOrder = 0;
        tiling.baseM = 16;
        tiling.baseN = 16;
        tiling.baseK = 16;
        var.tiling_.SetTiling(&tiling);
        var.tpipe_ = &pipe;
    }

    void SetSingleCoreParams(int32_t singleM, int32_t singleN, int32_t singleK, bool isIntraBlock = false)
    {
        if (isIntraBlock) {
            intraBlockMatmul.singleCoreM = singleM;
            intraBlockMatmul.singleCoreN = singleN;
            intraBlockMatmul.singleCoreK = singleK;
            intraBlockMatmul.M = singleM;
            intraBlockMatmul.N = singleN;
            intraBlockMatmul.Ka = singleK;
            intraBlockMatmul.Kb = singleK;
        } else {
            var.singleCoreM_ = singleM;
            var.singleCoreN_ = singleN;
            var.singleCoreK_ = singleK;
            M_ = singleM;
            N_ = singleN;
            Ka_ = singleK;
            Kb_ = singleK;
            Kc_ = singleN;
            MATMUL_MODULE(MLoop)->Init(var.singleCoreM_);
            MATMUL_MODULE(KLoop)->Init(var.singleCoreK_);
        }
        MATMUL_MODULE(MLoop)->Init(var.singleCoreM_);
        MATMUL_MODULE(KLoop)->Init(var.singleCoreK_);
        MATMUL_MODULE(NLoop)->Init(var.singleCoreN_);
    }

    void SetInitAParams(int32_t stepM, int32_t stepKa, int32_t baseM, int32_t baseK, int32_t depth, bool isIntraBlock = false) {
        tiling.stepM = stepM;
        tiling.stepKa = stepKa;
        tiling.baseM = baseM;
        tiling.baseK = baseK;
        tiling.depthA1 = depth;
        var.baseUseK_ = baseK;
        var.baseUseStepKa_ = baseK * stepKa;
        if (isIntraBlock) {
            intraBlockMatmul.baseUseM = baseM;
            intraBlockMatmul.mIter = intraBlockMatmul.singleCoreM / baseM;
            intraBlockMatmul.kIter = intraBlockMatmul.singleCoreK / baseK;
        } else {
            var.baseUseM_ = baseM;
            var.mIter_ = var.singleCoreM_ / baseM;
            var.kIter_ = var.singleCoreK_ / baseK;
        }
    }

    void SetALayoutInfo(int32_t b, int32_t s, int32_t n, int32_t g, int32_t d, int32_t batch)
    {
        tiling.ALayoutInfoB = b;
        tiling.ALayoutInfoS = s;
        tiling.ALayoutInfoN = n;
        tiling.ALayoutInfoG = g;
        tiling.ALayoutInfoD = d;
        batchA_ = batch;
    }

    void SetInitBParams(int32_t stepN, int32_t stepKb, int32_t baseN, int32_t baseK, int32_t depth, bool isIntraBlock = false) {
        tiling.stepN = stepN;
        tiling.stepKb = stepKb;
        tiling.baseN = baseN;
        tiling.baseK = baseK;
        tiling.depthB1 = depth;
        var.baseUseK_ = baseK;
        var.baseUseStepKb_ = baseK * stepKb;
        if (isIntraBlock) {
            intraBlockMatmul.baseUseN = baseN;
            intraBlockMatmul.nIter = intraBlockMatmul.singleCoreN / baseN;
            intraBlockMatmul.kIter = intraBlockMatmul.singleCoreK / baseK;
        } else {
            var.baseUseN_ = baseN;
            var.nIter_ = var.singleCoreN_ / baseN;
            var.kIter_ = var.singleCoreK_ / baseK;
        }
    }

    void SetBLayoutInfo(int32_t b, int32_t s, int32_t n, int32_t g, int32_t d, int32_t batch)
    {
        tiling.BLayoutInfoB = b;
        tiling.BLayoutInfoS = s;
        tiling.BLayoutInfoN = n;
        tiling.BLayoutInfoG = g;
        tiling.BLayoutInfoD = d;
        batchB_ = batch;
    }

    void SetTranspose(bool isATrans, bool isBTrans, bool isIntraBlock = false)
    {
        if (isIntraBlock) {
            intraBlockMatmul.isTransposeA = isATrans;
            intraBlockMatmul.isTransposeB = isBTrans;
        } else {
            var.isTransposeA_ = isATrans;
            var.isTransposeB_ = isBTrans;
        }
    }

    void SetSelfDefineData(uint64_t dataPtr) {
        var.dataPtr_ = dataPtr;
    }

    void SetUserDefineInfo(uint64_t tilingPtr) {
        var.tilingPtr_ = tilingPtr;
    }

private:
    TCubeTiling tiling;
    TPipe pipe;
    VAR_PARAMS var;
    int32_t M_;
    int32_t N_;
    int32_t Ka_;
    int32_t Kb_;
    int32_t Kc_;
    int32_t batchA_;
    int32_t batchB_;

    struct IntraBlock {
        __aicore__ inline IntraBlock(){};
        __gm__ SrcT* aGlobal;
        __gm__ SrcT* bGlobal;
        int M;
        int N;
        int Ka;
        int Kb;
        int Kc;
        int singleCoreM;
        int singleCoreN;
        int singleCoreK;
        int mIter;
        int nIter;
        int kIter;
        int baseUseM;
        int baseUseN;
        // measured in cube block
        int blockUseM;
        int blockUseN;
        int tailM, tailK, tailN;
        int cacheProcA = 0;
        bool enableBias = false;
        bool isTransposeA;
        bool isTransposeB;
        bool fakeMsg = false;
    };

    IntraBlock intraBlockMatmul;
};
}

class TestMatmulShapeInfoA : public testing::Test {
protected:
    void SetUp() {}
    void TearDown() {}

private:
    using A_TYPE = MatmulType<AscendC::TPosition::GM, CubeFormat::ND, half, false>;
    using B_TYPE = MatmulType<AscendC::TPosition::GM, CubeFormat::ND, half, false>;
    using C_TYPE = MatmulType<AscendC::TPosition::GM, CubeFormat::ND, float>;
    using BIAS_TYPE = MatmulType<AscendC::TPosition::GM, CubeFormat::ND, float>;

    MatmulImpl<A_TYPE, B_TYPE, C_TYPE, BIAS_TYPE, CFG_NORM, void> mm;
};

TEST_F(TestMatmulShapeInfoA, test_get_singleCore_params)
{
    mm.SetSingleCoreParams(32, 32, 16);
    EXPECT_EQ(mm.GetSingleCoreM(), 32);
    EXPECT_EQ(mm.GetSingleCoreN(), 32);

    EXPECT_EQ(mm.GetKIter(), 1);
    EXPECT_EQ(mm.GetMIter(), 2);
    EXPECT_EQ(mm.GetNIter(), 2);

    EXPECT_EQ(mm.GetOrgKa(), 16);
    EXPECT_EQ(mm.GetOrgKb(), 16);
    EXPECT_EQ(mm.GetOrgKc(), 32);
    EXPECT_EQ(mm.GetOrgN(), 32);
    EXPECT_EQ(mm.GetOrgM(), 32);
}
