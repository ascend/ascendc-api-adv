/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023. All rights reserved.
 *
 * @brief load data instruction ut for ascend910B1
 *
 */
#include <gtest/gtest.h>
#include "kernel_operator.h"
#include "lib/matmul/tiling.h"
#include "impl/matmul/modules/matmul_param.h"
#include "impl/matmul/modules/matmul_policy.h"
#include "impl/matmul/modules/resource/cube_in_buffer/cube_in_buffer.h"
#include "impl/matmul/modules/matmul_private_modules.h"

using namespace std;
using namespace AscendC;


namespace {
template <const auto& MM_CFG, typename IMPL, typename A_TYPE, typename B_TYPE, typename C_TYPE, typename BIAS_TYPE>
class CustomMatmulPolicy : public Impl::Detail::MatmulPolicy<MM_CFG, IMPL, A_TYPE, B_TYPE, C_TYPE, BIAS_TYPE>
{
public:
    using CubeInBufferA = Impl::Detail::CubeInBuffer<IMPL, MatmulInputAType<A_TYPE, typename A_TYPE::T>, MM_CFG>;
    using CubeInBufferB = Impl::Detail::CubeInBuffer<IMPL, MatmulInputBType<B_TYPE, typename A_TYPE::T>, MM_CFG>;
};

template <class A_TYPE, class B_TYPE, class C_TYPE, class BIAS_TYPE, const MatmulConfig& MM_CFG, class MM_CB,
MATMUL_POLICY_DEFAULT_OF(MatmulPolicy)>
class MatmulImpl
: MATMUL_IMPORT_MODULE(CubeInBufferA)
, MATMUL_IMPORT_MODULE_PRIVATE(MatmulShapeTiling)
{
    MATMUL_ALLOW_USING(CubeInBufferA);
    MATMUL_ALLOW_USING_PRIVATE(MatmulShapeTiling);

public:
    using CubeInBufferA::Init;
    using CubeInBufferA::Destroy;
    using CubeInBufferA::AllocTensor;
    using CubeInBufferA::FreeTensor;
    using CubeInBufferA::Hit;
    using CubeInBufferA::GetBuffer;
    using CubeInBufferA::Reset;
    using CubeInBufferA::EnQue;
    using CubeInBufferA::DeQue;

public:
    using VAR_PARAMS =
        typename Impl::Detail::MatmulParams<A_TYPE, B_TYPE, C_TYPE, BIAS_TYPE, MM_CFG, GetMatmulVersion(MM_CFG)>::PARAMS;
    MatmulImpl() {
        InitVar();
    }

    VAR_PARAMS& GetVar() {
        return var;
    }

    void InitVar() {
        var.tiling_.SetTiling(&tiling);
        var.tpipe_ = &pipe;
    }

    void SetInitParams(int32_t stepM, int32_t stepKa, int32_t baseM, int32_t baseK) {
        tiling.stepM = stepM;
        tiling.stepKa = stepKa;
        tiling.baseM = baseM;
        tiling.baseK = baseK;
        tiling.iterateOrder = 0;
    }

    void SetRuntimeParams(int32_t baseUseM, int32_t baseUseK) {
        var.baseUseM_ = baseUseM;
        var.baseUseK_ = baseUseK;
    }

private:
    TCubeTiling tiling;
    TPipe pipe;
    VAR_PARAMS var;
};
}

class test_cube_in_buffer_double_buffer : public testing::Test {
protected:
    void SetUp() {}
    void TearDown() {}

private:
    using A_TYPE = MatmulType<AscendC::TPosition::GM, CubeFormat::ND, half, false>;
    using B_TYPE = MatmulType<AscendC::TPosition::GM, CubeFormat::ND, half, false>;
    using C_TYPE = MatmulType<AscendC::TPosition::GM, CubeFormat::ND, float>;
    using BIAS_TYPE = MatmulType<AscendC::TPosition::GM, CubeFormat::ND, float>;

    MatmulImpl<A_TYPE, B_TYPE, C_TYPE, BIAS_TYPE, CFG_MDL, void, CustomMatmulPolicy> mm;
};

TEST_F(test_cube_in_buffer_double_buffer, DISABLED_get_iter_index) {
    mm.SetInitParams(2, 2, 32, 32);
    int32_t mIter = 2;
    int32_t kIter = 3;
    mm.Init(1024, 4);
}

TEST_F(test_cube_in_buffer_double_buffer, DISABLED_tiling_set_single_que) {
    mm.SetInitParams(2, 2, 32, 32);
    int32_t mIter = 2;
    int32_t kIter = 2;
    int32_t nIter = 2;
    int32_t hitCnt = 0;
    mm.Init(1024, 4);
    int32_t stepMIdx = 0;
    LocalTensor<half> fakeTensor;
    for (int32_t m = 0; m < mIter; m=m+2) {
        for (int32_t n = 0; n < nIter; n++) {
            for (int32_t k = 0; k < kIter; k++) {
                int32_t iterIndex = 0;
                if (mm.Hit(iterIndex, stepMIdx)) {
                    fakeTensor = mm.GetBuffer(iterIndex, stepMIdx);
                    hitCnt++;
                } else {
                    fakeTensor = mm.AllocTensor(iterIndex);
                    mm.EnQue(fakeTensor);
                    mm.DeQue();
                }
                mm.FreeTensor(stepMIdx, fakeTensor);
                stepMIdx++;
            }
        }
        mm.Reset();
    }
    mm.Destroy();
    ASSERT_EQ(hitCnt, 2);
}

TEST_F(test_cube_in_buffer_double_buffer, DISABLED_tiling_set_db_que) {
    mm.SetInitParams(2, 2, 32, 32);
    int32_t mIter = 2;
    int32_t kIter = 2;
    int32_t nIter = 2;
    int32_t hitCnt = 0;
    mm.Init(1024, 8);
    int32_t stepMIdx = 0;
    LocalTensor<half> fakeTensor;
    for (int32_t m = 0; m < mIter; m=m+2) {
        for (int32_t n = 0; n < nIter; n++) {
            for (int32_t k = 0; k < kIter; k++) {
                int32_t iterIndex = 0;
                if (mm.Hit(iterIndex, stepMIdx)) {
                    fakeTensor = mm.GetBuffer(iterIndex, stepMIdx);
                    hitCnt++;
                } else {
                    fakeTensor = mm.AllocTensor(iterIndex);
                    mm.EnQue(fakeTensor);
                    mm.DeQue();
                }
                mm.FreeTensor(stepMIdx, fakeTensor);
            }
        }
        stepMIdx++;
        mm.Reset();
    }
    mm.Destroy();
    ASSERT_EQ(hitCnt, 2);
}
