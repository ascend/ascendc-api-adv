/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2023. All rights reserved.
 *
 * @brief load data instruction ut for ascend910B1
 *
 */
#include <gtest/gtest.h>
#include "kernel_operator.h"
#include "lib/matmul/tiling.h"
#include "impl/matmul/modules/matmul_param.h"
#include "impl/matmul/modules/matmul_policy.h"
#define private public
#include "impl/matmul/modules/matmul_private_modules.h"
#include "impl/matmul/modules/stage/compute/mmad_compute.h"

using namespace std;
using namespace AscendC;


namespace {

template <const auto& MM_CFG, typename IMPL, typename A_TYPE, typename B_TYPE, typename C_TYPE, typename BIAS_TYPE>
class CustomMatmulPolicy : public Impl::Detail::MatmulPolicy<MM_CFG, IMPL, A_TYPE, B_TYPE, C_TYPE, BIAS_TYPE>
{
public:
    using MmadCompute = Impl::Detail::MmadCompute<IMPL, float, typename A_TYPE::T, typename B_TYPE::T, MM_CFG>;
};

template <class A_TYPE, class B_TYPE, class C_TYPE, class BIAS_TYPE, const MatmulConfig& MM_CFG, class MM_CB,
MATMUL_POLICY_DEFAULT_OF(MatmulPolicy)>
class MatmulImpl
: MATMUL_IMPORT_MODULE(MmadCompute) {
    MATMUL_ALLOW_USING(MmadCompute);

public:
    MatmulImpl() {};
    TPipe pipe;
};
}

class test_matmul_mmad_compute : public testing::Test {
protected:
    void SetUp() {}
    void TearDown() {}

private:
    using L0cT = float;

    using A_TYPE = MatmulType<AscendC::TPosition::TSCM, CubeFormat::ND, half>;
    using B_TYPE = MatmulType<AscendC::TPosition::TSCM, CubeFormat::ND, half>;
    using C_TYPE = MatmulType<AscendC::TPosition::GM, CubeFormat::ND, L0cT>;
    using BIAS_TYPE = MatmulType<AscendC::TPosition::GM, CubeFormat::ND, float>;

    MatmulImpl<A_TYPE, B_TYPE, C_TYPE, BIAS_TYPE, CFG_NORM, void, CustomMatmulPolicy> mm;
};

TEST_F(test_matmul_mmad_compute, case0) {
    // input: M : 16, K : 16, N : 16, isATrans : false, isBTrans : false
    TBuf<TPosition::A2> l0aBuf;
    mm.pipe.InitBuffer(l0aBuf, 65536);
    auto l0a = l0aBuf.Get<half>();
    TBuf<TPosition::B2> l0bBuf;
    mm.pipe.InitBuffer(l0bBuf, 65536);
    auto l0b = l0bBuf.Get<half>();
    TBuf<TPosition::CO1> co1BUf;
    mm.pipe.InitBuffer(co1BUf, 65536);
    auto l0c = co1BUf.Get<float>();
    mm.Compute(l0c, l0a, l0b, 16, 16, 16, false, false);
}