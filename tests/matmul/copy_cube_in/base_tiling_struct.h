struct TilingParams {
    __aicore__ TilingParams() {}
    __aicore__ TilingParams(uint32_t coreNum, uint32_t M, uint32_t N, uint32_t K, uint32_t singleCoreM,
        uint32_t singleCoreN, uint32_t singleCoreK, uint32_t baseM, uint32_t baseN, uint32_t baseK, uint32_t depthA1,
        uint32_t depthB1, uint32_t stepM, uint32_t stepN, uint32_t stepKa, uint32_t stepKb, uint32_t isbias,
        uint32_t iterateOrder, uint32_t batchNum = 0) : coreNum_(coreNum), M_(M), N_(N), K_(K),
        singleCoreM_(singleCoreM), singleCoreN_(singleCoreN), singleCoreK_(singleCoreK), baseM_(baseM), baseN_(baseN),
        baseK_(baseK), depthA1_(depthA1), depthB1_(depthB1), stepM_(stepM), stepN_(stepN), stepKa_(stepKa),
        stepKb_(stepKb), isbias_(isbias), iterateOrder_(iterateOrder), batchNum_(batchNum) {}
    __aicore__ void GetTiling(TCubeTiling &tiling)
    {
        tiling.usedCoreNum = coreNum_;
        tiling.M = M_;
        tiling.N = N_;
        tiling.Ka = K_;
        tiling.Kb = K_;
        tiling.singleCoreM = singleCoreM_;
        tiling.singleCoreN = singleCoreN_;
        tiling.singleCoreK = singleCoreK_;
        tiling.baseM = baseM_;
        tiling.baseN = baseN_;
        tiling.baseK = baseK_;
        tiling.depthA1 = depthA1_;
        tiling.depthB1 = depthB1_;
        tiling.stepM = stepM_;
        tiling.stepN = stepN_;
        tiling.stepKa = stepKa_;
        tiling.stepKb = stepKb_;
        tiling.isBias = isbias_;
        tiling.iterateOrder = iterateOrder_;
    }
    uint32_t coreNum_;
    uint32_t M_;
    uint32_t N_;
    uint32_t K_;
    uint32_t singleCoreM_;
    uint32_t singleCoreN_;
    uint32_t singleCoreK_;
    uint32_t baseM_;
    uint32_t baseN_;
    uint32_t baseK_;
    uint32_t depthA1_;
    uint32_t depthB1_;
    uint32_t stepM_;
    uint32_t stepN_;
    uint32_t stepKa_;
    uint32_t stepKb_;
    uint32_t isbias_;
    uint32_t iterateOrder_;
    uint32_t batchNum_ = 0;
};

struct TilingParamsBatch {
    __aicore__ TilingParamsBatch() {}
    __aicore__ TilingParamsBatch(uint32_t coreNum, uint32_t M, uint32_t N, uint32_t K, uint32_t singleCoreM,
        uint32_t singleCoreN, uint32_t singleCoreK, uint32_t baseM, uint32_t baseN, uint32_t baseK, uint32_t depthA1,
        uint32_t depthB1, uint32_t stepM, uint32_t stepN, uint32_t stepKa, uint32_t stepKb, uint32_t isbias,
        uint32_t iterateOrder, uint32_t batchM, uint32_t batchN, uint32_t batchNum, uint32_t ALayoutInfoB,
        uint32_t ALayoutInfoS, uint32_t ALayoutInfoN, uint32_t ALayoutInfoG, uint32_t ALayoutInfoD,
        uint32_t BLayoutInfoB, uint32_t BLayoutInfoS, uint32_t BLayoutInfoN, uint32_t BLayoutInfoG,
        uint32_t BLayoutInfoD, uint32_t CLayoutInfoB, uint32_t CLayoutInfoS1, uint32_t CLayoutInfoN,
        uint32_t CLayoutInfoG, uint32_t CLayoutInfoS2) : coreNum_(coreNum), M_(M), N_(N), K_(K),
        singleCoreM_(singleCoreM), singleCoreN_(singleCoreN), singleCoreK_(singleCoreK), baseM_(baseM), baseN_(baseN),
        baseK_(baseK), depthA1_(depthA1), depthB1_(depthB1), stepM_(stepM), stepN_(stepN), stepKa_(stepKa),
        stepKb_(stepKb), isbias_(isbias), iterateOrder_(iterateOrder), batchM_(batchM), batchN_(batchN), batchNum_(batchNum), ALayoutInfoB_(ALayoutInfoB),
        ALayoutInfoS_(ALayoutInfoS), ALayoutInfoN_(ALayoutInfoN), ALayoutInfoG_(ALayoutInfoG), ALayoutInfoD_(ALayoutInfoD),
        BLayoutInfoB_(BLayoutInfoB), BLayoutInfoS_(BLayoutInfoS), BLayoutInfoN_(BLayoutInfoN), BLayoutInfoG_(BLayoutInfoG),
        BLayoutInfoD_(BLayoutInfoD),
        CLayoutInfoB_(CLayoutInfoB), CLayoutInfoS1_(CLayoutInfoS1), CLayoutInfoN_(CLayoutInfoN), CLayoutInfoG_(CLayoutInfoG),
        CLayoutInfoS2_(CLayoutInfoS2) {}

    __aicore__ void GetTiling(TCubeTiling &tiling)
    {
        tiling.usedCoreNum = coreNum_;
        tiling.M = M_;
        tiling.N = N_;
        tiling.Ka = K_;
        tiling.Kb = K_;
        tiling.singleCoreM = singleCoreM_;
        tiling.singleCoreN = singleCoreN_;
        tiling.singleCoreK = singleCoreK_;
        tiling.baseM = baseM_;
        tiling.baseN = baseN_;
        tiling.baseK = baseK_;
        tiling.depthA1 = depthA1_;
        tiling.depthB1 = depthB1_;
        tiling.stepM = stepM_;
        tiling.stepN = stepN_;
        tiling.stepKa = stepKa_;
        tiling.stepKb = stepKb_;
        tiling.isBias = isbias_;
        tiling.iterateOrder = iterateOrder_;
        tiling.batchM = batchM_;
        tiling.batchN = batchN_;
        tiling.BatchNum = batchNum_;
        tiling.ALayoutInfoB = ALayoutInfoB_;
        tiling.ALayoutInfoS = ALayoutInfoS_;
        tiling.ALayoutInfoN = ALayoutInfoN_;
        tiling.ALayoutInfoG = ALayoutInfoG_;
        tiling.ALayoutInfoD = ALayoutInfoD_;
        tiling.BLayoutInfoB = BLayoutInfoB_;
        tiling.BLayoutInfoS = BLayoutInfoS_;
        tiling.BLayoutInfoN = BLayoutInfoN_;
        tiling.BLayoutInfoG = BLayoutInfoG_;
        tiling.BLayoutInfoD = BLayoutInfoD_;
        tiling.CLayoutInfoB = CLayoutInfoB_;
        tiling.CLayoutInfoS1 = CLayoutInfoS1_;
        tiling.CLayoutInfoN = CLayoutInfoN_;
        tiling.CLayoutInfoG = CLayoutInfoG_;
        tiling.CLayoutInfoS2 = CLayoutInfoS2_;
    }

    uint32_t coreNum_;
    uint32_t M_;
    uint32_t N_;
    uint32_t K_;
    uint32_t singleCoreM_;
    uint32_t singleCoreN_;
    uint32_t singleCoreK_;
    uint32_t baseM_;
    uint32_t baseN_;
    uint32_t baseK_;
    uint32_t depthA1_;
    uint32_t depthB1_;
    uint32_t stepM_;
    uint32_t stepN_;
    uint32_t stepKa_;
    uint32_t stepKb_;
    uint32_t isbias_;
    uint32_t iterateOrder_;
    uint32_t batchM_;
    uint32_t batchN_;
    uint32_t batchNum_;
    uint32_t ALayoutInfoB_;
    uint32_t ALayoutInfoS_;
    uint32_t ALayoutInfoN_;
    uint32_t ALayoutInfoG_;
    uint32_t ALayoutInfoD_;
    uint32_t BLayoutInfoB_;
    uint32_t BLayoutInfoS_;
    uint32_t BLayoutInfoN_;
    uint32_t BLayoutInfoG_;
    uint32_t BLayoutInfoD_;
    uint32_t CLayoutInfoB_;
    uint32_t CLayoutInfoS1_;
    uint32_t CLayoutInfoN_;
    uint32_t CLayoutInfoG_;
    uint32_t CLayoutInfoS2_;
};
