/**
 * Copyright (c) 2024 Huawei Technologies Co., Ltd.
 * This file is a part of the CANN Open Software.
 * Licensed under CANN Open Software License Agreement Version 1.0 (the "License").
 * Please refer to the License for details. You may not use this file except in compliance with the License.
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR A PARTICULAR PURPOSE.
 * See LICENSE in the root of the software repository for the full text of the License.
 */
#include <gtest/gtest.h>
#include "graph/tensor.h"
#include <dlfcn.h>
#define private public
#define protected public
#include "lib/activation/softmax_tiling.h"
#include "tiling_api.h"
#include "platform_stub.h"
#include "impl/matmul/math_util.h"
#include "impl/matmul/matmul_tiling_algorithm.h"
#include "tiling/platform/platform_ascendc.h"
using namespace AscendC;
using namespace ge;
using namespace std;
using namespace matmul_tiling;

class TestTiling : public testing::Test {
protected:
    static void SetUpTestCase() {}
    static void TearDownTestCase() {}
    virtual void SetUp() {}
    void TearDown() {}
};


TEST_F(TestTiling, MultiCoreSmallMN)
{
    matmul_tiling::MultiCoreMatmulTiling rnnMatmul3,rnnMatmul4,rnnMatmul5;
    rnnMatmul3.SetAType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_FLOAT);
    rnnMatmul3.SetBType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_FLOAT);
    rnnMatmul3.SetCType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::NZ, matmul_tiling::DataType ::DT_FLOAT);
    rnnMatmul3.SetBiasType(matmul_tiling::TPosition::VECCALC, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_FLOAT);
    rnnMatmul3.SetSingleRange(-1,-1,-1,-1,-1,-1);
    auto ret = rnnMatmul3.SetBias(true);
    ret = rnnMatmul3.SetDim(24);
    ret = rnnMatmul3.SetOrgShape(5, 40, 986);
    ret = rnnMatmul3.SetShape(5, 10, 986);
    ret = rnnMatmul3.SetBufferSpace(-1, -1, -1); // will use all buffer space if not explicitly specified
    optiling::TCubeTiling tilingData;
    ret = rnnMatmul3.GetTiling(tilingData);
    rnnMatmul3.PrintTilingData();
    matmul_tiling::SysTilingTempBufSize bufSize;
    MultiCoreMatmulGetTmpBufSize(tilingData, bufSize);
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, MatmulApiTilingFP32OverFlow)
{
    matmul_tiling::PlatformInfo plat {.socVersion = platform_ascendc::SocVersion::ASCEND910B, .l1Size = 524288,
        .l0CSize = 131072, .ubSize = 196608, .l0ASize = 65536, .l0BSize = 65536};
    matmul_tiling::MatmulApiTiling stft(plat);
    stft.SetAType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_FLOAT);
    stft.SetBType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_FLOAT, true);
    stft.SetCType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_FLOAT);
    stft.SetBiasType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_FLOAT);
    auto ret = stft.SetOrgShape(378, 168, 960);
    ret = stft.SetShape(378, 168, 960);
    ret = stft.SetBufferSpace(-1, -1, -1); // will use all buffer space if not explicitly specified
    optiling::TCubeTiling tilingData;
    tilingData.set_usedCoreNum(1);
    ret = stft.GetTiling(tilingData);
    tilingData.set_iterateOrder(1);
    stft.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, MatmulApiTilingATransFP32OverFlow)
{
    matmul_tiling::PlatformInfo plat {.socVersion = platform_ascendc::SocVersion::ASCEND910B, .l1Size = 524288,
        .l0CSize = 131072, .ubSize = 196608, .l0ASize = 65536, .l0BSize = 65536};
    matmul_tiling::MatmulApiTiling stft(plat);
    stft.SetAType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_FLOAT, true);
    stft.SetBType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_FLOAT, true);
    stft.SetCType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_FLOAT);
    stft.SetBiasType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_FLOAT);
    auto ret = stft.SetOrgShape(378, 168, 960);
    ret = stft.SetShape(378, 168, 960);
    ret = stft.SetBufferSpace(-1, -1, -1); // will use all buffer space if not explicitly specified
    optiling::TCubeTiling tilingData;
    tilingData.set_usedCoreNum(1);
    ret = stft.GetTiling(tilingData);
    tilingData.set_iterateOrder(1);
    stft.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, MatmulMDL1951ND2NZNoFullLoad)
{
    matmul_tiling::MultiCoreMatmulTiling rnnMatmul3,rnnMatmul4,rnnMatmul5;
    rnnMatmul3.SetAType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_INT8, true);
    rnnMatmul3.SetBType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::NZ, matmul_tiling::DataType ::DT_INT8);
    rnnMatmul3.SetCType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_FLOAT16);
    rnnMatmul3.SetBiasType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_INT32);
    auto ret = rnnMatmul3.EnableBias(true);
    ret = rnnMatmul3.SetDim(8);
    ret = rnnMatmul3.SetOrgShape(1024, 5120, 3584);
    ret = rnnMatmul3.SetShape(1024, 5120, 3584);
    ret = rnnMatmul3.SetBufferSpace(-1, -1, -1); // will use all buffer space if not explicitly specified
    rnnMatmul3.SetMatmulConfigParams({1, false, ScheduleType::INNER_PRODUCT, MatrixTraverse::NOSET, true});
    rnnMatmul3.socVersion = platform_ascendc::SocVersion::ASCEND310P;
    optiling::TCubeTiling tilingDataA;
    ret = rnnMatmul3.GetTiling(tilingDataA);
    rnnMatmul3.PrintTilingData();
    EXPECT_EQ(ret, 0);

    rnnMatmul4.SetAType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_INT8);
    rnnMatmul4.SetBType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_INT8, true);
    rnnMatmul4.SetCType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_FLOAT16);
    rnnMatmul4.SetBiasType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_INT32);
    ret = rnnMatmul4.EnableBias(true);
    ret = rnnMatmul4.SetDim(8);
    ret = rnnMatmul4.SetOrgShape(5120, 1024, 3584);
    ret = rnnMatmul4.SetShape(5120, 1024, 3584);
    ret = rnnMatmul4.SetBufferSpace(-1, -1, -1); // will use all buffer space if not explicitly specified
    rnnMatmul4.SetMatmulConfigParams({1, false, ScheduleType::INNER_PRODUCT, MatrixTraverse::NOSET, true});
    rnnMatmul4.socVersion = platform_ascendc::SocVersion::ASCEND310P;
    optiling::TCubeTiling tilingDataB;
    ret = rnnMatmul4.GetTiling(tilingDataB);
    rnnMatmul4.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, MatmulMDL1951ND2NZFullLoad)
{
    matmul_tiling::MultiCoreMatmulTiling rnnMatmul3,rnnMatmul4;
    rnnMatmul3.SetAType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_INT8);
    rnnMatmul3.SetBType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_INT8, true);
    rnnMatmul3.SetCType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_FLOAT16);
    rnnMatmul3.SetBiasType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_INT32);
    auto ret = rnnMatmul3.EnableBias(true);
    ret = rnnMatmul3.SetDim(8);
    ret = rnnMatmul3.SetOrgShape(32, 2048, 64);
    ret = rnnMatmul3.SetShape(32, 2048, 64);
    ret = rnnMatmul3.SetBufferSpace(-1, -1, -1); // will use all buffer space if not explicitly specified
    rnnMatmul3.SetMatmulConfigParams({1, false, ScheduleType::INNER_PRODUCT, MatrixTraverse::NOSET, true});
    rnnMatmul3.socVersion = platform_ascendc::SocVersion::ASCEND310P;
    optiling::TCubeTiling tilingDataA;
    ret = rnnMatmul3.GetTiling(tilingDataA);
    rnnMatmul3.PrintTilingData();
    EXPECT_EQ(ret, 0);

    rnnMatmul4.SetAType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_INT8, true);
    rnnMatmul4.SetBType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_INT8);
    rnnMatmul4.SetCType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_FLOAT16);
    rnnMatmul4.SetBiasType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_INT32);
    ret = rnnMatmul4.EnableBias(true);
    ret = rnnMatmul4.SetDim(8);
    ret = rnnMatmul4.SetOrgShape(2048, 32, 64);
    ret = rnnMatmul4.SetShape(2048, 32, 64);
    ret = rnnMatmul4.SetBufferSpace(-1, -1, -1); // will use all buffer space if not explicitly specified
    rnnMatmul4.SetMatmulConfigParams({1, false, ScheduleType::INNER_PRODUCT, MatrixTraverse::NOSET, true});
    rnnMatmul4.socVersion = platform_ascendc::SocVersion::ASCEND310P;
    optiling::TCubeTiling tilingDataB;
    ret = rnnMatmul4.GetTiling(tilingDataB);
    rnnMatmul4.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, L1CacheUBCase01NoCacheND2NZ)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(1024, 10240, 1280);
    tiling.SetOrgShape(1024, 10240, 1280);
    tiling.EnableBias(true);
    tiling.socVersion = platform_ascendc::SocVersion::ASCEND310P;
    tiling.SetMatmulConfigParams(1, true, ScheduleType::INNER_PRODUCT, MatrixTraverse::NOSET, true);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
    EXPECT_EQ(tilingData.get_depthAL1CacheUB(), 2);
    EXPECT_EQ(tilingData.get_depthBL1CacheUB(), 4);
}

TEST_F(TestTiling, AFullLoadND2NZCase)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(32, 512000, 128);
    tiling.SetOrgShape(32, 512000, 128);
    tiling.EnableBias(true);
    tiling.SetBufferSpace(-1, -1, -1);
    tiling.SetMatmulConfigParams(1, false, ScheduleType::INNER_PRODUCT, MatrixTraverse::NOSET, true);
    tiling.socVersion = platform_ascendc::SocVersion::ASCEND310P;
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, BothNotFullLoadND2NZCase)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(512, 1024, 128000);
    tiling.SetOrgShape(512, 1024, 128000);
    tiling.EnableBias(true);
    tiling.SetBufferSpace(-1, -1, -1);
    tiling.SetMatmulConfigParams(1, false, ScheduleType::INNER_PRODUCT, MatrixTraverse::NOSET, true);
    tiling.socVersion = platform_ascendc::SocVersion::ASCEND310P;
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, TestMatmulApiTilngSingleCoreFullLoadND2NZCase)
{
    optiling::TCubeTiling tilingData;
    MultiCoreMatmulTiling tilingApi;
    tilingApi.SetDim(24);

    tilingApi.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16, true);
    tilingApi.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tilingApi.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tilingApi.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);

    tilingApi.SetOrgShape(2048, 2048, 204);
    tilingApi.SetShape(2048, 2048, 204);
    tilingApi.EnableBias(false);
    tilingApi.SetBufferSpace(-1, -1, -1);
    tilingApi.SetMatmulConfigParams(1, false, ScheduleType::INNER_PRODUCT, MatrixTraverse::NOSET, true);
    tilingApi.socVersion = platform_ascendc::SocVersion::ASCEND310P;
    int64_t res = tilingApi.GetTiling(tilingData);
    tilingApi.PrintTilingData();
    EXPECT_EQ(res, 0);
}

TEST_F(TestTiling, TestMatmulApiTilngInt8ND2NZCase13)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT8, true);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT8, true);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT32);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT32);
    tiling.SetShape(1024, 1024, 1024);
    tiling.SetOrgShape(1024, 1024, 1024);
    tiling.EnableBias(true);
    tiling.SetBufferSpace(-1, -1, -1);
    tiling.SetMatmulConfigParams(1, false, ScheduleType::INNER_PRODUCT, MatrixTraverse::NOSET, true);
    tiling.socVersion = platform_ascendc::SocVersion::ASCEND310P;

    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, MatmulApiTilingFP32)
{
    matmul_tiling::MatmulApiTiling stft;
    stft.SetAType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_FLOAT);
    stft.SetBType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_FLOAT, true);
    stft.SetCType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_FLOAT);
    stft.SetBiasType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_FLOAT);
    auto ret = stft.SetOrgShape(28, 784, 128);
    ret = stft.SetShape(28, 784, 112);
    ret = stft.SetBufferSpace(-1, -1, -1); // will use all buffer space if not explicitly specified
    optiling::TCubeTiling tilingData;
    tilingData.set_usedCoreNum(1);
    ret = stft.GetTiling(tilingData);
    tilingData.set_iterateOrder(1);
    stft.PrintTilingData();
    matmul_tiling::SysTilingTempBufSize bufSize;
    MatmulGetTmpBufSize(tilingData, bufSize);
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, TestArithProgressionTiling)
{
    uint32_t maxValue = 0;
    uint32_t minValue = 0;
    AscendC::GetArithProgressionMaxMinTmpSize(maxValue, minValue);
    EXPECT_EQ(maxValue, 0);
    EXPECT_EQ(minValue, 0);
}

TEST_F(TestTiling, PlatformConstructor)
{
    matmul_tiling::PlatformInfo plat {.socVersion = platform_ascendc::SocVersion::ASCEND910B, .l1Size = 524288,
        .l0CSize = 131072, .ubSize = 196608, .l0ASize = 65536, .l0BSize = 65536};
    MatmulApiTiling tiling(plat);
    tiling.SetAType(TPosition::TSCM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(32, 4096, 80);
    tiling.SetOrgShape(32, 4096, 80);
    tiling.SetBias(true);
    tiling.SetBufferSpace(256 * 1024, -1, -1);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    tiling.PrintTilingDataInfo(tilingData);
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, TestMatmulApiTilingL0DB)
{
    matmul_tiling::PlatformInfo plat {.socVersion = platform_ascendc::SocVersion::ASCEND910B, .l1Size = 524288,
        .l0CSize = 131072, .ubSize = 196608, .l0ASize = 65536, .l0BSize = 65536};
    MatmulApiTiling tiling(plat);
    tiling.SetAType(TPosition::TSCM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(2048, 20480, 16);
    tiling.SetOrgShape(2048, 20480, 16);
    tiling.SetBias(false);
    tiling.SetBufferSpace(-1, -1, -1, -1);
    tiling.SetMatmulConfigParams({1, false, ScheduleType::OUTER_PRODUCT, MatrixTraverse::FIRSTM});
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, TestMatmulApiTilingL0DBError)
{
    matmul_tiling::PlatformInfo plat {.socVersion = platform_ascendc::SocVersion::ASCEND910B, .l1Size = 524288,
        .l0CSize = 131072, .ubSize = 196608, .l0ASize = 65536, .l0BSize = 65536};
    MatmulApiTiling tiling(plat);
    tiling.SetAType(TPosition::TSCM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(16, 16, 2048);
    tiling.SetOrgShape(16, 16, 2048);
    tiling.SetFixSplit(16, 16, -1);
    tiling.SetBias(false);
    tiling.SetBufferSpace(-1, -1, -1, -1);
    tiling.SetMatmulConfigParams({1, false, ScheduleType::OUTER_PRODUCT, MatrixTraverse::FIRSTN});
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, -1);
}

TEST_F(TestTiling, TestMatmulApiTilingNormL0DB)
{
    matmul_tiling::PlatformInfo plat {.socVersion = platform_ascendc::SocVersion::ASCEND910B, .l1Size = 524288,
        .l0CSize = 131072, .ubSize = 196608, .l0ASize = 65536, .l0BSize = 65536};
    MatmulApiTiling tiling(plat);
    tiling.SetAType(TPosition::TSCM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(20480, 2048, 16);
    tiling.SetOrgShape(20480, 2048, 16);
    tiling.EnableBias(false);
    tiling.SetBufferSpace(-1, -1, -1, -1);
    tiling.SetMatmulConfigParams({0, false, ScheduleType::OUTER_PRODUCT, MatrixTraverse::FIRSTN});
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, TestMatmulApiTilingBmmL0DBError)
{
    matmul_tiling::PlatformInfo plat {.socVersion = platform_ascendc::SocVersion::ASCEND910B, .l1Size = 524288,
        .l0CSize = 131072, .ubSize = 196608, .l0ASize = 65536, .l0BSize = 65536};
    MatmulApiTiling tiling(plat);
    tiling.SetAType(TPosition::TSCM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(16, 16, 2048);
    tiling.SetOrgShape(16, 16, 2048);
    tiling.SetFixSplit(16, 16, -1);
    tiling.EnableBias(false);
    tiling.SetBufferSpace(-1, -1, -1, -1);
    tiling.SetMatmulConfigParams({0, false, ScheduleType::OUTER_PRODUCT, MatrixTraverse::FIRSTN});
    tiling.SetBatchNum(2);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, -1);
}

TEST_F(TestTiling, TestInt4BaseK)
{
    matmul_tiling::PlatformInfo plat {.socVersion = platform_ascendc::SocVersion::ASCEND910B, .l1Size = 524288,
        .l0CSize = 131072, .ubSize = 196608, .l0ASize = 65536, .l0BSize = 65536};
    MatmulApiTiling tiling(plat);
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT4);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT4);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT32);
    tiling.SetBias(false);
    tiling.SetShape(144, 256, 32);
    tiling.SetOrgShape(144, 256, 32);
    tiling.SetBufferSpace(256 * 1024, 128 * 1024, -1);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(tilingData.get_baseK() % 64, 0);
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, Tiling_310p_NotAligned)
{
    matmul_tiling::PlatformInfo plat {.socVersion = platform_ascendc::SocVersion::ASCEND310P, .l1Size = 1048576,
        .l0CSize = 262144, .ubSize = 262144, .l0ASize = 65536, .l0BSize = 65536};
    matmul_tiling::MultiCoreMatmulTiling rnnMatmul3(plat);
    rnnMatmul3.SetAType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::VECTOR,
        matmul_tiling::DataType ::DT_FLOAT16, false);
    rnnMatmul3.SetBType(matmul_tiling::TPosition::VECCALC, matmul_tiling::CubeFormat::NZ,
        matmul_tiling::DataType ::DT_FLOAT16, true);
    rnnMatmul3.SetCType(matmul_tiling::TPosition::VECCALC, matmul_tiling::CubeFormat::ND,
        matmul_tiling::DataType ::DT_FLOAT16);
    auto ret = rnnMatmul3.SetBias(false);
    ret = rnnMatmul3.SetOrgShape(1, 494, 128);
    ret = rnnMatmul3.SetShape(1, 494, 128);
    ret = rnnMatmul3.SetBufferSpace(1046528, 262144); // will use all buffer space if not explicitly specified
    ret = rnnMatmul3.SetFixSplit(16, 256, 16);
    rnnMatmul3.SetTraverse(matmul_tiling::MatrixTraverse::FIRSTN);
    optiling::TCubeTiling tilingData;
    ret = rnnMatmul3.GetTiling(tilingData);
    rnnMatmul3.PrintTilingData();
    EXPECT_EQ(ret, -1);
}

TEST_F(TestTiling, Tiling_BatchMatmul)
{
    matmul_tiling::BatchMatmulTiling bmm;
    optiling::TCubeTiling tilingData;
    int ret = bmm.GetTiling(tilingData);
    bmm.PrintTilingData();
    matmul_tiling::SysTilingTempBufSize bufSize;
    BatchMatmulGetTmpBufSize(tilingData, bufSize);
    EXPECT_EQ(ret, -1);


    bmm.bufferPool_.l1Size = 256;
    bmm.bufferPool_.l0CSize = 256;
    bmm.bufferPool_.ubSize = 256;
    bmm.bufferPool_.btSize = 256;
    ret = bmm.SetBufferSpace(16, -16, 16, 16);
    EXPECT_EQ(ret, -1);
    ret = bmm.SetBufferSpace(16, 16, -16, 16);
    EXPECT_EQ(ret, -1);
    ret = bmm.SetBufferSpace(16, 16, 16, -16);
    EXPECT_EQ(ret, -1);

    bmm.bufferPool_.l1Size = 256;
    bmm.bufferPool_.l0CSize = 256;
    bmm.bufferPool_.ubSize = 256;
    bmm.bufferPool_.btSize = 256;
    bmm.bufferPool_.l0ASize = 256;
    bmm.bufferPool_.l0BSize = 256;
    bmm.bufferPool_.l0CSize = 256;
    bmm.aType_.dataType = matmul_tiling::DataType::DT_INT8;
    bmm.bType_.dataType = matmul_tiling::DataType::DT_INT8;
    bmm.cType_.dataType = matmul_tiling::DataType::DT_INT8;
    bmm.baseM = 256;
    bmm.baseK = 256;
    bool retParam = bmm.CheckSetParam();
    EXPECT_EQ(retParam, false);

    bmm.baseM = 1;
    bmm.baseK = 16;
    bmm.baseN = 256;
    retParam = bmm.CheckSetParam();
    EXPECT_EQ(retParam, false);

    bmm.baseM = 16;
    bmm.baseK = 1;
    bmm.baseN = 32;
    retParam = bmm.CheckSetParam();
    EXPECT_EQ(retParam, false);

    bmm.socVersion = platform_ascendc::SocVersion::ASCEND910;
    bmm.isBias = true;
    bmm.biasType_.pos = TPosition::TSCM;
    retParam = bmm.CheckSetParam();
    EXPECT_EQ(retParam, false);

    EXPECT_EQ(bmm.SetSingleBatch(2, 2), 0);
}

TEST_F(TestTiling, Tiling_BatchMatmulWithCppStruct)
{
    matmul_tiling::BatchMatmulTiling bmm;
    TCubeTiling tilingData;
    int ret = bmm.GetTiling(tilingData);
    bmm.PrintTilingData();
    matmul_tiling::SysTilingTempBufSize bufSize;
    EXPECT_EQ(BatchMatmulGetTmpBufSizeV2(tilingData, bufSize), 0);
    EXPECT_EQ(ret, -1);
}

TEST_F(TestTiling, ATscmCase)
{
    fe::PlatFormInfos platform_info;
    auto plat = platform_ascendc::PlatformAscendC(&platform_info);
    MatmulApiTiling tiling(plat);
    tiling.SetAType(TPosition::TSCM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(32, 4096, 80);
    tiling.SetOrgShape(32, 4096, 80);
    tiling.SetBias(true);
    tiling.SetBufferSpace(256 * 1024, -1, -1);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, BmmFailedCase)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::TSCM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(32, 4096, 80);
    tiling.SetOrgShape(32, 4096, 80);
    tiling.SetALayout(1, 32, 2, 3, 80);
    tiling.SetBLayout(1, 4096, 2, 3, 80);
    tiling.SetCLayout(1, 32, 2, 3, 4096);
    tiling.SetBatchNum(2);
    tiling.SetBias(true);
    tiling.SetBufferSpace(256 * 1024, -1, -1);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, -1);
}

TEST_F(TestTiling, BmmNormalFailedCase)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::TSCM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBatchInfoForNormal(2, 2, 32, 4096, 80);
    tiling.SetBias(true);
    tiling.SetBufferSpace(256 * 1024, -1, -1);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, -1);
}

TEST_F(TestTiling, BTscmCase)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::TSCM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(512, 32, 512);
    tiling.SetOrgShape(512, 32, 512);
    tiling.SetBias(true);
    tiling.SetBufferSpace(512 * 1024 - 64 - 32 * 512 * 2, -1, -1);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, ATscmBTscmCase)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::TSCM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::TSCM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(1024, 72, 1024);
    tiling.SetOrgShape(1024, 72, 1024);
    tiling.SetBias(true);
    tiling.SetBufferSpace(-1, -1, -1);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, ATransBTransmCase)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::TSCM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16, true);
    tiling.SetBType(TPosition::TSCM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16, true);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(1023, 72, 1023);
    tiling.SetOrgShape(1023, 72, 1023);
    tiling.SetBias(true);
    tiling.SetBufferSpace(-1, -1, -1);
    tiling.SetMatmulConfigParams(0);
    tiling.socVersion = platform_ascendc::SocVersion::ASCEND310P;
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, L1CacheUBCase01NoCache)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(1024, 10240, 1280);
    tiling.SetOrgShape(1024, 10240, 1280);
    tiling.SetBias(true);
    tiling.socVersion = platform_ascendc::SocVersion::ASCEND310P;
    tiling.SetMatmulConfigParams(1, true);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
    EXPECT_EQ(tilingData.get_depthAL1CacheUB(), 0);
    EXPECT_EQ(tilingData.get_depthBL1CacheUB(), 0);
}

TEST_F(TestTiling, L1CacheUBCase02NeiABFullLoad)
{
    matmul_tiling::PlatformInfo plat {.socVersion = platform_ascendc::SocVersion::ASCEND310P, .l1Size = 512 * 1024 - 256,
        .l0CSize = 128 * 1024, .ubSize = 192 * 1024 - 256, .l0ASize = 64 * 1024, .l0BSize = 64 * 1024};
    MatmulApiTiling tiling(plat);
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(4096, 2560, 320);
    tiling.SetOrgShape(4096, 2560, 320);
    tiling.SetBias(true);
    tiling.SetMatmulConfigParams(1, true);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
    EXPECT_EQ(tilingData.get_depthAL1CacheUB(), 0);
    EXPECT_EQ(tilingData.get_depthBL1CacheUB(), 3);
}

TEST_F(TestTiling, L1CacheUBCase03BothABFullLoad)
{
    matmul_tiling::PlatformInfo plat {.socVersion = platform_ascendc::SocVersion::ASCEND310P, .l1Size = 512 * 1024 - 256,
        .l0CSize = 128 * 1024, .ubSize = 192 * 1024 - 256, .l0ASize = 64 * 1024, .l0BSize = 64 * 1024};
    MatmulApiTiling tiling(plat);
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(16, 16, 2);
    tiling.SetOrgShape(16, 16, 2);
    tiling.SetBias(true);
    tiling.SetMatmulConfigParams(1, true);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
    EXPECT_EQ(tilingData.get_depthAL1CacheUB(), 1);
    EXPECT_EQ(tilingData.get_depthBL1CacheUB(), 1);
}

TEST_F(TestTiling, L1CacheUBCase04OnlyAFullLoad)
{
    matmul_tiling::PlatformInfo plat {.socVersion = platform_ascendc::SocVersion::ASCEND310P, .l1Size = 512 * 1024 - 256,
        .l0CSize = 128 * 1024, .ubSize = 192 * 1024 - 256, .l0ASize = 64 * 1024, .l0BSize = 64 * 1024};
    MatmulApiTiling tiling(plat);
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(16, 12288, 16);
    tiling.SetOrgShape(16, 12288, 16);
    tiling.SetBias(true);
    tiling.socVersion = platform_ascendc::SocVersion::ASCEND310P;
    tiling.SetMatmulConfigParams(1, true);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
    EXPECT_EQ(tilingData.get_depthAL1CacheUB(), 1);
}

TEST_F(TestTiling, L1CacheUBCase04OnlyBFullLoad)
{
    matmul_tiling::PlatformInfo plat {.socVersion = platform_ascendc::SocVersion::ASCEND310P, .l1Size = 512 * 1024 - 256,
        .l0CSize = 128 * 1024, .ubSize = 192 * 1024 - 256, .l0ASize = 64 * 1024, .l0BSize = 64 * 1024};
    MatmulApiTiling tiling(plat);
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(12288, 16, 16);
    tiling.SetOrgShape(12288, 16, 16);
    tiling.SetBias(true);
    tiling.SetMatmulConfigParams(1, true);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
    EXPECT_EQ(tilingData.get_depthBL1CacheUB(), 1);
}

TEST_F(TestTiling, L1CacheUBCase05BothCache)
{
    matmul_tiling::PlatformInfo plat {.socVersion = platform_ascendc::SocVersion::ASCEND310P, .l1Size = 512 * 1024 - 256,
        .l0CSize = 128 * 1024, .ubSize = 192 * 1024 - 256, .l0ASize = 64 * 1024, .l0BSize = 64 * 1024};
    MultiCoreMatmulTiling tiling(plat);
    tiling.SetDim(8);
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetOrgShape(1024, 5120, 640);
    tiling.SetShape(1024, 5120, 640);
    tiling.SetBias(true);
    tiling.SetMatmulConfigParams(1, true);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
    EXPECT_EQ(tilingData.get_depthAL1CacheUB(), 1);
    EXPECT_EQ(tilingData.get_depthBL1CacheUB(), 2);
}

TEST_F(TestTiling, L1CacheUBCase06AMatIsTSCM)
{
    matmul_tiling::PlatformInfo plat {.socVersion = platform_ascendc::SocVersion::ASCEND310P, .l1Size = 512 * 1024 - 256,
        .l0CSize = 128 * 1024, .ubSize = 192 * 1024 - 256, .l0ASize = 64 * 1024, .l0BSize = 64 * 1024};
    MatmulApiTiling tiling(plat);
    tiling.SetAType(TPosition::TSCM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(16, 64, 4096);
    tiling.SetOrgShape(16, 64, 4096);
    tiling.SetBias(true);
    tiling.SetMatmulConfigParams(1, true);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
    EXPECT_EQ(tilingData.get_depthAL1CacheUB(), 0);
    EXPECT_EQ(tilingData.get_depthBL1CacheUB(), 1);
}

TEST_F(TestTiling, L1CacheUBCase07BMatIsTSCM)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::TSCM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(64, 16, 4096);
    tiling.SetOrgShape(64, 16, 4096);
    tiling.SetBias(true);
    tiling.socVersion = platform_ascendc::SocVersion::ASCEND310P;
    tiling.SetMatmulConfigParams(1, true);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
    EXPECT_EQ(tilingData.get_depthAL1CacheUB(), 0);
    EXPECT_EQ(tilingData.get_depthBL1CacheUB(), 0);
}

TEST_F(TestTiling, L1CacheUBCase08MultiCore)
{
    MultiCoreMatmulTiling tiling;
    tiling.SetDim(8);
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetOrgShape(4096, 2560, 320);
    tiling.SetShape(4096, 2560, 320);
    tiling.SetBias(true);
    tiling.socVersion = platform_ascendc::SocVersion::ASCEND310P;
    tiling.SetMatmulConfigParams(1, true);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, ATscmFP32Case)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::TSCM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(128, 8, 8192);
    tiling.SetOrgShape(128, 8, 8192);
    tiling.SetBias(true);
    tiling.SetBufferSpace(512 * 1024 - 64 - 128 * 16 * 4, -1, -1);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, FP32Case)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(2048, 40, 2048);
    tiling.SetOrgShape(2048, 40, 2048);
    tiling.SetBias(true);
    tiling.SetBufferSpace(-1, -1, -1);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, FP32Case2)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT, true);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT, true);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(2048, 40, 2048);
    tiling.SetOrgShape(2048, 40, 2048);
    tiling.SetBias(true);
    tiling.SetBufferSpace(-1, -1, -1);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, BothFullLoadCase)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(32, 64, 128);
    tiling.SetOrgShape(32, 64, 128);
    tiling.SetBias(true);
    tiling.SetBufferSpace(-1, -1, -1);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, AFullLoadCase)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(32, 512000, 128);
    tiling.SetOrgShape(32, 512000, 128);
    tiling.SetBias(true);
    tiling.SetBufferSpace(-1, -1, -1);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, BFullLoadCase)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(102400, 32, 128);
    tiling.SetOrgShape(102400, 32, 128);
    tiling.SetBias(true);
    tiling.SetBufferSpace(-1, -1, -1);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, BothNotFullLoadCase)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(512, 1024, 128000);
    tiling.SetOrgShape(512, 1024, 128000);
    tiling.SetBias(true);
    tiling.SetBufferSpace(-1, -1, -1);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, TestMatmulApiTilngCase1)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(1024, 1024, 1024);
    tiling.SetOrgShape(1024, 1024, 1024);
    tiling.SetBias(true);
    tiling.SetBufferSpace(-1, 128 * 1024, -1);
    tiling.SetDoubleBuffer(false, false, false, false, false, false);
    tiling.SetTraverse(MatrixTraverse::FIRSTM);
    tiling.SetMadType(MatrixMadType::NORMAL);
    tiling.SetSplitRange(128, 128, 128);
    tiling.SetFixSplit(16, 16, 16);
    optiling::TCubeTiling tilingData;
    tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(tilingData.get_baseM(), 16);
}

TEST_F(TestTiling, TestMatmulApiTilngCase2)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(64, 64, 64);
    tiling.SetOrgShape(64, 64, 64);
    tiling.SetBias(false);
    tiling.SetBufferSpace(-1, 128 * 1024, -1);
    tiling.SetDoubleBuffer(true, true, true, true, true, true);
    tiling.SetTraverse(MatrixTraverse::FIRSTM);
    tiling.SetMadType(MatrixMadType::NORMAL);

    optiling::TCubeTiling tilingData;
    tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(tilingData.get_baseM(), 64);
}

TEST_F(TestTiling, TestMatmulApiTilngCase3)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(2048, 64, 64);
    tiling.SetOrgShape(2048, 64, 64);
    tiling.SetBias(false);
    tiling.SetBufferSpace(-1, 128 * 1024, -1);
    tiling.SetDoubleBuffer(true, true, true, true, true, true);
    tiling.SetMadType(MatrixMadType::NORMAL);

    optiling::TCubeTiling tilingData;
    tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(tilingData.get_baseM(), 256);
}

TEST_F(TestTiling, TestMatmulApiTilngCase4)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(64, 2048, 64);
    tiling.SetOrgShape(64, 2048, 64);
    tiling.SetBias(false);
    tiling.SetBufferSpace(-1, 128 * 1024, -1);
    tiling.SetDoubleBuffer(true, true, true, true, true, true);
    tiling.SetTraverse(MatrixTraverse::FIRSTM);
    tiling.SetMadType(MatrixMadType::NORMAL);

    optiling::TCubeTiling tilingData;
    tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(tilingData.get_baseM(), 64);
}

TEST_F(TestTiling, TestMatmulApiTilngCase5)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(1024, 1024, 1024);
    tiling.SetOrgShape(1024, 1024, 1024);
    tiling.SetBias(true);
    tiling.SetBufferSpace(-1, 128 * 1024, -1);
    tiling.SetDoubleBuffer(false, false, false, false, false, false);
    tiling.SetTraverse(MatrixTraverse::FIRSTM);
    tiling.SetMadType(MatrixMadType::NORMAL);
    tiling.SetFixSplit(64, -1, -1);

    optiling::TCubeTiling tilingData;
    tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(tilingData.get_baseM(), 64);

    tiling.SetFixSplit(-1, 16, -1);
    tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(tilingData.get_baseN(), 16);
}

TEST_F(TestTiling, TestMatmulApiTilngCase6)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(1024, 1024, 1024);
    tiling.SetOrgShape(1024, 1024, 1024);
    tiling.SetBias(true);
    tiling.SetBufferSpace(-1, 128 * 1024, -1);
    tiling.SetDoubleBuffer(false, false, false, false, false, false);
    tiling.SetTraverse(MatrixTraverse::FIRSTM);
    tiling.SetMadType(MatrixMadType::NORMAL);
    tiling.SetFixSplit(-1, 64, -1);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, TestMatmulApiTilngWithCppStruct)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(1024, 1024, 1024);
    tiling.SetOrgShape(1024, 1024, 1024);
    tiling.EnableBias(true);
    tiling.SetBufferSpace(-1, 128 * 1024, -1);
    tiling.SetDoubleBuffer(false, false, false, false, false, false);
    tiling.SetTraverse(MatrixTraverse::FIRSTM);
    tiling.SetMadType(MatrixMadType::NORMAL);
    tiling.SetFixSplit(-1, 64, -1);
    TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
    matmul_tiling::SysTilingTempBufSize bufSize;
    EXPECT_EQ(MatmulGetTmpBufSizeV2(tilingData, bufSize), 0);
}

TEST_F(TestTiling, TestMatmulApiTilng310B)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(1024, 1024, 1024);
    tiling.SetOrgShape(1024, 1024, 1024);
    tiling.SetBias(true);
    tiling.SetBufferSpace(-1, 128 * 1024, -1);
    tiling.SetDoubleBuffer(false, false, false, false, false, false);
    tiling.SetTraverse(MatrixTraverse::FIRSTM);
    tiling.SetMadType(MatrixMadType::NORMAL);
    tiling.SetFixSplit(-1, 64, -1);
    tiling.SetMatmulConfigParams(0);
    tiling.socVersion = platform_ascendc::SocVersion::ASCEND310B;
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, TestDiffKaKbMatmulApiTilng)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(1024, 1024, 1024);
    tiling.SetOrgShape(1024, 1024, 1024, 1280);
    tiling.SetBias(true);
    tiling.SetBufferSpace(-1, 128 * 1024, -1);
    tiling.SetDoubleBuffer(false, false, false, false, false, false);
    tiling.SetTraverse(MatrixTraverse::FIRSTM);
    tiling.SetMadType(MatrixMadType::NORMAL);
    tiling.SetFixSplit(64, -1, -1);

    optiling::TCubeTiling tilingData;
    tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(tilingData.get_baseM(), 64);

    tiling.SetFixSplit(-1, 16, -1);
    tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(tilingData.get_baseN(), 16);

    EXPECT_EQ(tilingData.get_Ka(), 1024);
    EXPECT_EQ(tilingData.get_Kb(), 1280);
}

TEST_F(TestTiling, TestMatmulApiTilngIteratorOrder)
{
    SingleCoreStatus status;
    MatmulApiTiling tiling;
    tiling.SetShape(128, 128, 128);
    tiling.SetOrgShape(128, 128, 128);
    status.l1Status.kAL1 = 1;
    status.l1Status.kBL1 = 1;

    MatmulTilingAlgorithm algoIns(&tiling);

    int32_t ret = algoIns.GetIteratorOrder(status, 128, 128, 128);
    EXPECT_EQ(ret, 0);

    status.l1Status.kAL1 = 8;
    status.l1Status.kBL1 = 1;
    ret = algoIns.GetIteratorOrder(status, 128, 128, 128);
    EXPECT_EQ(ret, 0);

    status.l1Status.kAL1 = 1;
    status.l1Status.kBL1 = 8;
    ret = algoIns.GetIteratorOrder(status, 128, 128, 128);
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, TestSetBufferSpace)
{
    MatmulApiTiling tiling;
    tiling.SetBufferSpace(1024);
    EXPECT_EQ(tiling.bufferPool_.l1Size, 1024);
}

TEST_F(TestTiling, TestSoftMaxTiling)
{
    std::vector<int64_t> shapeDims = { 128, 128 };
    optiling::SoftMaxTiling tilingData;
    auto softmaxShape = ge::Shape(shapeDims);
    uint32_t softmaxTmpSize = 100 * 1024 * 4;
    uint32_t softmaxNeedMinSize = GetSoftMaxMinTmpSize(softmaxShape, 2, true);
    EXPECT_EQ(softmaxNeedMinSize, (16 + 128 + 64) * 4);
    uint32_t softmaxFlashNeedMinSize = GetSoftMaxFlashMinTmpSize(softmaxShape, 2, true, true);
    EXPECT_EQ(softmaxFlashNeedMinSize, (16 * 4 + 128 * 2) * 4);
    softmaxFlashNeedMinSize = GetSoftMaxFlashMinTmpSize(softmaxShape, 4, true, true);
    EXPECT_EQ(softmaxFlashNeedMinSize, (8 * 4 + 128 * 2) * 4);
    softmaxFlashNeedMinSize = GetSoftMaxFlashMinTmpSize(softmaxShape, 4, false, true);
    EXPECT_EQ(softmaxFlashNeedMinSize, (8 + 128 + 64) * 4);
    uint32_t softmaxGradNeedMinSize = GetSoftMaxGradMinTmpSize(softmaxShape, 2, true, true);
    EXPECT_EQ(softmaxGradNeedMinSize, (16 * 2 + 128 * 3 + 64) * 4);
    softmaxGradNeedMinSize = GetSoftMaxGradMinTmpSize(softmaxShape, 4, true, true);
    EXPECT_EQ(softmaxGradNeedMinSize, (8 + 128 + 64) * 4);

    uint32_t softmaxNeedMaxSize = GetSoftMaxMaxTmpSize(softmaxShape, 2, true);
    EXPECT_EQ(softmaxNeedMaxSize, 128 * (16 + 128 + 64) * 4);
    uint32_t softmaxFlashNeedMaxSize = GetSoftMaxFlashMaxTmpSize(softmaxShape, 2, true, true);
    EXPECT_EQ(softmaxFlashNeedMaxSize, 128 * (16 * 4 + 128 * 2) * 4);
    softmaxFlashNeedMaxSize = GetSoftMaxFlashMaxTmpSize(softmaxShape, 4, false, true);
    EXPECT_EQ(softmaxFlashNeedMaxSize, 128 * (8 + 128 + 64) * 4);
    softmaxFlashNeedMaxSize = GetSoftMaxFlashMaxTmpSize(softmaxShape, 4, true, true);
    EXPECT_EQ(softmaxFlashNeedMaxSize, 128 * (8 * 4 + 128 * 2) * 4);
    uint32_t softmaxGradNeedMaxSize = GetSoftMaxGradMaxTmpSize(softmaxShape, 2, true, true);
    EXPECT_EQ(softmaxGradNeedMaxSize, 128 * (16 * 2 + 128 * 3 + 64) * 4);
    softmaxGradNeedMaxSize = GetSoftMaxGradMaxTmpSize(softmaxShape, 4, true, true);
    EXPECT_EQ(softmaxGradNeedMaxSize, 128 * (8 + 128 + 64) * 4);

    SoftMaxTilingFunc(softmaxShape, 2, softmaxTmpSize, tilingData);
    EXPECT_EQ(tilingData.get_reduceM(), 64);
    bool flag = IsBasicBlockInSoftMax(tilingData);
    EXPECT_EQ(flag, true);
    SoftMaxFlashTilingFunc(softmaxShape, 2, 77952, tilingData, true);
    EXPECT_EQ(tilingData.get_reduceM(), 32);
    SoftMaxFlashTilingFunc(softmaxShape, 2, 77952, tilingData, false);
    EXPECT_EQ(tilingData.get_reduceM(), 64);
    SoftMaxGradTilingFunc(softmaxShape, 2, softmaxTmpSize, tilingData, false);
    EXPECT_EQ(tilingData.get_reduceM(), 64);
    SoftMaxGradTilingFunc(softmaxShape, 4, softmaxTmpSize, tilingData, false);
    EXPECT_EQ(tilingData.get_reduceM(), 64);
    SoftMaxGradTilingFunc(softmaxShape, 2, 133120, tilingData, true);
    EXPECT_EQ(tilingData.get_reduceM(), 64);
}

TEST_F(TestTiling, TestLogSoftMaxTiling)
{
    std::vector<int64_t> shapeDims = { 128, 128 };
    optiling::LogSoftMaxTiling tilingData;
    auto softmaxShape = ge::Shape(shapeDims);
    uint32_t softmaxTmpSize = 100 * 1024 * 4;
    uint32_t softmaxNeedMinSize = GetLogSoftMaxMinTmpSize(softmaxShape, 2, true);
    uint32_t softmaxNeedMaxSize = GetLogSoftMaxMaxTmpSize(softmaxShape, 2, true);

    LogSoftMaxTilingFunc(softmaxShape, 2, softmaxTmpSize, tilingData);
    EXPECT_EQ(tilingData.get_reduceM(), 64);
}

TEST_F(TestTiling, TestSoftMaxFlashV2TilingMaxMinTmpSize)
{
    uint32_t softmaxflashV2NeedMinLength = 0;
    uint32_t softmaxflashV2NeedMaxLength = 0;

    std::vector<int64_t> shapeDims = { 3, 3, 448 };
    auto softmaxShape = ge::Shape(shapeDims);
    uint32_t dataTypeSize1 = 2;
    uint32_t dataTypeSize2 = 2;
    uint32_t isUpdate = 0;
    uint32_t isBasicBlock = 0;
    uint32_t isFlashOutputBrc = 1;

    softmaxflashV2NeedMinLength = GetSoftMaxFlashV2MinTmpSize(softmaxShape, dataTypeSize1, dataTypeSize2, isUpdate, isBasicBlock, isFlashOutputBrc);
    EXPECT_EQ(softmaxflashV2NeedMinLength, 19584);

    softmaxflashV2NeedMaxLength = GetSoftMaxFlashV2MaxTmpSize(softmaxShape, dataTypeSize1, dataTypeSize2, isUpdate, isBasicBlock, isFlashOutputBrc);
    EXPECT_EQ(softmaxflashV2NeedMaxLength, 19584);

    shapeDims = {7, 1072};
    softmaxShape = ge::Shape(shapeDims);
    dataTypeSize1 = 2;
    dataTypeSize2 = 2;
    isUpdate = 0;
    isBasicBlock = 0;
    isFlashOutputBrc = 1;

    softmaxflashV2NeedMinLength = GetSoftMaxFlashV2MinTmpSize(softmaxShape, dataTypeSize1, dataTypeSize2, isUpdate, isBasicBlock, isFlashOutputBrc);
    EXPECT_EQ(softmaxflashV2NeedMinLength, 32704);

    softmaxflashV2NeedMaxLength = GetSoftMaxFlashV2MaxTmpSize(softmaxShape, dataTypeSize1, dataTypeSize2, isUpdate, isBasicBlock, isFlashOutputBrc);
    EXPECT_EQ(softmaxflashV2NeedMaxLength, 32704);

    shapeDims = {1, 2, 3, 1, 2, 1, 16};
    softmaxShape = ge::Shape(shapeDims);
    dataTypeSize1 = 2;
    dataTypeSize2 = 2;
    isUpdate = 0;
    isBasicBlock = 0;
    isFlashOutputBrc = 1;

    softmaxflashV2NeedMinLength = GetSoftMaxFlashV2MinTmpSize(softmaxShape, dataTypeSize1, dataTypeSize2, isUpdate, isBasicBlock, isFlashOutputBrc);
    EXPECT_EQ(softmaxflashV2NeedMinLength, 5376);

    softmaxflashV2NeedMaxLength = GetSoftMaxFlashV2MaxTmpSize(softmaxShape, dataTypeSize1, dataTypeSize2, isUpdate, isBasicBlock, isFlashOutputBrc);
    EXPECT_EQ(softmaxflashV2NeedMaxLength, 5376);

    shapeDims = {2, 6, 1, 16};
    softmaxShape = ge::Shape(shapeDims);
    dataTypeSize1 = 2;
    dataTypeSize2 = 2;
    isUpdate = 0;
    isBasicBlock = 0;
    isFlashOutputBrc = 1;

    softmaxflashV2NeedMinLength = GetSoftMaxFlashV2MinTmpSize(softmaxShape, dataTypeSize1, dataTypeSize2, isUpdate, isBasicBlock, isFlashOutputBrc);
    EXPECT_EQ(softmaxflashV2NeedMinLength, 5376);

    softmaxflashV2NeedMaxLength = GetSoftMaxFlashV2MaxTmpSize(softmaxShape, dataTypeSize1, dataTypeSize2, isUpdate, isBasicBlock, isFlashOutputBrc);
    EXPECT_EQ(softmaxflashV2NeedMaxLength, 5376);

    shapeDims = {6, 1664};
    softmaxShape = ge::Shape(shapeDims);
    dataTypeSize1 = 2;
    dataTypeSize2 = 2;
    isUpdate = 0;
    isBasicBlock = 0;
    isFlashOutputBrc = 1;

    softmaxflashV2NeedMinLength = GetSoftMaxFlashV2MinTmpSize(softmaxShape, dataTypeSize1, dataTypeSize2, isUpdate, isBasicBlock, isFlashOutputBrc);
    EXPECT_EQ(softmaxflashV2NeedMinLength, 42240);

    softmaxflashV2NeedMaxLength = GetSoftMaxFlashV2MaxTmpSize(softmaxShape, dataTypeSize1, dataTypeSize2, isUpdate, isBasicBlock, isFlashOutputBrc);
    EXPECT_EQ(softmaxflashV2NeedMaxLength, 42240);

    shapeDims = {2, 1760 };
    softmaxShape = ge::Shape(shapeDims);
    dataTypeSize1 = 2;
    dataTypeSize2 = 2;
    isUpdate = 0;
    isBasicBlock = 0;
    isFlashOutputBrc = 1;

    softmaxflashV2NeedMinLength = GetSoftMaxFlashV2MinTmpSize(softmaxShape, dataTypeSize1, dataTypeSize2, isUpdate, isBasicBlock, isFlashOutputBrc);
    EXPECT_EQ(softmaxflashV2NeedMinLength, 14848);

    softmaxflashV2NeedMaxLength = GetSoftMaxFlashV2MaxTmpSize(softmaxShape, dataTypeSize1, dataTypeSize2, isUpdate, isBasicBlock, isFlashOutputBrc);
    EXPECT_EQ(softmaxflashV2NeedMaxLength, 14848);

    shapeDims = {1, 5536 };
    softmaxShape = ge::Shape(shapeDims);
    dataTypeSize1 = 2;
    dataTypeSize2 = 2;
    isUpdate = 0;
    isBasicBlock = 0;
    isFlashOutputBrc = 1;

    softmaxflashV2NeedMinLength = GetSoftMaxFlashV2MinTmpSize(softmaxShape, dataTypeSize1, dataTypeSize2, isUpdate, isBasicBlock, isFlashOutputBrc);
    EXPECT_EQ(softmaxflashV2NeedMinLength, 22528);

    softmaxflashV2NeedMaxLength = GetSoftMaxFlashV2MaxTmpSize(softmaxShape, dataTypeSize1, dataTypeSize2, isUpdate, isBasicBlock, isFlashOutputBrc);
    EXPECT_EQ(softmaxflashV2NeedMaxLength, 22528);

    shapeDims = {2, 2, 2352};
    softmaxShape = ge::Shape(shapeDims);
    dataTypeSize1 = 2;
    dataTypeSize2 = 2;
    isUpdate = 0;
    isBasicBlock = 0;
    isFlashOutputBrc = 1;

    softmaxflashV2NeedMinLength = GetSoftMaxFlashV2MinTmpSize(softmaxShape, dataTypeSize1, dataTypeSize2, isUpdate, isBasicBlock, isFlashOutputBrc);
    EXPECT_EQ(softmaxflashV2NeedMinLength, 39168);

    softmaxflashV2NeedMaxLength = GetSoftMaxFlashV2MaxTmpSize(softmaxShape, dataTypeSize1, dataTypeSize2, isUpdate, isBasicBlock, isFlashOutputBrc);
    EXPECT_EQ(softmaxflashV2NeedMaxLength, 39168);

    shapeDims = {2, 2, 2, 480 };
    softmaxShape = ge::Shape(shapeDims);
    dataTypeSize1 = 2;
    dataTypeSize2 = 2;
    isUpdate = 0;
    isBasicBlock = 0;
    isFlashOutputBrc = 1;

    softmaxflashV2NeedMinLength = GetSoftMaxFlashV2MinTmpSize(softmaxShape, dataTypeSize1, dataTypeSize2, isUpdate, isBasicBlock, isFlashOutputBrc);
    EXPECT_EQ(softmaxflashV2NeedMinLength, 18432);

    softmaxflashV2NeedMaxLength = GetSoftMaxFlashV2MaxTmpSize(softmaxShape, dataTypeSize1, dataTypeSize2, isUpdate, isBasicBlock, isFlashOutputBrc);
    EXPECT_EQ(softmaxflashV2NeedMaxLength, 18432);

    shapeDims = {2, 3632};
    softmaxShape = ge::Shape(shapeDims);
    dataTypeSize1 = 2;
    dataTypeSize2 = 2;
    isUpdate = 1;
    isBasicBlock = 0;
    isFlashOutputBrc = 1;

    softmaxflashV2NeedMinLength = GetSoftMaxFlashV2MinTmpSize(softmaxShape, dataTypeSize1, dataTypeSize2, isUpdate, isBasicBlock, isFlashOutputBrc);
    EXPECT_EQ(softmaxflashV2NeedMinLength, 29824);

    softmaxflashV2NeedMaxLength = GetSoftMaxFlashV2MaxTmpSize(softmaxShape, dataTypeSize1, dataTypeSize2, isUpdate, isBasicBlock, isFlashOutputBrc);
    EXPECT_EQ(softmaxflashV2NeedMaxLength, 29824);

    shapeDims = {2, 4, 96};
    softmaxShape = ge::Shape(shapeDims);
    dataTypeSize1 = 2;
    dataTypeSize2 = 2;
    isUpdate = 1;
    isBasicBlock = 0;
    isFlashOutputBrc = 1;

    softmaxflashV2NeedMinLength = GetSoftMaxFlashV2MinTmpSize(softmaxShape, dataTypeSize1, dataTypeSize2, isUpdate, isBasicBlock, isFlashOutputBrc);
    EXPECT_EQ(softmaxflashV2NeedMinLength, 6144);

    softmaxflashV2NeedMaxLength = GetSoftMaxFlashV2MaxTmpSize(softmaxShape, dataTypeSize1, dataTypeSize2, isUpdate, isBasicBlock, isFlashOutputBrc);
    EXPECT_EQ(softmaxflashV2NeedMaxLength, 6144);
}

TEST_F(TestTiling, TestSoftMaxFlashV2Tiling)
{
    std::vector<int64_t> shapeDims = { 128, 128 };
    optiling::SoftMaxTiling tilingData;
    auto softmaxShape = ge::Shape(shapeDims);
    uint32_t maxSumTypeSize = 2;
    uint32_t inputTypeSize = 2;
    uint32_t softmaxflashV2NeedMinLength =
        GetSoftMaxFlashV2MinTmpSize(softmaxShape, inputTypeSize, maxSumTypeSize, false, false);
    EXPECT_EQ(softmaxflashV2NeedMinLength, (128 + 64 + 16 * 2) * 4);
    softmaxflashV2NeedMinLength = GetSoftMaxFlashV2MinTmpSize(softmaxShape, inputTypeSize, maxSumTypeSize, true, false);
    EXPECT_EQ(softmaxflashV2NeedMinLength, (128 + 64 + 16 * 2) * 4);
    softmaxflashV2NeedMinLength = GetSoftMaxFlashV2MinTmpSize(softmaxShape, inputTypeSize, maxSumTypeSize, false, true);
    EXPECT_EQ(softmaxflashV2NeedMinLength, 8 * (128 + 64 + 16) * 4);

    // isFlashOutputBrc
    softmaxflashV2NeedMinLength = GetSoftMaxFlashV2MinTmpSize(softmaxShape, inputTypeSize, maxSumTypeSize, false, false, true);
    EXPECT_EQ(softmaxflashV2NeedMinLength, (128 + 64 + 16 * 2) * 4 * 16);
    softmaxflashV2NeedMinLength = GetSoftMaxFlashV2MinTmpSize(softmaxShape, inputTypeSize, maxSumTypeSize, true, false, true);
    EXPECT_EQ(softmaxflashV2NeedMinLength, (128 + 64 + 16 * 2) * 4 * 16);
    softmaxflashV2NeedMinLength = GetSoftMaxFlashV2MinTmpSize(softmaxShape, inputTypeSize, maxSumTypeSize, false, true, true);
    EXPECT_EQ(softmaxflashV2NeedMinLength, (128 + 64 + 16) * 4 * 16);

    uint32_t softmaxflashV2NeedMaxLength =
        GetSoftMaxFlashV2MaxTmpSize(softmaxShape, inputTypeSize, maxSumTypeSize, false, false);
    EXPECT_EQ(softmaxflashV2NeedMaxLength, 128 * (128 + 64 + 16 * 2) * 4);
    softmaxflashV2NeedMaxLength = GetSoftMaxFlashV2MaxTmpSize(softmaxShape, inputTypeSize, maxSumTypeSize, true, false);
    EXPECT_EQ(softmaxflashV2NeedMaxLength, 128 * (128 + 64 + 16 * 2) * 4);
    softmaxflashV2NeedMaxLength = GetSoftMaxFlashV2MaxTmpSize(softmaxShape, inputTypeSize, maxSumTypeSize, false, true);
    EXPECT_EQ(softmaxflashV2NeedMaxLength, 64 * (128 + 64 + 16) * 4);

    maxSumTypeSize = 4;
    softmaxflashV2NeedMinLength = GetSoftMaxFlashV2MinTmpSize(softmaxShape, inputTypeSize, maxSumTypeSize, true, false);
    EXPECT_EQ(softmaxflashV2NeedMinLength, (128 + 64 + 8 * 2) * 4);
    softmaxflashV2NeedMinLength = GetSoftMaxFlashV2MinTmpSize(softmaxShape, inputTypeSize, maxSumTypeSize, false, true);
    EXPECT_EQ(softmaxflashV2NeedMinLength, 8 * (128 + 64 + 8) * 4);

    // isFlashOutputBrc
    softmaxflashV2NeedMinLength = GetSoftMaxFlashV2MinTmpSize(softmaxShape, inputTypeSize, maxSumTypeSize, true, false, true);
    EXPECT_EQ(softmaxflashV2NeedMinLength, (128 + 64 + 8 * 2) * 4 * 16);
    softmaxflashV2NeedMinLength = GetSoftMaxFlashV2MinTmpSize(softmaxShape, inputTypeSize, maxSumTypeSize, false, true, true);
    EXPECT_EQ(softmaxflashV2NeedMinLength, (128 + 64 + 8) * 4 * 16);

    softmaxflashV2NeedMaxLength = GetSoftMaxFlashV2MaxTmpSize(softmaxShape, inputTypeSize, maxSumTypeSize, true, false);
    EXPECT_EQ(softmaxflashV2NeedMaxLength, 128 * (128 + 64 + 8 * 2) * 4);
    softmaxflashV2NeedMaxLength = GetSoftMaxFlashV2MaxTmpSize(softmaxShape, inputTypeSize, maxSumTypeSize, false, true);
    EXPECT_EQ(softmaxflashV2NeedMaxLength, 64 * (128 + 64 + 8) * 4);

    uint32_t workLength = 100 * 1024;
    SoftMaxFlashV2TilingFunc(softmaxShape, inputTypeSize, maxSumTypeSize, workLength, tilingData, false, false);
    EXPECT_EQ(tilingData.get_reduceM(), 120);
    SoftMaxFlashV2TilingFunc(softmaxShape, inputTypeSize, maxSumTypeSize, workLength, tilingData, false, true);
    EXPECT_EQ(tilingData.get_reduceM(), 64);
    SoftMaxFlashV2TilingFunc(softmaxShape, inputTypeSize, maxSumTypeSize, workLength, tilingData, true, false);
    EXPECT_EQ(tilingData.get_reduceM(), 120);
    SoftMaxFlashV2TilingFunc(softmaxShape, inputTypeSize, maxSumTypeSize, workLength, tilingData, true, true);
    EXPECT_EQ(tilingData.get_reduceM(), 64);

    // isFlashOutputBrc
    SoftMaxFlashV2TilingFunc(softmaxShape, inputTypeSize, maxSumTypeSize, workLength, tilingData, false, false, true);
    EXPECT_EQ(tilingData.get_reduceM(), 112);
    SoftMaxFlashV2TilingFunc(softmaxShape, inputTypeSize, maxSumTypeSize, workLength, tilingData, false, true, true);
    EXPECT_EQ(tilingData.get_reduceM(), 64);
    SoftMaxFlashV2TilingFunc(softmaxShape, inputTypeSize, maxSumTypeSize, workLength, tilingData, true, false, true);
    EXPECT_EQ(tilingData.get_reduceM(), 112);
    SoftMaxFlashV2TilingFunc(softmaxShape, inputTypeSize, maxSumTypeSize, workLength, tilingData, true, true, true);
    EXPECT_EQ(tilingData.get_reduceM(), 64);

    inputTypeSize = 4;
    softmaxflashV2NeedMinLength = GetSoftMaxFlashV2MinTmpSize(softmaxShape, inputTypeSize, maxSumTypeSize, true, true);
    EXPECT_EQ(softmaxflashV2NeedMinLength, 8 * (64 + 8) * 4);
    softmaxflashV2NeedMaxLength = GetSoftMaxFlashV2MaxTmpSize(softmaxShape, inputTypeSize, maxSumTypeSize, true, true);
    EXPECT_EQ(softmaxflashV2NeedMaxLength, 64 * (64 + 8) * 4);
    SoftMaxFlashV2TilingFunc(softmaxShape, inputTypeSize, maxSumTypeSize, workLength, tilingData, true, true);
    EXPECT_EQ(tilingData.get_reduceM(), 64);

    // isFlashOutputBrc
    softmaxflashV2NeedMinLength = GetSoftMaxFlashV2MinTmpSize(softmaxShape, inputTypeSize, maxSumTypeSize, true, true, true);
    EXPECT_EQ(softmaxflashV2NeedMinLength, (64 + 8) * 4 * 8);
    SoftMaxFlashV2TilingFunc(softmaxShape, inputTypeSize, maxSumTypeSize, workLength, tilingData, true, true, true);
    EXPECT_EQ(tilingData.get_reduceM(), 64);
}
TEST_F(TestTiling, TestSoftMaxFlashV2TilingBasicBlock)
{
    std::vector<int64_t> shapeDims = { 8, 1024 };
    optiling::SoftMaxTiling tilingData;
    auto softmaxShape = ge::Shape(shapeDims);
    uint32_t maxSumTypeSize = 4;
    uint32_t inputTypeSize = 4;
    uint32_t softmaxflashV2NeedMinLength =
        GetSoftMaxFlashV2MinTmpSize(softmaxShape, inputTypeSize, maxSumTypeSize, true, true);
    EXPECT_EQ(softmaxflashV2NeedMinLength, 8*(8 + 128) * 4);
    uint32_t softmaxflashV2NeedMaxLength =
        GetSoftMaxFlashV2MaxTmpSize(softmaxShape, inputTypeSize, maxSumTypeSize, true, true);
    EXPECT_EQ(softmaxflashV2NeedMaxLength, 8*(8 + 128) * 4);

    // isFlashOutputBrc
    softmaxflashV2NeedMinLength = GetSoftMaxFlashV2MinTmpSize(softmaxShape, inputTypeSize, maxSumTypeSize, true, true, true);
    EXPECT_EQ(softmaxflashV2NeedMinLength, (8 + 128) * 4 * 8);

    uint32_t workLength = 32 * 1024;
    SoftMaxFlashV2TilingFunc(softmaxShape, inputTypeSize, maxSumTypeSize, workLength, tilingData, true, true);
    EXPECT_EQ(tilingData.get_reduceM(), 8);

    inputTypeSize = 2;
    workLength = 64 * 1024;
    softmaxflashV2NeedMinLength = GetSoftMaxFlashV2MinTmpSize(softmaxShape, inputTypeSize, maxSumTypeSize, true, true);
    EXPECT_EQ(softmaxflashV2NeedMinLength, 8 * (8 + 1024 + 64 * 2) * 4);
    softmaxflashV2NeedMaxLength = GetSoftMaxFlashV2MaxTmpSize(softmaxShape, inputTypeSize, maxSumTypeSize, true, true);
    EXPECT_EQ(softmaxflashV2NeedMaxLength, 8 * (8 + 1024 + 64 * 2) * 4);
    SoftMaxFlashV2TilingFunc(softmaxShape, inputTypeSize, maxSumTypeSize, workLength, tilingData, true, true);
    EXPECT_EQ(tilingData.get_reduceM(), 8);

    // isFlashOutputBrc
    softmaxShape = ge::Shape({ 64, 512 });
    softmaxflashV2NeedMinLength = GetSoftMaxFlashV2MinTmpSize(softmaxShape, inputTypeSize, maxSumTypeSize, true, true, true);
    EXPECT_EQ(softmaxflashV2NeedMinLength, (8 + 512 + 64) * 4 * 16);
    SoftMaxFlashV2TilingFunc(softmaxShape, inputTypeSize, maxSumTypeSize, workLength, tilingData, true, true, true);
    EXPECT_EQ(tilingData.get_reduceM(), 16);
}

TEST_F(TestTiling, TestSoftMaxFlashV3Tiling)
{
    std::vector<int64_t> shapeDims = { 10, 1024 };
    optiling::SoftMaxTiling tilingData;
    auto softmaxShape = ge::Shape(shapeDims);
    uint32_t maxSumTypeSize = 4;
    uint32_t inputTypeSize = 2;
    uint32_t softmaxflashV3NeedMinLength = 0;
    uint32_t softmaxflashV3NeedMaxLength = 0;
    GetSoftMaxFlashV3MaxMinTmpSize(softmaxShape, inputTypeSize, maxSumTypeSize, softmaxflashV3NeedMaxLength,
        softmaxflashV3NeedMinLength, false, false);
    
    EXPECT_EQ(softmaxflashV3NeedMinLength, (8 * 5 + 2 * 1024 + 64) * 4);
    EXPECT_EQ(softmaxflashV3NeedMaxLength, 10 * (8 * 5 + 2 * 1024 + 64) * 4);

    softmaxflashV3NeedMinLength = 0;
    softmaxflashV3NeedMaxLength = 0;
    GetSoftMaxFlashV3MaxMinTmpSize(softmaxShape, inputTypeSize, 0, softmaxflashV3NeedMaxLength,
        softmaxflashV3NeedMinLength, false, false);

    EXPECT_EQ(softmaxflashV3NeedMinLength, 0);
    EXPECT_EQ(softmaxflashV3NeedMaxLength, 0);

    uint32_t workLength = 76 * 1024;
    SoftMaxFlashV3TilingFunc(softmaxShape, inputTypeSize, maxSumTypeSize, workLength, tilingData, true, true);
    EXPECT_EQ(tilingData.get_reduceM(), 8);
}

TEST_F(TestTiling, TEstSwiGLUTilingHalf)
{
    std::vector<int64_t> shapeDims = {10, 512};
    auto swigluShape = ge::Shape(shapeDims);
    uint32_t maxValue;
    uint32_t minValue;
    GetSwiGLUMaxMinTmpSize(swigluShape, 2, maxValue, minValue, false);
    EXPECT_EQ(maxValue, 61440);
    EXPECT_EQ(minValue, 1536);
}

TEST_F(TestTiling, TEstSwiGLUTilingFloat)
{
    std::vector<int64_t> shapeDims = {64, 256};
    auto swigluShape = ge::Shape(shapeDims);
    uint32_t maxValue;
    uint32_t minValue;
    GetSwiGLUMaxMinTmpSize(swigluShape, 4, maxValue, minValue, false);
    EXPECT_EQ(maxValue, 0);
    EXPECT_EQ(minValue, 0);
}

TEST_F(TestTiling, TEstSwiGLUTilingHalf1024)
{
    std::vector<int64_t> shapeDims = {21, 512};
    auto swigluShape = ge::Shape(shapeDims);
    uint32_t maxValue;
    uint32_t minValue;
    GetSwiGLUMaxMinTmpSize(swigluShape, 2, maxValue, minValue, false);
    EXPECT_EQ(maxValue, 129024);
    EXPECT_EQ(minValue, 1536);
}

TEST_F(TestTiling, TestSwiGLUFactorFloat)
{
    std::vector<int64_t> shapeDims = {22, 512};
    auto swigluShape = ge::Shape(shapeDims);
    uint32_t maxLiveNodeCnt;
    uint32_t extraBuf;
    GetSwiGLUTmpBufferFactorSize(4, maxLiveNodeCnt, extraBuf);
    EXPECT_EQ(maxLiveNodeCnt, 0);
    EXPECT_EQ(extraBuf, 0);
}

TEST_F(TestTiling, TestSwiGLUFactorHalf)
{
    std::vector<int64_t> shapeDims = {21, 512};
    auto swigluShape = ge::Shape(shapeDims);
    uint32_t maxLiveNodeCnt;
    uint32_t extraBuf;
    GetSwiGLUTmpBufferFactorSize(2, maxLiveNodeCnt, extraBuf);
    EXPECT_EQ(maxLiveNodeCnt, 6);
    EXPECT_EQ(extraBuf, 0);
}

TEST_F(TestTiling, TestSigmoidTiling)
{
    std::vector<int64_t> shapeDims = { 128 };
    auto sigmoidShape = ge::Shape(shapeDims);
    uint32_t maxVal;
    uint32_t minVal;
    GetSigmoidMaxMinTmpSize(sigmoidShape, 4, false, maxVal, minVal);
    EXPECT_EQ(maxVal, 128 * 4);
    EXPECT_EQ(minVal, 256);
}

TEST_F(TestTiling, TestLayernormTiling)
{
    const uint32_t stackBufferSize = 100 * 1024;
    const uint32_t typeSize = 4;

    std::vector<int64_t> shapeDims = { 128, 128, 128, 128, 128, 128 };
    auto layernormShape = ge::Shape(shapeDims);
    const bool isReuseSource = false;
    optiling::LayerNormTiling tilling;

    uint32_t minValue = 0;
    uint32_t maxValue = 0;

    AscendC::GetLayerNormMaxMinTmpSize(layernormShape, typeSize, isReuseSource, maxValue, minValue);
    EXPECT_EQ(maxValue, 3 * (128 * 128 * 128) * typeSize + 2 * (128 * 128) * typeSize);
    EXPECT_EQ(minValue, 3 * 128 * typeSize + 2 * (128 * 128) * typeSize);

    AscendC::GetLayerNormNDTillingInfo(layernormShape, stackBufferSize, typeSize, isReuseSource, tilling);
    EXPECT_EQ(tilling.get_tmpBufSize(), stackBufferSize / sizeof(float));

    AscendC::GetLayerNormNDTilingInfo(layernormShape, stackBufferSize, typeSize, isReuseSource, tilling);
    EXPECT_EQ(tilling.get_tmpBufSize(), stackBufferSize / sizeof(float));
}

TEST_F(TestTiling, TestGroupnormTiling)
{
    const uint32_t stackBufferSize = 100 * 1024;
    const uint32_t typeSize = 4;
    const uint32_t groupNum = 4;

    std::vector<int64_t> shapeDims = { 16, 16, 8, 8};
    auto groupnormShape = ge::Shape(shapeDims);
    const bool isReuseSource = false;
    optiling::GroupNormTiling tilling;

    uint32_t minValue = 0;
    uint32_t maxValue = 0;

    AscendC::GetGroupNormMaxMinTmpSize(groupnormShape, typeSize, isReuseSource, groupNum, maxValue, minValue);
    EXPECT_EQ(maxValue, 3 * (16 * 16 * 8 * 8) * typeSize + 2 * groupNum * 16 * typeSize);
    EXPECT_EQ(minValue, 3 * (16 / 4 * 8 * 8) * typeSize + 2 * groupNum * 16 * typeSize);

    AscendC::GetGroupNormNDTilingInfo(groupnormShape, stackBufferSize, typeSize, isReuseSource, groupNum, tilling);
    EXPECT_EQ(tilling.get_tmpBufSize(), stackBufferSize / sizeof(float));
}

TEST_F(TestTiling, TestRmsnormTiling)
{
    constexpr uint32_t bLength = 4;
    constexpr uint32_t sLength = 32;
    constexpr uint32_t hLength = 16;
    constexpr uint32_t bsLength = bLength * sLength;
    constexpr uint32_t bshLength = bLength * sLength * hLength;
    std::vector<int64_t> shapeDims = {bLength, sLength, hLength};
    auto shape = ge::Shape(shapeDims);
    constexpr uint32_t typeSize = 4;
    constexpr uint32_t ONE_BLK_FLOAT = 8;

    uint32_t maxValue = 0;
    uint32_t minValue = 0;
    // common scene
    bool res = AscendC::GetRmsNormMaxMinTmpSize(shape, typeSize, maxValue, minValue);
    const uint32_t goldenMax = (bshLength + bsLength) * typeSize;
    uint32_t goldenMin = (hLength + ONE_BLK_FLOAT) * typeSize;
    EXPECT_EQ(res, true);
    EXPECT_EQ(maxValue, goldenMax);
    EXPECT_EQ(minValue, goldenMin);

    // basic block scene 1: input shape is illegal, fail to get minSize
    res = AscendC::GetRmsNormMaxMinTmpSize(shape, typeSize, maxValue, minValue, true);
    EXPECT_EQ(res, false);

    constexpr uint32_t BASIC_BLK_HLENGTH = 64;
    constexpr uint32_t BASIC_BLK_BSLENGTH = 8;
    shapeDims[2] = BASIC_BLK_HLENGTH;
    auto shape_basic_blk = ge::Shape(shapeDims);// 4,32,64
    // basic block scene 2: get minSize successfully
    res = AscendC::GetRmsNormMaxMinTmpSize(shape_basic_blk, typeSize, maxValue, minValue, true);
    goldenMin = (64 + 8) * typeSize;
    EXPECT_EQ(res, true);
    EXPECT_EQ(minValue, goldenMin);

    // basic block scene: get basic block using minTmpSize
    // goldenMin should be (BASIC_BLK_HLENGTH(64) * BASIC_BLK_BSLENGTH(8) + bsLength) * typeSize
    optiling::RmsNormTiling tiling;
    uint32_t tmpSize = (64 + 8) * 4; // shape: 4,32,64
    res = AscendC::GetRmsNormTilingInfo(shape_basic_blk, shape_basic_blk, minValue, typeSize, tiling, true);
    EXPECT_EQ(res, true);
    EXPECT_EQ(tiling.get_mainBshLength(), 64);
    EXPECT_EQ(tiling.get_mainBsLength(), 1);

    auto shape1 = ge::Shape({1,7,16});
    res = AscendC::GetRmsNormMaxMinTmpSize(shape1, typeSize, maxValue, minValue);
    goldenMin = (8 + 16) * typeSize;
    EXPECT_EQ(minValue, goldenMin);

    uint32_t stackBufferSize = 100 * 1024;
    // common scene: get tiling info successfully, shape: 1,7,16
    res = AscendC::GetRmsNormTilingInfo(shape1, shape1, stackBufferSize, typeSize, tiling);
    EXPECT_EQ(res, true);
    EXPECT_EQ(tiling.get_mainBshLength(), 1*7*16);
    EXPECT_EQ(tiling.get_mainBsLength(), 7);

    stackBufferSize = hLength;
    // common scene: fail to get tiling info because of small stack buffer
    res = AscendC::GetRmsNormTilingInfo(shape, shape, stackBufferSize, typeSize, tiling);
    EXPECT_EQ(res, false);

    // basic block scene: get basic block tiling info successfully
    stackBufferSize = 100 * 1024; // shape: 4,32,64
    res = AscendC::GetRmsNormTilingInfo(shape_basic_blk, shape_basic_blk, stackBufferSize, typeSize, tiling, true);
    EXPECT_EQ(res, true);
    EXPECT_EQ(tiling.get_mainBshLength(), 4*32*64);

    // basic block scene: get basic block tiling info successfully
    stackBufferSize = (8*128 + 7)*4;
    auto shape2 = ge::Shape({1,8,128});
    res = AscendC::GetRmsNormTilingInfo(shape2, shape2, stackBufferSize, typeSize, tiling, true);
    EXPECT_EQ(res, true);
    EXPECT_EQ(tiling.get_mainBshLength(), 896);
    EXPECT_EQ(tiling.get_mainBsLength(), 7);

    stackBufferSize = (8*128 + 8)*4; // shape: 1,8,128
    res = AscendC::GetRmsNormTilingInfo(shape2, shape2, stackBufferSize, typeSize, tiling, true);
    EXPECT_EQ(res, true);
    EXPECT_EQ(tiling.get_mainBshLength(), 128 * 8);
    EXPECT_EQ(tiling.get_mainBsLength(), 8);

    stackBufferSize = (8*128 + 9)*4; // shape: 1,8,128
    res = AscendC::GetRmsNormTilingInfo(shape2, shape2, stackBufferSize, typeSize, tiling, true);
    EXPECT_EQ(res, true);
    EXPECT_EQ(tiling.get_mainBshLength(), 128 * 8);
    EXPECT_EQ(tiling.get_mainBsLength(), 8);

    // general case: bs > 256, set bs to 2*255+2
    stackBufferSize = 32*512*4;
    auto shape3 = ge::Shape({1,512,16}); // bs bigger than max_repeat(255)
    res = AscendC::GetRmsNormTilingInfo(shape3, shape3, stackBufferSize, typeSize, tiling);
    EXPECT_EQ(res, true);
    EXPECT_EQ(tiling.get_mainBshLength(), 255*16);
    EXPECT_EQ(tiling.get_mainBsLength(), 255);
    EXPECT_EQ(tiling.get_tailBsLength(), 2);
    EXPECT_EQ(tiling.get_loopRound(), 2);

    // abnormal case: input shape != original shape
    res = AscendC::GetRmsNormTilingInfo(shape2, shape3, stackBufferSize, typeSize, tiling);
    EXPECT_EQ(res, false);

    // abnormal case: basic block doesnot support h >= 2048
    stackBufferSize = 16*2048*4;
    auto shape4 = ge::Shape({1,8,2048});
    res = AscendC::GetRmsNormTilingInfo(shape4, shape4, stackBufferSize, typeSize, tiling, true);
    EXPECT_EQ(res, false);

    stackBufferSize = 2048;
    shape4 = ge::Shape({14,1,56});
    res = AscendC::GetRmsNormTilingInfo(shape4, shape4, stackBufferSize, typeSize, tiling);
    EXPECT_EQ(res, true);
    EXPECT_EQ(tiling.get_mainBshLength(), 448);
    EXPECT_EQ(tiling.get_mainBsLength(), 8);
    EXPECT_EQ(tiling.get_tailBshLength(), 336);
    EXPECT_EQ(tiling.get_tailBsLength(), 6);
    EXPECT_EQ(tiling.get_loopRound(), 1);

    stackBufferSize = 2080;
    res = AscendC::GetRmsNormTilingInfo(shape4, shape4, stackBufferSize, typeSize, tiling);
    EXPECT_EQ(res, true);
    EXPECT_EQ(tiling.get_mainBshLength(),504);
    EXPECT_EQ(tiling.get_mainBsLength(), 9);
    EXPECT_EQ(tiling.get_tailBshLength(), 280);
    EXPECT_EQ(tiling.get_tailBsLength(), 5);
    EXPECT_EQ(tiling.get_loopRound(), 1);
}

TEST_F(TestTiling, TestBatchnormTiling)
{
    constexpr uint32_t bLength = 8;
    constexpr uint32_t sLength = 1;
    constexpr uint32_t hLength = 16;
    constexpr uint32_t originalBLength = 8;
    constexpr uint32_t shLength = sLength * hLength;
    constexpr uint32_t bshLength = originalBLength * sLength * hLength;
    std::vector<int64_t> shapeDims = { bLength, sLength, hLength };
    std::vector<int64_t> originShape_dims = { originalBLength, sLength, hLength };
    auto shape = ge::Shape(shapeDims);
    auto originShape = ge::Shape(originShape_dims);
    bool reuseSrc = false;
    constexpr uint32_t typeSize = 4;
    constexpr uint32_t halfTypeSize = 2;

    uint32_t maxValue = 0;
    uint32_t minValue = 0;
    // common scene
    bool res = AscendC::GetBatchNormMaxMinTmpSize(shape, originShape, typeSize, reuseSrc, maxValue, minValue, false);
    const uint32_t goldenMax = (3 * bshLength + 2 * shLength) * typeSize;
    uint32_t goldenMin = (3 * originalBLength * 8 + 2 * shLength) * typeSize;
    EXPECT_EQ(res, true);
    EXPECT_EQ(maxValue, goldenMax);
    EXPECT_EQ(minValue, goldenMin);

    // basic block scene 1: input shape is illegal, fail to get minSize
    res = AscendC::GetBatchNormMaxMinTmpSize(shape, originShape, typeSize, reuseSrc, maxValue, minValue, true);
    EXPECT_EQ(res, false);

    constexpr uint32_t BASIC_BLK_BLENGTH = 2;
    constexpr uint32_t BASIC_BLK_SHLENGTH = 64;
    originShape_dims[1] = 4;
    originShape_dims[2] = 16;
    auto shape_basic_blk = ge::Shape(originShape_dims);
    // basic block scene 2: get minSize successfully
    res = AscendC::GetBatchNormMaxMinTmpSize(shape_basic_blk, shape_basic_blk, typeSize, reuseSrc, maxValue, minValue,
        true);
    goldenMin = (3 * originalBLength * BASIC_BLK_SHLENGTH + 2 * 4 * 16) * typeSize;
    EXPECT_EQ(res, true);
    EXPECT_EQ(minValue, goldenMin);

    // basic block scene: get basic block using minTmpSize, shape = [8,4,16,2]
    optiling::BatchNormTiling tiling;
    res =
        AscendC::GetBatchNormNDTilingInfo(shape_basic_blk, shape_basic_blk, minValue, typeSize, reuseSrc, tiling, true);
    EXPECT_EQ(res, true);
    res = AscendC::GetBatchNormMaxMinTmpSize(shape_basic_blk, shape_basic_blk, halfTypeSize, reuseSrc, maxValue,
        minValue, true);
    res = AscendC::GetBatchNormNDTilingInfo(shape_basic_blk, shape_basic_blk, minValue, halfTypeSize, reuseSrc, tiling,
        true);
    EXPECT_EQ(res, true);

    uint32_t stackBufferSize = 100 * 1024;
    // common scene: get tiling info successfully
    res =
        AscendC::GetBatchNormNDTilingInfo(originShape, originShape, stackBufferSize, typeSize, reuseSrc, tiling, false);
    EXPECT_EQ(res, true);

    stackBufferSize = bLength;
    // common scene: fail to get tiling info because of small stack buffer
    res =
        AscendC::GetBatchNormNDTilingInfo(originShape, originShape, stackBufferSize, typeSize, reuseSrc, tiling, false);
    EXPECT_EQ(res, false);

    // basic block scene: get basic block tiling info successfully
    stackBufferSize = 100 * 1024;
    res = AscendC::GetBatchNormNDTilingInfo(shape_basic_blk, shape_basic_blk, stackBufferSize, typeSize, reuseSrc,
        tiling, true);
    EXPECT_EQ(res, true);

    // basic block scene: fail to get basic block using buffer less than minValue
    goldenMin = (3 * originalBLength * BASIC_BLK_SHLENGTH + 2 * 4 * 6 - 1) * typeSize;
    res = AscendC::GetBatchNormMaxMinTmpSize(shape_basic_blk, shape_basic_blk, typeSize, reuseSrc, maxValue, minValue,
        true);
    res = AscendC::GetBatchNormNDTilingInfo(shape_basic_blk, shape_basic_blk, goldenMin, typeSize, reuseSrc, tiling,
        true);
    EXPECT_EQ(res, false);
}

TEST_F(TestTiling, TestDeepnormTiling)
{
    const uint32_t stackBufferSize = 100 * 1024;
    const uint32_t typeSize = 4;
    const int64_t bLength = 2;
    const int64_t sLength = 8;
    const int64_t hLength = 128;
    const int64_t oriHLength = 120;

    std::vector<int64_t> shapeDims = {bLength, sLength, hLength};
    std::vector<int64_t> original_shape_dims = {bLength, sLength, oriHLength};
    auto deepnormShape = ge::Shape(shapeDims);
    auto oriDeepNormShape = ge::Shape(original_shape_dims);
    const bool varFalse = false;
    const bool varTrue = true;
    optiling::DeepNormTiling tiling;

    uint32_t maxValue = 0;
    uint32_t minValue = 0;
    // reuse = false, isbasic = false
    AscendC::GetDeepNormMaxMinTmpSize(deepnormShape, typeSize, varFalse, varFalse, maxValue, minValue);
    EXPECT_EQ(minValue, 3 * hLength * typeSize + 2 * (bLength * sLength) * typeSize);
    EXPECT_EQ(maxValue, 3 * (bLength * sLength * hLength) * typeSize + 2 * (bLength * sLength) * typeSize);

    // isbasic = true,  b*s must be divisible by 8
    std::vector<int64_t> wrong_shape_dims = {1, 9, 64};
    auto wrongDeepNormShape = ge::Shape(wrong_shape_dims);
    bool res = AscendC::GetDeepNormMaxMinTmpSize(wrongDeepNormShape, typeSize, varFalse, varTrue, maxValue, minValue);
    EXPECT_EQ(res, false);

    // isbasic = true, hLength must be divisible by 64
    wrong_shape_dims = {2, 8, 72};
    wrongDeepNormShape = ge::Shape(wrong_shape_dims);
    res = AscendC::GetDeepNormMaxMinTmpSize(wrongDeepNormShape, typeSize, varFalse, varTrue, maxValue, minValue);
    EXPECT_EQ(res, false);


    AscendC::GetDeepNormTilingInfo(deepnormShape, oriDeepNormShape, stackBufferSize, typeSize, varFalse, varFalse,
        tiling);
    EXPECT_EQ(tiling.get_tmpBufSize(), stackBufferSize / sizeof(float));

    // originalB = b, originalH = h
    wrong_shape_dims = {1, 8, 128};          // originalb != b
    wrongDeepNormShape = ge::Shape(wrong_shape_dims);
    res = AscendC::GetDeepNormTilingInfo(deepnormShape, wrongDeepNormShape, stackBufferSize, typeSize, varFalse,
        varFalse, tiling);
    EXPECT_EQ(res, false);

    // hlength must align to 32
    wrong_shape_dims = {1, 8, 4};
    wrongDeepNormShape = ge::Shape(wrong_shape_dims);
    res = AscendC::GetDeepNormTilingInfo(wrongDeepNormShape, wrongDeepNormShape, stackBufferSize, typeSize, varFalse,
        varFalse, tiling);
    EXPECT_EQ(res, false);

    // originalHlength <= hLength
    wrong_shape_dims = {2, 8, 136};
    wrongDeepNormShape = ge::Shape(wrong_shape_dims);
    res = AscendC::GetDeepNormTilingInfo(deepnormShape, wrongDeepNormShape, stackBufferSize, typeSize, varFalse,
        varFalse, tiling);
    EXPECT_EQ(res, false);

    // when basicblock, b*s must be divisible by 8
    wrong_shape_dims = {1, 4, 64};
    wrongDeepNormShape = ge::Shape(wrong_shape_dims);
    res = AscendC::GetDeepNormTilingInfo(wrongDeepNormShape, wrongDeepNormShape, stackBufferSize, typeSize, varFalse,
        varTrue, tiling);
    EXPECT_EQ(res, false);

    // when isbasicblock, origianlH must equal to H
    res = AscendC::GetDeepNormTilingInfo(deepnormShape, oriDeepNormShape, stackBufferSize, typeSize, varFalse, varTrue,
        tiling);
    EXPECT_EQ(res, false);

    // hLength <= 255 * 8
    wrong_shape_dims = {1, 4, 2048};
    wrongDeepNormShape = ge::Shape(wrong_shape_dims);
    res = AscendC::GetDeepNormTilingInfo(wrongDeepNormShape, wrongDeepNormShape, stackBufferSize, typeSize, varFalse,
        varFalse, tiling);
    EXPECT_EQ(res, false);

    // assume initial oneTmpSize is 9*n, update to 8*n for efficiency when isBasicBlock
    // tiling.oneTmpSize before update: 704   after update: 512
    std::vector<int64_t> basicblk_shape_dims = {4, 4, 64};
    auto basicblkDeepNormShape = ge::Shape(basicblk_shape_dims);
    res = AscendC::GetDeepNormTilingInfo(basicblkDeepNormShape, basicblkDeepNormShape, 8874, typeSize, varFalse,
        varTrue, tiling);
    EXPECT_EQ(tiling.get_oneTmpSize(), 512);
}

TEST_F(TestTiling, TestMatmulApiTilngFactorSplit1)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(176, 1680, 608);
    tiling.SetOrgShape(176, 1680, 608);
    tiling.SetBias(false);
    tiling.SetBufferSpace(-1, 128 * 1024, -1);
    optiling::TCubeTiling tilingData;
    tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(tilingData.get_baseM(), 128);
}


TEST_F(TestTiling, TestMatmulApiTilngFactorSplit2)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(1680, 176, 608);
    tiling.SetOrgShape(1680, 176, 608);
    tiling.SetBias(false);
    tiling.SetBufferSpace(-1, 128 * 1024, -1);
    optiling::TCubeTiling tilingData;
    tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(tilingData.get_baseM(), 128);
}

TEST_F(TestTiling, TestMatmulApiTilngDimfactors)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(8272, 3216, 16);
    tiling.SetOrgShape(8272, 3216, 16);
    tiling.SetBias(false);
    tiling.SetBufferSpace(-1, 128 * 1024, -1);

    optiling::TCubeTiling tilingData;
    tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(tilingData.get_baseK(), 16);
}


// when A matrix or B matrix is in TSCM, then calculaye loadSize should ignore it;
TEST_F(TestTiling, TestMatmulApiTilngBMatrixTSCM)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::TSCM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(32, 64, 128);
    tiling.SetOrgShape(32, 64, 128);
    tiling.SetBias(false);
    tiling.SetBufferSpace(2048, -1, 9362);
    optiling::TCubeTiling tilingData;
    tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(tilingData.get_baseK(), 32);
}

TEST_F(TestTiling, TestMatmulApiTilngFailed1)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::NZ, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBType(TPosition::GM, CubeFormat::NZ, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetCType(TPosition::GM, CubeFormat::NZ, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::NZ, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(83, 592, 160);
    tiling.SetOrgShape(83, 592, 160);
    tiling.SetBias(false);
    tiling.SetBufferSpace(-1, -1, -1);
    optiling::TCubeTiling tilingData;
    tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
}

// single m = 970, upper round is 976 which is 61 times of 16. the value is not good.
// so tiling func will round to 992, which is 62 times of 16
TEST_F(TestTiling, TestMatmulApiTilngFailed2)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::NZ, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::NZ, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::NZ, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::NZ, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(970, 32, 182);
    tiling.SetOrgShape(970, 32, 182);
    tiling.SetBias(false);
    tiling.SetBufferSpace(-1, -1, -1);
    optiling::TCubeTiling tilingData;
    tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
}

// when B matrix is full load, l1Status.nBL1 * l0Status.nL0 is large then coreStatus.n or
// then calculate bl1size is smaller then the baseN * baseK * depthB1,
// which result in final L1Size is larged then the provided size;
TEST_F(TestTiling, TestMatmulApiTilngFailed3)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::NZ, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBType(TPosition::GM, CubeFormat::NZ, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetCType(TPosition::GM, CubeFormat::NZ, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::NZ, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(83, 592, 160);
    tiling.SetOrgShape(83, 592, 160);
    tiling.SetBias(false);
    tiling.SetBufferSpace(-1, -1, -1);
    optiling::TCubeTiling tilingData;
    tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
}

// baseM * basek + BIAS are larger then l1size
TEST_F(TestTiling, TestMatmulApiTilngFailed4)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBType(TPosition::TSCM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(32, 64, 128);
    tiling.SetOrgShape(32, 64, 128);
    tiling.SetFixSplit(32, 64, -1);
    tiling.SetBias(true);
    tiling.SetBufferSpace(4096 * 4 + 64 * 4, -1, -1, 1024);
    optiling::TCubeTiling tilingData;
    tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
}

TEST_F(TestTiling, TestMatmulApiTilngMultiCoreCase1)
{
    MultiCoreMatmulTiling tiling;
    tiling.SetDim(24);
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetOrgShape(65536, 32768, 65536);
    tiling.SetShape(65536, 32768, 65536);
    tiling.SetBias(false);
    tiling.SetBufferSpace(-1, -1, -1);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, TestMatmulApiTilngMultiCoreCase2)
{
    MultiCoreMatmulTiling tiling;
    tiling.SetDim(2);
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetOrgShape(64, 64, 64);
    tiling.SetShape(64, 64, 64);
    tiling.SetBias(false);
    tiling.SetBufferSpace(-1, -1, -1);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, TestMatmulApiTilngMultiCoreCase3)
{
    MultiCoreMatmulTiling tiling;
    tiling.SetDim(2);
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetOrgShape(65536, 64, 32);
    tiling.SetShape(65536, 64, 32);
    tiling.SetBias(false);
    tiling.SetBufferSpace(-1, -1, -1);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, TestMatmulApiTilngMultiCoreCase4)
{
    MultiCoreMatmulTiling tiling;
    tiling.SetDim(2);
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetOrgShape(2048, 2048, 256);
    tiling.SetShape(2048, 2048, 256);
    tiling.SetBias(false);
    tiling.SetBufferSpace(-1, -1, -1);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, TestMatmulApiTilngMultiCoreWithCppStruct)
{
    MultiCoreMatmulTiling tiling;
    tiling.SetDim(2);
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetOrgShape(2048, 2048, 256);
    tiling.SetShape(2048, 2048, 256);
    tiling.EnableBias(false);
    tiling.SetBufferSpace(-1, -1, -1);
    TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
    matmul_tiling::SysTilingTempBufSize bufSize;
    EXPECT_EQ(MultiCoreMatmulGetTmpBufSizeV2(tilingData, bufSize), 0);

}

TEST_F(TestTiling, TestMatmulApiTilngKNotAlign)
{
    optiling::TCubeTiling tilingData;
    MultiCoreMatmulTiling tilingApi;
    tilingApi.SetDim(24);

    tilingApi.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tilingApi.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tilingApi.SetCType(TPosition::GM, CubeFormat::NZ, matmul_tiling::DataType::DT_FLOAT);
    tilingApi.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);

    tilingApi.SetOrgShape(192, 384, 1952);
    tilingApi.SetShape(192, 384, 1952);
    tilingApi.SetBias(false);

    tilingApi.SetBufferSpace(-1, -1, -1);
    int64_t res = tilingApi.GetTiling(tilingData);
    tilingApi.PrintTilingData();
    EXPECT_EQ(res, 0);
}

TEST_F(TestTiling, TestMatmulApiTilngMultiCoreBTSCM)
{
    optiling::TCubeTiling tilingData;
    MultiCoreMatmulTiling tilingApi;
    tilingApi.SetDim(24);

    tilingApi.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tilingApi.SetBType(TPosition::TSCM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tilingApi.SetCType(TPosition::GM, CubeFormat::NZ, matmul_tiling::DataType::DT_FLOAT);
    tilingApi.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);

    tilingApi.SetOrgShape(65536, 64, 64);
    tilingApi.SetShape(65536, 64, 64);
    tilingApi.SetBias(false);

    tilingApi.SetBufferSpace(-1, -1, -1);
    int64_t res = tilingApi.GetTiling(tilingData);
    tilingApi.PrintTilingData();
    EXPECT_EQ(res, 0);
}

TEST_F(TestTiling, TestMatmulApiTilngMultiCoreBTSCM1)
{
    optiling::TCubeTiling tilingData;
    MultiCoreMatmulTiling tilingApi;
    tilingApi.SetDim(48);

    tilingApi.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tilingApi.SetBType(TPosition::TSCM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tilingApi.SetCType(TPosition::GM, CubeFormat::NZ, matmul_tiling::DataType::DT_FLOAT);
    tilingApi.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);

    tilingApi.SetOrgShape(64, 256 * 4, 256 * 4);
    tilingApi.SetShape(64, 256 * 4, 256 * 4);
    tilingApi.SetBias(false);

    tilingApi.SetBufferSpace(-1, -1, -1);
    int64_t res = tilingApi.GetTiling(tilingData);
    tilingApi.PrintTilingData();
    EXPECT_EQ(res, 0);
}

TEST_F(TestTiling, TestMatmulApiTilngMultiCoreBTSCM2)
{
    optiling::TCubeTiling tilingData;
    MultiCoreMatmulTiling tilingApi;
    tilingApi.SetDim(48);

    tilingApi.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tilingApi.SetBType(TPosition::TSCM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tilingApi.SetCType(TPosition::GM, CubeFormat::NZ, matmul_tiling::DataType::DT_FLOAT);
    tilingApi.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);

    tilingApi.SetOrgShape(48, 256, 256);
    tilingApi.SetShape(48, 256, 256);
    tilingApi.SetBias(false);

    tilingApi.SetBufferSpace(-1, -1, -1);
    int64_t res = tilingApi.GetTiling(tilingData);
    tilingApi.PrintTilingData();
    EXPECT_EQ(res, 0);
}

TEST_F(TestTiling, TestMatmulApiTilngMultiCoreBTSCM3)
{
    optiling::TCubeTiling tilingData;
    MultiCoreMatmulTiling tilingApi;
    tilingApi.SetDim(48);

    tilingApi.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tilingApi.SetBType(TPosition::TSCM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tilingApi.SetCType(TPosition::GM, CubeFormat::NZ, matmul_tiling::DataType::DT_FLOAT);
    tilingApi.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);

    tilingApi.SetOrgShape(16, 512, 512);
    tilingApi.SetShape(16, 512, 512);
    tilingApi.SetBias(false);

    tilingApi.SetBufferSpace(-1, -1, -1);
    int64_t res = tilingApi.GetTiling(tilingData);
    tilingApi.PrintTilingData();
    EXPECT_EQ(res, 0);
}

TEST_F(TestTiling, TestMatmulApiTilngMultiCoreBTSCM4)
{
    optiling::TCubeTiling tilingData;
    MultiCoreMatmulTiling tilingApi;
    tilingApi.SetDim(48);

    tilingApi.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tilingApi.SetBType(TPosition::TSCM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tilingApi.SetCType(TPosition::GM, CubeFormat::NZ, matmul_tiling::DataType::DT_FLOAT);
    tilingApi.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);

    tilingApi.SetOrgShape(2560, 256, 256);
    tilingApi.SetShape(2560, 256, 256);
    tilingApi.SetBias(false);

    tilingApi.SetBufferSpace(-1, -1, -1);
    int64_t res = tilingApi.GetTiling(tilingData);
    tilingApi.PrintTilingData();
    EXPECT_EQ(res, 0);
}

TEST_F(TestTiling, TestMatmulApiTilngSingleCoreFullLoadCase)
{
    optiling::TCubeTiling tilingData;
    MultiCoreMatmulTiling tilingApi;
    tilingApi.SetDim(24);

    tilingApi.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16, true);
    tilingApi.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tilingApi.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tilingApi.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);

    tilingApi.SetOrgShape(2048, 2048, 204);
    tilingApi.SetShape(2048, 2048, 204);
    tilingApi.SetBias(false);
    tilingApi.SetBufferSpace(-1, -1, -1);
    int64_t res = tilingApi.GetTiling(tilingData);
    tilingApi.PrintTilingData();
    EXPECT_EQ(res, 0);
}

TEST_F(TestTiling, TestMatmulApiTilngMultiCoreBTSCM5)
{
    optiling::TCubeTiling tilingData;
    MultiCoreMatmulTiling tilingApi;
    tilingApi.SetDim(12);

    tilingApi.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tilingApi.SetBType(TPosition::TSCM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tilingApi.SetCType(TPosition::GM, CubeFormat::NZ, matmul_tiling::DataType::DT_FLOAT);
    tilingApi.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);

    tilingApi.SetOrgShape(32, 256, 768);
    tilingApi.SetShape(32, 256, 768);
    tilingApi.SetBias(true);
    tilingApi.SetAlignSplit(-1, 64, -1);
    tilingApi.SetBufferSpace(-1, -1, -1);
    int64_t res = tilingApi.GetTiling(tilingData);
    tilingApi.PrintTilingData();
    EXPECT_EQ(res, 0);
}

TEST_F(TestTiling, TestLayernormGradTiling)
{
    const uint32_t stackBufferSize = 100 * 1024;

    std::vector<int64_t> shapeDims = { 128, 128, 128, 128, 128, 128 };
    auto layernormgradShape = ge::Shape(shapeDims);
    optiling::LayerNormGradTiling tiling;

    AscendC::GetLayerNormGradNDTilingInfo(layernormgradShape, stackBufferSize, 4, false, tiling);
    EXPECT_EQ(tiling.get_stackBufferSize(), stackBufferSize);

    uint32_t maxValue = 0;
    uint32_t minValue = 0;
    AscendC::GetLayerNormGradMaxMinTmpSize(layernormgradShape, 2, false, maxValue, minValue);
    EXPECT_EQ(maxValue, (128 * 128 * 128) * 9 * 2);
    EXPECT_EQ(minValue, 128 * 128 * 9 * 2);

    AscendC::GetLayerNormGradMaxMinTmpSize(layernormgradShape, 4, true, maxValue, minValue);
    EXPECT_EQ(maxValue, (128 * 128 * 128) * 4 * 4);
    EXPECT_EQ(minValue, 128 * 128 * 4 * 4);

    AscendC::GetLayerNormGradMaxMinTmpSize(layernormgradShape, 4, false, maxValue, minValue);
    EXPECT_EQ(maxValue, (128 * 128 * 128) * 6 * 4);
    EXPECT_EQ(minValue, 128 * 128 * 6 * 4);
}

TEST_F(TestTiling, TestLayernormGradBetaTiling)
{
    const uint32_t stackBufferSize = 100 * 1024 * 1024;
    const uint32_t typeSize = 4;

    std::vector<int64_t> shapeDims = { 128, 128, 128, 128, 128, 128 };
    auto layernormgradbetaShape = ge::Shape(shapeDims);
    const bool isReuseSource = false;

    optiling::LayerNormGradBetaTiling tiling;

    uint32_t maxValue = 0;
    uint32_t minValue = 0;
    AscendC::GetLayerNormGradBetaMaxMinTmpSize(layernormgradbetaShape, 4, isReuseSource, maxValue, minValue);
    EXPECT_EQ(maxValue, (128 * 128 * 128) * 4 * 2);
    EXPECT_EQ(minValue, 128 * 4 * 2);

    AscendC::GetLayerNormGradBetaMaxMinTmpSize(layernormgradbetaShape, 2, isReuseSource, maxValue, minValue);
    EXPECT_EQ(maxValue, (128 * 128 * 128) * 4 * 4);
    EXPECT_EQ(minValue, 128 * 4 * 4);

    AscendC::GetLayerNormGradBetaNDTilingInfo(layernormgradbetaShape, stackBufferSize, typeSize, isReuseSource, tiling);
    EXPECT_EQ(tiling.get_stackBufferSize(), stackBufferSize / sizeof(float));
}

TEST_F(TestTiling, TestMatmulApiTilngL0BNoDB)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(16, 2048, 128);
    tiling.SetOrgShape(2048, 2048, 128);
    tiling.SetFixSplit(16, 2048, -1);
    tiling.SetBias(false);
    tiling.SetBufferSpace(453632, 131072, -1, 1024);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, TestMatmulApiTilngL0ANoDB)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(2048, 16, 128);
    tiling.SetOrgShape(2048, 2048, 128);
    tiling.SetFixSplit(2048, 16, -1);
    tiling.SetBias(false);
    tiling.SetBufferSpace(453632, 131072, -1, 1024);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

class DynamicRnnTiling {
public:
    int32_t sysAivCoreNum;
    int32_t batch;
    int32_t inputSize;
    int32_t hiddenSize;
    int32_t maxUbSize;
    optiling::TCubeTiling matmulTiling;
    int32_t baseM;
    int32_t baseN;
    int32_t baseK;
    int32_t singleM;
    int32_t singleN;
    int32_t singleK;
    int32_t usedCoreNum;
};

class DynamicRNNTilingDataTik2 {
public:
    optiling::TCubeTiling inputMMParam;
    optiling::TCubeTiling hiddenMMParam;
};

struct RnnParams {
    uint32_t batch;
    uint32_t inputSize;
    uint32_t hiddenSize;
    uint32_t sysAivCoreNum;
    uint32_t maxUbSize;
};

class RnnTilingbTestSuite : public testing::Test, public testing::WithParamInterface<RnnParams> {
protected:
    void SetUp() {}
    void TearDown() {}
};

INSTANTIATE_TEST_CASE_P(TEST_RNN_TILING, RnnTilingbTestSuite,
    ::testing::Values(RnnParams { 1280, 256, 128, 48, 24 * 1024 }, RnnParams { 36, 512, 256, 48, 128 * 1024 - 64 },
    RnnParams { 48, 512, 256, 48, 128 * 1024 - 64 }, RnnParams { 64, 512, 256, 48, 128 * 1024 - 64 },
    RnnParams { 36, 768, 1024, 48, 128 * 1024 - 64 }, RnnParams { 48, 768, 1024, 48, 128 * 1024 - 64 },
    RnnParams { 64, 768, 1024, 48, 128 * 1024 - 64 }, RnnParams { 16, 16, 512, 48, 128 * 1024 - 64 },
    RnnParams { 64, 256, 256, 48, 128 * 1024 - 64 }, RnnParams { 64, 128, 128, 48, 128 * 1024 - 64 },
    RnnParams { 64, 256, 128, 48, 128 * 1024 - 64 }, RnnParams { 64, 512, 128, 48, 128 * 1024 - 64 },
    RnnParams { 64, 512, 256, 48, 128 * 1024 - 64 }, RnnParams { 1280, 256, 128, 48, 128 * 1024 - 64 },
    RnnParams { 1280, 256, 256, 48, 128 * 1024 - 64 }, RnnParams { 1280, 512, 128, 48, 128 * 1024 - 64 },
    RnnParams { 1280, 512, 256, 48, 128 * 1024 - 64 }, RnnParams { 1920, 256, 128, 48, 128 * 1024 - 64 },
    RnnParams { 1920, 256, 256, 48, 128 * 1024 - 64 }, RnnParams { 1920, 512, 128, 48, 128 * 1024 - 64 },
    RnnParams { 1920, 512, 256, 48, 128 * 1024 - 64 }, RnnParams { 2560, 256, 128, 48, 128 * 1024 - 64 },
    RnnParams { 2560, 256, 256, 48, 128 * 1024 - 64 }, RnnParams { 2560, 512, 128, 48, 128 * 1024 - 64 },
    RnnParams { 2560, 512, 256, 48, 128 * 1024 - 64 }, RnnParams { 48, 512, 256, 48, 128 * 1024 - 64 },
    RnnParams { 64, 1536, 1024, 48, 128 * 1024 - 64 }, RnnParams { 2560, 5120, 9760, 48, 128 * 1024 - 64 },
    RnnParams { 479, 96, 381, 48, 128 * 1024 - 64 }));

TEST_P(RnnTilingbTestSuite, TestMatmulApiTilngRnnRealCase)
{
    auto param = GetParam();
    DynamicRNNTilingDataTik2 tilingData;
    DynamicRnnTiling rnnParams;
    matmul_tiling::MultiCoreMatmulTiling rnnMatmul, rnnMatmul1, rnnMatmul2;
    rnnParams.batch = param.batch;
    rnnParams.inputSize = param.inputSize;
    rnnParams.hiddenSize = param.hiddenSize;
    rnnParams.sysAivCoreNum = param.sysAivCoreNum;
    rnnParams.maxUbSize = param.maxUbSize;
    int32_t dataType = 0;

    bool isFullLoadWeightOne = false;
    bool isFullLoadWeight = false;

    rnnMatmul.SetAType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, (matmul_tiling::DataType)dataType);
    rnnMatmul.SetBType(matmul_tiling::TPosition::TSCM, matmul_tiling::CubeFormat::ND,
        (matmul_tiling::DataType)dataType);
    rnnMatmul.SetCType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::NZ, matmul_tiling::DataType ::DT_FLOAT);
    rnnMatmul.SetBiasType(matmul_tiling::TPosition::VECCALC, matmul_tiling::CubeFormat::ND,
        (matmul_tiling::DataType)dataType);
    rnnMatmul1.SetAType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, (matmul_tiling::DataType)dataType);
    rnnMatmul1.SetBType(matmul_tiling::TPosition::TSCM, matmul_tiling::CubeFormat::ND,
        (matmul_tiling::DataType)dataType);
    rnnMatmul1.SetCType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::NZ,
        matmul_tiling::DataType ::DT_FLOAT);
    rnnMatmul1.SetBiasType(matmul_tiling::TPosition::VECCALC, matmul_tiling::CubeFormat::ND,
        (matmul_tiling::DataType)dataType);
    rnnMatmul2.SetAType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, (matmul_tiling::DataType)dataType);
    rnnMatmul2.SetBType(matmul_tiling::TPosition::TSCM, matmul_tiling::CubeFormat::ND,
        (matmul_tiling::DataType)dataType);
    rnnMatmul2.SetCType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::NZ,
        matmul_tiling::DataType ::DT_FLOAT);
    rnnMatmul2.SetBiasType(matmul_tiling::TPosition::VECCALC, matmul_tiling::CubeFormat::ND,
        (matmul_tiling::DataType)dataType);
    // full load
    auto ret = rnnMatmul.SetBias(true);
    ret = rnnMatmul.SetDim(rnnParams.sysAivCoreNum / 4);
    int32_t input_align = MathUtil::CeilDivision(rnnParams.inputSize, 16) * 16;
    int32_t hidden_align = MathUtil::CeilDivision(rnnParams.hiddenSize, 16) * 16;
    ret = rnnMatmul.SetOrgShape(rnnParams.batch, rnnParams.hiddenSize * 4, input_align + hidden_align);
    ret = rnnMatmul.SetShape(rnnParams.batch, rnnParams.hiddenSize, rnnParams.inputSize + rnnParams.hiddenSize);
    ret = rnnMatmul.SetBufferSpace(-1, rnnParams.maxUbSize, rnnParams.maxUbSize);
    ret = rnnMatmul.SetAlignSplit(-1, 64, -1);
    ret = rnnMatmul.GetTiling(rnnParams.matmulTiling);
    if (ret == 0) { // 0 success full    1 not full
        isFullLoadWeight = true;
    }
    if (isFullLoadWeight) {
        std::cout << "full load two weight" << std::endl;
        int32_t dim, mDim, nDim;
        rnnParams.baseM = rnnMatmul.GetBaseM();
        rnnParams.baseN = rnnMatmul.GetBaseN();
        rnnParams.baseK = rnnMatmul.GetBaseK(); // get output info after cut
        ret = rnnMatmul.GetSingleShape(rnnParams.singleM, rnnParams.singleN, rnnParams.singleK); // get single process info
        ret = rnnMatmul.GetCoreNum(dim, mDim,
            nDim); // get used blockdim after multi-cores cut, carried by user to kernel， contrl Kernel
        // input mm
        int32_t l1_left = 512 * 1024 - 64 - rnnParams.singleN * (input_align + hidden_align) * sizeof(float) * 2;
        ret = rnnMatmul1.SetBufferSpace(l1_left, rnnParams.maxUbSize, rnnParams.maxUbSize);
        ret = rnnMatmul1.SetOrgShape(rnnParams.batch, rnnParams.hiddenSize * 4, rnnParams.inputSize);
        ret = rnnMatmul1.SetShape(rnnParams.batch, rnnParams.hiddenSize, rnnParams.inputSize);
        ret = rnnMatmul1.SetSingleShape(rnnParams.singleM, rnnParams.singleN, rnnParams.inputSize);
        ret = rnnMatmul1.SetFixSplit(rnnParams.baseM, rnnParams.baseN, -1);
        ret = rnnMatmul1.SetBias(true);
        ret = rnnMatmul1.SetDim(dim * 4);
        ret = rnnMatmul1.SetAlignSplit(-1, 64, -1);
        ret = rnnMatmul1.GetTiling(tilingData.inputMMParam);
        tilingData.inputMMParam.set_singleCoreN(rnnParams.singleN / 4);
        int32_t l1UsedSize = (tilingData.inputMMParam.get_baseM() * tilingData.inputMMParam.get_baseK() *
            tilingData.inputMMParam.get_depthA1() +
            rnnParams.singleN * (hidden_align + input_align) * 2) *
            sizeof(float);
        EXPECT_LT(l1UsedSize, 512 * 1024 - 64);
        // hidden mm
        l1_left = 512 * 1024 - 64 - rnnParams.singleN * (input_align + hidden_align) * sizeof(float) * 2;
        ret = rnnMatmul2.SetBufferSpace(l1_left, rnnParams.maxUbSize, rnnParams.maxUbSize);
        ret = rnnMatmul2.SetOrgShape(rnnParams.batch, rnnParams.hiddenSize * 4, rnnParams.hiddenSize);
        ret = rnnMatmul2.SetShape(rnnParams.batch, rnnParams.hiddenSize, rnnParams.hiddenSize);
        ret = rnnMatmul2.SetSingleShape(rnnParams.singleM, rnnParams.singleN, rnnParams.hiddenSize);
        ret = rnnMatmul2.SetFixSplit(rnnParams.baseM, rnnParams.baseN, -1);
        ret = rnnMatmul2.SetBias(false);
        ret = rnnMatmul2.SetAlignSplit(-1, 64, -1);
        ret = rnnMatmul2.SetDim(dim * 4);

        ret = rnnMatmul2.GetTiling(tilingData.hiddenMMParam);
        tilingData.hiddenMMParam.set_singleCoreN(rnnParams.singleN / 4);
        l1UsedSize = (tilingData.hiddenMMParam.get_baseM() * tilingData.hiddenMMParam.get_baseK() *
            tilingData.hiddenMMParam.get_depthA1() +
            rnnParams.singleN * (hidden_align + input_align) * 2) *
            sizeof(float);
        EXPECT_LT(l1UsedSize, 512 * 1024 - 64);
        rnnParams.usedCoreNum = dim * 4;
    } else { // part of full load
        // two matmul time sharing
        auto ret = rnnMatmul.SetBias(true);
        ret = rnnMatmul.SetDim(rnnParams.sysAivCoreNum / 4);
        ret = rnnMatmul.SetOrgShape(rnnParams.batch, rnnParams.hiddenSize * 4, input_align + hidden_align);
        ret = rnnMatmul.SetShape(rnnParams.batch, rnnParams.hiddenSize, max(rnnParams.inputSize, rnnParams.hiddenSize));
        ret = rnnMatmul.SetBufferSpace(-1, rnnParams.maxUbSize, rnnParams.maxUbSize);
        ret = rnnMatmul.SetAlignSplit(-1, 64, -1);
        ret = rnnMatmul.GetTiling(rnnParams.matmulTiling);
        if (ret == 0) { // 0 success full  1 not full
            isFullLoadWeightOne = true;
        }
        if (isFullLoadWeightOne) {
            std::cout << "only load one weight" << std::endl;
            int32_t dim, mDim, nDim;
            rnnParams.baseM = rnnMatmul.GetBaseM();
            rnnParams.baseN = rnnMatmul.GetBaseN();
            rnnParams.baseK = rnnMatmul.GetBaseK(); // get output info after cut
            ret = rnnMatmul.GetSingleShape(rnnParams.singleM, rnnParams.singleN,
                rnnParams.singleK); // get single process info
            ret = rnnMatmul.GetCoreNum(dim, mDim,
                nDim); // get used blockdim after multi-cores cut, carried by user to kernel， contrl Kernel business
            // input mm
            ret = rnnMatmul1.SetBufferSpace(-1, rnnParams.maxUbSize, rnnParams.maxUbSize);
            ret = rnnMatmul1.SetOrgShape(rnnParams.batch, rnnParams.hiddenSize * 4, rnnParams.inputSize);
            ret = rnnMatmul1.SetShape(rnnParams.batch, rnnParams.hiddenSize, rnnParams.inputSize);
            ret = rnnMatmul1.SetSingleShape(rnnParams.singleM, rnnParams.singleN, rnnParams.inputSize);
            ret = rnnMatmul1.SetFixSplit(rnnParams.baseM, rnnParams.baseN, -1);
            ret = rnnMatmul1.SetBias(true);
            ret = rnnMatmul1.SetDim(dim);
            ret = rnnMatmul1.SetAlignSplit(-1, 64, -1);
            ret = rnnMatmul1.GetTiling(tilingData.inputMMParam);
            tilingData.inputMMParam.set_singleCoreN(rnnParams.singleN / 4);
            int32_t l1UsedSize = (tilingData.inputMMParam.get_baseM() * tilingData.inputMMParam.get_baseK() *
                tilingData.inputMMParam.get_depthA1() +
                rnnParams.singleN * (input_align)*2) *
                sizeof(float);
            EXPECT_LT(l1UsedSize, 512 * 1024 - 64);
            // hidden mm
            ret = rnnMatmul2.SetBufferSpace(-1, rnnParams.maxUbSize, rnnParams.maxUbSize);
            ret = rnnMatmul2.SetOrgShape(rnnParams.batch, rnnParams.hiddenSize * 4, rnnParams.hiddenSize);
            ret = rnnMatmul2.SetShape(rnnParams.batch, rnnParams.hiddenSize, rnnParams.hiddenSize);
            ret = rnnMatmul2.SetSingleShape(rnnParams.singleM, rnnParams.singleN, rnnParams.hiddenSize);
            ret = rnnMatmul2.SetFixSplit(rnnParams.baseM, rnnParams.baseN, -1);
            ret = rnnMatmul2.SetBias(false);
            ret = rnnMatmul2.SetAlignSplit(-1, 64, -1);
            ret = rnnMatmul2.SetDim(dim);
            ret = rnnMatmul2.GetTiling(tilingData.hiddenMMParam);
            tilingData.hiddenMMParam.set_singleCoreN(rnnParams.singleN / 4);
            l1UsedSize = (tilingData.hiddenMMParam.get_baseM() * tilingData.hiddenMMParam.get_baseK() *
                tilingData.hiddenMMParam.get_depthA1() +
                rnnParams.singleN * (hidden_align)*2) *
                sizeof(float);
            EXPECT_LT(l1UsedSize, 512 * 1024 - 64);
            rnnParams.usedCoreNum = dim * 4;
        } else { // no cache, reset AB，mm cache mechanism lose efficacy
            std::cout << "can not load any weight" << std::endl;
            rnnMatmul.SetAType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND,
                (matmul_tiling::DataType)dataType);
            rnnMatmul.SetBType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND,
                (matmul_tiling::DataType)dataType);
            rnnMatmul.SetCType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::NZ,
                matmul_tiling::DataType ::DT_FLOAT);
            rnnMatmul.SetBiasType(matmul_tiling::TPosition::VECCALC, matmul_tiling::CubeFormat::ND,
                (matmul_tiling::DataType)dataType);

            rnnMatmul1.SetAType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND,
                (matmul_tiling::DataType)dataType);
            rnnMatmul1.SetBType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND,
                (matmul_tiling::DataType)dataType);
            rnnMatmul1.SetCType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::NZ,
                matmul_tiling::DataType ::DT_FLOAT);
            rnnMatmul1.SetBiasType(matmul_tiling::TPosition::VECCALC, matmul_tiling::CubeFormat::ND,
                (matmul_tiling::DataType)dataType);

            rnnMatmul2.SetAType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND,
                (matmul_tiling::DataType)dataType);
            rnnMatmul2.SetBType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND,
                (matmul_tiling::DataType)dataType);
            rnnMatmul2.SetCType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::NZ,
                matmul_tiling::DataType ::DT_FLOAT);
            rnnMatmul2.SetBiasType(matmul_tiling::TPosition::VECCALC, matmul_tiling::CubeFormat::ND,
                (matmul_tiling::DataType)dataType);
            auto ret = rnnMatmul.SetBias(true);
            ret = rnnMatmul.SetDim(rnnParams.sysAivCoreNum);
            ret = rnnMatmul.SetOrgShape(rnnParams.batch, rnnParams.hiddenSize * 4,
                rnnParams.inputSize + rnnParams.hiddenSize);
            ret = rnnMatmul.SetShape(rnnParams.batch, rnnParams.hiddenSize, rnnParams.inputSize + rnnParams.hiddenSize);
            ret = rnnMatmul.SetBufferSpace(-1, rnnParams.maxUbSize,
                rnnParams.maxUbSize); // set the space that can be used, by default, all space of the chip is used.
            ret = rnnMatmul.GetTiling(rnnParams.matmulTiling);
            int32_t dim, mDim, nDim;
            rnnParams.baseM = rnnMatmul.GetBaseM();
            rnnParams.baseN = rnnMatmul.GetBaseN();
            rnnParams.baseK = rnnMatmul.GetBaseK(); // get output info
            ret = rnnMatmul.GetSingleShape(rnnParams.singleM, rnnParams.singleN,
                rnnParams.singleK); //  get single core data
            ret = rnnMatmul.GetCoreNum(dim, mDim,
                nDim); // get used blockdim after multi-cores cut, carried by user to kernel， contrl Kernel business
            // input mm
            ret = rnnMatmul1.SetBufferSpace(-1, rnnParams.maxUbSize, rnnParams.maxUbSize);
            ret = rnnMatmul1.SetOrgShape(rnnParams.batch, rnnParams.hiddenSize * 4, rnnParams.inputSize);
            ret = rnnMatmul1.SetShape(rnnParams.batch, rnnParams.hiddenSize, rnnParams.inputSize);
            ret = rnnMatmul1.SetSingleShape(rnnParams.singleM, rnnParams.singleN, rnnParams.inputSize);
            ret = rnnMatmul1.SetFixSplit(rnnParams.baseM, rnnParams.baseN, -1);
            ret = rnnMatmul1.SetBias(true);
            ret = rnnMatmul1.SetDim(dim);
            ret = rnnMatmul1.GetTiling(tilingData.inputMMParam);
            int32_t l1UsedSize = (tilingData.inputMMParam.get_baseM() * tilingData.inputMMParam.get_baseK() *
                tilingData.inputMMParam.get_depthA1() +
                tilingData.inputMMParam.get_baseN() * tilingData.inputMMParam.get_baseK() *
                tilingData.inputMMParam.get_depthB1()) *
                sizeof(float);
            EXPECT_LT(l1UsedSize, 512 * 1024 - 64);
            // hidden mm
            ret = rnnMatmul2.SetBufferSpace(-1, rnnParams.maxUbSize, rnnParams.maxUbSize);
            ret = rnnMatmul2.SetOrgShape(rnnParams.batch, rnnParams.hiddenSize * 4, rnnParams.hiddenSize);
            ret = rnnMatmul2.SetShape(rnnParams.batch, rnnParams.hiddenSize, rnnParams.hiddenSize);
            ret = rnnMatmul2.SetSingleShape(rnnParams.singleM, rnnParams.singleN, rnnParams.hiddenSize);
            ret = rnnMatmul2.SetFixSplit(rnnParams.baseM, rnnParams.baseN, -1);
            ret = rnnMatmul2.SetBias(false);
            ret = rnnMatmul2.SetDim(dim);
            ret = rnnMatmul2.GetTiling(tilingData.hiddenMMParam);
            l1UsedSize = (tilingData.hiddenMMParam.get_baseM() * tilingData.hiddenMMParam.get_baseK() *
                tilingData.hiddenMMParam.get_depthA1() +
                tilingData.hiddenMMParam.get_baseN() * tilingData.hiddenMMParam.get_baseK() *
                tilingData.hiddenMMParam.get_depthB1()) *
                sizeof(float);
            EXPECT_LT(l1UsedSize, 512 * 1024 - 64);
            // mm basic property
            rnnParams.usedCoreNum = dim;
        }
    }
}

TEST_F(TestTiling, TestMatmulApiTilngSetShapeZero)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(64, 2048, 0);
    tiling.SetOrgShape(64, 2048, 0);
    tiling.SetBias(false);
    tiling.SetBufferSpace(-1, 128 * 1024, -1);

    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    EXPECT_EQ(ret, -1);
}

TEST_F(TestTiling, TestMatmulApiTilngInt8Case1)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT8);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT8);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT32);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT32);
    tiling.SetShape(1024, 1024, 1024);
    tiling.SetOrgShape(1024, 1024, 1024);
    tiling.SetBias(true);
    tiling.SetBufferSpace(-1, -1, -1);

    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, TestMatmulApiTilngInt8Case2)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT8);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT8);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT32);
    tiling.SetShape(4096, 4096, 4096);
    tiling.SetOrgShape(4096, 4096, 4096);
    tiling.SetBias(true);
    tiling.SetDequantType(DequantType::TENSOR);
    tiling.SetBufferSpace(-1, -1, -1);

    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, TestMatmulApiTilngInt8Case3)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT8);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT8);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT32);
    tiling.SetShape(32, 16, 32);
    tiling.SetOrgShape(32, 16, 32);
    tiling.SetBias(true);
    tiling.SetDequantType(DequantType::TENSOR);
    tiling.SetBufferSpace(-1, -1, -1);

    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, TestMatmulApiTilngInt8Case4)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT8);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT8);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT32);
    tiling.SetShape(16, 64, 16);
    tiling.SetOrgShape(16, 64, 16);
    tiling.SetBias(false);
    tiling.SetDequantType(DequantType::TENSOR);
    tiling.SetBufferSpace(-1, -1, -1);

    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, TestMatmulApiTilngInt8Case5)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::TSCM, CubeFormat::ND, matmul_tiling::DataType::DT_INT8);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT8);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT32);
    tiling.SetShape(32, 4096, 80);
    tiling.SetOrgShape(32, 4096, 80);
    tiling.SetBias(true);
    tiling.SetDequantType(DequantType::TENSOR);
    tiling.SetBufferSpace(256 * 1024, -1, -1);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, TestMatmulApiTilngInt8Case6)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT8);
    tiling.SetBType(TPosition::TSCM, CubeFormat::ND, matmul_tiling::DataType::DT_INT8);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT16);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT32);
    tiling.SetShape(512, 32, 512);
    tiling.SetOrgShape(512, 32, 512);
    tiling.SetBias(true);
    tiling.SetDequantType(DequantType::TENSOR);
    tiling.SetBufferSpace(512 * 1024 - 128 - 32 * 512 * 2, -1, -1);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, TestMatmulApiTilngInt8Case7)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT8);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT8);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT32);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT32);
    tiling.SetShape(176, 1680, 608);
    tiling.SetOrgShape(176, 1680, 608);
    tiling.SetBias(false);
    tiling.SetBufferSpace(-1, 128 * 1024, -1);
    optiling::TCubeTiling tilingData;
    tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(tilingData.get_baseM(), 128);
}

TEST_F(TestTiling, TestMatmulApiTilngInt8Case8)
{
    MultiCoreMatmulTiling tiling;
    tiling.SetDim(24);
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT8);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT8);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT32);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT32);
    tiling.SetOrgShape(65536, 32768, 65536);
    tiling.SetShape(65536, 32768, 65536);
    tiling.SetBias(false);
    tiling.SetBufferSpace(-1, -1, -1);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, TestAscendQuantTiling)
{
    std::vector<int64_t> shapeDims = { 512 };
    auto shape = ge::Shape(shapeDims);
    uint32_t maxValue;
    uint32_t minValue;
    GetAscendQuantMaxMinTmpSize(shape, 2, maxValue, minValue);
    EXPECT_EQ(minValue, 2 * 256);
    EXPECT_EQ(maxValue, 2 * 512);
}

TEST_F(TestTiling, TestAscendDequantTiling)
{
    // 2d input shape
    std::vector<int64_t> shape_dims = {10, 32};
    auto shape = ge::Shape(shape_dims);
    uint32_t maxValue;
    uint32_t minValue;

    GetAscendDequantMaxMinTmpSize(shape, 2, maxValue, minValue);
    EXPECT_EQ(minValue, 4 * (64 + 32 + 40));
    EXPECT_EQ(maxValue, 4 * (64 + 32 * 10 + 40));

    // 1d input shape
    std::vector<int64_t> shape_dims_1d = {320};
    auto shape_1d = ge::Shape(shape_dims_1d);

    GetAscendDequantMaxMinTmpSize(shape_1d, 2, maxValue, minValue);
    EXPECT_EQ(minValue, 4 * (64 + 1 * 320 + 328));
    EXPECT_EQ(maxValue, 4 * (64 + 1 * 320 + 328));
}

TEST_F(TestTiling, TestAntiquantTilingNoTransposePerChannelHalf)
{
    std::vector<int64_t> srcDims = { 640, 5120 };
    auto srcShape = ge::Shape(srcDims);
    std::vector<int64_t> offsetDSms = { 1, 5120 };
    auto offsetShape = ge::Shape(offsetDSms);
    bool isTranspose = false;
    uint32_t maxValue;
    uint32_t minValue;
    GetAscendAntiQuantMaxMinTmpSize(srcShape, offsetShape, isTranspose, ge::DT_INT8, ge::DT_FLOAT16, maxValue, minValue);
    EXPECT_EQ(minValue, 0);
    EXPECT_EQ(maxValue, 0);
}

TEST_F(TestTiling, TestAntiquantTilingNoTransposePerChannel)
{
    std::vector<int64_t> srcDims = { 640, 5120 };
    auto srcShape = ge::Shape(srcDims);
    std::vector<int64_t> offsetDSms = { 1, 5120 };
    auto offsetShape = ge::Shape(offsetDSms);
    bool isTranspose = false;
    uint32_t maxValue;
    uint32_t minValue;
    GetAscendAntiQuantMaxMinTmpSize(srcShape, offsetShape, isTranspose, ge::DT_INT8, ge::DT_BF16, maxValue, minValue);
    uint32_t expectValue = 5120 * 2 * sizeof(float) + 64 * 640 * sizeof(float);
    EXPECT_EQ(minValue, expectValue);
    EXPECT_EQ(maxValue, expectValue);
}

TEST_F(TestTiling, TestAntiquantTilingNoTransposePerTensor)
{
    std::vector<int64_t> srcDims = { 640, 5120 };
    auto srcShape = ge::Shape(srcDims);
    std::vector<int64_t> offsetDSms = { 1 };
    auto offsetShape = ge::Shape(offsetDSms);
    bool isTranspose = false;
    uint32_t maxValue;
    uint32_t minValue;
    GetAscendAntiQuantMaxMinTmpSize(srcShape, offsetShape, isTranspose, ge::DT_INT8, ge::DT_BF16, maxValue, minValue);
    EXPECT_EQ(minValue, 1024);
    EXPECT_EQ(maxValue, 640 * 5120 * sizeof(float));
}

TEST_F(TestTiling, TestAntiquantTilingTransposePerChannel)
{
    std::vector<int64_t> srcDims = { 64, 512 };
    auto srcShape = ge::Shape(srcDims);
    std::vector<int64_t> offsetDSms = { 64, 1 };
    auto offsetShape = ge::Shape(offsetDSms);
    bool isTranspose = true;
    uint32_t maxValue;
    uint32_t minValue;
    GetAscendAntiQuantMaxMinTmpSize(srcShape, offsetShape, isTranspose, ge::DT_INT8, ge::DT_BF16, maxValue, minValue);
    EXPECT_EQ(minValue, 80 * 64 * sizeof(float));
    EXPECT_EQ(maxValue, 80 * 64 * sizeof(float));
}

TEST_F(TestTiling, TestAntiquantTilingTransposePerTensor)
{
    std::vector<int64_t> srcDims = { 640, 5120 };
    auto srcShape = ge::Shape(srcDims);
    std::vector<int64_t> offsetDSms = { 1 };
    auto offsetShape = ge::Shape(offsetDSms);
    bool isTranspose = true;
    uint32_t maxValue;
    uint32_t minValue;
    GetAscendAntiQuantMaxMinTmpSize(srcShape, offsetShape, isTranspose, ge::DT_INT8, ge::DT_BF16, maxValue, minValue);
    EXPECT_EQ(minValue, 1024);
    EXPECT_EQ(maxValue, 640 * 5120 * sizeof(float));
}

TEST_F(TestTiling, TestGeluTiling)
{
    std::vector<int64_t> shapeDims = { 128, 128 };
    auto geluShape = ge::Shape(shapeDims);
    uint32_t maxValue = 0;
    uint32_t minValue = 0;
    GetGeluMaxMinTmpSize(geluShape, 4, maxValue, minValue);
    EXPECT_EQ(maxValue, 3 * 128 * 128 * 4);
    EXPECT_EQ(minValue, 3 * 256);
}

TEST_F(TestTiling, TestMatmulApiTilngInt8Case9)
{
    MultiCoreMatmulTiling tiling;
    tiling.SetDim(2);
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT8);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT8);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT32);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT32);
    tiling.SetOrgShape(64, 64, 64);
    tiling.SetShape(64, 64, 64);
    tiling.SetBias(false);
    tiling.SetBufferSpace(-1, -1, -1);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, TestMatmulApiTilngInt8Case10)
{
    MultiCoreMatmulTiling tiling;
    tiling.SetDim(2);
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT8);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT8);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT32);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT32);
    tiling.SetOrgShape(65536, 64, 32);
    tiling.SetShape(65536, 64, 32);
    tiling.SetBias(false);
    tiling.SetBufferSpace(-1, -1, -1);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, TestMatmulApiTilngInt8Case11)
{
    MultiCoreMatmulTiling tiling;
    tiling.SetDim(2);
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT8);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT8);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT32);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT32);
    tiling.SetOrgShape(2048, 2048, 256);
    tiling.SetShape(2048, 2048, 256);
    tiling.SetBias(false);
    tiling.SetBufferSpace(-1, -1, -1);
    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, TestMatmulApiTilngInt8Case12)
{
    matmul_tiling::PlatformInfo plat {.socVersion = platform_ascendc::SocVersion::ASCEND310P, .l1Size = 1048576,
        .l0CSize = 262144, .ubSize = 262144, .l0ASize = 65536, .l0BSize = 65536};
    matmul_tiling::MultiCoreMatmulTiling rnnMatmul3(plat);
    rnnMatmul3.SetAType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND,
        matmul_tiling::DataType::DT_INT8, false);
    rnnMatmul3.SetBType(matmul_tiling::TPosition::VECCALC, matmul_tiling::CubeFormat::NZ,
        matmul_tiling::DataType::DT_INT8, true);
    rnnMatmul3.SetCType(matmul_tiling::TPosition::VECCALC, matmul_tiling::CubeFormat::NZ,
        matmul_tiling::DataType ::DT_FLOAT16);
    auto ret = rnnMatmul3.SetBias(false);
    ret = rnnMatmul3.SetOrgShape(1, 494, 128);
    ret = rnnMatmul3.SetShape(1, 494, 128);
    ret = rnnMatmul3.SetBufferSpace(1046528, 262144); // set the space that can be used, by default, all space of the chip is used.
    ret = rnnMatmul3.SetFixSplit(16, 256, 16);
    ret = rnnMatmul3.SetDequantType(DequantType::TENSOR);
    rnnMatmul3.SetTraverse(matmul_tiling::MatrixTraverse::FIRSTN);
    optiling::TCubeTiling tilingData;
    ret = rnnMatmul3.GetTiling(tilingData);
    rnnMatmul3.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, TestMatmulApiTilngInt8Case13)
{
    MatmulApiTiling tiling;
    tiling.SetAType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT8, true);
    tiling.SetBType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT8, true);
    tiling.SetCType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT32);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_INT32);
    tiling.SetShape(1024, 1024, 1024);
    tiling.SetOrgShape(1024, 1024, 1024);
    tiling.SetBias(true);
    tiling.SetBufferSpace(-1, -1, -1);

    optiling::TCubeTiling tilingData;
    int ret = tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, TestAscendSiluTiling)
{
    std::vector<int64_t> shapeDims = { 512 };
    auto shape = ge::Shape(shapeDims);
    uint32_t maxValue;
    uint32_t minValue;
    GetSiluTmpSize(shape, 8, true, maxValue, minValue);
    EXPECT_EQ(minValue, 0);
    EXPECT_EQ(maxValue, 0);
}

TEST_F(TestTiling, TestAscendSwishTiling)
{
    std::vector<int64_t> shapeDims = { 512 };
    auto shape = ge::Shape(shapeDims);
    uint32_t maxValue;
    uint32_t minValue;
    GetSwishTmpSize(shape, 8, true, maxValue, minValue);
    EXPECT_EQ(minValue, 0);
    EXPECT_EQ(maxValue, 0);
}

#if __CCE_AICORE__ == 220
extern void platfrom_stub_set_chip_version(const char *num);
TEST_F(TestTiling, TestTopkTiling_TopKModeNomal_isInitIndexTrue_Float_Inner64)
{
    enum TopKMode topkMode = TopKMode::TOPK_NORMAL;
    bool isInitIndex = true;
    const int32_t outter = 1;
    const int32_t inner = 64;
    const int32_t k = 10;
    uint32_t dataTypeSize = 4;
    bool isReuseSource = true;
    uint32_t maxValue = 0;
    uint32_t minValue = 0;
    optiling::TopkTiling tilingData;
    fe::PlatFormInfos platformInfo;
    auto plat = platform_ascendc::PlatformAscendC(&platformInfo);
    platfrom_stub_set_chip_version("Ascend910B");
    TopKTilingFunc(plat, inner, outter, k, dataTypeSize, isInitIndex, topkMode, true, tilingData);
    EXPECT_EQ(tilingData.get_tmpLocalSize(), 256);
    EXPECT_EQ(tilingData.get_allDataSize(), 64);
    EXPECT_EQ(tilingData.get_innerDataSize(), 128);
    EXPECT_EQ(tilingData.get_sortRepeat(), 2);
    EXPECT_EQ(tilingData.get_mrgSortRepeat(), 16);
    EXPECT_EQ(tilingData.get_kAlignFourBytes(), 16);
    EXPECT_EQ(tilingData.get_kAlignTwoBytes(), 16);
    EXPECT_EQ(tilingData.get_maskOffset(), 16);
    EXPECT_EQ(tilingData.get_maskVreducev2FourBytes(), 20);
    EXPECT_EQ(tilingData.get_maskVreducev2TwoBytes(), 40);
    EXPECT_EQ(tilingData.get_mrgSortSrc1offset(), 2);
    EXPECT_EQ(tilingData.get_mrgSortSrc2offset(), 4);
    EXPECT_EQ(tilingData.get_mrgSortSrc3offset(), 6);
    EXPECT_EQ(tilingData.get_mrgSortTwoQueueSrc1Offset(), 2);
    EXPECT_EQ(tilingData.get_mrgFourQueueTailPara1(), 128);
    EXPECT_EQ(tilingData.get_mrgFourQueueTailPara2(), 1);
    GetTopKMaxMinTmpSize(plat, inner, outter, isReuseSource, isInitIndex, topkMode, true, 4, maxValue, minValue);
    EXPECT_EQ(maxValue, 1024);
    EXPECT_EQ(minValue, 1024);
}

TEST_F(TestTiling, TestTopkTiling_TopKModeNomal_isInitIndexFalse_Float_Inner64)
{
    enum TopKMode topkMode = TopKMode::TOPK_NORMAL;
    bool isInitIndex = false;
    const int32_t outter = 1;
    const int32_t inner = 64;
    const int32_t k = 10;
    uint32_t dataTypeSize = 4;
    bool isReuseSource = true;
    uint32_t maxValue = 0;
    uint32_t minValue = 0;
    optiling::TopkTiling tilingData;
    fe::PlatFormInfos platformInfo;
    auto plat = platform_ascendc::PlatformAscendC(&platformInfo);
    platfrom_stub_set_chip_version("Ascend910B");
    TopKTilingFunc(plat, inner, outter, k, dataTypeSize, isInitIndex, topkMode, true, tilingData);
    EXPECT_EQ(tilingData.get_tmpLocalSize(), 320);
    EXPECT_EQ(tilingData.get_allDataSize(), 64);
    EXPECT_EQ(tilingData.get_innerDataSize(), 128);
    EXPECT_EQ(tilingData.get_sortRepeat(), 2);
    EXPECT_EQ(tilingData.get_mrgSortRepeat(), 16);
    EXPECT_EQ(tilingData.get_kAlignFourBytes(), 16);
    EXPECT_EQ(tilingData.get_kAlignTwoBytes(), 16);
    EXPECT_EQ(tilingData.get_maskOffset(), 16);
    EXPECT_EQ(tilingData.get_maskVreducev2FourBytes(), 20);
    EXPECT_EQ(tilingData.get_maskVreducev2TwoBytes(), 40);
    EXPECT_EQ(tilingData.get_mrgSortSrc1offset(), 2);
    EXPECT_EQ(tilingData.get_mrgSortSrc2offset(), 4);
    EXPECT_EQ(tilingData.get_mrgSortSrc3offset(), 6);
    EXPECT_EQ(tilingData.get_mrgSortTwoQueueSrc1Offset(), 2);
    EXPECT_EQ(tilingData.get_mrgFourQueueTailPara1(), 128);
    EXPECT_EQ(tilingData.get_mrgFourQueueTailPara2(), 1);
    EXPECT_EQ(tilingData.get_srcIndexOffset(), 256);
    GetTopKMaxMinTmpSize(plat, inner, outter, isReuseSource, isInitIndex, topkMode, false, 4, maxValue, minValue);
    EXPECT_EQ(maxValue, 1280);
    EXPECT_EQ(minValue, 1280);
}

TEST_F(TestTiling, TestTopkTiling_TopKModeNomal_isInitIndexTrue_Half_Inner64)
{
    enum TopKMode topkMode = TopKMode::TOPK_NORMAL;
    bool isInitIndex = true;
    const int32_t outter = 1;
    const int32_t inner = 64;
    const int32_t k = 10;
    uint32_t dataTypeSize = 2;
    bool isReuseSource = true;
    uint32_t maxValue = 0;
    uint32_t minValue = 0;
    optiling::TopkTiling tilingData;
    fe::PlatFormInfos platformInfo;
    auto plat = platform_ascendc::PlatformAscendC(&platformInfo);
    platfrom_stub_set_chip_version("Ascend910B");
    TopKTilingFunc(plat, inner, outter, k, dataTypeSize, isInitIndex, topkMode, true, tilingData);
    EXPECT_EQ(tilingData.get_tmpLocalSize(), 512);
    EXPECT_EQ(tilingData.get_allDataSize(), 64);
    EXPECT_EQ(tilingData.get_innerDataSize(), 256);
    EXPECT_EQ(tilingData.get_sortRepeat(), 2);
    EXPECT_EQ(tilingData.get_mrgSortRepeat(), 16);
    EXPECT_EQ(tilingData.get_kAlignFourBytes(), 16);
    EXPECT_EQ(tilingData.get_kAlignTwoBytes(), 16);
    EXPECT_EQ(tilingData.get_maskOffset(), 16);
    EXPECT_EQ(tilingData.get_maskVreducev2FourBytes(), 20);
    EXPECT_EQ(tilingData.get_maskVreducev2TwoBytes(), 40);
    EXPECT_EQ(tilingData.get_mrgSortSrc1offset(), 4);
    EXPECT_EQ(tilingData.get_mrgSortSrc2offset(), 8);
    EXPECT_EQ(tilingData.get_mrgSortSrc3offset(), 12);
    EXPECT_EQ(tilingData.get_mrgSortTwoQueueSrc1Offset(), 4);
    EXPECT_EQ(tilingData.get_mrgFourQueueTailPara1(), 128);
    EXPECT_EQ(tilingData.get_mrgFourQueueTailPara2(), 2);
    GetTopKMaxMinTmpSize(plat, inner, outter, isReuseSource, isInitIndex, topkMode, true, 4, maxValue, minValue);
    EXPECT_EQ(maxValue, 1024);
    EXPECT_EQ(minValue, 1024);
}

TEST_F(TestTiling, TestTopkTiling_TopKModeNomal_isInitIndexFalse_Half_Inner64)
{
    enum TopKMode topkMode = TopKMode::TOPK_NORMAL;
    bool isInitIndex = false;
    const int32_t outter = 1;
    const int32_t inner = 64;
    const int32_t k = 10;
    uint32_t dataTypeSize = 2;
    bool isReuseSource = true;
    uint32_t maxValue = 0;
    uint32_t minValue = 0;
    optiling::TopkTiling tilingData;
    fe::PlatFormInfos platformInfo;
    auto plat = platform_ascendc::PlatformAscendC(&platformInfo);
    platfrom_stub_set_chip_version("Ascend910B");
    TopKTilingFunc(plat, inner, outter, k, dataTypeSize, isInitIndex, topkMode, true, tilingData);
    EXPECT_EQ(tilingData.get_tmpLocalSize(), 640);
    EXPECT_EQ(tilingData.get_allDataSize(), 64);
    EXPECT_EQ(tilingData.get_innerDataSize(), 256);
    EXPECT_EQ(tilingData.get_sortRepeat(), 2);
    EXPECT_EQ(tilingData.get_mrgSortRepeat(), 16);
    EXPECT_EQ(tilingData.get_kAlignFourBytes(), 16);
    EXPECT_EQ(tilingData.get_kAlignTwoBytes(), 16);
    EXPECT_EQ(tilingData.get_maskOffset(), 16);
    EXPECT_EQ(tilingData.get_maskVreducev2FourBytes(), 20);
    EXPECT_EQ(tilingData.get_maskVreducev2TwoBytes(), 40);
    EXPECT_EQ(tilingData.get_mrgSortSrc1offset(), 4);
    EXPECT_EQ(tilingData.get_mrgSortSrc2offset(), 8);
    EXPECT_EQ(tilingData.get_mrgSortSrc3offset(), 12);
    EXPECT_EQ(tilingData.get_mrgSortTwoQueueSrc1Offset(), 4);
    EXPECT_EQ(tilingData.get_mrgFourQueueTailPara1(), 128);
    EXPECT_EQ(tilingData.get_mrgFourQueueTailPara2(), 2);
    EXPECT_EQ(tilingData.get_srcIndexOffset(), 512);
    GetTopKMaxMinTmpSize(plat, inner, outter, isReuseSource, isInitIndex, topkMode, false, 4, maxValue, minValue);
    EXPECT_EQ(maxValue, 1280);
    EXPECT_EQ(minValue, 1280);
}

TEST_F(TestTiling, TestTopkTiling_TopKModeSmall_isInitIndexTrue_Float_Inner64)
{
    enum TopKMode topkMode = TopKMode::TOPK_NSMALL;
    bool isInitIndex = true;
    const int32_t outter = 1;
    const int32_t inner = 64;
    const int32_t k = 10;
    uint32_t dataTypeSize = 4;
    bool isReuseSource = true;
    uint32_t maxValue = 0;
    uint32_t minValue = 0;
    optiling::TopkTiling tilingData;
    fe::PlatFormInfos platformInfo;
    auto plat = platform_ascendc::PlatformAscendC(&platformInfo);
    platfrom_stub_set_chip_version("Ascend910B");
    TopKTilingFunc(plat, inner, outter, k, dataTypeSize, isInitIndex, topkMode, false, tilingData);
    EXPECT_EQ(tilingData.get_allDataSize(), 64);
    EXPECT_EQ(tilingData.get_kAlignFourBytes(), 16);
    EXPECT_EQ(tilingData.get_tmpLocalSize(), 208);
    EXPECT_EQ(tilingData.get_topkMrgSrc1MaskSizeOffset(), 192);
    EXPECT_EQ(tilingData.get_maskOffset(), 10);
    GetTopKMaxMinTmpSize(plat, inner, outter, isReuseSource, isInitIndex, topkMode, true, 4, maxValue, minValue);
    EXPECT_EQ(maxValue, 576);
    EXPECT_EQ(minValue, 576);
}

TEST_F(TestTiling, TestTopkTiling_TopKModeSmall_isInitIndexFalse_Float_Inner64)
{
    enum TopKMode topkMode = TopKMode::TOPK_NSMALL;
    bool isInitIndex = false;
    const int32_t outter = 1;
    const int32_t inner = 64;
    const int32_t k = 10;
    uint32_t dataTypeSize = 4;
    bool isReuseSource = true;
    uint32_t maxValue = 0;
    uint32_t minValue = 0;
    optiling::TopkTiling tilingData;
    fe::PlatFormInfos platformInfo;
    auto plat = platform_ascendc::PlatformAscendC(&platformInfo);
    platfrom_stub_set_chip_version("Ascend910B");
    TopKTilingFunc(plat, inner, outter, k, dataTypeSize, isInitIndex, topkMode, true, tilingData);
    EXPECT_EQ(tilingData.get_allDataSize(), 64);
    EXPECT_EQ(tilingData.get_kAlignFourBytes(), 16);
    EXPECT_EQ(tilingData.get_topkMrgSrc1MaskSizeOffset(), 128);
    EXPECT_EQ(tilingData.get_maskOffset(), 10);
    EXPECT_EQ(tilingData.get_tmpLocalSize(), 208);
    EXPECT_EQ(tilingData.get_topkNSmallSrcIndexOffset(), 144);

    GetTopKMaxMinTmpSize(plat, inner, outter, isReuseSource, isInitIndex, topkMode, false, 4, maxValue, minValue);
    EXPECT_EQ(maxValue, 1088);
    EXPECT_EQ(minValue, 1088);
}

TEST_F(TestTiling, TestTopkTiling_TopKModeSmall_isInitIndexTrue_Half_Inner64)
{
    enum TopKMode topkMode = TopKMode::TOPK_NSMALL;
    bool isInitIndex = true;
    const int32_t outter = 1;
    const int32_t inner = 64;
    const int32_t k = 10;
    uint32_t dataTypeSize = 2;
    bool isReuseSource = true;
    uint32_t maxValue = 0;
    uint32_t minValue = 0;
    optiling::TopkTiling tilingData;
    fe::PlatFormInfos platformInfo;
    auto plat = platform_ascendc::PlatformAscendC(&platformInfo);
    platfrom_stub_set_chip_version("Ascend910B");
    TopKTilingFunc(plat, inner, outter, k, dataTypeSize, isInitIndex, topkMode, true, tilingData);
    EXPECT_EQ(tilingData.get_allDataSize(), 64);
    EXPECT_EQ(tilingData.get_kAlignFourBytes(), 16);
    EXPECT_EQ(tilingData.get_topkMrgSrc1MaskSizeOffset(), 256);
    EXPECT_EQ(tilingData.get_tmpLocalSize(), 288);
    EXPECT_EQ(tilingData.get_maskOffset(), 10);
    GetTopKMaxMinTmpSize(plat, inner, outter, isReuseSource, isInitIndex, topkMode, false, 4, maxValue, minValue);
    EXPECT_EQ(maxValue, 832);
    EXPECT_EQ(minValue, 832);
}

TEST_F(TestTiling, TestTopkTiling_TopKModeSmall_isInitIndexFalse_Half_Inner64)
{
    enum TopKMode topkMode = TopKMode::TOPK_NSMALL;
    bool isInitIndex = false;
    const int32_t outter = 1;
    const int32_t inner = 64;
    const int32_t k = 10;
    uint32_t dataTypeSize = 2;
    bool isReuseSource = true;
    uint32_t maxValue = 0;
    uint32_t minValue = 0;
    optiling::TopkTiling tilingData;
    platfrom_stub_set_chip_version("Ascend910B");
    fe::PlatFormInfos platformInfo;
    auto plat = platform_ascendc::PlatformAscendC(&platformInfo);
    TopKTilingFunc(plat, inner, outter, k, dataTypeSize, isInitIndex, topkMode, false, tilingData);
    EXPECT_EQ(tilingData.get_allDataSize(), 64);
    EXPECT_EQ(tilingData.get_kAlignFourBytes(), 16);
    EXPECT_EQ(tilingData.get_topkMrgSrc1MaskSizeOffset(), 320);
    EXPECT_EQ(tilingData.get_maskOffset(), 10);
    EXPECT_EQ(tilingData.get_tmpLocalSize(), 480);
    EXPECT_EQ(tilingData.get_topkNSmallSrcIndexOffset(), 352);
    GetTopKMaxMinTmpSize(plat, inner, outter, isReuseSource, isInitIndex, topkMode, true, 4, maxValue, minValue);
    EXPECT_EQ(maxValue, 832);
    EXPECT_EQ(minValue, 832);
}

TEST_F(TestTiling, TestTopkTiling_DataTypeSize0_FAILED)
{
    enum TopKMode topkMode = TopKMode::TOPK_NSMALL;
    bool isInitIndex = false;
    const int32_t outter = 1;
    const int32_t inner = 64;
    const int32_t k = 10;
    uint32_t dataTypeSize = 0;
    bool isReuseSource = true;
    uint32_t maxValue = 0;
    uint32_t minValue = 0;
    optiling::TopkTiling tilingData;
    platfrom_stub_set_chip_version("Ascend910B");
    fe::PlatFormInfos platformInfo;
    auto plat = platform_ascendc::PlatformAscendC(&platformInfo);
    auto res = TopKTilingFunc(plat, inner, outter, k, dataTypeSize, isInitIndex, topkMode, false, tilingData);
    EXPECT_EQ(res, false);
}

TEST_F(TestTiling, TestTopkTiling_TopKModeSmall_isInitIndexFalse_Half_k)
{
    enum TopKMode topkMode = TopKMode::TOPK_NSMALL;
    bool isInitIndex = false;
    const int32_t outter = 1;
    const int32_t inner = 64;
    int32_t k = 13;
    uint32_t dataTypeSize = 2;
    bool isReuseSource = true;
    uint32_t maxValue = 0;
    uint32_t minValue = 0;
    optiling::TopkTiling tilingData;
    fe::PlatFormInfos platformInfo;
    auto plat = platform_ascendc::PlatformAscendC(&platformInfo);
    platfrom_stub_set_chip_version("Ascend910B");
    TopKTilingFunc(plat, inner, outter, k, dataTypeSize, isInitIndex, topkMode, true, tilingData);
    EXPECT_EQ(tilingData.get_allDataSize(), 64);
    EXPECT_EQ(tilingData.get_kAlignFourBytes(), 16);
    EXPECT_EQ(tilingData.get_topkMrgSrc1MaskSizeOffset(), 256);
    EXPECT_EQ(tilingData.get_maskOffset(), 13);
    EXPECT_EQ(tilingData.get_tmpLocalSize(), 416);
    EXPECT_EQ(tilingData.get_topkNSmallSrcIndexOffset(), 288);
    GetTopKMaxMinTmpSize(plat, inner, outter, isReuseSource, isInitIndex, topkMode, true, 4, maxValue, minValue);
    EXPECT_EQ(maxValue, 832);
    EXPECT_EQ(minValue, 832);
    k = 17;
    TopKTilingFunc(plat, inner, outter, k, dataTypeSize, isInitIndex, topkMode, true, tilingData);
    EXPECT_EQ(tilingData.get_allDataSize(), 64);
    EXPECT_EQ(tilingData.get_kAlignFourBytes(), 24);
    k = 21;
    TopKTilingFunc(plat, inner, outter, k, dataTypeSize, isInitIndex, topkMode, true, tilingData);
    EXPECT_EQ(tilingData.get_allDataSize(), 64);
    EXPECT_EQ(tilingData.get_kAlignFourBytes(), 24);
    k = 25;
    TopKTilingFunc(plat, inner, outter, k, dataTypeSize, isInitIndex, topkMode, true, tilingData);
    EXPECT_EQ(tilingData.get_allDataSize(), 64);
    EXPECT_EQ(tilingData.get_kAlignFourBytes(), 32);
    k = 29;
    TopKTilingFunc(plat, inner, outter, k, dataTypeSize, isInitIndex, topkMode, true, tilingData);
    EXPECT_EQ(tilingData.get_allDataSize(), 64);
    EXPECT_EQ(tilingData.get_kAlignFourBytes(), 32);
}

TEST_F(TestTiling, TestTopkTiling_TopKModeSmall_isInitIndexFalse_Float_k32)
{
    enum TopKMode topkMode = TopKMode::TOPK_NSMALL;
    bool isInitIndex = false;
    const int32_t outter = 1;
    const int32_t inner = 64;
    const int32_t k = 32;
    uint32_t dataTypeSize = 4;
    bool isReuseSource = true;
    uint32_t maxValue = 0;
    uint32_t minValue = 0;
    optiling::TopkTiling tilingData;
    fe::PlatFormInfos platformInfo;
    auto plat = platform_ascendc::PlatformAscendC(&platformInfo);
    platfrom_stub_set_chip_version("Ascend910B");
    TopKTilingFunc(plat, inner, outter, k, dataTypeSize, isInitIndex, topkMode, true, tilingData);
    EXPECT_EQ(tilingData.get_allDataSize(), 64);
    EXPECT_EQ(tilingData.get_kAlignFourBytes(), 32);
    EXPECT_EQ(tilingData.get_topkMrgSrc1MaskSizeOffset(), 128);
    EXPECT_EQ(tilingData.get_maskOffset(), 32);
    EXPECT_EQ(tilingData.get_tmpLocalSize(), 208);
    EXPECT_EQ(tilingData.get_topkNSmallSrcIndexOffset(), 144);
    GetTopKMaxMinTmpSize(plat, inner, outter, isReuseSource, isInitIndex, topkMode, true, 4, maxValue, minValue);
    EXPECT_EQ(maxValue, 832);
    EXPECT_EQ(minValue, 832);
}
#endif

#if __CCE_AICORE__ == 200
extern void platfrom_stub_set_chip_version(const char *num);
TEST_F(TestTiling, TestTopkTiling_TopKModeNormal310P_FLOAT)
{
    enum TopKMode topkMode = TopKMode::TOPK_NORMAL;
    bool isInitIndex = false;
    const int32_t outter = 1;
    const int32_t inner = 64;
    const int32_t k = 32;
    uint32_t dataTypeSize = 4;
    bool isReuseSource = true;
    uint32_t maxValue = 0;
    uint32_t minValue = 0;
    optiling::TopkTiling tilingData;
    fe::PlatFormInfos platformInfo;
    auto plat = platform_ascendc::PlatformAscendC(&platformInfo);
    platfrom_stub_set_chip_version("Ascend310P");
    TopKTilingFunc(plat, inner, outter, k, dataTypeSize, isInitIndex, topkMode, true, tilingData);
    EXPECT_EQ(tilingData.get_tmpLocalSize(), 1088);
    EXPECT_EQ(tilingData.get_srcIndexOffset(), 1024);
    EXPECT_EQ(tilingData.get_innerDataSize(), 512);
    EXPECT_EQ(tilingData.get_sortRepeat(), 4);
    EXPECT_EQ(tilingData.get_copyUbToUbBlockCount(), 64);
    EXPECT_EQ(tilingData.get_mrgSortSrc1offset(), 8);
    EXPECT_EQ(tilingData.get_mrgSortSrc2offset(), 16);
    EXPECT_EQ(tilingData.get_mrgSortSrc3offset(), 24);
    EXPECT_EQ(tilingData.get_mrgSortRepeat(), 16);
    EXPECT_EQ(tilingData.get_maskOffset(), 32);
    EXPECT_EQ(tilingData.get_kAlignTwoBytes(), 32);
    EXPECT_EQ(tilingData.get_kAlignFourBytes(), 32);
    GetTopKMaxMinTmpSize(plat, inner, outter, isReuseSource, isInitIndex, topkMode, true, dataTypeSize, maxValue, minValue);
    EXPECT_EQ(maxValue, 4352);
    EXPECT_EQ(minValue, 4352);
}

TEST_F(TestTiling, TestTopkTiling_TopKModeNormal310P_HALF)
{
    fe::PlatFormInfos platformInfo;
    auto plat = platform_ascendc::PlatformAscendC(&platformInfo);
    platfrom_stub_set_chip_version("Ascend310P");
    enum TopKMode topkMode = TopKMode::TOPK_NORMAL;
    bool isInitIndex = false;
    const int32_t outter = 1;
    const int32_t inner = 32;
    const int32_t k = 8;
    uint32_t dataTypeSize = 2;
    bool isReuseSource = true;
    uint32_t maxValue = 0;
    uint32_t minValue = 0;
    optiling::TopkTiling tilingData;
    TopKTilingFunc(plat, inner, outter, k, dataTypeSize, isInitIndex, topkMode, true, tilingData);
    EXPECT_EQ(tilingData.get_tmpLocalSize(), 576);
    EXPECT_EQ(tilingData.get_srcIndexOffset(), 512);
    EXPECT_EQ(tilingData.get_innerDataSize(), 256);
    EXPECT_EQ(tilingData.get_sortRepeat(), 2);
    EXPECT_EQ(tilingData.get_copyUbToUbBlockCount(), 16);
    EXPECT_EQ(tilingData.get_mrgSortSrc1offset(), 8);
    EXPECT_EQ(tilingData.get_mrgSortSrc2offset(), 16);
    EXPECT_EQ(tilingData.get_mrgSortSrc3offset(), 24);
    EXPECT_EQ(tilingData.get_mrgSortRepeat(), 8);
    EXPECT_EQ(tilingData.get_maskOffset(), 16);
    EXPECT_EQ(tilingData.get_kAlignTwoBytes(), 16);
    EXPECT_EQ(tilingData.get_kAlignFourBytes(), 8);
    GetTopKMaxMinTmpSize(plat, inner, outter, isReuseSource, isInitIndex, topkMode, true, dataTypeSize, maxValue, minValue);
    EXPECT_EQ(maxValue, 1152);
    EXPECT_EQ(minValue, 1152);
}

TEST_F(TestTiling, TestTopkTiling_TopKModeSmall310P_FLOAT)
{
    fe::PlatFormInfos platformInfo;
    auto plat = platform_ascendc::PlatformAscendC(&platformInfo);
    platfrom_stub_set_chip_version("Ascend310P");
    enum TopKMode topkMode = TopKMode::TOPK_NSMALL;
    bool isInitIndex = false;
    const int32_t outter = 1;
    const int32_t inner = 32;
    const int32_t k = 5;
    uint32_t dataTypeSize = 4;
    bool isReuseSource = true;
    uint32_t maxValue = 0;
    uint32_t minValue = 0;
    optiling::TopkTiling tilingData;
    TopKTilingFunc(plat, inner, outter, k, dataTypeSize, isInitIndex, topkMode, true, tilingData);
    EXPECT_EQ(tilingData.get_tmpLocalSize(), 512);
    EXPECT_EQ(tilingData.get_srcIndexOffset(), 288);
    EXPECT_EQ(tilingData.get_innerDataSize(), 256);
    EXPECT_EQ(tilingData.get_sortRepeat(), 2);
    EXPECT_EQ(tilingData.get_copyUbToUbBlockCount(), 32);
    EXPECT_EQ(tilingData.get_mrgSortSrc1offset(), 256);
    EXPECT_EQ(tilingData.get_vreduceIdxMask0(), 31);
    EXPECT_EQ(tilingData.get_vreduceValMask0(), 31);
    EXPECT_EQ(tilingData.get_vreduceValMask1(), 0);
    EXPECT_EQ(tilingData.get_srcIndexOffset(), 288);
    EXPECT_EQ(tilingData.get_maskOffset(), 5);
    GetTopKMaxMinTmpSize(plat, inner, outter, isReuseSource, isInitIndex, topkMode, true, dataTypeSize, maxValue, minValue);
    EXPECT_EQ(maxValue, 2048);
    EXPECT_EQ(minValue, 2048);
}

TEST_F(TestTiling, TestTopkTiling_TopKModeSmall310P_HALF)
{
    fe::PlatFormInfos platformInfo;
    auto plat = platform_ascendc::PlatformAscendC(&platformInfo);
    platfrom_stub_set_chip_version("Ascend310P");
    enum TopKMode topkMode = TopKMode::TOPK_NSMALL;
    bool isInitIndex = false;
    const int32_t outter = 1;
    const int32_t inner = 128;
    const int32_t k = 32;
    uint32_t dataTypeSize = 2;
    bool isReuseSource = true;
    uint32_t maxValue = 0;
    uint32_t minValue = 0;
    optiling::TopkTiling tilingData;
    TopKTilingFunc(plat, inner, outter, k, dataTypeSize, isInitIndex, topkMode, true, tilingData);
    EXPECT_EQ(tilingData.get_tmpLocalSize(), 2048);
    EXPECT_EQ(tilingData.get_srcIndexOffset(), 1280);
    EXPECT_EQ(tilingData.get_innerDataSize(), 1024);
    EXPECT_EQ(tilingData.get_sortRepeat(), 8);
    EXPECT_EQ(tilingData.get_copyUbToUbBlockCount(), 64);
    EXPECT_EQ(tilingData.get_mrgSortSrc1offset(), 1024);
    EXPECT_EQ(tilingData.get_vreduceIdxMask0(), 4294967295);
    EXPECT_EQ(tilingData.get_vreduceValMask0(), 65535);
    EXPECT_EQ(tilingData.get_vreduceValMask1(), 65535);
    EXPECT_EQ(tilingData.get_srcIndexOffset(), 1280);
    EXPECT_EQ(tilingData.get_maskOffset(), 32);
    GetTopKMaxMinTmpSize(plat, inner, outter, isReuseSource, isInitIndex, topkMode, true, dataTypeSize, maxValue, minValue);
    EXPECT_EQ(maxValue, 4096);
    EXPECT_EQ(minValue, 4096);
}
#endif

TEST_F(TestTiling, TestGeGLUTilingFloat)
{
    std::vector<int64_t> shapeDims = { 128, 128 };
    auto GeGLUShape = ge::Shape(shapeDims);
    uint32_t GeGLUNeedMaxSize;
    uint32_t GeGLUNeedMinSize;
    GetGeGLUMaxMinTmpSize(GeGLUShape, 4, true, GeGLUNeedMaxSize, GeGLUNeedMinSize);
    EXPECT_EQ(GeGLUNeedMaxSize, 0);
    EXPECT_EQ(GeGLUNeedMinSize, 0);

    uint32_t maxLiveNodeCnt = 0xffff;
    uint32_t extraBuf = 0xffff;
    GetGeGLUTmpBufferFactorSize(4, maxLiveNodeCnt, extraBuf);
    EXPECT_EQ(maxLiveNodeCnt, 0);
    EXPECT_EQ(extraBuf, 0);
}

TEST_F(TestTiling, TestGeGLUTilingHalf)
{
    std::vector<int64_t> shapeDims = { 128, 128 };
    auto GeGLUShape = ge::Shape(shapeDims);
    uint32_t GeGLUNeedMaxSize;
    uint32_t GeGLUNeedMinSize;
    GetGeGLUMaxMinTmpSize(GeGLUShape, 2, true, GeGLUNeedMaxSize, GeGLUNeedMinSize);
    EXPECT_EQ(GeGLUNeedMaxSize, 131072);
    EXPECT_EQ(GeGLUNeedMinSize, 1024);
    GetGeGLUMaxMinTmpSize(GeGLUShape, 2, true, GeGLUNeedMaxSize, GeGLUNeedMinSize);
    uint32_t maxLiveNodeCnt = 0xffff;
    uint32_t extraBuf = 0xffff;
    GetGeGLUTmpBufferFactorSize(2, maxLiveNodeCnt, extraBuf);
    EXPECT_EQ(maxLiveNodeCnt, 4);
    EXPECT_EQ(extraBuf, 0);
}

TEST_F(TestTiling, TestReGluFloat16OrBf16)
{
    const std::vector<int64_t> srcShapeDims = { 8, 128 };
    const auto srcShape = ge::Shape(srcShapeDims);
    uint32_t maxValue;
    uint32_t minValue;
    GetReGluMaxMinTmpSize(srcShape, 2, false, maxValue, minValue);
    EXPECT_EQ(minValue, 256 * 6);
    EXPECT_EQ(maxValue, 8 * 128 * 2 * 6);
}

TEST_F(TestTiling, TestReGluFloat32)
{
    const std::vector<int64_t> srcShapeDims = { 8, 128 };
    const auto srcShape = ge::Shape(srcShapeDims);
    uint32_t maxValue;
    uint32_t minValue;
    GetReGluMaxMinTmpSize(srcShape, 4, false, maxValue, minValue);
    EXPECT_EQ(minValue, 256);
    EXPECT_EQ(maxValue, 256);
}

TEST_F(TestTiling, tiling_compute_error)
{
    MultiCoreMatmulTiling tiling;
    tiling.isBias = true;
    tiling.socVersion = platform_ascendc::SocVersion::ASCEND310P;
    tiling.biasType_.pos = TPosition::TSCM;
    int ret = tiling.Compute();
    EXPECT_EQ(ret, -1);

    BatchMatmulTiling bmm_tiling;
    bmm_tiling.isBias = true;
    bmm_tiling.socVersion = platform_ascendc::SocVersion::ASCEND310P;
    bmm_tiling.biasType_.pos = TPosition::TSCM;
    ret = bmm_tiling.Compute();
    EXPECT_EQ(ret, -1);
}

TEST_F(TestTiling, TestNormalizeTiling)
{
    // T: float + U: float
    uint32_t A = 4;
    uint32_t alignA = 8;
    uint32_t R = 32;
    std::vector<int64_t> shapeDims = {A, R};   // [A, R]
    auto shapeInput = ge::Shape(shapeDims);
    uint32_t maxsize = 0;
    uint32_t minsize = 0;
    uint32_t dtypesizeT = 4;
    uint32_t dtypesizeU = 4;
    bool isReuseSource = false;
    GetNormalizeMaxMinTmpSize(shapeInput, dtypesizeU, dtypesizeT, isReuseSource, true, false, maxsize, minsize);
    EXPECT_EQ(minsize, (2 * R) * sizeof(float));
    EXPECT_EQ(maxsize, (2 * R + 2 * alignA * R) * sizeof(float));

    // T: half + U: half
    dtypesizeT = 2;
    dtypesizeU = 2;
    GetNormalizeMaxMinTmpSize(shapeInput, dtypesizeU, dtypesizeT, isReuseSource, true, false, maxsize, minsize);
    EXPECT_EQ(minsize, (2 * R + 2 * R) * sizeof(float));
    EXPECT_EQ(maxsize, (2 * R + 2 * alignA * R) * sizeof(float));

    // T: half + U: float
    dtypesizeT = 2;
    dtypesizeU = 4;
    GetNormalizeMaxMinTmpSize(shapeInput, dtypesizeU, dtypesizeT, isReuseSource, true, false, maxsize, minsize);
    EXPECT_EQ(minsize, (2 * R) * sizeof(float));
    EXPECT_EQ(maxsize, (2 * R + 2 * alignA * R) * sizeof(float));
}

TEST_F(TestTiling, TestWelfordFinalizeTiling)
{
    std::vector<int64_t> shape_dims_1d = {64};
    auto shape_1d = ge::Shape(shape_dims_1d);
    uint32_t maxsize = 0;
    uint32_t minsize = 0;
    uint32_t dtypesize = 4;  // float类型
    bool isReuseSource = false;
    GetWelfordFinalizeMaxMinTmpSize(shape_1d, dtypesize, isReuseSource, maxsize, minsize);
    EXPECT_EQ(minsize, 4 * 256);
    EXPECT_EQ(maxsize, 4 * 256);

    std::vector<int64_t> shape_dims_2d = {4096};
    auto shape_2d = ge::Shape(shape_dims_2d);
    isReuseSource = false;
    GetWelfordFinalizeMaxMinTmpSize(shape_2d, dtypesize, isReuseSource, maxsize, minsize);
    EXPECT_EQ(minsize, 4 * 256);
    EXPECT_EQ(maxsize, 2 * (256 + 4096 * 4));

    std::vector<int64_t> shape_dims_3d = {8};
    auto shape_3d = ge::Shape(shape_dims_3d);
    isReuseSource = false;
    GetWelfordFinalizeMaxMinTmpSize(shape_3d, dtypesize, isReuseSource, maxsize, minsize);
    EXPECT_EQ(minsize, 4 * 256);
    EXPECT_EQ(maxsize, 4 * 256);

    std::vector<int64_t> shape_dims_4d = {72};
    auto shape_4d = ge::Shape(shape_dims_4d);
    isReuseSource = false;
    GetWelfordFinalizeMaxMinTmpSize(shape_4d, dtypesize, isReuseSource, maxsize, minsize);
    EXPECT_EQ(minsize, 4 * 256);
    EXPECT_EQ(maxsize, 2 * (256 + 72 * 4));
}

TEST_F(TestTiling, TestWelfordUpdateTiling)
{
    std::vector<int64_t> shapeDims1d = {1, 128};
    auto shape1d = ge::Shape(shapeDims1d);
    uint32_t maxsize = 0;
    uint32_t minsize = 0;
    uint32_t dtypesizeT = 2;  // half类型
    uint32_t dtypesizeU = 4;  // float类型
    bool isReuseSource = false;
    GetWelfordUpdateMaxMinTmpSize(shape1d, dtypesizeT, dtypesizeU, isReuseSource, false, maxsize, minsize);
    EXPECT_EQ(minsize, 3 * 256);
    EXPECT_EQ(maxsize, 2 * 3 * 256);

    std::vector<int64_t> shapeDims2d = {1, 72};
    auto shape2d = ge::Shape(shapeDims2d);
    dtypesizeT = 4;  // float类型
    dtypesizeU = 4;  // float类型
    isReuseSource = false;
    GetWelfordUpdateMaxMinTmpSize(shape2d, dtypesizeT, dtypesizeU, isReuseSource, false, maxsize, minsize);
    EXPECT_EQ(minsize, 2 * 256);
    EXPECT_EQ(maxsize, 2 * 2 * 256);

    std::vector<int64_t> shapeDims3d = {1, 256};
    auto shape3d = ge::Shape(shapeDims3d);
    dtypesizeT = 4;  // float类型
    dtypesizeU = 4;  // float类型
    isReuseSource = true;
    GetWelfordUpdateMaxMinTmpSize(shape3d, dtypesizeT, dtypesizeU, isReuseSource, false, maxsize, minsize);
    EXPECT_EQ(minsize, 1 * 256);
    EXPECT_EQ(maxsize, 4 * 1 * 256);
}

TEST_F(TestTiling, TestNZFp32UnalignedK)
{
    matmul_tiling::PlatformInfo plat {.socVersion = platform_ascendc::SocVersion::ASCEND910B, .l1Size = 524288,
        .l0CSize = 131072, .ubSize = 196608, .l0ASize = 65536, .l0BSize = 65536};
    matmul_tiling::MatmulApiTiling tiling(plat);
    tiling.SetAType(TPosition::GM, CubeFormat::NZ, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBType(TPosition::GM, CubeFormat::NZ, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetCType(TPosition::GM, CubeFormat::NZ, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetBiasType(TPosition::GM, CubeFormat::ND, matmul_tiling::DataType::DT_FLOAT);
    tiling.SetShape(192, 248, 160);
    tiling.SetOrgShape(192, 248, 160);
    tiling.EnableBias(false);
    tiling.SetBufferSpace(-1, -1, -1);
    optiling::TCubeTiling tilingData;
    tiling.GetTiling(tilingData);
    tiling.PrintTilingData();
    int32_t baseK = tiling.GetBaseK();
    EXPECT_EQ(baseK % 16, 0);
}

TEST_F(TestTiling, TestLayerNormRstdTiling)
{
    uint32_t A = 4;
    uint32_t R = 32;
    uint32_t AlignA = 8;
    std::vector<int64_t> shapeDims = {A, R};   // [A, R]
    auto shapeInput = ge::Shape(shapeDims);
    uint32_t maxsize = 0;
    uint32_t minsize = 0;
    bool isReuseSource = false;
    GetLayerNormMaxMinTmpSize(shapeInput, sizeof(float), isReuseSource, true, false, maxsize, minsize);
    EXPECT_EQ(minsize, (2 * R + AlignA) * sizeof(float));
    EXPECT_EQ(maxsize, (AlignA + 2 * A * R) * sizeof(float));

    GetLayerNormMaxMinTmpSize(shapeInput, sizeof(float), isReuseSource, true, false, maxsize, minsize);
    EXPECT_EQ(minsize, (2 * R + AlignA) * sizeof(float));
    EXPECT_EQ(maxsize, (AlignA + 2 * A * R) * sizeof(float));

    GetLayerNormMaxMinTmpSize(shapeInput, sizeof(float), isReuseSource, true, false, maxsize, minsize);
    EXPECT_EQ(minsize, (2 * R + AlignA) * sizeof(float));
    EXPECT_EQ(maxsize, (AlignA + 2 * A * R) * sizeof(float));

    A = 32;
    R = 8;
    std::vector<int64_t> shapeDims2 = {A, R};   // [A, R]
    auto shapeInput2 = ge::Shape(shapeDims2);
    GetLayerNormMaxMinTmpSize(shapeInput2, sizeof(float), isReuseSource, true, false, maxsize, minsize);
    EXPECT_EQ(minsize, (2 * R + 2 * R + A) * sizeof(float));
    EXPECT_EQ(maxsize, (A + 2 * A * R) * sizeof(float));
    optiling::LayerNormSeparateTiling tiling;
    uint32_t stackBufferSize = 4096;
    shapeDims = { A, R };
    auto layernormShape = ge::Shape(shapeDims);
    GetLayerNormNDTilingInfo(layernormShape, stackBufferSize, sizeof(float), false, true, tiling);
}

TEST_F(TestTiling, MultiCoreSparse)
{
    matmul_tiling::MatmulApiTiling sparseMatmul;
    sparseMatmul.SetAType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_INT8);
    sparseMatmul.SetBType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_INT8, true);
    sparseMatmul.SetCType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_INT32);
    sparseMatmul.SetBiasType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_INT8);
    sparseMatmul.SetSparse(true);
    auto ret = sparseMatmul.SetOrgShape(1024, 64, 128);
    ret = sparseMatmul.SetShape(1024, 64, 128);
    ret = sparseMatmul.SetFixSplit(-1, -1, 80);
    ret = sparseMatmul.SetBufferSpace(-1, -1, -1); // will use all buffer space if not explicitly specified
    optiling::TCubeTiling tilingData;
    tilingData.set_usedCoreNum(1);
    ret = sparseMatmul.GetTiling(tilingData);
    tilingData.set_iterateOrder(1);
    sparseMatmul.PrintTilingData();
    matmul_tiling::SysTilingTempBufSize bufSize;
    MatmulGetTmpBufSize(tilingData, bufSize);
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, MultiCoreSparseAlignBaseK)
{
    matmul_tiling::MatmulApiTiling sparseMatmul;
    sparseMatmul.SetAType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_INT8);
    sparseMatmul.SetBType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_INT8, true);
    sparseMatmul.SetCType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_INT32);
    sparseMatmul.SetBiasType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_INT8);
    sparseMatmul.SetSparse(true);
    auto ret = sparseMatmul.SetOrgShape(28, 784, 16);
    ret = sparseMatmul.SetShape(28, 784, 16);
    ret = sparseMatmul.SetBufferSpace(-1, -1, -1); // will use all buffer space if not explicitly specified
    optiling::TCubeTiling tilingData;
    tilingData.set_usedCoreNum(1);
    ret = sparseMatmul.GetTiling(tilingData);
    tilingData.set_iterateOrder(1);
    sparseMatmul.PrintTilingData();
    matmul_tiling::SysTilingTempBufSize bufSize;
    MatmulGetTmpBufSize(tilingData, bufSize);
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, MultiCoreSparseADbOff)
{
    matmul_tiling::MatmulApiTiling sparseMatmul;
    sparseMatmul.SetAType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_INT8);
    sparseMatmul.SetBType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_INT8, true);
    sparseMatmul.SetCType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_INT32);
    sparseMatmul.SetBiasType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_INT8);
    sparseMatmul.SetSparse(true);
    auto ret = sparseMatmul.SetOrgShape(1024, 64, 128);
    ret = sparseMatmul.SetShape(1024, 64, 128);
    ret = sparseMatmul.SetFixSplit(1024, 16, -1);
    ret = sparseMatmul.SetBufferSpace(-1, -1, -1);
    optiling::TCubeTiling tilingData;
    tilingData.set_usedCoreNum(1);
    ret = sparseMatmul.GetTiling(tilingData);
    tilingData.set_iterateOrder(1);
    sparseMatmul.PrintTilingData();
    matmul_tiling::SysTilingTempBufSize bufSize;
    MatmulGetTmpBufSize(tilingData, bufSize);
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, MultiCoreSparseBDbOff)
{
    matmul_tiling::MatmulApiTiling sparseMatmul;
    sparseMatmul.SetAType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_INT8);
    sparseMatmul.SetBType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_INT8, true);
    sparseMatmul.SetCType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_INT32);
    sparseMatmul.SetBiasType(matmul_tiling::TPosition::GM, matmul_tiling::CubeFormat::ND, matmul_tiling::DataType ::DT_INT8);
    sparseMatmul.SetSparse(true);
    auto ret = sparseMatmul.SetOrgShape(64, 1024, 128);
    ret = sparseMatmul.SetShape(64, 1024, 128);
    ret = sparseMatmul.SetFixSplit(16, 1024, -1);
    ret = sparseMatmul.SetBufferSpace(-1, -1, -1);
    optiling::TCubeTiling tilingData;
    tilingData.set_usedCoreNum(1);
    ret = sparseMatmul.GetTiling(tilingData);
    tilingData.set_iterateOrder(1);
    sparseMatmul.PrintTilingData();
    matmul_tiling::SysTilingTempBufSize bufSize;
    MatmulGetTmpBufSize(tilingData, bufSize);
    EXPECT_EQ(ret, 0);
}

TEST_F(TestTiling, TestFmodTilingFloat)
{
    std::vector<int64_t> shapeDims = { 128, 128 };
    auto fmodShape = ge::Shape(shapeDims);
    uint32_t maxValue = 0;
    uint32_t minValue = 0;
    GetFmodMaxMinTmpSize(fmodShape, 4, false,  maxValue, minValue);
    EXPECT_EQ(minValue, 256);
    EXPECT_EQ(maxValue, 128 * 128 * 1 * 4);

    uint32_t maxLiveNodeCnt;
    uint32_t extraBuf;
    GetFmodTmpBufferFactorSize(4, maxLiveNodeCnt, extraBuf);
    EXPECT_EQ(maxLiveNodeCnt, 1);
    EXPECT_EQ(extraBuf, 0);
}

TEST_F(TestTiling, TestFmodTilingHalf)
{
    std::vector<int64_t> shapeDims = { 128, 128 };
    auto fmodShape = ge::Shape(shapeDims);
    uint32_t maxValue = 0;
    uint32_t minValue = 0;
    GetFmodMaxMinTmpSize(fmodShape, 2, false,  maxValue, minValue);
    EXPECT_EQ(minValue, 256 * 8);
    EXPECT_EQ(maxValue, 128 * 128 * 8 * 2);

    uint32_t maxLiveNodeCnt;
    uint32_t extraBuf;
    GetFmodTmpBufferFactorSize(2, maxLiveNodeCnt, extraBuf);
    EXPECT_EQ(maxLiveNodeCnt, 8);
    EXPECT_EQ(extraBuf, 0);
}

TEST_F(TestTiling, TestFmodTilingHalf512)
{
    std::vector<int64_t> shapeDims = { 512 };
    auto truncShape = ge::Shape(shapeDims);
    uint32_t maxValue = 0;
    uint32_t minValue = 0;
    GetFmodMaxMinTmpSize(truncShape, 2, false, maxValue, minValue);
    EXPECT_EQ(maxValue, 512 * 8 * 2);
    EXPECT_EQ(minValue, 256 * 8);
}

TEST_F(TestTiling, TestTruncTilingFloat)
{
    std::vector<int64_t> shapeDims = { 128, 128 };
    auto truncShape = ge::Shape(shapeDims);
    uint32_t maxValue = 0;
    uint32_t minValue = 0;
    GetTruncMaxMinTmpSize(truncShape, 4, false,  maxValue, minValue);
    EXPECT_EQ(minValue, 256 * 1);
    EXPECT_EQ(maxValue, 128 * 128 * 1 * 4);

    uint32_t maxLiveNodeCnt;
    uint32_t extraBuf;
    GetTruncTmpBufferFactorSize(4, maxLiveNodeCnt, extraBuf);
    EXPECT_EQ(maxLiveNodeCnt, 1);
    EXPECT_EQ(extraBuf, 0);
}

TEST_F(TestTiling, TestTruncTilingHalf)
{
    std::vector<int64_t> shapeDims = { 128, 128 };
    auto truncShape = ge::Shape(shapeDims);
    uint32_t maxValue = 0;
    uint32_t minValue = 0;
    GetTruncMaxMinTmpSize(truncShape, 2, false,  maxValue, minValue);
    EXPECT_EQ(minValue, 256 * 2);
    EXPECT_EQ(maxValue, 128 * 128 * 2 * 2);

    uint32_t maxLiveNodeCnt;
    uint32_t extraBuf;
    GetTruncTmpBufferFactorSize(2, maxLiveNodeCnt, extraBuf);
    EXPECT_EQ(maxLiveNodeCnt, 2);
    EXPECT_EQ(extraBuf, 0);
}

TEST_F(TestTiling, TestTruncTilingHalf512)
{
    std::vector<int64_t> shapeDims = { 512 };
    auto truncShape = ge::Shape(shapeDims);
    uint32_t maxValue = 0;
    uint32_t minValue = 0;
    GetTruncMaxMinTmpSize(truncShape, 2, false, maxValue, minValue);
    EXPECT_EQ(maxValue, 512 * 2 * 2);
    EXPECT_EQ(minValue, 256 * 2);
}
